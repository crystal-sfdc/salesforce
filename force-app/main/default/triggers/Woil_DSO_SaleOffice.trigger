trigger Woil_DSO_SaleOffice on Contact (before insert,before update) {
    if(trigger.isBefore && (trigger.isInsert  || trigger.isUpdate ))
    {
        //check Mobile no.....................................
        Set<String> conMobile = new  Set<String>();
              if(trigger.isInsert){
            List<Contact> PreviousContact = [select id , MobilePhone from Contact ];
            for(Contact pCon:PreviousContact){
                conMobile.add(String.valueOf(pCon.MobilePhone));
            } 
        }
        else{
            List<Contact> PreviousContact = [select id , MobilePhone from Contact where id NOT IN : trigger.oldMap.keySet() ];
            for(Contact pCon:PreviousContact){
                conMobile.add(String.valueOf(pCon.MobilePhone));
            } 
        }
     Map<Id,Contact> conOldInfo = trigger.oldMap;
        List<Woil_User_Territory__c> SalesOfficeInfo =  [SELECT Woil_Sales_Office__c, Woil_User__c, Id, Name FROM Woil_User_Territory__c];
        Map<Id,Woil_User_Territory__c> SalesOfficeMap = new Map<Id,Woil_User_Territory__c>();
        for(Woil_User_Territory__c sal : SalesOfficeInfo){
            SalesOfficeMap.put(sal.Woil_User__c,sal);
        }
      for(Contact conInfo :trigger.new){
         if(!Pattern.matches('^[0-9]{14}$', conInfo.Woil_Aadhar_Number__c )){
                conInfo.Woil_Aadhar_Number__c.addError('Please Enter correct Aadhar Number ');
            }
            // mobile validatrion--------------------------------------------------
            if(!Pattern.matches('^[0-9]{10}$', String.valueOf(conInfo.MobilePhone))){
                conInfo.MobilePhone.addError('Please Enter correct Phone ');
            }
               if(!Pattern.matches('^[0-9]{6}$', String.valueOf(conInfo.Woil_DSQ_H_Pin__c))){
                conInfo.Woil_DSQ_H_Pin__c.addError('Please Enter correct Pin Code ');
            }
            if(conMobile.contains(String.valueOf(conInfo.MobilePhone))){
                conInfo.MobilePhone.addError('This phone already exist');
            }
            // mobile validatrion after change mobile number--------------------------------------------------
            if(trigger.isUpdate){
                if(conOldInfo.get(conInfo.Id).MobilePhone != conInfo.MobilePhone){
                    conInfo.Woil_Active__c = false;
                    
                }
            }
            
            if(SalesOfficeMap.get(conInfo.OwnerId) != null){
                conInfo.Woil_Sales_Office__c= SalesOfficeMap.get(conInfo.OwnerId).Woil_Sales_Office__c;
            }
            else{
                if(trigger.isInsert ){
                    conInfo.addError('Your Territory mapping missing please do assign manadtory ');
                }
                else{
                    conInfo.addError('Your Territory mapping missing please do assign manadtory');
                }
            }
        }
    }
}