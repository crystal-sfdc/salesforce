trigger Woil_ContentVersionTrigger on ContentVersion (before insert, before update, before delete, after undelete) {

    Woil_ContentVersionTriggerHandler.isUploadAllowed(trigger.new);
}