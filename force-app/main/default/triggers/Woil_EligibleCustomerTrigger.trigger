trigger Woil_EligibleCustomerTrigger on Woil_Eligible_Customer__c (after insert, after update, after delete, after undelete) {
    
    if(trigger.isBefore){
        
        if(trigger.isInsert){}
        if(trigger.isUpdate){}
        if(trigger.isDelete){}
    }
    else{
        
        if(trigger.isInsert){
            Woil_EligibleCustomerTriggerHandler.shareSchemeWithCustomers(trigger.newMap, NULL);
        }
        
        if(trigger.isUpdate){
            Woil_EligibleCustomerTriggerHandler.shareSchemeWithCustomers(trigger.newMap, trigger.oldMap);
        }
        
        if(trigger.isDelete){
            Woil_EligibleCustomerTriggerHandler.shareSchemeWithCustomers(NULL, trigger.oldMap);
        }
        
        if(trigger.isUndelete){
            Woil_EligibleCustomerTriggerHandler.shareSchemeWithCustomers(trigger.newMap, NULL);
        }
    }
}