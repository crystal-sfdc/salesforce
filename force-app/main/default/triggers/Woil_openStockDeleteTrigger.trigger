trigger Woil_openStockDeleteTrigger on Woil_Opening_Stock__c (before delete) {
    /* 
Map<Id,Woil_Opening_Stock__c> openstockAndLineItem = new Map<Id,Woil_Opening_Stock__c>([SELECT Id, Woil_Posted__c,(SELECT Id,Woil_Opening_Stock__c FROM Opening_Stock_Line_Item__r) FROM Woil_Opening_Stock__c where id ='a0d9D000000gMHnQAM']);
System.debug(openstockAndLineItem.get('a0d9D000000gMHnQAM').Opening_Stock_Line_Item__r);
List<Woil_OpeningStockLineItem__c> s =  openstockAndLineItem.get('a0d9D000000gMHnQAM').Opening_Stock_Line_Item__r;
System.debug(s);
*/
    if(Trigger.isBefore && Trigger.isDelete){
        List<Woil_OpeningStockLineItem__c> allRelatedItem = new List<Woil_OpeningStockLineItem__c>();
        Set<Id> OpningStockId = trigger.oldMap.keySet();
        Map<Id,Woil_Opening_Stock__c> openstockAndLineItem = new Map<Id,Woil_Opening_Stock__c>([SELECT Id, Woil_Posted__c,(SELECT Id,Woil_Opening_Stock__c FROM Opening_Stock_Line_Item__r) FROM Woil_Opening_Stock__c where Id IN : OpningStockId]);
        
        for(Woil_Opening_Stock__c openStok : trigger.old){
            if(openStok.Woil_Posted__c != true){
                for(Woil_OpeningStockLineItem__c item : openstockAndLineItem.get(openStok.Id).Opening_Stock_Line_Item__r){
                    allRelatedItem.add(item);
                }
            }
            else{
                openStok.addError('We cannot delete an Opening Stock record once Posted!');
            }
            
        }
        delete allRelatedItem;
        
    }
    
}