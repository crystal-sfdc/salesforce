trigger Woil_OrderTrigger on Order (after update, before Insert, before update){
    if(trigger.isAfter && trigger.isUpdate)
        Woil_OrderTriggerHandler.afterUpdate(Trigger.New, Trigger.OldMap);
    if(trigger.isBefore && trigger.isInsert){
        System.debug('#######Before Insert Trigger########');
        Woil_OrderTriggerHandler.beforeInsert(Trigger.New);
    }
    if(trigger.isBefore && trigger.isUpdate){
        Woil_OrderTriggerHandler.checkPoExpiryDate(Trigger.NewMap, Trigger.OldMap);
    }
        
}