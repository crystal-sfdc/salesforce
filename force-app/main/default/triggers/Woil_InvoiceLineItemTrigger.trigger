trigger Woil_InvoiceLineItemTrigger on Woil_Invoice_Line_Item__c (before insert) {
    
    if(trigger.isBefore && trigger.isInsert)
    {
        Woil_InvoiceLineItemTriggerHandler.isBeforeInsert(trigger.new);
        
    }
}