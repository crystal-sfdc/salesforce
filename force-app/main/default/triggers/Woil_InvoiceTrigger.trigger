trigger Woil_InvoiceTrigger on Invoice__c (before insert,after insert,before update, after update) {
    if(trigger.isBefore && trigger.isInsert){
    Woil_InvoiceTrigger_Handler.isBeforeInsert(trigger.new);
    }
    
    if(trigger.isAfter && trigger.isInsert){
    Woil_InvoiceTrigger_Handler.isAfterInsert(trigger.new);
    Woil_InvoiceTrigger_Handler.updateOrderLookupStatus(Trigger.newMap);
    }
    
   if(trigger.isBefore && trigger.isUpdate){
    Woil_InvoiceTrigger_Handler.isBeforeUpdate(trigger.new,Trigger.oldMap);
    }

    
     if(trigger.isAfter && trigger.isUpdate){
    Woil_InvoiceTrigger_Handler.isAfterUpdate(trigger.new,Trigger.oldMap);
    }
    
    
}