trigger Woil_BuyerSellerMappingTrigger on WOIL_Buyer_Seller_Mapping__c (after update, after insert) {

    Woil_ByuerSellerMappingTriggerHandler bsmHandler = new Woil_ByuerSellerMappingTriggerHandler();
    if(Trigger.isInsert && Trigger.isAfter){
        bsmHandler.shareRecordVisibility(trigger.newMap,null);
    }

    if(Trigger.isUpdate && Trigger.isAfter){
        bsmHandler.shareRecordVisibility(trigger.newMap,trigger.oldMap);
    }
}