trigger Woil_OutletTrigger on Outlet_Master__c (after update) {
    if(trigger.isAfter && trigger.isUpdate)
        Woil_OutletTriggerHandler.afterUpdate(Trigger.New, Trigger.OldMap);
}