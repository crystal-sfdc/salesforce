trigger Woil_ShipToAddress_Trigger on Woil_ShipToAddress__c (before insert,before update) {
    if(trigger.isBefore && trigger.isInsert){
       Woil_ShipToAddress_Trigger_handler.BeforeInsert(trigger.new);
    }
        if(trigger.isBefore && trigger.isUpdate){
                 Woil_ShipToAddress_Trigger_handler.BeforeUpdate(trigger.new);
  
        }
}