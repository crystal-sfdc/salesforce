trigger Woil_ModelSparePart on Woil_Model_SparePart_Mapping__c (after insert) {
    
    Map<String, String> mapOfModalSpare= new Map<String, String>();
    
    if(Trigger.isInsert && Trigger.isAfter) {
        for(Woil_Model_SparePart_Mapping__c tempMapping: Trigger.new) {
            mapOfModalSpare.put(tempMapping.Woil_SparePart_Code__c, tempMapping.Woil_Part_Type__c);
       }
     
        List<Product2> updateProducts= new List<Product2>();
        List<Product2> getSpareProducts= [SELECT Id, Name, Woil_Part_Type__c FROM Product2 WHERE Name IN:mapOfModalSpare.keySet()];  
        
        for(Product2 prod:getSpareProducts) {
            prod.Woil_Part_Type__c= mapOfModalSpare.get(prod.Name);
            updateProducts.add(prod);
        }
       update updateProducts;      
    }
}