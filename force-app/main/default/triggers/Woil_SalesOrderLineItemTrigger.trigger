trigger Woil_SalesOrderLineItemTrigger on Woil_Sales_Order_Line_Item__c (after delete,before delete) {
    if(trigger.isDelete && trigger.isAfter){
        Woil_SalesOrderLineItemTriggerHandler.removeSerialScanItems(trigger.old);
    }

    if(trigger.isDelete && trigger.isbefore){
        Woil_SalesOrderLineItemTriggerHandler.checkParentStatus(trigger.oldMap);
    }
}