import { LightningElement,track, wire } from 'lwc';
import fetchMaterialCodeValues from'@salesforce/apex/Woil_SellOutPortal.fetchMaterialCodeValues';
import createDataTable from'@salesforce/apex/Woil_SellOutPortal.createDataTable';

export default class Woil_TPPortalSellOutScreen extends LightningElement {
    @track name;
    @track picklistVal;
    @track dataWrapValue;
 
    @wire(fetchMaterialCodeValues, {}) selectTargetValues;
        
    selectOptionChanveValue(event){       
        this.picklistVal = event.target.value;
   }  

    nameChange(event) {
        this.name= event.target.value;
    }

    addRecord(event){
        createDataTable({materialCode: this.picklistVal,serialNumb: this.name})         
                .then(result => {
                    console.log('Success result',result);
                    this.dataWrapValue = result;
                    this.error = undefined;
                })
                .catch(error => {
                    this.dataWrapValue = undefined;
                    this.error = error;
                });
    }
}