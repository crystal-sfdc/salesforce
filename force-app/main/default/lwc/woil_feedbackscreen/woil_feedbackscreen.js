import { api, LightningElement, track, wire } from 'lwc';
import fetchRecordTypeInfoValues from '@salesforce/apex/DependentPicklistGenerator.fetchRecordTypeInfoValues';
import { getPicklistValues, getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import getTicketCreation from '@salesforce/apex/Woil_CaseEscalationController.getTicketCreation';
import CASE_Sub_Category from '@salesforce/schema/Woil_Case_Escalation__c.Woil_Sub_Category__c';
import CASE_Category from '@salesforce/schema/Woil_Case_Escalation__c.Woil_Category__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


export default class Woil_feedbackscreen extends LightningElement {

    @track defaultrectype;
    @track recordinfo;
    @track categoryValues;
    @track subcategoryValues;
    @api recordId;
    
    //@track recordTypeId;
    @api uploadfilearray =[];
    selectedCategoryValue;
    selectedSubcategoryValue;
    optionVal;
    strCategory;
    strSubCategory;
    strCategoryCheck;
    strSubCategoyCheck;
    caseEscalationRecordTypeId;
    strDescription;
    error;
    uploadedFiles;
    
    //method to fetch the available recordtype
   @wire(fetchRecordTypeInfoValues) wiredFetchrecordtypeValues({ error, data }) {
        if (data){
            
            console.log('Data is',data);
            this.recordinfo = JSON.parse(data);
            console.log('Json Data',JSON.parse(data));

        }else if(error){
            console.error('Check error',error);
        }
    }  

    get recordTypeId() {
        var recordtypeinfo = this.recordinfo;
        var uiCombobox = [];
        
        console.log("recordtype",recordtypeinfo);
       for(var eachRecordtype in  recordtypeinfo)
        {
            if(recordtypeinfo.hasOwnProperty(eachRecordtype)){
                uiCombobox.push({ label: recordtypeinfo[eachRecordtype].name, value: recordtypeinfo[eachRecordtype].recordTypeId  });
            }
        }
        
        console.log('uiCombobox' + JSON.stringify(uiCombobox));
        console.log('uiCombobox size ' + uiCombobox.length);
        
        if(uiCombobox.length === 1){
            this.defaultrectype = uiCombobox[0].value;
            console.log('Piclist value is ',this.defaultrectype);
            this.optionVal=uiCombobox[0].value;
            
        }
        return uiCombobox;
    }

    //onchange event of recordtype selection 
    changeHandler(event){
         
        this.optionVal=event.target.value;
        console.log('optionVal',this.optionVal);
        this.categoryValues = undefined;
        this.selectedCategoryValue = undefined;
        this.subcategoryValues = undefined;
        this.selectedSubcategoryValue = undefined;
   
}



//method to fetch the sub category based on record type selected
@wire(getPicklistValues, 
    {
        
        recordTypeId : '$optionVal',
        fieldApiName : CASE_Sub_Category
        
    })    
    
    wiredSubCategoryPickListValue({ data, error }){
        //console.log('caseInfo',this.caseInfo);
        console.log('Sub category picklist value wire is called');
        if(data){
            this.subcategorydata = data;
            this.error = undefined;
        } else if(error){
            this.error = error;
            this.categoryValues = undefined; 
        }
        
    }
//method to fetch the category based on record type selected
@wire(getPicklistValues,
    {
        recordTypeId : '$optionVal',
        fieldApiName : CASE_Category
    })    
wiredCategoryPickListValue({ data, error }){
    console.log('Category picklist value wire is called');
    if(data){
        this.categoryValues = data.values;
        this.error = undefined;
    }else if(error){
        this.error = error;
        this.categoryValues = undefined; 
    }
} 
    


    handleCategoryChange(event){
        let key = this.subcategorydata.controllerValues[event.target.value];
        this.subcategoryValues = this.subcategorydata.values.filter(opt => opt.validFor.includes(key));
        this.selectedCategoryValue = event.target.value;
        this.strCategory = event.target.value;
    }
        
    handleSubCategoryChange(event){
        this.selectedSubcategoryValue =event.target.value;
        this.strSubCategory = event.target.value;
    }

    handleDescChange(event){
        this.strDescription = event.target.value;
    }

    handleCancel(event) {
        this.template.querySelectorAll("lightning-combobox").forEach(element => {
            element.value = null;
          });
         
          const closeQA = new CustomEvent('close');
          this.dispatchEvent(closeQA);
    }

    get acceptedFormats() {
        return ['.pdf', '.png','.jpg','.jpeg','.txt','.doc','.docx','.xls'];
    }

    handleUploadFinished(event) {
        // Get the list of uploaded files
        //var uploadfilearray = [];
        const uploadedFiles = event.detail.files;
        let uploadedFileNames = '';
        for(let i = 0; i < uploadedFiles.length; i++) {
            uploadedFileNames += uploadedFiles[i].name + ', ';
            this.uploadfilearray.push(uploadedFiles[i]);
        }
        console.log('uploadedFiles ',uploadedFiles);
        console.log('Copy of uploadedFiles',this.uploadfilearray);

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: uploadedFiles.length + ' Files uploaded Successfully: ' + uploadedFileNames,
                variant: 'success',
            }),
        );
    }

    createRecord(){

        var inp = this.template.querySelectorAll("lightning-combobox");

            inp.forEach(function(element){
               
                if(element.name == "Category")
                    this.strCategoryCheck = element.value;
                if(element.name == "Sub-Category")
                    this.strSubCategoyCheck = element.value;    
            },this);

            if(this.strCategoryCheck != null && this.strCategoryCheck != '' && this.strSubCategoyCheck != null && this.strSubCategoyCheck !=''){
                console.log('strCategory: ',this.strCategory);
                console.log('strSubCategory: ',this.strSubCategory);
                console.log('uploadfilearray',this.uploadfilearray);
                let uploadfileJson = JSON.stringify(this.uploadfilearray);
                console.log('Upload file JSON',uploadfileJson);
                
                getTicketCreation({ strCategory: this.strCategory,strSubCategory:this.strSubCategory, strDescription:this.strDescription,recordTypeSelect: this.optionVal,jsonuploadfilearray: uploadfileJson})         
                .then(result => {
                    console.log('Success result',result);
                    this.ticketCheck = result;
                    this.error = undefined;
                    if(this.ticketCheck.includes('Yes')){
                        const casenum = this.ticketCheck.split(' '); 
                        console.log('casenum',casenum);
                        const evt = new ShowToastEvent({
                            title: 'Success',
                            message: 'Case with Case Number '+casenum[1] +' is created',
                            variant: 'success',
                            mode: 'dismissable'
                        });
                        this.dispatchEvent(evt);
                        this.handleCancel();
                        this.refreshComponent();

                    }else if(this.ticketCheck == 'No'){
                        const evt = new ShowToastEvent({
                            title: 'Success',
                            message: 'Mail is sent to the concerned person.',
                            variant: 'success',
                            mode: 'dismissable'
                        });
                        this.dispatchEvent(evt);
                        this.handleCancel();

                    }else{
                        const evt = new ShowToastEvent({
                            title: 'Error',
                            message: 'Please contact your system admin',
                            variant: 'error',
                            mode: 'dismissable'
                        });
                        this.dispatchEvent(evt);
                        //this.handleCancel();

                    }


                })
                .catch(error => {
                    this.ticketCheck = undefined;
                    this.error = error;
                });
            }
            else{
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: 'Please select sub-category',
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(evt);
            }    
    }

    refreshComponent(event){
        console.log('Refresh event is called');
        eval("$A.get('e.force:refreshView').fire();");
    }

    
    
}