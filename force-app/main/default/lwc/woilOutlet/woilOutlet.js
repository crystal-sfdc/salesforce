import { LightningElement ,api, wire, track} from 'lwc';
import getOutletList from '@salesforce/apex/Woil_OutletController.getOutletList';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import Outlet_Type from '@salesforce/schema/Woil_Outlet_Request__c.Woil_Outlet_Type__c';
import Outlet_Req from '@salesforce/schema/Woil_Outlet_Request__c';
//import UpsertOutlet from '@salesforce/apex/Woil_OutletController.UpsertOutlet';

import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';


export default class WoilOutlet extends LightningElement {
    SearchValue;
    Output_Type;
       
        @track error;
        @track outletList ;

        @track OutletRecord = {};

        handleFieldChange(e) {
            this.OutletRecord[e.currentTarget.fieldName] = e.target.value;
        }
        
       /*  @wire(getOutletList,{SearchValue: 'SearchValue'})
       wiredAccounts({
            error,
            data
        }) {
            if (data) {
                this.outletList = data;
            } else if (error) {
                this.error = error;
            }
        }*/

        @wire(getObjectInfo, { objectApiName: Outlet_Req })
        objectInfo;

        @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: Outlet_Type})
    TypePicklistValues;
    
        handleGetOutlets(){
            getOutletList({ SearchValue: this.SearchValue })
            .then(result =>{
                this.outletList = result;
            })
            .catch(error =>{
                this.errorMsg = error;
            })
        }

        handleSearchValue(event){
            this.SearchValue = event.target.value;
        }
    
        handleChange(event){
            this.Output_Type = event.detail.value;
        }

        upsertOutlet() {
            console.log('inside kkkk::::');
            /*UpsertOutlet({ con: { ...this.OutletRecord, sobjectType: Outlet_Req.objectApiName } })
                .then((contact) => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Outlet upserted from apex => ' + contact.Id,
                            variant: 'success'
                        })
                    );
                })
                .catch((err) => console.error(err));*/
        }
        handleReset(event) {
            const inputFields = this.template.querySelectorAll(
                'lightning-input-field'
            );
            if (inputFields) {
                inputFields.forEach(field => {
                    field.reset();
                });
            }
         }
    
}