import { api, LightningElement, track, wire } from 'lwc';
import {  getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import fetchDivision from '@salesforce/apex/Woil_inventorryController.getDivisionList';
import Product_OBJECT from '@salesforce/schema/Product2';

export default class Woil_inventory extends LightningElement {
    @track defaultrectype;
    @track categoryValues;
    @track recordinfo;

    optionVal;
    error;
    selectedCategoryValue='Select Product Category';
    division;



    @wire(fetchDivision) wiredFetchrecordtypeValues({ error, data }) {
        if (data){
            
            console.log('Data is',data);
            this.recordinfo = JSON.parse(data);
            console.log('Json Data',JSON.parse(data));

        }else if(error){
            console.error('Check error',error);
        }
    }  

    get recordTypeId() {
        var recordtypeinfo = this.recordinfo;
        var uiCombobox = [];
        
        console.log("recordtype",recordtypeinfo);
       for(var eachRecordtype in  recordtypeinfo)
        {
            if(recordtypeinfo.hasOwnProperty(eachRecordtype)){
                uiCombobox.push({ label: recordtypeinfo[eachRecordtype].name, value: recordtypeinfo[eachRecordtype].recordTypeId  });
            }
        }
        
        console.log('uiCombobox' + JSON.stringify(uiCombobox));
        console.log('uiCombobox size ' + uiCombobox.length);
        
        if(uiCombobox.length === 1){
            this.defaultrectype = uiCombobox[0].value;
            console.log('Piclist value is ',this.defaultrectype);
            this.optionVal=uiCombobox[0].value;
            
        }
        return uiCombobox;
    }
    changeHandler(event){
        this.optionVal=event.target.value;
        this.categoryValues= undefined;
        this.selectedCategoryValue= undefined;
        console.log('optionVal',this.optionVal);
        const rectype = this.optionVal;
            const valueChangeDivEvent = new CustomEvent("valuechangeDiv", {
                detail: {rectype}

              });
              this.dispatchEvent(valueChangeDivEvent);  
        
    }

    @wire(getPicklistValuesByRecordType, {
        recordTypeId : '$optionVal',
        objectApiName : Product_OBJECT
    })wiredRecordtypeValues({data, error}){
            if(data){
                console.log(' Picklist Values ', data.picklistFieldValues.Woil_Category__c.values);
                this.categoryValues = data.picklistFieldValues.Woil_Category__c.values;
            }
            if(error){
                console.log(error);
            }
        }
        handleCategoryChange(event){
            this.selectedCategoryValue = event.target.value;
            const value = event.target.value;
            //const rectype = this.optionVal;
            const valueChangeEvent = new CustomEvent("valuechange", {
                detail: { value}

              });
              this.dispatchEvent(valueChangeEvent);  
        }
        
}