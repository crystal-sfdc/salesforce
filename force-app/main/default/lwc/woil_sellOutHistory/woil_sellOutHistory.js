import { LightningElement, track, wire } from 'lwc';
import getSelloutData from '@salesforce/apex/Woil_SellOutPortal.getSelloutData';

export default class Woil_sellOutHistory extends LightningElement {
    @track columns = [{
        label: 'Material Code',
        fieldName: 'materialCode',
        type: 'text',
        sortable: true
    },
    {
        label: 'Material Description',
        fieldName: 'materialDesc',
        type: 'text',
        
    },
    {
        label: 'Serial Number',
        fieldName: 'serialNumb',
        type: 'text',
        sortable: true
    },
    {
        label: 'Customer Code',
        fieldName: 'customerCode',
        type: 'text',
    },
    {
        label: 'SellOut Date',
        fieldName: 'transactionDate',
        type: 'Date',
    }
];
@track error;
    @track accList ;
    
    @wire(getSelloutData)
    wiredAccounts({
        error,
        data
    }) {
        if (data) {
            console.log('data',data);
            if(data.length>0){
                console.log('inside');
                this.accList = data;
            }else{
                this.error = 'You have not captured any SellOut.';
            }
        } else if (error) {
            this.error = error;
        }
    }
}