public class Woil_CustomerTrigger_handler {
    public static void BeforeInsert(List<Account> accountInfo){
        set<String> priceList_name = new set<String>();
        Map<String,Id> RId = new Map<String,Id>();
        for(RecordType rt : [select id,name from RecordType where SobjectType = 'Account']){
            RId.put(rt.Name.toLowercase() , rt.Id);
        }
        //customer type pickList-------------------------------------------
        Schema.DescribeFieldResult fieldDescription = Account.Woil_Customer_Type__c.getDescribe();
        List<Schema.PicklistEntry> entries = fieldDescription.getPicklistValues();
        set<String> customerType=new set<String>();
        for (Schema.PicklistEntry entry : entries) {
            customerType.add(entry.getValue());
        }
        System.debug(customerType);
        //customer type pickList-------------------------------------------
        
        for(Account acc: accountInfo){
            priceList_name.add(acc.PriceList__c);
        } 
        List<Woil_Price_List__c> woilPriceList =  [SELECT Id, Name, Woil_Description__c,Woil_Group__c FROM Woil_Price_List__c where name IN : priceList_name ];
        
        System.debug(woilPriceList);
        Map<String,Woil_Price_List__c> priceListMap = new Map<String , Woil_Price_List__c>();
        for( Woil_Price_List__c p : woilPriceList){
            priceListMap.put(p.Name,p);
        }
        
        for(Account accFinal: accountInfo){
            if(priceListMap.containsKey(accFinal.PriceList__c)){
                accFinal.Woil_Price_List__c = priceListMap.get(accFinal.PriceList__c).Id;
                accFinal.RecordTypeId  =  RId.get(priceListMap.get(accFinal.PriceList__c).Woil_Group__c.toLowercase());
                System.debug( RId.get(priceListMap.get(accFinal.PriceList__c).Woil_Group__c));
                if(customerType.contains(priceListMap.get(accFinal.PriceList__c).Woil_Description__c)){
                    accFinal.Woil_Customer_Type__c = priceListMap.get(accFinal.PriceList__c).Woil_Description__c;
                }
            }
        }
    }
    
    
}