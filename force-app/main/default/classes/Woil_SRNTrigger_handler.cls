public class Woil_SRNTrigger_handler {
    public static Map<Id,Decimal> InvoiceVsQty=new Map<Id,Decimal> ();
    public static List<Woil_SRN__c> LstSRN=new List<Woil_SRN__c>();
    public static Map<Id, Woil_SRN__c> objMap =new Map<Id, Woil_SRN__c>();
    public static void BeforeUpdate(List<Woil_SRN__c> Lstgrn,Map<Id,Woil_SRN__c> mapSRN){
        
        objMap = new Map<Id, Woil_SRN__c>([
            SELECT Id, Woil_Account__r.Name 
            FROM Woil_SRN__c
            WHERE Id IN : Lstgrn
        ]);
        
        System.debug('Lstgrn::'+Lstgrn+'::mapSRN::'+mapSRN);
        for(Woil_SRN__c grn:Lstgrn){
            
            if(grn.Woil_Posted__c == true && mapSRN.get(grn.Id).Woil_posted__c != true){
                InvoiceVsQty.put(grn.Woil_Sales_Invoice__c , grn.Woil_Total_Rejected_Qty_FG__c+grn.Woil_Total_Rejected_Qty_SP__c);
            }
            System.debug('InvoiceVsQty===> '+InvoiceVsQty);
            updateInvoiceQty(InvoiceVsQty); 
            
            if(grn.Woil_On_Spot_Return__c==true && (mapSRN.get(grn.Id).Woil_division__c==null && grn.Woil_division__c!=null) ){
                grn.Woil_Status__c='Approved'; 
                getLineItemsforEmail(grn);                
            }else if(mapSRN.get(grn.Id).Woil_status__c!='Approved' && grn.Woil_Status__c=='Approved' && grn.Woil_Posted__c!=true){
                //getLineItemsforEmail(grn);
                Woil_PostGRNController.postSRN(grn.Id);
                grn.Woil_Return_Order_Date__c=Date.today();
                grn.Woil_Posted__c=true;
            }          
        }        
    }
    public static void getLineItemsforEmail(Woil_SRN__c srn){
        List<productWrapper> Lstprodwrap=new List<productWrapper>();
        List<Woil_SRN_Line_Item__c> SRNItem=new List<Woil_SRN_Line_Item__c>();
        List<Woil_SRN_Serial_Number__c> LstSRNserial=new List<Woil_SRN_Serial_Number__c>();
        Map<String,String> SRNSerial=new Map<String,String>();
        SRNItem=[Select id,Woil_Ordered_Quantity__c,Woil_product__r.Woil_Is_Serializable__c,Woil_product__r.Name,Woil_SRN__r.Woil_On_Spot_Return__c from Woil_SRN_Line_Item__c where Woil_SRN__c=:srn.Id];
        
        for(Woil_SRN_Line_Item__c item:SRNItem)  {
            
            if(item.Woil_product__r.Woil_Is_Serializable__c==true && item.Woil_SRN__r.Woil_On_Spot_Return__c==true){
                SRNSerial.put(item.id,null);
            } else if(item.Woil_product__r.Woil_Is_Serializable__c==true && item.Woil_SRN__r.Woil_On_Spot_Return__c==false){
                SRNSerial.put(item.id,null);
            }else{
                productWrapper prod=new productWrapper();
                prod.prodCode=item.Woil_product__r.Name;
                prod.Quantity=item.Woil_Ordered_Quantity__c;
                Lstprodwrap.add(prod); 
            }               
        }
        LstSRNserial=[Select Id,Woil_Serial_Number__c,Woil_SRN_Line_Item__r.Woil_Ordered_Quantity__c,Woil_SRN_Line_Item__r.Woil_product__r.Name from Woil_SRN_Serial_Number__c where Id=:SRNSerial.Keyset() ];
        for(Woil_SRN_Serial_Number__c serial:LstSRNserial){
            productWrapper prod=new productWrapper();
            prod.prodCode=serial.Woil_SRN_Line_Item__r.Woil_product__r.Name;
            prod.serialNo= serial.Woil_Serial_Number__c;
            prod.Quantity=1;
            Lstprodwrap.add(prod);
        }
        for(Woil_Serial_Number_Scan__c scan:[select Woil_Purchase_Return__c,id,Woil_Serial_Number_Scan__c.Woil_Product__r.Name,Woil_Serial_Number__c from Woil_Serial_Number_Scan__c where Woil_Purchase_Return__c=:srn.Id])
        {
            productWrapper prod=new productWrapper();
            prod.prodCode=scan.Woil_Product__r.Name;
            prod.serialNo= scan.Woil_Serial_Number__c;
            prod.Quantity=1;
            Lstprodwrap.add(prod); 
        }
        Map<String, Woil_Default_Value_Configuration__mdt> mapEd = Woil_Default_Value_Configuration__mdt.getAll();
        String sendTo;
        for(String nameEmailDomain : mapEd.keySet()){
            if(srn.Woil_Division__c.trim()==System.label.Woil_Finished_Goods.trim() && nameEmailDomain=='CFA_AG_EMAIL')
                sendTo =mapEd.get('CFA_AG_EMAIL').Woil_Default_Value__c;
            else if(srn.Woil_Division__c.trim()==   System.label.Woil_Spare_Part_Order_Type.trim() && nameEmailDomain=='CFA_AS_EMAIL')
                sendTo =mapEd.get('CFA_AS_EMAIL').Woil_Default_Value__c;
            
        }
        
        String emailBody='TP '+objMap.get(srn.Id).Woil_Account__r.Name+ ' has placed return order '+srn.Name +'.The product details as below:';
        emailBody=emailBody+getTableBody(Lstprodwrap);
        System.debug('body::::'+emailBody+'::sendTo::'+sendTo);
        //sendEmail(null,sendTo,emailBody); 
        
    }
    
    public static void sendEmail(string sender,String sendTo,String body){
        List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List <String> LstsendTo = sendTo.split(',');
        LstsendTo.addAll(sendTo.split(','));
        mail.setToAddresses(LstsendTo);
        mail.setReplyTo(sender);
        mail.setSenderDisplayName('Salesforce User');
        mail.setSubject('Test Mail');     
        mail.setHtmlBody(body);
        //mail.setHtmlBody('Test mail from apex class');
        mails.add(mail);
        system.debug('Email is sent');
        List<Messaging.SendEmailResult> mail_results=Messaging.sendEmail(mails);
        System.debug('mail_results::'+mail_results);
        
    }
    
    public static string getTableBody(List<productWrapper> myList){
        
        String htmlBody = '';    
        htmlBody = '<table border="1" style="border-collapse: collapse"><caption>Product Details</caption><tr><th>Product Code</th><th>Serial No</th><th>Quantity</th</tr>';
        for(productWrapper l : myList){
            
            String prod = l.prodCode; if(l.prodCode == null){prod = '[Not Provided]';}
            String serial = l.serialNo; if(l.serialNo == null){serial = '';}
            Decimal quantity = l.Quantity; if(l.Quantity == null){quantity = 0;}
            htmlBody += '<tr><td>' + prod + '</td><td>' + serial + '</td></tr>' + quantity + '</td></tr>';
            
        }
        htmlBody += '</table>';
        return htmlBody;
        
    }
    public static void updateInvoiceQty(Map<Id,Decimal> InvoiceVsQty){
        //if(!Woil_UtilityClass.firstcall) {
        //    Woil_UtilityClass.firstcall = true;
            System.debug('InvoiceVsQty'+InvoiceVsQty);
            if(InvoiceVsQty!=null){
                List<Invoice__c> invoices=[Select Id,Woil_Accepted_or_Rejected_Quantity__c,Woil_Total_Quantity__c,Woil_Rejected_Quantity__c from Invoice__c where Id in :InvoiceVsQty.keySet()];
                System.debug('invoice'+invoices);
                for(Invoice__c inv:invoices){
                    if(InvoiceVsQty.containsKey(inv.Id)){
                        System.debug('#####'+inv.Woil_Rejected_Quantity__c+' + '+(InvoiceVsQty.get(inv.Id)));
                        inv.Woil_Rejected_Quantity__c=(InvoiceVsQty.get(inv.Id)!=null?InvoiceVsQty.get(inv.Id):0)+(inv.Woil_Rejected_Quantity__c!=null?inv.Woil_Rejected_Quantity__c:0);
                        System.debug('#####Result'+inv.Woil_Rejected_Quantity__c);
                        /* Decimal qty=(inv.Woil_Rejected_Quantity__c!=null?inv.Woil_Rejected_Quantity__c:0)+(inv.Woil_Accepted_or_Rejected_Quantity__c!=null?inv.Woil_Accepted_or_Rejected_Quantity__c:0);
InvoiceVsQty.put(inv.Id,qty); 
inv.Woil_Accepted_or_Rejected_Quantity__c=qty;

if(inv.Woil_Accepted_or_Rejected_Quantity__c==inv.Woil_Total_Quantity__c){
inv.Woil_Status__c='Processed';
}*/
                    }            
                }
                System.debug('invoice11:::'+invoices);
                update invoices;
            }
      //  }
    }
    public  class productWrapper{
        public  String prodCode;
        public  String serialNo;
        public  Decimal Quantity;
        
    }
}