global class Woil_BatchCaseEscalationClass implements Database.Batchable<sObject>,Database.Stateful {
    
    Map<String,Id> mapOfFinalCaseFirst = new Map<String,Id>();
global Database.QueryLocator start(Database.BatchableContext BC) {
    // collect the batches of records or objects to be passed to execute
        
    String query ='Select Id,AccountId, CaseNumber, OwnerId,woil_category__c,woil_sub_category__c,LastModifiedDate,Status,Woil_Days_since_last_update__c,Woil_Case_Escalation_RecordTypeId__c,Woil_Case_Escalation__c,Woil_First_Escalation__c,Woil_Second_Escalation__c,Woil_Third_Escalatons__c from Case where Status != \'Closed\'';
    return Database.getQueryLocator(query);
}
    
global void execute(Database.BatchableContext BC, List<Case> caseList) {
    System.debug('In execute method');
    System.debug('CaseObj '+caseList);
    System.debug('Size of caseList is '+caseList.size());
   
    //Variable declaration

    Set<Id> accountId = new Set<Id>();
    Set<Id> accplanId = new Set<Id>();
    Set<Id> acccccId = new Set<Id>();
    List<Woil_Account_Plant__c> accountPlant = new List<Woil_Account_Plant__c>();
    List<Id> salesofficeId = new List<Id>();
    List<Case> updateCaseList = new List<Case>();
    List<String> caseDivision = new List<String>();
    Set<String> roleStringfirst = new Set<String>();
    Set<String> roleStringSecond = new Set<String>();
    Set<String> roleStringthird = new Set<String>();
    Set<Id> finalaccPlant = new Set<Id>();
    List<Id> caseEscalationId = new List<Id>();
    List<Woil_Case_Escalation__c> caseEscalObj = new List<Woil_Case_Escalation__c>();
    //Map<Id,Woil_Case_Escalation__c> mapOfCaseEscalation = new Map<Id,Woil_Case_Escalation__c>();
    Map<Id,Case> mapofCase = new Map<Id,Case>();
    Map<Id,Woil_Case_Escalation__c> mapofAssociatedCaseEscalation = new Map<Id,Woil_Case_Escalation__c>();
    Map<Id,Case> mapofCaseFirstEscalation = new Map<Id,Case>();
    Map<Id,Case> mapofCaseSecondEscalation = new Map<Id,Case>();
    Map<Id,Case> mapofCaseThirdEscalation = new Map<Id,Case>();
    Map<Id,Woil_Account_Plant__c> mapOfAccountPlant = new Map<Id,Woil_Account_Plant__c>();
    Map<Id,Id> mapofAcccountID = new Map<Id,Id>(); 
    Map<Id,Id> mapOfAccountPlantID = new Map<Id,Id>();
    Map<Id,Woil_Sales_Office__c> mapOffinalSalesOffice = new Map<Id,Woil_Sales_Office__c>();
    Map<Id,Id> mapOfCaseEscalRecurssion = new Map<Id,Id>();
    Map<String,Id> mapOfUserFirstList = new Map<String,Id>(); 
    
    Map<String,Id> mapofSalesAccId = new Map<String,Id>();
    //Map<Id,Woil_Case_Escalation__c> mapOfCaseEscalation = new Map<Id,Woil_Case_Escalation__c>();

    
    
    Boolean firstEscalation;
    Boolean secondEscalation;
    Boolean thirdEscalation;
    String escalation;


   

    try {  
    if(!caseList.isEmpty()){
        
    for(Case caseObj : caseList) {        
        accountId.add(caseObj.AccountId);
        mapofAcccountID.put(caseObj.Id,caseObj.AccountId);
        caseDivision.add(caseObj.Woil_Case_Escalation_RecordTypeId__c);
        caseEscalationId.add(caseObj.Woil_Case_Escalation__c);
        mapofCase.put(caseObj.Id,caseObj);

      
    }
    System.debug('MapOfcase'+mapofCase);
    System.debug('Map oF case size '+mapofCase.size());
    if(!caseEscalationId.isEmpty()){
        Map<Id,Woil_Case_Escalation__c> mapOfCaseEscalation = new Map<Id,Woil_Case_Escalation__c>([select id,Name,Woil_Level_0_role__c,Woil_Level_1_role__c,Woil_Level_2_role__c,Woil_Level_3_role__c,Woil_First_Escalation_TAT__c,Woil_Second_Escalation_TAT__c,Woil_Third_Escalation_TAT__c from Woil_Case_Escalation__c where Id In: caseEscalationId]); 
       
        System.debug('Size of map of caseescalation '+mapOfCaseEscalation.keySet().Size());
        System.debug('map of mapOfCaseEscalation'+mapOfCaseEscalation);

        for(Id caseEscalId: mapOfCaseEscalation.keySet()){
            for(Id caseId: mapOfCase.keySet()){
                if(caseEscalId == mapofCase.get(caseId).Woil_Case_Escalation__c){
                   
                    mapofAssociatedCaseEscalation.put(caseId,mapOfCaseEscalation.get(caseEscalId));
                    
                    Woil_Case_Escalation__c objEscal = mapOfCaseEscalation.get(caseEscalId);
                    

                    if(String.isNotBlank(objEscal.Woil_Level_1_role__c)){ 
                        
                        List<String> roleList = mapOfCaseEscalation.get(caseEscalId).Woil_Level_1_role__c.split(';');
                        for(String roleStr :roleList){
                            roleStringfirst.add(roleStr);
                        }
                }
                    if(String.isNotBlank(objEscal.Woil_Level_2_role__c)){ 
                        
                        List<String> roleSecondList = mapOfCaseEscalation.get(caseEscalId).Woil_Level_2_role__c.split(';');
                        for(String roleStr :roleSecondList){
                            roleStringSecond.add(roleStr);
                        }
                    }

                    if(String.isNotBlank(objEscal.Woil_Level_3_role__c)){
                        
                        List<String> roleThirdList = mapOfCaseEscalation.get(caseEscalId).Woil_Level_3_role__c.split(';');
                        for(String roleStr :roleThirdList){
                            roleStringthird.add(roleStr);
                        }  
                    }
                }
            }
        }
        System.debug('mapofAssociatedCaseEscalation: '+mapofAssociatedCaseEscalation);
        System.debug('mapOfAssociate Case escalation SIze '+mapofAssociatedCaseEscalation.keySet().size());

        system.debug('Account Id'+accountId);
        system.debug('roleStringfirst'+roleStringfirst);
        system.debug('roleStringSecond'+roleStringSecond);
        system.debug('roleStringthird'+roleStringthird);

        if(!accountId.isEmpty()){
            

            accountPlant =[Select id,Woil_Trade_Partner__c,Woil_Sales_Office__c,Woil_Division__c,
            Woil_Sales_Office__r.Woil_Parent_Territory__c,
            Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c,
            Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c from Woil_Account_Plant__c where Woil_Trade_Partner__c IN: accountId];     
        }
        System.debug('accountPlant'+accountPlant);

        for(Id casObjId : mapofCase.keySet()){
            Case caseObj = mapofCase.get(casObjId);
            if(mapofAssociatedCaseEscalation.keySet().contains(casObjId)){
                Woil_Case_Escalation__c objEscal = mapofAssociatedCaseEscalation.get(casObjId);
                Decimal intEscal;
                if(objEscal.Woil_Third_Escalation_TAT__c!=null){
                    intEscal = objEscal.Woil_Third_Escalation_TAT__c;
                }else{
                    intEscal = 0;
                }

               
                system.debug('INtEscal '+intEscal);
                System.debug('casObjId'+casObjId);
                System.debug('caseObj.Woil_Days_since_last_update__c'+Integer.valueOf(caseObj.Woil_Days_since_last_update__c));
                System.debug('Flag'+caseObj.Woil_First_Escalation__c + caseObj.Woil_Second_Escalation__c + caseObj.Woil_Third_Escalatons__c);
                if(objEscal.Woil_First_Escalation_TAT__c!=null || objEscal.Woil_Second_Escalation_TAT__c!=null){
                    if( (objEscal.Woil_First_Escalation_TAT__c != null) && caseObj.Woil_Days_since_last_update__c >= objEscal.Woil_First_Escalation_TAT__c && caseObj.Woil_Days_since_last_update__c < (objEscal.Woil_First_Escalation_TAT__c + objEscal.Woil_Second_Escalation_TAT__c) && caseObj.Woil_First_Escalation__c == false){
                        mapofCaseFirstEscalation.put(casObjId, caseObj);
                        acccccId.add(mapofCase.get(casObjId).AccountId);
                        escalation ='First';
                        caseObj.Woil_First_Escalation__c = true;
                        updateCaseList.add(caseObj);
                }
                else if((objEscal.Woil_Second_Escalation_TAT__c != null) && caseObj.Woil_Days_since_last_update__c >= objEscal.Woil_Second_Escalation_TAT__c && caseObj.Woil_Days_since_last_update__c < (objEscal.Woil_Second_Escalation_TAT__c + intEscal) && caseObj.Woil_First_Escalation__c == true && caseObj.Woil_Second_Escalation__c == false){
                       
                        mapofCaseSecondEscalation.put(casObjId, caseObj);
                        acccccId.add(mapofCase.get(casObjId).AccountId);
                        escalation ='Second';
                        caseObj.Woil_Second_Escalation__c = true;
                        updateCaseList.add(caseObj);
                    }    
                   
                
                else if( caseObj.Woil_Days_since_last_update__c == intEscal && caseObj.Woil_First_Escalation__c == true && caseObj.Woil_Second_Escalation__c == true && caseObj.Woil_Third_Escalatons__c == false){
                    
                    mapofCaseThirdEscalation.put(casObjId, caseObj);
                    acccccId.add(mapofCase.get(casObjId).AccountId);
                    escalation ='Third';
                    caseObj.Woil_Third_Escalatons__c = true;
                    updateCaseList.add(caseObj);
                }    
            }
               
            }
            }
        }
        

        System.debug('Escalation '+escalation);
        if(String.isNotBlank(escalation)){
            if(escalation == 'First'){

                for(Id caseObjId : mapofCaseFirstEscalation.keySet()){
                    for(Woil_Account_Plant__c accPl: accountPlant){
                        if(mapofCaseFirstEscalation.get(caseObjId).AccountId == accpl.Woil_Trade_Partner__c){
                            if(mapofCaseFirstEscalation.get(caseObjId).Woil_Case_Escalation_RecordTypeId__c == accPl.Woil_Division__c){
                                finalaccPlant.add(accPl.Woil_Sales_Office__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c);
                                mapOfAccountPlantID.put(caseObjId,accPl.Id);
                            }
                        }
                    } 
                }
                system.debug('Map mapOfAccountPlantID '+mapOfAccountPlantID);
                system.debug('Map mapOfAccountPlantID Size'+mapOfAccountPlantID.size());
                System.debug('finalaccPlant'+finalaccPlant);


                List<Woil_User_Territory__c> userFirstEscal =[select Id,Woil_User__r.Email,Woil_User__r.UserRole.Name,Woil_Sales_Office__c from Woil_User_Territory__c where Woil_Sales_Office__c IN: finalaccPlant and Woil_User__r.UserRole.Name IN: roleStringfirst];
                List<Id> saID = new List<Id>();
                for(Woil_User_Territory__c ut: userFirstEscal){
                    saID.add(ut.Woil_Sales_Office__c);
                }
                
                Map<Id,Woil_Sales_Office__c> userFirstSalesoffice = new Map<Id,Woil_Sales_Office__c>([select Id,Woil_Territory_Model__c from Woil_Sales_Office__c where Id IN: saID]);
                System.debug('userFirstEscal'+userFirstEscal);
                List<Woil_Account_Plant__c> acplanID = [select id,Woil_Sales_Office__c,Woil_Trade_Partner__c from Woil_Account_Plant__c where Woil_Sales_Office__c IN:userFirstSalesoffice.keySet() and Woil_Trade_Partner__c IN: acccccId];
                
                system.debug('Map userFirstSalesoffice '+userFirstSalesoffice);
                system.debug('Map userFirstSalesoffice Size'+userFirstSalesoffice.size());
                System.debug('acplanID'+acplanID);

                //List<Id> accPlanFirstescal = new List<Id>();
                for(Id salesOffID: userFirstSalesoffice.keySet()){
                    for(Woil_Account_Plant__c accPlanID1: acplanID){
                        
                        if(accPlanID1.Woil_Sales_Office__c == salesOffID){
                            String tempKey = salesOffID+','+accPlanID1.Woil_Trade_Partner__c;
                            mapofSalesAccId.put(tempKey,accPlanID1.Id);
                        }
                        
                    }
                }

                

                system.debug('Map mapofSalesAccId '+mapofSalesAccId);
                system.debug('Map mapofSalesAccId Size'+mapofSalesAccId.size());
            
                for (Woil_User_Territory__c usr: userFirstEscal){
                    //accPlanFirstescal.add(usr.Woil_Sales_Office__r.Woil_Account_Plant__c);
                    mapOfUserFirstList.put(usr.Woil_Sales_Office__c+' '+usr.Woil_User__r.Email,usr.Woil_User__c);
                }
                for(ID caseID : mapOfAccountPlantID.keySet()){
                    
                    for(String mapOfId:mapofSalesAccId.keySet()){
                        String salesSO = mapOfId.substringBefore(',');
                        System.debug('salesSO'+salesSO);
                        //System.debug('mapOfAccountPlantID.get(caseID)'+mapOfAccountPlantID.get(caseID));
                        //System.debug('mapofSalesAccId.get(mapOfId)'+mapofSalesAccId.get(mapOfId));
                        if(mapOfAccountPlantID.get(caseID)==mapofSalesAccId.get(mapOfId)) {
                            for(String str : mapOfUserFirstList.keySet()){
                                String strSO = mapOfId.substringBefore(',');
                                if(strSo == salesSO){
                                    String tempEmail = str.substringAfter(' ');
                                    String tempVar = caseID+' '+tempEmail;

                                    mapOfFinalCaseFirst.put(tempVar,mapOfUserFirstList.get(str)); 
                                }
                            }
                                
                        } 
                        
                    }
                }


            } else if(escalation == 'Second'){
                System.debug('Second');
                for(Id caseObjId : mapofCaseSecondEscalation.keySet()){
                    for(Woil_Account_Plant__c accPl: accountPlant){
                        if(mapofCaseSecondEscalation.get(caseObjId).AccountId == accpl.Woil_Trade_Partner__c){
                            if(mapofCaseSecondEscalation.get(caseObjId).Woil_Case_Escalation_RecordTypeId__c == accPl.Woil_Division__c){
                                finalaccPlant.add(accPl.Woil_Sales_Office__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c);
                                mapOfAccountPlantID.put(caseObjId,accPl.Id);
                            }
                        }
                    } 
                }
                system.debug('Map mapOfAccountPlantID second escalation '+mapOfAccountPlantID);
                system.debug('Map mapOfAccountPlantID Size second escalation'+mapOfAccountPlantID.size());
                System.debug('finalaccPlant'+finalaccPlant);
                List<Woil_User_Territory__c> userFirstEscal =[select Id,Woil_User__r.Email,Woil_User__r.UserRole.Name,Woil_Sales_Office__c from Woil_User_Territory__c where Woil_Sales_Office__c IN: finalaccPlant and Woil_User__r.UserRole.Name IN: roleStringSecond];
                List<Id> saID = new List<Id>();
                for(Woil_User_Territory__c ut: userFirstEscal){
                    saID.add(ut.Woil_Sales_Office__c);
                }
                
                Map<Id,Woil_Sales_Office__c> userFirstSalesoffice = new Map<Id,Woil_Sales_Office__c>([select Id,Woil_Territory_Model__c from Woil_Sales_Office__c where Id IN: saID]);
                System.debug('userFirstEscal'+userFirstEscal);
                List<Woil_Account_Plant__c> acplanID = [select id,Woil_Sales_Office__c,Woil_Trade_Partner__c from Woil_Account_Plant__c where Woil_Sales_Office__c IN:userFirstSalesoffice.keySet() and Woil_Trade_Partner__c IN: acccccId];
                
                system.debug('Map userFirstSalesoffice '+userFirstSalesoffice);
                system.debug('Map userFirstSalesoffice Size'+userFirstSalesoffice.size());
                System.debug('acplanID'+acplanID);

                //List<Id> accPlanFirstescal = new List<Id>();
                for(Id salesOffID: userFirstSalesoffice.keySet()){
                    for(Woil_Account_Plant__c accPlanID1: acplanID){
                        
                        if(accPlanID1.Woil_Sales_Office__c == salesOffID){
                            String tempKey = salesOffID+','+accPlanID1.Woil_Trade_Partner__c;
                            mapofSalesAccId.put(tempKey,accPlanID1.Id);
                        }
                        
                    }
                }

                

                system.debug('Map mapofSalesAccId '+mapofSalesAccId);
                system.debug('Map mapofSalesAccId Size'+mapofSalesAccId.size());
            
                for (Woil_User_Territory__c usr: userFirstEscal){
                    //accPlanFirstescal.add(usr.Woil_Sales_Office__r.Woil_Account_Plant__c);
                    mapOfUserFirstList.put(usr.Woil_Sales_Office__c+' '+usr.Woil_User__r.Email,usr.Woil_User__c);
                }
                for(ID caseID : mapOfAccountPlantID.keySet()){
                    
                    for(String mapOfId:mapofSalesAccId.keySet()){
                        String salesSO = mapOfId.substringBefore(',');
                        System.debug('salesSO'+salesSO);
                        //System.debug('mapOfAccountPlantID.get(caseID)'+mapOfAccountPlantID.get(caseID));
                        //System.debug('mapofSalesAccId.get(mapOfId)'+mapofSalesAccId.get(mapOfId));
                        if(mapOfAccountPlantID.get(caseID)==mapofSalesAccId.get(mapOfId)) {
                            for(String str : mapOfUserFirstList.keySet()){
                                String strSO = mapOfId.substringBefore(',');
                                if(strSo == salesSO){
                                    String tempEmail = str.substringAfter(' ');
                                    String tempVar = caseID+' '+tempEmail;

                                    mapOfFinalCaseFirst.put(tempVar,mapOfUserFirstList.get(str)); 
                                }
                            }
                                
                        } 
                        
                    }
                }

            }

            else if(escalation == 'Third'){
                System.debug('Third');
                for(Id caseObjId : mapofCaseThirdEscalation.keySet()){
                    for(Woil_Account_Plant__c accPl: accountPlant){
                        if(mapofCaseThirdEscalation.get(caseObjId).AccountId == accpl.Woil_Trade_Partner__c){
                            if(mapofCaseThirdEscalation.get(caseObjId).Woil_Case_Escalation_RecordTypeId__c == accPl.Woil_Division__c){
                                finalaccPlant.add(accPl.Woil_Sales_Office__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c);
                                finalaccPlant.add(accPl.Woil_Sales_Office__r.Woil_Parent_Territory__r.Woil_Parent_Territory__r.Woil_Parent_Territory__c);
                                mapOfAccountPlantID.put(caseObjId,accPl.Id);
                            }
                        }
                    } 
                }
                system.debug('Map mapOfAccountPlantID second escalation '+mapOfAccountPlantID);
                system.debug('Map mapOfAccountPlantID Size second escalation'+mapOfAccountPlantID.size());
                System.debug('finalaccPlant'+finalaccPlant);
                List<Woil_User_Territory__c> userFirstEscal =[select Id,Woil_User__r.Email,Woil_User__r.UserRole.Name,Woil_Sales_Office__c from Woil_User_Territory__c where Woil_Sales_Office__c IN: finalaccPlant and Woil_User__r.UserRole.Name IN: roleStringthird];
                List<Id> saID = new List<Id>();
                for(Woil_User_Territory__c ut: userFirstEscal){
                    saID.add(ut.Woil_Sales_Office__c);
                }
                
                Map<Id,Woil_Sales_Office__c> userFirstSalesoffice = new Map<Id,Woil_Sales_Office__c>([select Id,Woil_Territory_Model__c from Woil_Sales_Office__c where Id IN: saID]);
                System.debug('userFirstEscal'+userFirstEscal);
                List<Woil_Account_Plant__c> acplanID = [select id,Woil_Sales_Office__c,Woil_Trade_Partner__c from Woil_Account_Plant__c where Woil_Sales_Office__c IN:userFirstSalesoffice.keySet() and Woil_Trade_Partner__c IN: acccccId];
                
                system.debug('Map userFirstSalesoffice '+userFirstSalesoffice);
                system.debug('Map userFirstSalesoffice Size'+userFirstSalesoffice.size());
                System.debug('acplanID'+acplanID);

                //List<Id> accPlanFirstescal = new List<Id>();
                for(Id salesOffID: userFirstSalesoffice.keySet()){
                    for(Woil_Account_Plant__c accPlanID1: acplanID){
                        
                        if(accPlanID1.Woil_Sales_Office__c == salesOffID){
                            String tempKey = salesOffID+','+accPlanID1.Woil_Trade_Partner__c;
                            mapofSalesAccId.put(tempKey,accPlanID1.Id);
                        }
                        
                    }
                }

                

                system.debug('Map mapofSalesAccId '+mapofSalesAccId);
                system.debug('Map mapofSalesAccId Size'+mapofSalesAccId.size());
            
                for (Woil_User_Territory__c usr: userFirstEscal){
                    //accPlanFirstescal.add(usr.Woil_Sales_Office__r.Woil_Account_Plant__c);
                    mapOfUserFirstList.put(usr.Woil_Sales_Office__c+' '+usr.Woil_User__r.Email,usr.Woil_User__c);
                }
                for(ID caseID : mapOfAccountPlantID.keySet()){
                    
                    for(String mapOfId:mapofSalesAccId.keySet()){
                        String salesSO = mapOfId.substringBefore(',');
                        System.debug('salesSO'+salesSO);
                        //System.debug('mapOfAccountPlantID.get(caseID)'+mapOfAccountPlantID.get(caseID));
                        //System.debug('mapofSalesAccId.get(mapOfId)'+mapofSalesAccId.get(mapOfId));
                        if(mapOfAccountPlantID.get(caseID)==mapofSalesAccId.get(mapOfId)) {
                            for(String str : mapOfUserFirstList.keySet()){
                                String strSO = mapOfId.substringBefore(',');
                                if(strSo == salesSO){
                                    String tempEmail = str.substringAfter(' ');
                                    String tempVar = caseID+' '+tempEmail;

                                    mapOfFinalCaseFirst.put(tempVar,mapOfUserFirstList.get(str)); 
                                }
                            }
                                
                        } 
                        
                    }
                }

            }
            system.debug('Map mapofCaseFirstEscalation '+mapofCaseFirstEscalation);
            system.debug('Map mapofCaseFirstEscalation Size'+mapofCaseFirstEscalation.size());

            system.debug('Map mapOfUserFirstList '+mapOfUserFirstList);
            system.debug('Map mapOfUserFirstList Size'+mapOfUserFirstList.size());
            
            system.debug('Map mapOfFinalCaseFirst '+mapOfFinalCaseFirst);
            system.debug('Map mapOfFinalCaseFirst Size'+mapOfFinalCaseFirst.size());


            Database.update(updateCaseList);
        }
}    


    
        
} catch(Exception e) {
        System.debug('The following error has occurred.'); 
        System.debug('Exception caught: ' + e.getMessage()); 
    }
        
}   
    
global void finish(Database.BatchableContext BC) {
    // execute any post-processing operations like sending email
    System.debug('Inside finish method');
    system.debug('Map mapOfFinalCaseFirst '+mapOfFinalCaseFirst);
            system.debug('Map mapOfFinalCaseFirst Size'+mapOfFinalCaseFirst.size());
    

            
    Map<Id,Id> mapOfUserID = new Map<Id,Id>();         
    List<Case> updateCaseList = new List<Case>();
    List<String> sendToEmail = new List<String>();
    List<Id> caseIdList = new List<Id>();
    for(String tempString: mapOfFinalCaseFirst.keySet()){
        
        String email = tempString.substringAfter(' ');
        Id caseId = tempString.substringBefore(' ');
        sendToEmail.add(email);
        caseIdList.add(caseId);
        mapOfUserID.put(caseId,mapOfFinalCaseFirst.get(tempString));
        
    }
    System.debug('caseIdList'+caseIdList);
   
    if(!sendToEmail.isEmpty()){

        if(!caseIdList.isEmpty()){
            system.debug('sendTo '+ sendToEmail);
            List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
            for(Case caseObj: [Select id, OwnerID,CaseNumber, Woil_Description__c from Case where ID IN: caseIdList]){
                caseObj.OwnerId = mapOfUserID.get(caseObj.Id);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(sendToEmail);
                mail.setReplyTo('ashishsingh10@kpmg.com');
                mail.setSenderDisplayName('Salesforce User');
                mail.setSubject('Test Mail');
                String body = 'Dear ' + ', ';
                body += 'This is a test email related to your Case number .'+caseObj.CaseNumber;
                body += caseObj.Woil_Description__c;
                
                mail.setHtmlBody(body);
                mails.add(mail);
                updateCaseList.add(caseObj);

            }
        
            Messaging.sendEmail(mails);
            System.debug('Mail content'+mails);
            system.debug('Email is sent from finish'); 
            System.debug('updateCaseList'+updateCaseList);
            if(!updateCaseList.isEmpty()){
                Database.update(updateCaseList);
            }
        }
    }
}




}