public class Woil_OutletTriggerHandler {
    public static void afterUpdate(List<Outlet_Master__c> outletNewList, Map<Id,Outlet_Master__c> outletOldMap){
        WOIL_Buyer_Seller_Mapping__c buyerSellerMappingObj;
        Account parentAccount;
        for(Outlet_Master__c outletNewObj : outletNewList){
            System.debug('outletNewObj.id'+outletNewObj.id);
            System.debug('outletNewObj.Woil_Status__c'+outletNewObj.Woil_Status__c);
            if((outletNewObj.Woil_Status__c == 'Approval Pending' && outletOldMap.get(outletNewObj.Id).Woil_Status__c != 'Approval Pending')
               ||(outletNewObj.Woil_Status__c == 'Rejected' && outletOldMap.get(outletNewObj.Id).Woil_Status__c != 'Rejected')
               ||(outletNewObj.Woil_Status__c == 'Active' && outletOldMap.get(outletNewObj.Id).Woil_Status__c != 'Active')){
                   buyerSellerMappingObj =   [select Id, 
                                              Outlet__r.Woil_Status__c,
                                              Status__c,
                                              Parent__r.Woil_Status__c
                                              from  WOIL_Buyer_Seller_Mapping__c 
                                              where Outlet__r.Id = :outletNewObj.id];
                   System.debug('Fetched buyerSellerMappingObj Woil_OutletTriggerHandler=======>'+buyerSellerMappingObj);
                   //Change Buyerseller mapping status
                   buyerSellerMappingObj.Status__c = outletNewObj.Woil_Status__c;
                   
                   parentAccount = [Select Woil_Status__c from Account where id = : buyerSellerMappingObj.Parent__r.id];



                   //Change Parent customer status 
                   System.debug('buyerSellerMappingObj.Parent__r.Woil_Status__c===> '+buyerSellerMappingObj.Parent__r.Woil_Status__c);
                   if(parentAccount.Woil_Status__c != 'Active'){
                       parentAccount.Woil_Status__c = outletNewObj.Woil_Status__c;
                   }
                   System.debug('Ready for update buyerSellerMappingObj Woil_OutletTriggerHandler=======>'+buyerSellerMappingObj);
               }
        }
        if(buyerSellerMappingObj != null)
            update buyerSellerMappingObj;
        if(parentAccount != null)
        update parentAccount;
    }
}
    
    
    
    
    
/*
 *  * 
 * Optimised code Written by Gaurav not tested need to test first
 * 
 *     
public static void afterUpdate(Map<id,Outlet_Master__c> outletNewMap, List<Outlet_Master__c> outletOldList){
        for(Outlet_Master__c outletOldObj : outletOldList){
            if((outletNewMap.get(outletOldObj.Id).Woil_Status__c == 'Approval Pending' && outletOldObj.Woil_Status__c != 'Approval Pending')
               ||(outletNewMap.get(outletOldObj.Id).Woil_Status__c == 'Rejected' && outletOldObj.Woil_Status__c != 'Rejected')
               ||(outletNewMap.get(outletOldObj.Id).Woil_Status__c == 'Active' && outletOldObj.Woil_Status__c != 'Active')){    
                   List <WOIL_Buyer_Seller_Mapping__c> buyerSellerMappingObjList = [select Id, Outlet__r.Woil_Status__c,
                                                                                    Status__c, Parent__r.Woil_Status__c
                                                                                    from  WOIL_Buyer_Seller_Mapping__c 
                                                                                    where Outlet__r.Id IN : outletNewMap.keySet()];
                   
                   List<Account> parentAccountList = new List<Account>{};
                       
                       for(WOIL_Buyer_Seller_Mapping__c buyerSellerObj: buyerSellerMappingObjList){
                           Account parentAccObj = new Account(id = buyerSellerObj.Outlet__c, 
                                                              Woil_Status__c = buyerSellerObj.Outlet__r.Woil_Status__c);
                           parentAccountList.add(parentAccObj);
                           buyerSellerObj.Status__c = buyerSellerObj.Outlet__r.Woil_Status__c;
                       }
                   
                   update buyerSellerMappingObjList;
                   update parentAccountList;
               }
        }
    }

*/