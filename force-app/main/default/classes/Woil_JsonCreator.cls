public class Woil_JsonCreator {
    public SO_Transaction SO_Transaction {get;set;} 
    public Woil_JsonCreator(SO_Transaction SO_Transaction){
        this.SO_Transaction = SO_Transaction;
    }
    
    public class Header {
        public String Purchase_Order_ID {get;set;} 
        public String Purchase_Order_Number {get;set;} 
        public String Order_Type {get;set;} 
        public String Sales_Org {get;set;} 
        public String Distribution_Channel {get;set;} 
        public String Division {get;set;} 
        public String Purchase_Order_date {get;set;} 
        public String Currencys {get;set;} // in json: Currency
        public String Sold_to_Party {get;set;} 
        public String Ship_to_Party {get;set;} 
        public String Bill_to_Party {get;set;} 
        public String Payer {get;set;} 
        public String Expiry_date {get;set;} 
        public String Revised_expiry_date {get;set;}
        Public Header(String Purchase_Order_ID, String purchase_Order_Number, String order_Type, 
                      String distribution_Channel, String division, String purchase_Order_date, 
                      String sold_to_Party, String ship_to_Party, String bill_to_Party, 
                      String payer, String expiry_date, String revised_expiry_date){
                         this.Purchase_Order_ID = Purchase_Order_ID;
                         this.Purchase_Order_Number = purchase_Order_Number; 
                         this.Order_Type = order_Type; 
                         this.Sales_Org = 'IN01'; 
                         this.Distribution_Channel = distribution_Channel; 
                         this.Division = division; 
                         this.Purchase_Order_date = purchase_Order_date; 
                         this.Currencys = '';
                         this.Sold_to_Party = sold_to_Party; 
                         this.Ship_to_Party = ship_to_Party; 
                         this.Bill_to_Party = bill_to_Party; 
                         this.Payer = payer; 
                         this.Expiry_date = expiry_date; 
                         this.Revised_expiry_date = revised_expiry_date;                        
                     }
    }
    
    
    public class ItemDetails {
        public String Material_Code {get;set;} 
        public String Sales_Unit {get;set;} 
        public String Mat_Pricing_Group {get;set;} 
        public String Storage_Location {get;set;} 
        public String Plant {get;set;} 
        public String Delivery_Date {get;set;} 
        public String Quantity {get;set;}
        public String Line_Item_no {get;set;}
        public ItemDetails(String Line_Item_no, String material_Code, String plant, String quantity){
            				   this.Line_Item_no = Line_Item_no;
                               this.Material_Code = material_Code; 
                               this.Sales_Unit = ''; 
                               this.Mat_Pricing_Group = 'NR'; 
                               this.Storage_Location = ''; 
                               this.Plant = plant; 
                               this.Delivery_Date = ''; 
                               this.Quantity = quantity;                          
                           }
    }
    
    public class SO_Transaction {
        public Header Header {get;set;} 
        public List<ItemDetails> ItemDetails {get;set;}
        public SO_Transaction(Header headerObj, List<ItemDetails> itemDetailsList){
            this.Header = headerObj;
            this.ItemDetails = itemDetailsList;
        }
    }
}