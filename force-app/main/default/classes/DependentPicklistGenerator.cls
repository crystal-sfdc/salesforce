public with sharing class DependentPicklistGenerator {
    @AuraEnabled(cacheable = true)
    public static string fetchRecordTypeInfoValues(){

        system.debug('Class is called');
       /* String userName = UserInfo.getUserName();
        User activeUser = [Select id,Email,ContactId From User where Username = : userName limit 1];
        String userEmail = activeUser.Email;
        system.debug('ActiveUser'+activeUser);*/

        id userId = UserInfo.getUserId();
        User u = [select id, contactId from User where id = : userId];
        id getContactId = u.contactId;
        system.debug('getContactId' + getContactId);
        
        Id accID = [select Id,AccountID from contact where id =: getContactId].AccountID;
        system.debug('accID'+accID);

        

        Set<String> divisionList = new Set<String>();
        Set<RecordTypeInfo> recordTypeNameList = new Set<RecordTypeInfo>();
        //system.debug('Email id is: '+ userEmail);
        if(String.isNotBlank(accID)){

            /*Account acc = [select Id,Name from Account where OwnerID =:activeUser.id limit 1];
            system.debug('Acc is:'+ acc);*/
        
        

        List<Woil_Account_Plant__c> accountPlan = [select id,Woil_Division__c from Woil_Account_Plant__c where Woil_Trade_Partner__c =:accID];
       
        Schema.DescribeSObjectResult R = Woil_Case_Escalation__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();   
        if(!accountPlan.isEmpty()){
            for(Woil_Account_Plant__c accPlanObj: accountPlan){
                if(accPlanObj.Woil_Division__c =='AG'){
                    divisionList.add('Finish Good');
                    for( Schema.RecordTypeInfo recordType : RT )
                    {
                        if(recordType.isAvailable())
                            { 
                                if(recordType.Name == 'Finish Good') {
                                    recordTypeNameList.add(recordType);
                                }
                            }
                    }    
                }else if(accPlanObj.Woil_Division__c =='AS'){  
                    divisionList.add('Spares Part');
                    for( Schema.RecordTypeInfo recordType : RT )
                    {
                        if(recordType.isAvailable())
                            { 
                                if(recordType.Name == 'Spares Part') {
                                    recordTypeNameList.add(recordType);
                                }
                            }
                    }
                        
                }
                //divisionList.add(accPlanObj.Woil_Division__c);
            }
        }
        system.debug('division accplan is: '+recordTypeNameList);
    }

       
        /*
        list<RecordTypeInfo> recordTypeNameList = new list<RecordTypeInfo>();
        Schema.DescribeSObjectResult R = Woil_Case_Escalation__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        for( Schema.RecordTypeInfo recordType : RT )
        {
        if(recordType.isAvailable())
         { 
         
        if(recordType.Name!='Master') {
            recordTypeNameList.add(recordType);
         
            }
          }
        }
        System.debug('RecordType'+recordTypeNameList);
        System.debug('Json Recordtype'+JSON.serialize(recordTypeNameList));*/
        return JSON.serialize(recordTypeNameList);
        //return divisionList;
    }

    

    @AuraEnabled(cacheable=true)
    public static List<Case> getCaseList() {
        return [SELECT Id, CaseNumber, Status, woil_category__c,
        woil_sub_category__c 
        FROM Case];
    }
    
}