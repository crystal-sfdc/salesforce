public class Woil_InvoiceLineItemTriggerHandler {
    public static void isBeforeInsert(List<Woil_Invoice_Line_Item__c> newInvoiceLineItem){
        
        //only for API-------------------------------------------------    
        Set<String> invoiceId = new  Set<String>();
        Set<String> ProductId = new  Set<String>();
        for(Woil_Invoice_Line_Item__c inLineItem : newInvoiceLineItem){
            if(inLineItem.Woil_SAP_Invoice_Id__c != null && inLineItem.Woil_Material_Code__c != null){
                invoiceId.add(inLineItem.Woil_SAP_Invoice_Id__c);
                ProductId.add(inLineItem.Woil_Material_Code__c);   
            }
        }
        
        if(invoiceId.size() > 0 && ProductId.size() > 0){
            System.debug(invoiceId.size());
            System.debug(ProductId.size());
            
            Map<String,Invoice__c> invoiceInfoMap =new Map<String,Invoice__c>();
            Map<String,Product2> productInfoMap = new Map<String,Product2>();
            List<Invoice__c> invoiceList = [ select id ,Woil_SAP_Invoice_ID__c from Invoice__c where Woil_SAP_Invoice_ID__c IN : invoiceId];
            List<Product2> productList=[select id ,Name from Product2 where Name IN : ProductId ];       
            
            for(Invoice__c invo : invoiceList ){
                invoiceInfoMap.put(invo.Woil_SAP_Invoice_ID__c,invo);
            }
            for(Product2 pro : productList){
                productInfoMap.put(pro.Name,pro);
                
            }
            for(Woil_Invoice_Line_Item__c inoLinefinal : newInvoiceLineItem )
            {
                inoLinefinal.Woil_Invoice__c = invoiceInfoMap.get(inoLinefinal.Woil_SAP_Invoice_Id__c).Id;
                inoLinefinal.Woil_Product__c = productInfoMap.get(inoLinefinal.Woil_Material_Code__c).Id;
                
            }        
        }
    }
}