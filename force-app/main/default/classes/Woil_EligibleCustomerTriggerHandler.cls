public class Woil_EligibleCustomerTriggerHandler {

    public static void shareSchemeWithCustomers(Map<Id,Woil_Eligible_Customer__c> eligibleCustomerNewMap,Map<Id,Woil_Eligible_Customer__c> eligibleCustomerOldMap){
        
        Map<Id,Woil_Eligible_Customer__c> eligibleCustomerProcessedMap = new Map<Id,Woil_Eligible_Customer__c>();
        Map<Id,Set<Id>> dsoUserIdMap = new Map<Id,Set<Id>>();
        
        // insert event
        if((trigger.isInsert)) {
            
            Set<Id> customerIdSet = new Set<Id>();
            
            for(Woil_Eligible_Customer__c eligibleCustomer : eligibleCustomerNewMap.values()) {
                if(eligibleCustomer.Woil_Customer__c != NULL && eligibleCustomer.Woil_Scheme__c != NULL){
                    eligibleCustomerProcessedMap.put(eligibleCustomer.Id, eligibleCustomer);
                    customerIdSet.add(eligibleCustomer.Woil_Customer__c);
                }
            }
            
            dsoUserIdMap = prepareDSOUserMap(customerIdSet);
            createSharingRecords(eligibleCustomerProcessedMap, dsoUserIdMap);
        }

        // update event
        else if(trigger.isUpdate){
            
            Set<Id> oldSchemeIdSet = new Set<Id>();
            Set<Id> oldCustomerIdSet = new Set<Id>();
            Set<Id> newCustomerIdSet = new Set<Id>();
            Set<Id> dsoUserIdSet = new Set<Id>();
            
            for(Woil_Eligible_Customer__c eligibleCustomer : eligibleCustomerNewMap.values()) {
                if((eligibleCustomer.Woil_Customer__c != NULL && eligibleCustomer.Woil_Scheme__c != NULL) && (eligibleCustomer.Woil_Customer__c != eligibleCustomerOldMap.get(eligibleCustomer.Id).Woil_Customer__c || eligibleCustomer.Woil_Scheme__c != eligibleCustomerOldMap.get(eligibleCustomer.Id).Woil_Scheme__c)){
                    
                    oldSchemeIdSet.add(eligibleCustomerOldMap.get(eligibleCustomer.Id).Woil_Scheme__c);
                    oldCustomerIdSet.add(eligibleCustomerOldMap.get(eligibleCustomer.Id).Woil_Customer__c);
                    
                    eligibleCustomerProcessedMap.put(eligibleCustomer.Id, eligibleCustomer);
                    newCustomerIdSet.add(eligibleCustomer.Woil_Customer__c);
                }
            }

            for(User userRec : [SELECT Id, AccountId FROM User WHERE IsActive = true AND AccountId IN :oldCustomerIdSet]){
                dsoUserIdSet.add(userRec.Id);
            }

            deleteSharingRecords(oldSchemeIdSet, dsoUserIdSet);

            dsoUserIdMap = prepareDSOUserMap(newCustomerIdSet);
            createSharingRecords(eligibleCustomerProcessedMap, dsoUserIdMap);
        }
        // delete event
        else if(trigger.isDelete){
            
            Set<Id> schemeIdSet = new Set<Id>();
            Set<Id> dsoUserIdSet = new Set<Id>();
            Set<Id> customerIdSet = new Set<Id>();
            
            for(Woil_Eligible_Customer__c eligibleCustomer : eligibleCustomerOldMap.values()) {
                if(eligibleCustomer.Woil_Customer__c != NULL && eligibleCustomer.Woil_Scheme__c != NULL){
                    schemeIdSet.add(eligibleCustomer.Woil_Scheme__c);
                    customerIdSet.add(eligibleCustomer.Woil_Customer__c);
                }
            }

            for(User userRec : [SELECT Id, AccountId FROM User WHERE IsActive = true AND AccountId IN :customerIdSet]){
                dsoUserIdSet.add(userRec.Id);
            }

            deleteSharingRecords(schemeIdSet, dsoUserIdSet);
        }
    }

    public static Map<Id,Set<Id>> prepareDSOUserMap(Set<Id> customerIdSet){

        Map<Id,Set<Id>> dsoUserIdMap = new Map<Id,Set<Id>>();

        for(User userRec : [SELECT Id, AccountId FROM User WHERE IsActive = true AND AccountId IN :customerIdSet]){
                
            if(!dsoUserIdMap.containsKey(userRec.AccountId)){
                dsoUserIdMap.put(userRec.AccountId, new Set<Id>());
            }
            Set<Id> tempUserIdSet = dsoUserIdMap.get(userRec.AccountId);
            tempUserIdSet.add(userRec.Id);
            dsoUserIdMap.put(userRec.AccountId,tempUserIdSet);
        }

        return dsoUserIdMap;
    }
    
    public static void createSharingRecords(Map<Id,Woil_Eligible_Customer__c> eligibleCustomerProcessedMap, Map<Id,Set<Id>> dsoUserIdMap){

        List<Woil_Scheme_Master__Share> schemeShareRecList = new List<Woil_Scheme_Master__Share>();

        for(Woil_Eligible_Customer__c eligibleCustomer : eligibleCustomerProcessedMap.values()){
            if(dsoUserIdMap.containsKey(eligibleCustomer.Woil_Customer__c)){
                for(Id userId : dsoUserIdMap.get(eligibleCustomer.Woil_Customer__c)){
                    Woil_Scheme_Master__Share schemeShareRec = new Woil_Scheme_Master__Share();
                    schemeShareRec.ParentId = eligibleCustomer.Woil_Scheme__c;
                    schemeShareRec.AccessLevel = 'Read';
                    schemeShareRec.UserOrGroupId = userId;
                    schemeShareRecList.add(schemeShareRec);
                }
            }
        }

        insert schemeShareRecList;
    }

    public static void deleteSharingRecords(Set<Id> schemeIdSet, Set<Id> dsoUserIdSet){

        List<Woil_Scheme_Master__Share> obsoleteSchemeShareList = [SELECT Id FROM Woil_Scheme_Master__Share WHERE ParentId IN : schemeIdSet AND UserOrGroupId IN : dsoUserIdSet AND rowcause != 'Owner'];
        delete obsoleteSchemeShareList;
    }
}