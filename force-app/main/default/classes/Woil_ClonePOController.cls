public without sharing class Woil_ClonePOController {
    @AuraEnabled
    public static String checkStatusController(String recordId){

        if(!Woil_UtilityClass.isLogCommunityUser()){
            return 'Not a Community user';
        }

        return 'Community User';
    }

    @AuraEnabled
    public static String clonePOController(String recordId){
        try{
        List<OrderItem> orderItemList = new List<OrderItem>();

        List<Woil_Default_Value_Configuration__mdt> poDateList = [SELECT Id, Woil_Default_Value__c, Label 
                                                                    FROM Woil_Default_Value_Configuration__mdt 
                                                                    WHERE DeveloperName = 'Po_Expiry_Date'];

        Order ord = [SELECT Id, AccountId, Pricebook2Id, EffectiveDate, Woil_Supplier_Name__c, Woil_Supplier_Code__c, Woil_ShipToAddress__c, Woil_Division__c, Woil_Sold_To_Code__c, Woil_Bill_to_Code__c, PoDate, Status, Type, Woil_SAP_Order_Type__c,
                    (SELECT OrderId, Product2Id, ListPrice, Quantity, PricebookEntryId, UnitPrice, Description FROM OrderItems) FROM Order WHERE Id = :  recordId];

        Order clonedOrd = ord.clone(false, true, false, false);
        clonedOrd.Status = 'Draft';
        clonedOrd.PoDate = Date.today().addDays(integer.valueof(poDateList[0].Woil_Default_Value__c));
        clonedOrd.EffectiveDate = Date.today();
        insert clonedOrd;        
        

        for(OrderItem oItem : ord.OrderItems){
            OrderItem clonedOrderItem = oItem.clone(false, true, false, false);
            clonedOrderItem.OrderId = clonedOrd.Id;
            orderItemList.add(clonedOrderItem);
        }

        if(orderItemList.size() > 0){
            insert orderItemList;
        }
        

        return 'Success#'+clonedOrd.Id;
        }catch(Exception ex){
            return 'error#'+ex.getMessage();
        }
    }
}