public class Woil_PurchaseReturnController {
    
    @AuraEnabled
    public static void validEntrycheck(String recordId) {
        System.debug('RecordID of Page=======>'+recordId);
        Woil_SRN__c srnObj = [SELECT Id, Woil_On_Spot_Return__c, Woil_Status__c,Woil_Division__c
                              FROM Woil_SRN__c where id = : recordId];
        //Valid entry check for approval
        if(srnObj.Woil_Status__c == 'Approval Pending' || 
           srnObj.Woil_Status__c == 'Rejected' || 
           srnObj.Woil_Status__c ==  'Active')
            throw new AuraHandledException(srnObj.Woil_Status__c);
        
        // Check IsOnSpot--> if yes-->do Auto aproval and postreturn.
        // else check serialization is must before approval, if false give error
        
        if(srnObj.Woil_On_Spot_Return__c){
            srnObj.Woil_Status__c = 'Active';
            update srnObj;
            Approval.lock(recordId);
            System.debug('srnObj after AutoAproval===>'+srnObj);
            //Add the logic of Auto PostReturn
            throw new AuraHandledException('Case is of OnSpot, AutoApproval and AutoPostReturn done');
        }
        else{
            Boolean isSerialized = false;
            
            List<Woil_SRN_Line_Item__c> srnLineItemObjList = [SELECT Id, Woil_SRN__c, Woil_SKU_Code__c,
                                                              Woil_Warehouse_Location__c,Woil_SRN__r.Woil_Account__c,
                                                              Woil_Product__r.Woil_Is_Serializable__c,
                                                              Woil_Ordered_Quantity__c 
                                                              FROM Woil_SRN_Line_Item__c 
                                                              Where Woil_SRN__c  = : recordID
                                                              AND Woil_SRN__r.Woil_On_Spot_Return__c = false];
            if(srnLineItemObjList.size() <= 0)
                throw new AuraHandledException('Please add the product first');
            else{
                for(Woil_SRN_Line_Item__c obj :srnLineItemObjList){
                    if(obj.Woil_Product__r.Woil_Is_Serializable__c == true){
                        isSerialized = true;
                        break;
                    }
                }
            }
            
            List<String> SKUCodes=new List<String>();
            String whLocation,custCode;
            for(Woil_SRN_Line_Item__c srnLineItemObj : srnLineItemObjList){           
                SKUCodes.add(srnLineItemObj.Woil_SKU_Code__c);
                whLocation=srnLineItemObjList[0].Woil_Warehouse_Location__c;
                custCode=srnLineItemObjList[0].Woil_SRN__r.Woil_Account__c;
            }
            System.debug('######Select Woil_Warehouse_Location__c, Woil_Purchase_Return__c, Woil_Product__r.Name from Woil_Serial_Number_Scan__c WHERE Woil_Product__r.Name = '+
                        SKUCodes+' And Woil_Warehouse_Location__c = '+ whLocation +
                         'AND Woil_Purchase_Return__c = '+recordId+ ' AND Woil_Customer__c= '+custCode);
            
            Integer SerialNumberScanCount =  [Select count() 
                                              from Woil_Serial_Number_Scan__c 
                                              WHERE Woil_Product__r.Name IN: SKUCodes AND 
                                              Woil_Warehouse_Location__r.Name =: whLocation AND 
                                              Woil_Purchase_Return__c=:recordId AND 
                                              Woil_Customer__c=:custCode];
            
            if(SerialNumberScanCount == 0 && isSerialized == true)
                throw new AuraHandledException('Please serialized the product first');
            
        }
        
        //Valid return check
        String status = Woil_UtilityClass.checkStockAvailability(recordId);
        system.debug('Woil_UtilityClass.checkStockAvailability()===> '+status);
        if(status ==  'notBelong')
            throw new AuraHandledException('notBelong');
        else if(status.contains('notAvailable'))
            throw new AuraHandledException('notAvailable');
    }
    
    
    @AuraEnabled
    public static List<User> getApprovers() {
        Boolean IsCommunity = false;
        ID approver1 ;
        ID approver2 ;
        List<Object> approvers = new List<Object>();
        IsCommunity = Woil_UtilityClass.isLogCommunityUser();
        System.debug('IsCommunity======> '+IsCommunity);
        List<User> UrManager;
        if(IsCommunity){
            // TP--run all 3 step
            ID accID = Woil_UtilityClass.getAccountIdFromContactID(Woil_UtilityClass.getUserContactID(UserInfo.getUserId()));
            System.debug('UserInfo.getUserId=============>'+UserInfo.getUserId());
            User userRecord = [SELECT ManagerId, Woil_Support_User__c, id FROM User where id = : UserInfo.getUserId()] ;
            List<AccountTeamMember> listofAccountTeamMember = [SELECT User.Name, User.ManagerId, UserId 
                                                               FROM AccountTeamMember  
                                                               where AccountId = : accID];
            set<String> Userinfo=new set<String>();
            for(AccountTeamMember AtM: listofAccountTeamMember){
                Userinfo.add(AtM.UserId);
            }
            UrManager = [SELECT Name, id, ManagerId, Manager.Name FROM User where id IN : Userinfo ];
            System.debug(UrManager);
            return UrManager;
            
        }
        return UrManager;
    }
    
    @AuraEnabled
    public static String submitApprovalProcess(String recordId, String firstApprover, String secondApprover) {
        Savepoint savePoint = Database.setSavepoint();
        System.debug(recordId);
        System.debug(firstApprover);
        System.debug(secondApprover);
        try{
            //System.debug([select count() from ProcessInstance where targetobjectid=:recordId]);
            if(recordId !=null && firstApprover !=null && secondApprover !=null){
                Woil_SRN__c srnObj = new Woil_SRN__c(id = recordId, 
                                                     Woil_First_Approver__c = firstApprover, 
                                                     Woil_Second_Approver__c = secondApprover);
                update srnObj;
            }
            Woil_SRN__c purchaseReturnObj = [SELECT Id, CreatedById FROM Woil_SRN__c where id = : recordId];
            System.debug('purchaseReturnObj.CreatedById===> '+purchaseReturnObj.CreatedById);
            //send Approvel request-------------
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setComments('Submitting request for approve Purchase Return SRN ');
            approvalRequest.setObjectId(recordId); 
            approvalRequest.setSubmitterId(purchaseReturnObj.CreatedById);
            approvalRequest.setNextApproverIds(new Id[] {firstApprover});//1st approver
            Approval.ProcessResult result = Approval.process(approvalRequest);
            System.debug(result.isSuccess());
            System.debug(result.getErrors());
        }
        catch(Exception e){
            System.debug('The following exception has occurred=====> ' + e.getMessage());
            
            if(Approval.isLocked(recordId))
                Approval.unlock(recordId);
            Database.RollBack(savePoint);
            return  e.getMessage();
            
        }
        return 'success';
    }
    
}