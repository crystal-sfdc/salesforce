global class Woil_SellOutPortal {
    
    @AuraEnabled()
    public static List <customValueWrapper> fetchMaterialCodeValues(){
        List <customValueWrapper> customObjWrapper = new List <customValueWrapper>();
        try{
            
            system.debug('Class is called');
            
            id userId = UserInfo.getUserId();
            system.debug('userId'+userId);
            User u = [select id, contactId from User where id = : userId];
            id getContactId = u.contactId;
            system.debug('getContactId' + getContactId);
            
            Id accID = [select Id,AccountID from contact where id =: getContactId].AccountID;
            system.debug('accID'+accID);
            Map<Id,Woil_Partner_Product_Mapping__c> MapOfserialNumberTransaction = new  Map<Id,Woil_Partner_Product_Mapping__c>([Select id,Woil_Customer_Name__c,Woil_Product__r.Name from Woil_Partner_Product_Mapping__c where Woil_Customer_Name__c =:accID]);
            Map<String,String> mapOfMaterialCode = new Map<String,String>();
            
            System.debug('serialNumberTransactionList'+MapOfserialNumberTransaction);
            if(!MapOfserialNumberTransaction.keySet().isEmpty()){
                for(Id serialTransObj : MapOfserialNumberTransaction.keySet()){
                    mapOfMaterialCode.put(MapOfserialNumberTransaction.get(serialTransObj).Woil_Product__r.Name,MapOfserialNumberTransaction.get(serialTransObj).Woil_Product__r.Name);
                    
                }
                for(String str: mapOfMaterialCode.keySet()){
                    customValueWrapper selectOptionValueWrapper = new customValueWrapper();
                    selectOptionValueWrapper.custFldlabel = str;
                    selectOptionValueWrapper.custFldvalue = str;
                    customObjWrapper.add(selectOptionValueWrapper);
                }
                
                System.debug('customObjWrapper'+customObjWrapper);    
            }
            
            
            
        }catch(Exception e){
            System.debug('The following error has occurred.'); 
            System.debug('Exception caught: ' + e.getMessage()); 
        }
        
        return customObjWrapper;
    }
    
    @AuraEnabled()
    public static List<dataValueWrapper> createDataTable(String materialCode, String serialNumb,String CustName , String Address, String Phone){
        List<dataValueWrapper> dataList = new List<dataValueWrapper>();
        try{
            Integer i = 0; 
            System.debug('materialCode'+materialCode);
            System.debug('serialNumb'+serialNumb);
            String inpString= materialCode + ','+ serialNumb;
            Map<String,Decimal> mapOfSerialNumberTrans = new Map<String,Decimal>();
            
            id userId = UserInfo.getUserId();
            User u = [select id, contactId from User where id = : userId];
            id getContactId = u.contactId;
            Id accID = [select Id,AccountID from contact where id =: getContactId].AccountID;
            Account acc = [select Id,Name,Woil_Account_Number__c from Account where id =:accID];    
            
            List<Woil_Serial_Number_Transaction__c> serialTransList = [select id,Woil_Product__r.Name,Woil_Product__r.Description, Woil_Serial_Number__c,Woil_Quantity__c,Woil_Sales_Motion__c,Woil_Serial_Transaction_Type__c from Woil_Serial_Number_Transaction__c where Woil_Serial_Number__c =:serialNumb and Woil_Product__r.Name =:materialCode and Woil_Customer__r.Woil_Account_Number__c =:acc.Woil_Account_Number__c];
            System.debug('serialTransList'+serialTransList);
            for(Woil_Serial_Number_Transaction__c serialObj: serialTransList){
                if(serialObj.Woil_Sales_Motion__c == 'SellOut'){
                    System.debug('in sellout');
                    dataValueWrapper objData1 = new dataValueWrapper();
                    objData1.errorString = 'Serial number is out of stock';
                    dataList.add(objData1);
                    break; 
                }
                else 
                {
                    if(serialObj.Woil_Serial_Transaction_Type__c == 'Purchase'){
                        System.debug('in purchase');
                        if(serialObj.Woil_Quantity__c > 0){
                            dataValueWrapper objData = new dataValueWrapper();
                            objData.materialCode = serialObj.Woil_Product__r.Name;
                            objData.materialDesc = serialObj.Woil_Product__r.Description;
                            objData.serialNumb = serialObj.Woil_Serial_Number__c;
                            objData.sellOut = true;
                            //new change-------------------------------------
                                                        objData.CustomerName = CustName;

                                                        objData.CustomerAddress = Address;

                                                        objData.Phone = Phone;
                            //new change-------------------------------------
                        
                            objData.sellDate = System.today().format();
                            dataList.add(objData);
                            break;
                        }
                        
                    }else{
                        System.debug('in purchase if');
                        dataValueWrapper objData1 = new dataValueWrapper();
                        objData1.errorString = 'Serial number is out of stock';
                        dataList.add(objData1);
                        break;
                    }
                }
            }
            
            System.debug('mapOfSerialNumberTrans'+mapOfSerialNumberTrans);
            System.debug('dataList'+dataList);
            
        }catch(Exception e){
            System.debug('The following error has occurred.'); 
            System.debug('Exception caught: ' + e.getMessage()); 
        }
        return dataList;
    }






    
    @AuraEnabled
    public static void addSellOutDetail(String selectedRows){
        try {
            
            List<String> materialCodeStrList = new List<String>();
            List<String> serialNumberStrList = new List<String>(); 
            List<Woil_Serial_Bank__c> updateSerialBankList = new List<Woil_Serial_Bank__c>();
            List<Woil_Serial_Number_Transaction__c> updateSerialTransList = new List<Woil_Serial_Number_Transaction__c>();
            Map<String,dataValueWrapper> DataWrapperMap = new  Map<String,dataValueWrapper>();
            List<dataValueWrapper> wrapResp = (List<dataValueWrapper>)JSON.deserialize(selectedRows,List<dataValueWrapper>.class);
            if(!wrapResp.isEmpty()){
                for(dataValueWrapper wrep :wrapResp){
                    materialCodeStrList.add(wrep.materialCode);
                    serialNumberStrList.add(wrep.serialNumb);
                    DataWrapperMap.put(wrep.serialNumb,wrep);
                }  
                //add new ------------------------------------------------
                /*
					Woi_Customer_Name__c
					Woil_Customer_Address__c
					Woil_Phone__c
                 */
                List<Woil_Serial_Bank__c> serialBankList = [select Id,Woi_Customer_Name__c,Woil_Customer_Address__c,Woil_Phone__c, Woil_SellOut__c,Woil_SellOut_Date__c,Woil_Product__r.Name,Woil_Serial_Number__c from Woil_Serial_Bank__c where Woil_Product__r.Name IN: materialCodeStrList and Woil_Serial_Number__c IN: serialNumberStrList]; 
                System.debug('serialBankList ' +serialBankList);
             
                /*
                 *     @auraEnabled public string CustomerName {get;set;}
        @auraEnabled public string CustomerAddress {get;set;}
        @auraEnabled public string Phone {get;set;}
                 */
                
                
                if(!serialBankList.isEmpty()){
                    for(Woil_Serial_Bank__c serBank : serialBankList){
                        serBank.Woil_SellOut__c = true;
                        /*
                         * Add neww
                         */ 
                        serBank.Woi_Customer_Name__c=DataWrapperMap.get(serBank.Woil_Serial_Number__c).CustomerName;
                        serBank.Woil_Customer_Address__c=DataWrapperMap.get(serBank.Woil_Serial_Number__c).CustomerAddress;
                        serBank.Woil_Phone__c=DataWrapperMap.get(serBank.Woil_Serial_Number__c).Phone;
                        serBank.Woil_SellOut_Date__c = System.today();
                       updateSerialBankList.add(serBank);
                    }
                    System.debug('Updated Serial Bank '+updateSerialBankList);
                }
                
                id userId = UserInfo.getUserId();
                
                User u = [select id, contactId from User where id = : userId];
                id getContactId = u.contactId;
                Id accID = [select Id,AccountID from contact where id =: getContactId].AccountID;
                Account acc = [select Id,Name,Woil_Account_Number__c from Account where id =:accID];
                
                
                List<Woil_Serial_Number_Transaction__c> serialTransList = [Select id,Woil_Product__r.Name,Woil_Product__c,Woil_Serial_Number__c,Woil_Customer__c,Woil_Warehouse_Location__c,Woil_Invoice__c,Woil_Order__c,Woil_Quantity__c,Woil_Serial_Transaction_Type__c,Woil_Sales_Motion__c,Woil_Transaction_Date__c,Woil_Current_Customer_Code__c from Woil_Serial_Number_Transaction__c where Woil_Product__r.Name IN: materialCodeStrList and Woil_Serial_Number__c IN:serialNumberStrList and Woil_Quantity__c>0];
                System.debug('serialTransList'+serialTransList);
                if(!serialTransList.isEmpty()){
                    for(Woil_Serial_Number_Transaction__c serObj : serialTransList){
                        if(serObj.Woil_Quantity__c>0){
                            Woil_Serial_Number_Transaction__c obj =  new Woil_Serial_Number_Transaction__c();
                            obj.Woil_Product__c = serObj.Woil_Product__c;
                            obj.Woil_Serial_Number__c = serObj.Woil_Serial_Number__c;
                            obj.Woil_Current_Customer_Code__c = acc.Woil_Account_Number__c;
                            obj.Woil_Customer__c = acc.Id;
                            obj.Woil_Warehouse_Location__c = serObj.Woil_Warehouse_Location__c;
                            obj.Woil_Invoice__c = serObj.Woil_Invoice__c;
                            obj.Woil_Order__c = serObj.Woil_Order__c;
                            obj.Woil_Quantity__c = -1;
                            obj.Woil_Serial_Transaction_Type__c = 'SellOut';
                            obj.Woil_Sales_Motion__c = 'SellOut';
                            obj.Woil_Transaction_Date__c = System.today();
                            
                            updateSerialTransList.add(obj);
                        }                    
                    }
                    System.debug('updateSerialTransList'+updateSerialTransList);
                }
                if(!updateSerialTransList.isEmpty()){
                    Database.insert(updateSerialTransList);
                }
                if(!updateSerialBankList.isEmpty()){
                    Database.update(updateSerialBankList);
                }
            }
            
        } 
        catch (Exception e) {
            System.debug('The following error has occurred.'); 
            System.debug('Exception caught: ' + e.getMessage());
        }
    }
    
    
    @AuraEnabled
    public static List<dataValueWrapper> uploadFle(String File, String FileName, String FileType, Integer FileSize ){
        Blob payloadBlob = EncodingUtil.base64Decode(File);
        String payload = payloadBlob.toString();
        system.debug(payload);
        list<String> csvRowData = new List<String>();
        List<String> materialCodeList = new List<String>();
        List<String> serialNumbList = new List<String>();
        List<String> warehouseList = new List<String>();
        
        //Add new-------------------------------------------------
            Map<String,dataValueWrapper> DataWrapperMap = new  Map<String,dataValueWrapper>();


            
                /*
                 *     @auraEnabled public string CustomerName {get;set;}
        @auraEnabled public string CustomerAddress {get;set;}
        @auraEnabled public string Phone {get;set;}
                 */
        
        
        List<dataValueWrapper> dataList = new List<dataValueWrapper>();
        List<String> str = payload.split('\r\n');
        System.debug('Str'+str);
        try{
            
            for(Integer i = 1; i < str.size(); i++){
                csvRowData = str[i].split(',');
                dataValueWrapper datawrap=new dataValueWrapper();
                materialCodeList.add(csvRowData[0]);
                serialNumbList.add(csvRowData[1]);
                warehouseList.add(csvRowData[2]);
                
                //Add new---------------------------------------------------------------------
                datawrap.CustomerName=csvRowData[3];
                datawrap.CustomerAddress=csvRowData[4];
                datawrap.Phone=csvRowData[5];
                DataWrapperMap.put(csvRowData[1],datawrap);
                
            }
            
            System.debug('materialCodeList====> '+materialCodeList);
            System.debug('serialNumbList====> '+serialNumbList);
            System.debug('warehouseList====> '+warehouseList);
            
            
            List<Woil_Serial_Number_Transaction__c> serialTransList = [select id,Woil_Product__r.Name,Woil_Product__r.Description, Woil_Serial_Number__c,Woil_Quantity__c,Woil_Sales_Motion__c,Woil_Serial_Transaction_Type__c,Woil_Current_Customer_Code__c from Woil_Serial_Number_Transaction__c where Woil_Serial_Number__c IN:serialNumbList and Woil_Product__r.Name IN:materialCodeList ];
            System.debug('warehouseList====> '+serialTransList);
            Boolean check = false;
            
            for(Woil_Serial_Number_Transaction__c serialObj: serialTransList){
                Boolean checkPurchase = false;
                Boolean checkSellout = false;
                Boolean checkBool = false;
                Boolean addRec = false;
                if(serialObj.Woil_Sales_Motion__c == 'SellOut'){
                   
                    System.debug('in sellout');
                    dataValueWrapper objData1 = new dataValueWrapper();
                    objData1.materialCode = serialObj.Woil_Product__r.Name;
                    objData1.materialDesc = serialObj.Woil_Product__r.Description;
                    objData1.serialNumb = serialObj.Woil_Serial_Number__c;
                    objData1.warehouseLocation = serialObj.Woil_Current_Customer_Code__c;
                    //Add new--------------------------------------
                      objData1.CustomerName=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).CustomerName;
                objData1.CustomerAddress=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).CustomerAddress;
                objData1.Phone=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).Phone;
                    
                    
                    
                    objData1.sellOut = false;
                    objData1.sellDate = '';
                    System.debug('Datalist in motion'+dataList);
                    if(!dataList.isEmpty()){
                        for (Integer i = 0; i < dataList.size(); i++){
                            if(dataList[i].serialNumb == serialObj.Woil_Serial_Number__c && dataList[i].sellOut == true){
                                dataList.remove(i);
                                checkSellout = true;
                                System.debug('inside if loop of sellout');
                                System.debug('i of if sellout==>'+i);
                            }
                            else if(dataList[i].serialNumb == serialObj.Woil_Serial_Number__c && dataList[i].sellOut == false){
                                checkSellout = false;
                            }
                            else if(dataList[i].serialNumb != serialObj.Woil_Serial_Number__c){
                                checkSellout = true;
                                System.debug('i of else sellout==>'+i);
                            }
                        }
                        if(checkSellout){
                            dataList.add(objData1);
                        }
                        
                    }
                    else{
                        dataList.add(objData1);
                    }
                    
                    
                    
                    
                    //check = true;
                    
                }
                else 
                {
                    if(serialObj.Woil_Serial_Transaction_Type__c == 'Purchase'){
                        System.debug('in purchase');
                        
                        if(serialObj.Woil_Quantity__c > 0){
                            dataValueWrapper objData = new dataValueWrapper();
                            objData.materialCode = serialObj.Woil_Product__r.Name;
                            objData.materialDesc = serialObj.Woil_Product__r.Description;
                            objData.serialNumb = serialObj.Woil_Serial_Number__c;
                            objData.warehouseLocation = serialObj.Woil_Current_Customer_Code__c;
                            
                                      //Add new--------------------------------------
                      objData.CustomerName=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).CustomerName;
                objData.CustomerAddress=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).CustomerAddress;
                objData.Phone=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).Phone;
                    
                            
                            
                            
                            objData.sellOut = true;
                            objData.sellDate = System.today().format();
                            System.debug('dataList'+ dataList);
                            if(!dataList.isEmpty()){
                                for (Integer i = 0; i < dataList.size(); i++){
                                    System.debug('dataList[i].serialNumb ====>'+dataList[i].serialNumb + ' serialObj.Woil_Serial_Number__c====>' +serialObj.Woil_Serial_Number__c);
                                    if(dataList[i].serialNumb != serialObj.Woil_Serial_Number__c && !checkPurchase){
                                        System.debug('i of purchase==>'+i);
                                        checkPurchase = true;
                                        addRec= true;
                                    }
                                    
                                    else if(dataList[i].serialNumb == serialObj.Woil_Serial_Number__c){
                                        checkPurchase = true;
                                        addRec= false;
                                        break;
                                    }
                                }
                                if(addRec){
                                    dataList.add(objData);
                                }
                            }
                            else{
                                dataList.add(objData);
                            }
                            System.debug('dataList'+ dataList);
                            
                        }
                        
                    }
                    else{
                        System.debug('in purchase if');
                        
                        dataValueWrapper objData1 = new dataValueWrapper();
                        objData1.materialCode = serialObj.Woil_Product__r.Name;
                        objData1.materialDesc = serialObj.Woil_Product__r.Description;
                        objData1.serialNumb = serialObj.Woil_Serial_Number__c;
                        objData1.warehouseLocation = serialObj.Woil_Current_Customer_Code__c;
                        
                                  //Add new--------------------------------------
                      objData1.CustomerName=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).CustomerName;
                objData1.CustomerAddress=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).CustomerAddress;
                objData1.Phone=DataWrapperMap.get(serialObj.Woil_Serial_Number__c).Phone;
                    
                        
                        
                        objData1.sellOut = false;
                        objData1.sellDate = '';
                        System.debug('datalist in purchase'+dataList);
                        if(!dataList.isEmpty()){
                            for (Integer i = 0; i < dataList.size(); i++){
                                if(dataList[i].serialNumb == serialObj.Woil_Serial_Number__c && dataList[i].sellOut == true){
                                    dataList.remove(i);
                                    checkBool = true;
                                    System.debug('inside if loop of sellout');
                                    System.debug('i of if sellout==>'+i);
                                }
                                else if(dataList[i].serialNumb == serialObj.Woil_Serial_Number__c && dataList[i].sellOut == false){
                                    checkBool = false;
                                }
                                else if(dataList[i].serialNumb != serialObj.Woil_Serial_Number__c){
                                    checkBool = true;
                                    System.debug('i of else sellout==>'+i);
                                }
                            }
                            if(checkBool){
                                dataList.add(objData1);
                            }
                        }
                        
                        else{
                            dataList.add(objData1);   
                        }
                        
                        
                        
                    }
                }
            }
            System.debug('dataList'+dataList);
        }catch(Exception e){
            System.debug('The following error has occurred.'); 
            System.debug('Exception caught: ' + e.getMessage()); 
        }
        return dataList;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<dataValueWrapper> getSelloutData(){
        List<dataValueWrapper> dataList = new List<dataValueWrapper>();
        try {
            id userId = UserInfo.getUserId();
            User u = [select id, contactId from User where id = : userId];
            id getContactId = u.contactId;
            Id accID = [select Id,AccountID from contact where id =: getContactId].AccountID;
            List<String> matCode = new List<String>();    
            Map<Id,Woil_Partner_Product_Mapping__c> MapOfserialNumberTransaction = new  Map<Id,Woil_Partner_Product_Mapping__c>([Select id,Woil_Customer_Name__c,Woil_Product__r.Name from Woil_Partner_Product_Mapping__c where Woil_Customer_Name__c =:accID]);
            if(!MapOfserialNumberTransaction.keySet().isEmpty()){
                for(Id serialTransObj : MapOfserialNumberTransaction.keySet()){
                    matCode.add(MapOfserialNumberTransaction.get(serialTransObj).Woil_Product__r.Name);
                    
                }
            }
            System.debug('matcode===> '+matCode);
            List<Woil_Serial_Number_Transaction__c> serialTransList = [select id,Woil_Product__r.Name,Woil_Product__r.Description, Woil_Serial_Number__c,Woil_Quantity__c,Woil_Sales_Motion__c,Woil_Serial_Transaction_Type__c,Woil_Current_Customer_Code__c,Woil_Transaction_Date__c from Woil_Serial_Number_Transaction__c where OwnerId =: userId and Woil_Sales_Motion__c ='SellOut'];
            System.debug('serialTransList===>'+serialTransList);
            if(!serialTransList.isEmpty()){
                for(Woil_Serial_Number_Transaction__c serObj: serialTransList){
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.materialCode = serObj.Woil_Product__r.Name;
                    objData.materialDesc = serObj.Woil_Product__r.Description;
                    objData.transactionDate = serObj.Woil_Transaction_Date__c;
                    objData.customerCode = serObj.Woil_Current_Customer_Code__c;
                    objData.serialNumb = serObj.Woil_Serial_Number__c;
                    dataList.add(objData);
                }
            }
            System.debug('dataLst==>'+dataList);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return dataList;
    }
    
    @AuraEnabled
    public static String getTemplate(){
        
        String docmId = Woil_UtilityClass.getTemplateId(Label.Woil_SellOut_Template);
        return docmId;
        
    }
    
    public class customValueWrapper {
        @auraEnabled public string custFldlabel {get;set;}
        @auraEnabled public string custFldvalue {get;set;}
        
    }
    
    public class dataValueWrapper{
        @auraEnabled public string CustomerName {get;set;}
        @auraEnabled public string CustomerAddress {get;set;}
        @auraEnabled public string Phone {get;set;}

        @auraEnabled public string materialCode {get;set;}
        @auraEnabled public string materialDesc {get;set;}
        @auraEnabled public string warehouseLocation {get;set;}
        @auraEnabled public string serialNumb {get;set;}
        @auraEnabled public boolean sellOut {get;set;}
        @auraEnabled public String sellDate {get;set;}
        @auraEnabled public String errorString {get;set;}
        @auraEnabled public String customerCode {get;set;}
        @auraEnabled public Date transactionDate {get;set;}
    }
}