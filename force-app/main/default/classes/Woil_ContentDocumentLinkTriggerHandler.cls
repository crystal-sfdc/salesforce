public class Woil_ContentDocumentLinkTriggerHandler {
    
    public static void exposeAttachmentToCommunity(List<ContentDocumentLink> newContentDocList){
        
        for(ContentDocumentLink cdl : newContentDocList){
            if(cdl.LinkedEntityId != null && (Schema.Woil_Consumer_Finance_Master__c.SObjectType == cdl.LinkedEntityId.getSobjectType() || Schema.Woil_Scheme_Master__c.SObjectType == cdl.LinkedEntityId.getSobjectType())){
                cdl.visibility  =   'AllUsers';
            }
        }
    }
}