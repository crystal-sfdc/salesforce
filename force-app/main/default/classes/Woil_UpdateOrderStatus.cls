/**
* @Author Deepak Gupta
* @Date 02-Sep-2021
* @Description This batch Job update the status of order records on daily basis to
*			--> Expired, if status is GRN Pending,Draft,Error,Confirmed and PO Expiry Date is expired
* 			--> Partial Received but Expired ,if status = Partial Received and PO Expiry Date is expired
*/

public class Woil_UpdateOrderStatus implements Database.Batchable<sObject> {
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('select id,status,PoDate from order where status not in (\'Rejected\',\'Cancelled\',\'Full Received\',\'Expired\',\'Partial Received but Expired\') and id in (select orderid from orderitem)');
    }
    
    public void execute(Database.BatchableContext bc, List<order> orderList) {
        system.debug('** orderList records **'+orderList);
        List<order> orderListToBeUpdated = new List<order>();        
        for(order ord : orderList) {
            if((ord.status == 'Draft' || ord.status == 'Error' || ord.status == 'GRN Pending' || ord.status == 'Confirmed') && ord.PoDate < system.today()) {
                ord.Status = 'Expired';
                orderListToBeUpdated.add(ord);
            }
            if(ord.Status == 'Partial Received' && ord.PoDate <system.today()){
                ord.Status = 'Partial Received but Expired';
                orderListToBeUpdated.add(ord);   
            }
        }
        system.debug('** orderListToBeUpdated **'+orderListToBeUpdated);
        if(orderListToBeUpdated.size()> 0) {
            try{
                update orderListToBeUpdated;
            }catch(DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());  
            }
        }
    }
    
    public void finish(Database.BatchableContext bc) {
    }
}