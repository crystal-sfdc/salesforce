public class Woil_GRNTrigger_handler {
    public static Map<Id,Decimal> InvoiceVsQty=new Map<Id,Decimal> ();
    public static void BeforeInsert(List<GRN__c> Lstgrn,Map<Id,GRN__c> mapSRN){
        for(GRN__c grn:Lstgrn){
            if(grn.Woil_Posted__c==true  && (mapSRN.get(grn.Id).Woil_posted__c!=true && grn.Woil_posted__c==true)){
              InvoiceVsQty.put(grn.Woil_Sales_Invoice__c , grn.Woil_Total_Accepted_Quantity_FG__c+grn.Woil_Total_Accepted_Quantity_SP__c);
            }
        }
        System.debug('InvoiceVsQty'+InvoiceVsQty);
        List<Invoice__c> invoices=[Select Id,Woil_Accepted_or_Rejected_Quantity__c,Woil_Total_Quantity__c,Woil_Accepted_Quantity__c from Invoice__c where Id in :InvoiceVsQty.keySet()];
        System.debug('invoice'+invoices);
        for(Invoice__c inv:invoices){
            if(InvoiceVsQty.containsKey(inv.Id)){
               inv.Woil_Accepted_Quantity__c=(InvoiceVsQty.get(inv.Id)!=null?InvoiceVsQty.get(inv.Id):0)+(inv.Woil_Accepted_Quantity__c!=null?inv.Woil_Accepted_Quantity__c:0);
               /*Decimal qty=(inv.Woil_Accepted_Quantity__c!=null?inv.Woil_Accepted_Quantity__c:0)+(inv.Woil_Accepted_or_Rejected_Quantity__c!=null?inv.Woil_Accepted_or_Rejected_Quantity__c:0);
               //InvoiceVsQty.put(inv.Id,qty); 
               //inv.Woil_Accepted_or_Rejected_Quantity__c=qty;              
                if(inv.Woil_Accepted_or_Rejected_Quantity__c==inv.Woil_Total_Quantity__c){
                    inv.Woil_Status__c='Processed';
                    
                }*/
            }            
        }
        
        update invoices;
    }
}