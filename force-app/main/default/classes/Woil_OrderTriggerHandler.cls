public class Woil_OrderTriggerHandler {
    
    public static void afterUpdate(List<Order> orderNewList, Map<Id,Order> orderOldMap){
        for(Order orderObj : orderNewList){
            if(orderObj.Status == 'Approved' && orderOldMap.get(orderObj.Id).Status != 'Approved'){
                List<Order> orderObjList = [Select Id,AccountId, PoDate, OrderNumber, EffectiveDate, Woil_SAP_Order_Type__c, Woil_Division__c, Woil_Sold_To_Code__c, Woil_ShipToAddress__c,Woil_ShipToAddress__r.Name, Woil_Bill_to_Code__c, Account.Woil_Payer_Code__c, 
                                            (Select Product2.Name,Quantity from OrderItems),product__r.Name from order where id =: orderObj.Id ];
                
                
                String divisonfield = '';
                if(orderObjList[0].Woil_Division__c == 'Finish Good')
                    divisonfield = 'AG';
                else if(orderObjList[0].Woil_Division__c == 'Spare Part')
                    divisonfield = 'AS';
                
                List<Woil_Account_Plant__c> listOfAccountPlant = [Select id, Woil_Plant_Master__r.Woil_Plant_Code__c from Woil_Account_Plant__c where Woil_Trade_Partner__c = : orderObjList[0].AccountID AND Woil_Division__c = : divisonfield];
                List<Woil_JsonCreator.ItemDetails> itemDetailsObjList = new List<Woil_JsonCreator.ItemDetails>();
                Integer lineNumber = 1;
                for(Order obj : orderObjList){
                    for(OrderItem orderItemObj : obj.OrderItems){
                        
                        itemDetailsObjList.add(new Woil_JsonCreator.ItemDetails(String.valueOf(lineNumber),orderItemObj.Product2.Name, 
                                                                                listOfAccountPlant[0].Woil_Plant_Master__r.Woil_Plant_Code__c, 
                                                                                String.valueOf(orderItemObj.Quantity)));
                        lineNumber++;
                    }
                }
                
                
                Woil_JsonCreator.Header headerObj = new Woil_JsonCreator.Header(orderObjList[0].Id,
                                                                                orderObjList[0].OrderNumber, 
                                                                                orderObjList[0].Woil_SAP_Order_Type__c, 
                                                                                'AT', divisonfield, 
                                                                                String.valueOf(orderObjList[0].EffectiveDate), 
                                                                                orderObjList[0].Woil_Sold_To_Code__c, 
                                                                                orderObjList[0].Woil_ShipToAddress__r.Name, 
                                                                                orderObjList[0].Woil_Bill_to_Code__c, 
                                                                                orderObjList[0].Account.Woil_Payer_Code__c,
                                                                                String.valueOf(orderObjList[0].PoDate),'');
                
                
                
                Woil_JsonCreator.SO_Transaction soTransactionObj = new Woil_JsonCreator.SO_Transaction(headerObj,itemDetailsObjList);
                Woil_JsonCreator jsonCreator = new Woil_JsonCreator(soTransactionObj);
                System.debug('yyyyyyyyyyyyyyyyyyyyyy'+json.serialize(jsonCreator));
                doCalloutFromFuture(json.serialize(jsonCreator));
                System.debug('###################################');
            }
        }
    }

    public static void checkPoExpiryDate(Map<Id,Order> orderNewMap, Map<Id,Order> orderOldMap){
        for(Id orderObj : orderNewMap.keySet()){
            if(orderNewMap.get(orderObj).PoDate != orderOldMap.get(orderObj).PoDate && orderOldMap.get(orderObj).Woil_Is_Expiry_Edited_Once__c){
                orderNewMap.get(orderObj).addError('You can not edit Po Expiry date more than once.');
            }else if(orderNewMap.get(orderObj).PoDate != orderOldMap.get(orderObj).PoDate && !orderOldMap.get(orderObj).Woil_Is_Expiry_Edited_Once__c){
                orderNewMap.get(orderObj).Woil_Is_Expiry_Edited_Once__c = true;
            }   
        }
    }
    
    @future (callout=true)
    public static void doCalloutFromFuture(String reqJson) {
        
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:SAP_PI_Integration');
        req.setMethod('POST');
        req.setBody(reqJson);
        req.setTimeout(120000);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());
        System.debug('res.getStatusCode()----'+res.getStatusCode());
        
        //List<SO_Transaction_Response> SO_Transaction_Response = new List<SO_Transaction_Response>();
        Woil_OrderTriggerHandler sotrans = (Woil_OrderTriggerHandler)JSON.deserialize(res.getBody(), Woil_OrderTriggerHandler.class);
        if(sotrans.SO_Transaction_Response[0].Status == 'S'){
            Order o = new Order();
            o.Id = sotrans.SO_Transaction_Response[0].Purchase_Order_ID;
            o.Woil_Error_Message__c = '';
            o.Status = 'Confirmed';
            update o;
        }else if(sotrans.SO_Transaction_Response[0].Status == 'E'){
            if(sotrans.SO_Transaction_Response[0].Message.contains('Bal Sign-off Reqd') || sotrans.SO_Transaction_Response[0].Message.contains('Cheque Bouncing')){
                Order o = new Order();
                o.Id = sotrans.SO_Transaction_Response[0].Purchase_Order_ID;
                o.Status = 'Order Blocked';
                o.Woil_Error_Message__c = sotrans.SO_Transaction_Response[0].Message;
                update o;
            }else{
                Order o = new Order();
                o.Id = sotrans.SO_Transaction_Response[0].Purchase_Order_ID;
                o.Status = 'Error';
                o.Woil_Error_Message__c = sotrans.SO_Transaction_Response[0].Message;
                update o;
            }
            
        }
        System.debug('soTransactionList---'+sotrans);
        // Parse the JSON response
        if(res.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
        } else {
            System.debug(res.getBody());
        }
        
    }
    
    
    //public List<SO_Transaction_Response> SO_Transaction_ResponseList;
    
    public List<SO_Transaction_Response> SO_Transaction_Response{get;set;}
    
    public class SO_Transaction_Response {
        public Integer SO_Number{get;set;}
        public String Purchase_Order_ID{get;set;}
        public String Status{get;set;}
        public String Message{get;set;}
        
    }
    
    public static void beforeInsert(List<Order> orderList){
        List<Order> listOrdersOfSingleCustomers = [Select status from order where AccountId =: orderList[0].AccountId];
        for(Order orderObj : listOrdersOfSingleCustomers){
            if(orderObj.status == 'Order Blocked'){
                orderList[0].AccountId.addError('Customer have a blocked order so, Can not allow to place order');
                break;
            }
        }
    }
}