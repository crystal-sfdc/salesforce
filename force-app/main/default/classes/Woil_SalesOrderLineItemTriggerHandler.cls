public without sharing class Woil_SalesOrderLineItemTriggerHandler {
    public static void removeSerialScanItems(List<Woil_Sales_Order_Line_Item__c> oldSalesOrderItemList){
        Map<Id,List<Id>> productIdMap = new Map<Id,List<Id>>();
        Map<Id,List<Woil_Serial_Number_Scan__c>> serialScanMap = new Map<Id,List<Woil_Serial_Number_Scan__c>>();
        List<Woil_Serial_Number_Scan__c> smsToBeDeleted = new List<Woil_Serial_Number_Scan__c>();
        for(Woil_Sales_Order_Line_Item__c solt : oldSalesOrderItemList){
            if(!productIdMap.containsKey(solt.Woil_Sales_Order__c)){
                productIdMap.put(solt.Woil_Sales_Order__c, new List<Id>());
            }
            List<Id> tempList = productIdMap.get(solt.Woil_Sales_Order__c);
            tempList.add(solt.Woil_Product__c);
            productIdMap.put(solt.Woil_Sales_Order__c,tempList);
        }

        for(Woil_Serial_Number_Scan__c sns : [SELECT Id, Woil_Product__c,Woil_Sales_Order__c FROM Woil_Serial_Number_Scan__c
                                                WHERE Woil_Sales_Order__c IN: productIdMap.keySet()]){
            if(!serialScanMap.containsKey(sns.Woil_Sales_Order__c)){
                serialScanMap.put(sns.Woil_Sales_Order__c, new List<Woil_Serial_Number_Scan__c>());
            }
            List<Woil_Serial_Number_Scan__c> tempList = serialScanMap.get(sns.Woil_Sales_Order__c);
            tempList.add(sns);
            serialScanMap.put(sns.Woil_Sales_Order__c,tempList);
        }

        for(String salesId : serialScanMap.keySet()){
            for(Woil_Serial_Number_Scan__c sms : serialScanMap.get(salesId)){
                if(productIdMap.get(salesId).contains(sms.Woil_Product__c)){
                    smsToBeDeleted.add(sms);
                }
            }
        }

        if(!smsToBeDeleted.isEmpty()){
            delete smsToBeDeleted;
        }
    }

    public static void checkParentStatus(Map<Id,Woil_Sales_Order_Line_Item__c> oldSalesOrderItemMap){
        for(Woil_Sales_Order_Line_Item__c solt : [SELECT Id, Woil_Sales_Order__c,Woil_Sales_Order__r.Woil_Status__c
                                                    FROM Woil_Sales_Order_Line_Item__c WHERE Id IN : oldSalesOrderItemMap.keySet()]){
            if(solt.Woil_Sales_Order__r.Woil_Status__c == 'Processed'){
                solt.addError('You can not delete Sales Order Line Item when Sales Order Status is Processed.');

            }
        }
    }


}