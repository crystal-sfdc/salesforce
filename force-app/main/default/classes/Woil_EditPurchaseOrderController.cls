public with sharing class Woil_EditPurchaseOrderController {
    @AuraEnabled
    public static String getPOInformation(String recordId){
        Map<String,String> currentSession = Auth.SessionManagement.getCurrentSession();
        Boolean isLogCommunityUser = false;
        if(currentSession.get('LoginHistoryId')==null && currentSession.get('SourceIp')=='::' && currentSession.get('UserType')=='Standard'){
            isLogCommunityUser=true;
        }
        List<Order> orderList = [SELECT Account.Name,Woil_Bill_to_Code__c,PoDate,Woil_Supplier_Name__c,Woil_Supplier_Code__c,Woil_ShipToAddress__r.Name,Status,Type,Woil_Division__c
                                    FROM Order
                                    WHERE Id =: recordId];

        
        return JSON.serialize( new Woil_EditPurchaseOrderControllerrWrapper(orderList,isLogCommunityUser));
    }

    @AuraEnabled
    public static String updateOrderRecord(String orderRecord){

        Order orderToBeupdated = (Order)JSON.deserialize(orderRecord, Order.class);
        System.debug('In Apex Class');
        Database.SaveResult sr = Database.update(orderToBeupdated);

        if(sr.isSuccess()){
            return 'Success#'+sr.getId();
        }else{
            for(Database.Error err : sr.getErrors()){
                return 'Failed#'+err.getMessage();
            }

        }

      return 'Failed#Error';
    }

    public class Woil_EditPurchaseOrderControllerrWrapper{
        @AuraEnabled public List<Order> orderList {get;set;}
        @AuraEnabled public Boolean isCommunity {get;set;}

        public Woil_EditPurchaseOrderControllerrWrapper(List<Order> orderList, Boolean isCommunity){
            this.orderList = orderList;
            this.isCommunity = isCommunity;
        }
    }
}