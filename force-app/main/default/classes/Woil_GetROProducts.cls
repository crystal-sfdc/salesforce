global class Woil_GetROProducts {
    
       /**
    * --------------------------------------------------------------------------------------------------
    * Author - Shivam Baliyan
    * Added On - 19/8/2021
    * Description - Getting all Products based on Division(Finished Good, Spare Part)
    * --------------------------------------------------------------------------------------------------
    */

    @AuraEnabled
    public static String getProductController(String recordId,String selectedRows){

        List<String> selectedProdList = new List<String>();
		String statusPR=[Select Woil_status__c from Woil_SRN__c where id=:recordId].Woil_status__c;
        if(statusPR=='Draft'){
        if(!String.isBlank(selectedRows)){
            List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
            for(WrapperOrderLineItem wrap : wResp){
                selectedProdList.add(wrap.prodName);
            }
        }

        List<Woil_SRN__c> srnList = [SELECT Id, Woil_Posted__c,Woil_Division__c FROM Woil_SRN__c WHERE Id =: recordId];
        if(srnList[0].Woil_Posted__c != false){
            return 'Purchase Return is already posted';
        }else{
            List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            if(srnList[0].Woil_Division__c == 'Finish Good'){
                return fetchFinishGood(userList,'','',selectedProdList);
            }else if(srnList[0].Woil_Division__c == 'Spare Part'){
                return fetchSparePart(userList,false,'','','',selectedProdList);
            }
            return '';
            
        }
        }else{
          return 'Cannot add Products as Purchase Return is in '+statusPR+' stage';   
        }
    }

    /**
    * --------------------------------------------------------------------------------------------------
    * Author - Shivam Baliyan
    * Added On - 19/8/2021
    * Description - Getting Finish Good Products
    * --------------------------------------------------------------------------------------------------
    */

    public static String fetchFinishGood(List<User> userList,String searchString, String catSearch, List<String> selectedProduct){
        
        Map<String,String> plantCodeMap = new Map<String,String>();
        Map<String,String> materialMap = new Map<String,String>();
        Set<String> finalProductSet = new Set<String>();
        Map<Id,Product2> productMap = new Map<Id,Product2>();
        Map<Id, Decimal> prodPriceMap= new Map<Id, Decimal>();
        List<WrapperProduct> wrapProdList= new List<WrapperProduct>();
        Map<String, String> sPartMap = new Map<String, String>();
        Map<String, String> prodCatMap = new Map<String, String>();
        WrapperProductList wrapList = new WrapperProductList();
        List<Product2> productList = new List<Product2> ();
        

        List<Account> accList = [SELECT Id, (SELECT ID,Woil_Plant_Master__r.Woil_Plant_Code__c,Woil_Plant_Master__c FROM Account_Plants__r WHERE Woil_Division__c = 'AG'), 
                                     (SELECT Id, Woil_Product__c, Woil_Product__r.Name FROM Partner_Product_Mapping__r) 
                                      FROM Account WHERE Id =: userList[0].AccountId];
                for(Account acc : accList){
                    for(Woil_Account_Plant__c accPlant : acc.Account_Plants__r){
                        plantCodeMap.put(accPlant.Woil_Plant_Master__c,accPlant.Woil_Plant_Master__r.Woil_Plant_Code__c);
                    }
                    for(Woil_Partner_Product_Mapping__c ppm : acc.Partner_Product_Mapping__r){
                        materialMap.put(ppm.Woil_Product__c, ppm.Woil_Product__r.Name);
                    }
                }
                
                for(Woil_Plant_Product_Mapping__c ppm : [SELECT Woil_Product__c, Woil_Days_Of_Inventory__c 
                                                        FROM Woil_Plant_Product_Mapping__c 
                                                        WHERE Woil_Plant_Master__c IN:plantCodeMap.keySet() AND Woil_Product__c IN:materialMap.keySet()]){
                    finalProductSet.add(ppm.Woil_Product__c);
                }

                String searchStr=  '%' + searchString + '%';
                if(String.isNotBlank(catSearch)){
                    productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c, Description, RecordType.Name ,
                                                (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                                                FROM Product2 
                                                WHERE Id IN: finalProductSet AND IsActive= True AND (Name Like : searchStr OR Description Like : searchStr) AND Woil_Category__c=:catSearch LIMIT 20];
                }else{
                    productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c, Description, RecordType.Name ,
                                                (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                                                FROM Product2 
                                                WHERE Id IN: finalProductSet AND IsActive= True AND (Name Like : searchStr OR Description Like : searchStr) LIMIT 20];
                }

                for(Product2 prod : productList){
                    productMap.put(prod.Id,prod);
                    for(Woil_Product_Price__c prodprice : prod.Product_Price__r){
                        prodPriceMap.put(prodprice.Woil_Product__c,prodprice.Woil_Dealer_Price__c);
                    }
                }

                for(Id prodId: productMap.keySet()) {
                    WrapperProduct wrapProd= new WrapperProduct();
                    wrapProd.prodName= productMap.get(prodId).Name;
                    wrapProd.prodDescription= productMap.get(prodId).Description;
                    wrapProd.productId= prodId;
                    wrapProd.prodRecordType=productMap.get(prodId).RecordType.Name;
                    wrapProd.parentCode= productMap.get(prodId).Woil_Parent_Code__c;
                    wrapProd.prodDealerPrice= prodPriceMap.get(prodId);
                    wrapProd.prodCategory= productMap.get(prodId).Woil_Category__c;
                    if(selectedProduct.contains(productMap.get(prodId).Name)){
                        wrapProd.setCheckbox = true;
                    }else{
                        wrapProd.setCheckbox = false;
                    }
                    wrapProdList.add(wrapProd);
                }
                
        Schema.DescribeFieldResult fieldResultPart = Product2.Woil_Part_Type__c.getDescribe();
        List<Schema.PicklistEntry> pValuesPart = fieldResultPart.getPicklistValues();
        for (Schema.PicklistEntry p: pValuesPart) {
            sPartMap.put(p.getValue(), p.getLabel());
        }
        
                Schema.DescribeFieldResult fieldResultCat = Product2.Woil_Category__c.getDescribe();
                List<Schema.PicklistEntry> pValuesCat = fieldResultCat.getPicklistValues();
                for (Schema.PicklistEntry p: pValuesCat) {
                    
                    prodCatMap.put(p.getValue(), p.getLabel());
                }
                
                wrapList.wrapProductList = wrapProdList;
                wrapList.sPartTypeMap = sPartMap;
                wrapList.prodCategoryMap = prodCatMap;
				
                return JSON.serialize(wrapList);
    }

    

    /**
    * --------------------------------------------------------------------------------------------------
    * Author - Shivam Baliyan
    * Added On - 19/8/2021
    * Description - Getting Finish Good Products based on filter value
    * --------------------------------------------------------------------------------------------------
    */

    @AuraEnabled
    public static string filteredOnCategory(String orderID,String filterValue, String searchString, String selectedRows){

        List<String> selectedProdList = new List<String>();

        if(!String.isBlank(selectedRows)){
            List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
            for(WrapperOrderLineItem wrap : wResp){
                selectedProdList.add(wrap.prodName);
            }
        }

        List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
        return fetchFinishGood(userList,searchString,filterValue,selectedProdList);
    }    

    /**
    * --------------------------------------------------------------------------------------------------
    * Author - Shivam Baliyan
    * Added On - 19/8/2021
    * Description - Adding SalesOrderLineItem to Sales Order
    * --------------------------------------------------------------------------------------------------
    */
    
    @AuraEnabled
    public static string addSOLineItemtoOrder(String selectedRows, String ordId){
        try {
            List<Woil_SRN_Line_Item__c> ordItemList= new List<Woil_SRN_Line_Item__c>();
            List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
            Set<Id> prodIdSet=new Set<Id>();

           Woil_SRN__c ord= [SELECT Id, Woil_Division__c FROM Woil_SRN__c WHERE Id=:ordId];
            
            List<Woil_SRN_Line_Item__c> existingOrderItem= [SELECT Id, Woil_SRN__c,Woil_Quantity__c, Woil_Product__c FROM Woil_SRN_Line_Item__c WHERE Woil_SRN__c=:ordId]; 

            Map<Id,Woil_SRN_Line_Item__c> existingPdts= new Map<Id,Woil_SRN_Line_Item__c>();
            
            for(Woil_SRN_Line_Item__c tempVar : existingOrderItem){
                existingPdts.put(tempVar.Woil_Product__c, tempVar);
            }
            
            Boolean differentOrder = False;
            
            for(WrapperOrderLineItem tempVar : wResp){
                prodIdSet.add(tempVar.productId);
            }
            

           /* if(ord.Woil_Division__c == System.label.Woil_Finished_Goods) {
                if(existingOrderItem!=null && !existingOrderItem.isEmpty()){
                    if(wResp[0].prodCategory == 'AC' && existingOrderItem[0].Woil_Category__c != 'AC') {
                        differentOrder= True;
                    }else if(wResp[0].prodCategory != 'AC' && existingOrderItem[0].Woil_Category__c == 'AC'){
                        differentOrder= True;
                    }
                }
            }*/
            
            if(!differentOrder) {            

                for(WrapperOrderLineItem wrappObj: wResp) {
                    if(existingPdts.keySet().contains(wrappObj.productId)) {
                        Woil_SRN_Line_Item__c ordItem = existingPdts.get(wrappObj.productId);
                        ordItem.Woil_Quantity__c= existingPdts.get(wrappObj.productId).Woil_Quantity__c+(-wrappObj.quantity);
                        ordItemList.add(ordItem);
                    }
                    else {
                    Woil_SRN_Line_Item__c ordItem= new Woil_SRN_Line_Item__c();
                    ordItem.Woil_SRN__c = ordId;
                    ordItem.Woil_Product__c = wrappObj.productId;
                    ordItem.Woil_Quantity__c = -wrappObj.quantity;
                    ordItem.Woil_Unit_Price__c = WrappObj.prodDealerPrice;
                    //ordItem.Woil_Category__c = WrappObj.prodCategory;
                    //ordItem.Woil_Description__c = WrappObj.prodDescription;
                    ordItemList.add(ordItem);
                    } 
                }
                upsert ordItemList;
                return 'Product added Successfully';
            }
            else {
                throw new DifferentProductException();
            }
        }
        catch(DifferentProductException e){
            System.debug('The following error has occurred.');
            return 'Error: You cannot club AC with Non-AC products ';
        }
        catch(Exception e){
            System.debug('The following error has occurred.'); 
            System.debug('Exception caught: ' + e.getMessage()); 
            throw new AuraHandledException(e.getMessage());
        }
    }
    
     /**
    * --------------------------------------------------------------------------------------------------
    * Author - Shivam Baliyan
    * Added On - 19/8/2021
    * Description - Getting Spare Part Products
    * --------------------------------------------------------------------------------------------------
    */
    
    public static String fetchSparePart(List<User> userList, Boolean tooPartType, String searchString,String partType, String categoryType, List<String> selectedProduct){
        Set<Id> accPlantIds= new Set<Id>();
        Set<Id> plantProdSet = new Set<Id>();
        Map<String,String> modelSPMap= new Map<String,String>();

        Map<String,String> plantCodeMap = new Map<String,String>();
        Map<String,String> materialMap = new Map<String,String>();
        Set<String> finalProductSet = new Set<String>();
        Map<Id,Product2> productMap = new Map<Id,Product2>();
        Map<Id, Decimal> prodPriceMap= new Map<Id, Decimal>();
        List<WrapperProduct> wrapProdList= new List<WrapperProduct>();
        Map<String, String> sPartMap = new Map<String, String>();
        Map<String, String> prodCatMap = new Map<String, String>();
        WrapperProductList wrapList = new WrapperProductList();
        List<Product2> productList = new List<Product2> ();
        String searchStr=  '%' + searchString + '%';
        for(Woil_Account_Plant__c plantId: [SELECT Woil_Plant_Master__c FROM Woil_Account_Plant__c 
                                            WHERE Woil_Trade_Partner__c=:userList[0].AccountId AND Woil_Division__c = 'AS']) {
            accPlantIds.add(plantId.Woil_Plant_Master__c);
        }
                        
        for(Woil_Plant_Product_Mapping__c plantProd: [SELECT Woil_Product__c, Woil_Days_Of_Inventory__c FROM Woil_Plant_Product_Mapping__c 
                                                        WHERE Woil_Plant_Master__c IN:accPlantIds]) {
            plantProdSet.add(plantProd.Woil_Product__c);
        }
        
        for(Woil_Model_SparePart_Mapping__c str : [SELECT Id, Woil_SparePart_Code__c, Woil_Material_Code__c FROM Woil_Model_SparePart_Mapping__c]) {
            modelSPMap.put(str.Woil_SparePart_Code__c, str.Woil_Material_Code__c);
        }

        if(String.isBlank(categoryType) && String.isBlank(partType) ){
            productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                                        (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                                        FROM Product2 
                                        WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =:tooPartType AND Name Like : searchStr];
        }
        else if(String.isBlank(categoryType) && String.isNotBlank(partType)) { 
            productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                                        (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                                        FROM Product2 
                                        WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =:tooPartType AND Woil_Part_Type__c=:partType AND Name Like : searchStr];
        }
        else if(String.isNotBlank(categoryType) && String.isBlank(partType)) {
            productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                                        (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                                        FROM Product2 
                                        WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =:tooPartType AND Woil_Category__c=:categoryType AND Name Like : searchStr];
        }
       else {
        productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                                    (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                                    FROM Product2 
                                    WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =: tooPartType AND Woil_Category__c=:categoryType AND Woil_Part_Type__c=:partType AND Name Like : searchStr];
       }

        

        for(Product2 prod : productList){
            productMap.put(prod.Id,prod);
            for(Woil_Product_Price__c prodprice : prod.Product_Price__r){
                prodPriceMap.put(prodprice.Woil_Product__c,prodprice.Woil_Dealer_Price__c);
            }
        }

        for(Id prodId: productMap.keySet()) {
            WrapperProduct wrapProd= new WrapperProduct();
            wrapProd.prodName = productMap.get(prodId).Name;
            wrapProd.prodDescription = productMap.get(prodId).Description;
            wrapProd.productId = prodId;
            wrapProd.prodRecordType = productMap.get(prodId).RecordType.Name;
            wrapProd.parentCode = productMap.get(prodId).Woil_Parent_Code__c;
            wrapProd.prodDealerPrice = prodPriceMap.get(prodId);
            wrapProd.prodCategory = productMap.get(prodId).Woil_Category__c;
            wrapProd.modelName = modelSPMap.get(productMap.get(prodId).Name);
            wrapProd.partType = productMap.get(prodId).Woil_Part_Type__c;
            wrapProd.topPart = productMap.get(prodId).Woil_Top_Part__c;
            if(selectedProduct.contains(productMap.get(prodId).Name)){
                wrapProd.setCheckbox = true;
            }else{
                wrapProd.setCheckbox = false;
            }
            wrapProdList.add(wrapProd);
        }
        
         

        Schema.DescribeFieldResult fieldResultPart = Product2.Woil_Part_Type__c.getDescribe();
        List<Schema.PicklistEntry> pValuesPart = fieldResultPart.getPicklistValues();
        for (Schema.PicklistEntry p: pValuesPart) {
            sPartMap.put(p.getValue(), p.getLabel());
        }

        

        Schema.DescribeFieldResult fieldResultCat = Product2.Woil_Category__c.getDescribe();
        List<Schema.PicklistEntry> pValuesCat = fieldResultCat.getPicklistValues();
        for (Schema.PicklistEntry p: pValuesCat) {
            
            prodCatMap.put(p.getValue(), p.getLabel());
        }

        wrapList.wrapProductList = wrapProdList;
        wrapList.sPartTypeMap = sPartMap;
        wrapList.prodCategoryMap = prodCatMap;

        return JSON.serialize(wrapList);
    }

    @AuraEnabled
    public static string filterOnSparePart(String orderID,String topPartType, String categoryType,String partType,String searchString, String selectedRows){
         List<String> selectedProdList = new List<String>();

        if(!String.isBlank(selectedRows)){
            List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
            for(WrapperOrderLineItem wrap : wResp){
                selectedProdList.add(wrap.prodName);
            }
        }
        
        List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
        Boolean topPartStr = false;
        if(topPartType == 'false'){
            topPartStr = false;
        }else{
            topPartStr = true; 
        }
        return fetchSparePart(userList,topPartStr,searchString,partType,categoryType,selectedProdList);
    }
    
    public class WrapperProductList {
        @AuraEnabled public Map<String,String> prodCategoryMap {get;set;}
        @AuraEnabled public Map<String,String> sPartTypeMap {get;set;}
        @AuraEnabled public List<WrapperProduct> wrapProductList {get;set;}
        
    }

    public class WrapperProduct {
        @AuraEnabled public String prodName {get;set;}
        @AuraEnabled public Id productId {get;set;}
        @AuraEnabled public String prodDescription {get;set;}
        @AuraEnabled public Decimal prodDealerPrice {get;set;}
        @AuraEnabled public String prodCategory {get;set;}
        @AuraEnabled public string parentCode{get;set;}
        @AuraEnabled public string prodRecordType{get;set;}
        @AuraEnabled public string modelName{get;set;}
        @AuraEnabled public Boolean setCheckbox{get;set;}
        @AuraEnabled public string partType{get;set;}
        @AuraEnabled public Boolean topPart {get;set;}
    }

    public class WrapperOrderLineItem {
        public String prodName {get;set;}
        public String prodCategory {get;set;}
        public Decimal prodDealerPrice {get;set;}
        public String prodDescription {get;set;}
        public Decimal quantity {get;set;}
        public Id productId {get;set;}
    }
}