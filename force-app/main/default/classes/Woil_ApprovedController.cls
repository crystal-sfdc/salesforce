public with sharing class Woil_ApprovedController {
    @AuraEnabled(cacheable=true)
    public static screenInfowrapperClass newScreenControl (String recordId) {
       screenInfowrapperClass screenInfo = new screenInfowrapperClass();
        Id userId = userinfo.getUserId();
        User userDetails =[SELECT Id, Name, Email, Profile.Name, UserRole.Name FROM User where Id=:userId ];
        if(userDetails.UserRole.Name=='Area Sales Manager'){     
            screenInfo.UserAsmInfo = true;
        }
            Order o = [select id ,Status,AccountId,
                       (SELECT Id, IsDeleted, OrderId, PricebookEntryId, 
                        OriginalOrderItemId, AvailableQuantity, Quantity FROM OrderItems )
                       from order where id= :recordId];
            Account accRecoType = [select id ,recordTypeId,	OwnerId, recordtype.Name from Account  where Id = : o.AccountId];
            
          //  RecordType ret =[SELECT Id, Name, SobjectType FROM RecordType where Id =: Acc.recordTypeId];,(SELECT Id,  User.Name, UserId FROM AccountTeamMembers )
            
            if(o.OrderItems.size() == 0){
                            screenInfo.OrderItemInfo=true;
            }
            else if(o.Status !='Draft' && o.Status !='Error'){
                           screenInfo.OrderStatus = true;
            }
            else if(accRecoType.recordtype.Name == 'Distributor' || accRecoType.recordtype.Name == 'Direct Dealer'|| accRecoType.recordtype.Name == 'Spare Part Distributor') {
                          screenInfo.SelectApprover = true;
            }
            return screenInfo ;
 
    }
    
    @AuraEnabled(cacheable=true)
    public static  List<User> newTeamMemberRole (String recordId) {
        System.debug(recordId);
        Order o = [select id ,AccountId from order where id= : recordId];
        List<AccountTeamMember> oA = [SELECT Id,  User.Name, UserId FROM AccountTeamMember  where AccountId = : o.AccountId ];
        set<String> Userinfo=new set<String>();
        for(AccountTeamMember AtM: oA){
            Userinfo.add(AtM.UserId);
        }
        List<User> UrManager = [SELECT Name, id,ManagerId, Manager.Name FROM User where id IN : Userinfo ] ;
        System.debug(UrManager);
        
        return UrManager;
        //return oA;
    }
  
    @AuraEnabled
    public static void changeOrderStatus (String recordId) {
        Order upOrder = [select id ,Status from Order where id =:recordId];
        upOrder.Status = 'Approved';
        update upOrder;
        
    }
    @AuraEnabled
    public static String ApprovelProccess(String UserId,String RecordId) {
        List<String> approverIdList = new List<String>();
        approverIdList.add(UserId);
        System.debug(UserId);
        System.debug(RecordId);
        User UrManager = [SELECT ManagerId, id FROM User where id = : UserId] ;
        Order approval_Order = [select id , name , Status,
                                AccountId,Approver_one__c,
                                Approver_second__c from order where id= : RecordId];
        
       // Id Submiter_id = [select id , ownerId from Account where id=:approval_Order.AccountId].ownerId;
              //  System.debug(UrManager);
        System.debug(approval_Order);
      //  System.debug(Submiter_id);

        if(UserId !=null){
            approval_Order.Approver_one__c = UserId;
        }
        if(UrManager.id !=null){
            approval_Order.Approver_second__c = UrManager.ManagerId;
        }
        update approval_Order;
                System.debug(approval_Order);

        try{
        //send Approvel request-------------
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approve Order');
            req1.setObjectId(RecordId);
            //req1.setSubmitterId(Submiter_id);
            req1.setNextApproverIds(approverIdList);
            Approval.ProcessResult result = Approval.process(req1);
            System.debug(result.isSuccess());
            System.debug(result.getErrors());
		}
        catch(DmlException e){
        	Order FailCase = approval_Order;
            FailCase.Approver_one__c = '';
            FailCase.Approver_second__c = '';
            update FailCase;
            return  e.getMessage();
        }
        return 'sucess';
    }
    
    
        public class screenInfowrapperClass{
        @AuraEnabled public Boolean UserAsmInfo{get;set;}
        @AuraEnabled public Boolean OrderItemInfo{get;set;}
        @AuraEnabled public Boolean  OrderStatus{get;set;}
        @AuraEnabled public Boolean SelectApprover {get;set;}

        }
}