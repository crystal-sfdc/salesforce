/* Name of Class - Woil_OutletController
* Initial Author - GauravKumar12@kpmg.com
* Created Date - 19/8/2021
* Description -  In this class, we have three functions to search, create & update the records.
* Class used by Woil_OutletNew and Woil_OutletEdit Aura Components.
* --------------------------------------------------------------------------------------------------
*/public without sharing class Woil_OutletController {
   
    /*
    Used in "Woil_OutletNew"
    To search that the entered PAN/GST exist in the DB or not. 
    */
    @AuraEnabled
    public static List<Outlet_Master__c> getOutletList(String gstOrPan) {
        
        id userId1 = UserInfo.getUserId();
        system.debug('userId===> '+userId1);
        id conId = [select id, ContactId from User where id = : userId1].ContactId;
        system.debug('conId===> ' + conId);
        
        //For TP Portal
        if(conId != null){
            Id accId = [select Id,AccountID from contact where id =: conId].AccountID;
            system.debug('accID===> '+accId);
            List<Sobject> outletObjList = [SELECT Outlet__r.Woil_Outlet_Name__c, Outlet__r.Name, Outlet__r.Woil_PAN__c, 
                                           Outlet__r.Woil_GST__c, Outlet__r.Woil_Pin_Code__c, Outlet__r.Woil_City__c, 
                                           Outlet__r.Woil_State__c, Outlet__r.Woil_Mobile_Number__c, 
                                           Outlet__r.Woil_Contact_Person__c
                                           from WOIL_Buyer_Seller_Mapping__c 
                                           Where WOIL_Seller__c = : accId And 
                                           (Outlet__r.Woil_PAN__c = : gstOrPan OR Outlet__r.Woil_GST__c = : gstOrPan)];
            system.debug('outletObjList TP portal===> ' + outletObjList);
            return outletObjList;
        }
        //For Salesforce Org UI
        else{
            List<Outlet_Master__c> outletObjList = [SELECT Woil_Outlet_Name__c, Name, Woil_PAN__c, Woil_GST__c, 
                                                    Woil_Pin_Code__c, Woil_City__c, Woil_State__c,Woil_Mobile_Number__c,
                                                    Woil_Contact_Person__c
                                                    from Outlet_Master__c Where Woil_GST__c = : gstOrPan OR Woil_PAN__c = : gstOrPan];
            system.debug('outletObjList Salesforce Org===> ' + outletObjList);
            return outletObjList;
        }
    }
    
    /*
    Used in "Woil_OutletNew"
    To create multiple records in Outlet, Account and buyerseller mapping. 
    */
    @AuraEnabled
    public static Outlet_Master__c createOutletRec(Outlet_Master__c outletObj){
        Savepoint sp = Database.setSavepoint();
        try{
            Id userAccId =  Woil_UtilityClass.getAccountIdFromContactID(Woil_UtilityClass.getUserContactID(UserInfo.getUserId()));
            List<Account> accRecords = [SELECT Id, PriceList__c, Woil_Account_Number__c FROM Account];
            String sellerAccountPriceList = '';
            for(Account accObj : accRecords){
                if(accObj.id == userAccId){
                    sellerAccountPriceList = accObj.PriceList__c;
                    break;
                }
            }
            //Account SellerAccount = [select Id, PriceList__c from Account where id =: userAccId]; 
            
            System.debug('Recevied outletObj**********'+outletObj);
            System.debug('outletObj Id=====> '+outletObj.id);
            Id subDealerAccId;
            List<RecordType> recTypeObjList = [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Account'];
            Id rectypeId;
            for(RecordType recTypeObj :recTypeObjList){
                if(recTypeObj.Name == 'Sub Dealer')
                    rectypeId = recTypeObj.Id;
            }
            
            
            List<Account> subDealerAccount = [SELECT Id, Name FROM Account where 
                                            Woil_PAN_Number__c = : outletObj.Woil_PAN__c OR 
                                            Woil_GST_Number__c = : outletObj.Woil_GST__c Limit 1];
            
            if(subDealerAccount.size() > 0){
                subDealerAccount[0].PriceList__c = sellerAccountPriceList;
                subDealerAccount[0].BillingCountry = 'India';
                subDealerAccount[0].BillingState = outletObj.Woil_State__c;
                subDealerAccount[0].BillingCity = outletObj.Woil_City__c;
                subDealerAccount[0].BillingPostalCode = String.valueof(outletObj.Woil_Pin_Code__c);
                subDealerAccId = subDealerAccount[0].id;
                update subDealerAccount;
                System.debug('accObj Id=====> '+subDealerAccId);
                
            }
            else{
                                Set<String> customerCode = new Set<String>();
                for(Account accObj : accRecords){
                    if(accObj.Woil_Account_Number__c != null){
                        customerCode.add(accObj.Woil_Account_Number__c);
                    }
                    
                }
                System.debug('****'+customerCode);
                String customerCodeValue;
                Integer integerValue = 0;
                Integer integerMaxValue = 0;
                for(String value : customerCode){
                    if(value.contains('SD-') && !(value.contains('{SD-'))){
                        System.debug('$$$$$$$$$$$$$$cUSTOMER coDE '+value);
                        
                        integerValue = Integer.valueOf(value.right(4).substring(0, 4));
                        if(integerMaxValue < integerValue){
                            integerMaxValue = integerValue;
                        }
                    }  
                }
                customerCodeValue  = String.valueof(integerMaxValue+1);
                customerCodeValue = 'SD-'+customerCodeValue.leftPad(4, '0');
                System.debug(customerCodeValue);
                Account accObj = new Account(RecordTypeId = rectypeId,
                                             Woil_Status__c = 'Draft',
                                             Name = outletObj.Woil_Outlet_Name__c, 
                                             Woil_PAN_Number__c = outletObj.Woil_PAN__c,
                                             PriceList__c = sellerAccountPriceList,
                                             Woil_GST_Number__c = outletObj.Woil_GST__c,
                                             Woil_Mobile_Number__c = Long.valueOf(outletObj.Woil_Mobile_Number__c),
                                             Woil_Account_Number__c = customerCodeValue,
                                             Woil_Customer_Type__c = 'Sub Dealer',
                                             BillingCountry = 'India',
                                             BillingState = outletObj.Woil_State__c,
                                             BillingCity = outletObj.Woil_City__c,
                                             BillingPostalCode = String.valueof(outletObj.Woil_Pin_Code__c)
                                            );  
                
                
                System.debug('Customer Code=====> '+customerCodeValue);
                System.debug('Woil_Mobile_Number__c=====> '+outletObj.Woil_Mobile_Number__c);
                System.debug('PriceList__c=====> '+sellerAccountPriceList);
                insert accObj;
                subDealerAccId = accObj.id;
                System.debug('Account created accObj.Id=====> '+subDealerAccId);
                
            }
            
            insert outletObj;
            //system.debug('accID===> '+accId);
            system.debug('------------------Outlet__c===================> '+outletObj.id);
            system.debug('------------------WOIL_Seller__c===================> '+userAccId );
            system.debug('------------------Parent__c===================> '+subDealerAccId);
            system.debug('------------------Status__c===================> Draft');
            
            WOIL_Buyer_Seller_Mapping__c buyerSellerMappingObj = new WOIL_Buyer_Seller_Mapping__c(Outlet__c = outletObj.id,
                                                                                                  WOIL_Seller__c = userAccId ,
                                                                                                  Parent__c = subDealerAccId,
                                                                                                  Status__c = 'Draft',
                                                                                                  WOIL_Start_Date__c = System.today(),
                                                                                                  WOIL_End_Date__c = system.today().addYears(1));
            
            insert buyerSellerMappingObj;
            System.debug('buyerSellerMappingObj Id=====> '+buyerSellerMappingObj.id);
        }
        catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.RollBack(sp);
            outletObj.id = null;
        }
        
        return outletObj;
    }

    /*
    Used in "Woil_OutletNew"
    To autofill the City & State on PinCode Basis 
    */
    @AuraEnabled
    public static Woil_PinCode_Master__c searchByPinCode(String pinCode) {
        Woil_PinCode_Master__c pinCodeMasterObj =   [SELECT Id, Woil_pinCode__c, Woil_City__c, Woil_State__c 
                                                     FROM Woil_PinCode_Master__c where Woil_pinCode__c = : pinCode];
        return pinCodeMasterObj;
    }


    /*
    Used in "Woil_OutletEdit"
    To check that the record is under Approval process or not. 
    */
    @AuraEnabled
    public static Boolean checkObjectLock(String recordId) {
        if(Approval.isLocked(recordId) == true)
            return true;
        else
            return false;
    }
    

    /*
    Used in "Woil_OutletEdit"
    To fetch the existing field of the record. 
    */
    @AuraEnabled
    public static Outlet_Master__c getOutletRec(String recordId) {
        Outlet_Master__c outletObj = [SELECT Woil_Outlet_Name__c, Name, Woil_PAN__c, Woil_GST__c, 
                                      Woil_Pin_Code__c, Woil_City__c, Woil_State__c,Woil_Mobile_Number__c, 
                                      Woil_Contact_Person__c, Woil_Outlet_Category__c, Woil_Address__c
                                      from Outlet_Master__c Where id = : recordId];
        System.debug('===========> '+outletObj);
        return outletObj;
    }
    
    /*
    Used in "Woil_OutletEdit"
    To save the updated data. 
    */
    @AuraEnabled
    public static Outlet_Master__c updateOutletRec(Outlet_Master__c outletObj ){
        System.debug('outletObj**********'+outletObj);
        update outletObj;
        return outletObj;
    }
}