public class Woil_OutletApprovalProcessContoller {
    private static Boolean IsCommunity = false;
    private static ID approver1 ;
    private static ID approver2 ;
    private static ID approver3 ;
    
    
    @AuraEnabled
    public static User getSupportUser(String recordId) {
        System.debug('RecordID of Page=======>'+recordId);
        Outlet_Master__c outletObj = [SELECT Id, Woil_Status__c FROM Outlet_Master__c where id = : recordId];
        
        //ID accID = Woil_UtilityClass.getAccountIdFromContactID(Woil_UtilityClass.getUserContactID(UserInfo.getUserId()));
        System.debug('UserInfo.getUserId=============>'+UserInfo.getUserId());
        User userRecord = [SELECT ManagerId, Woil_Support_User__c, id, Woil_Support_User__r.Name FROM User where id = : UserInfo.getUserId()] ;
        if(outletObj.Woil_Status__c == 'Approval Pending' || 
           outletObj.Woil_Status__c == 'Rejected' || 
           outletObj.Woil_Status__c ==  'Active')
            throw new AuraHandledException(outletObj.Woil_Status__c);
        return userRecord;
    }
    
    
    @AuraEnabled
    public static List<User> getApprovers() {
        List<Object> approvers = new List<Object>();
        IsCommunity = Woil_UtilityClass.isLogCommunityUser();
        System.debug('IsCommunity======> '+IsCommunity);
        List<User> UrManager;
        if(IsCommunity){
            // TP--run all 3 step
            ID accID = Woil_UtilityClass.getAccountIdFromContactID(Woil_UtilityClass.getUserContactID(UserInfo.getUserId()));
            System.debug('UserInfo.getUserId=============>'+UserInfo.getUserId());
            User userRecord = [SELECT ManagerId, Woil_Support_User__c, id FROM User where id = : UserInfo.getUserId()] ;
            List<AccountTeamMember> listofAccountTeamMember = [SELECT User.Name, User.ManagerId, UserId 
                                                               FROM AccountTeamMember  
                                                               where AccountId = : accID];
            
            set<String> Userinfo=new set<String>();
            for(AccountTeamMember AtM: listofAccountTeamMember){
                Userinfo.add(AtM.UserId);
            }
            UrManager = [SELECT Name, id, ManagerId, Manager.Name FROM User where id IN : Userinfo ] ;
            System.debug(UrManager);
            return UrManager;
            
        }else{
            //Org---run only 2 step
            User userRecord = [SELECT ManagerId, Woil_Support_User__c, id FROM User where id = : UserInfo.getUserId()] ;
            approver1 = userRecord.Woil_Support_User__c;
            approver2 = userRecord.ManagerId;
            approvers.add(userRecord.Woil_Support_User__c);
            approvers.add(userRecord.ManagerId);
            approvers.add(String.valueOf(IsCommunity));
        }
        return UrManager;
    }
    
    
    @AuraEnabled
    public static String ApprovelProccessForTP(String firstApprover, String secondApprover, String thirdApprover, String RecordId) {
        approver1 = (Id)firstApprover;
        approver2 = (Id)secondApprover;
        //approver3 = (Id)thirdApprover;
        System.debug(recordId);
        User UrManager = [SELECT ManagerId, id FROM User where id = : firstApprover] ;
        //
        Outlet_Master__c approval_Outlet = [SELECT Id, Name, Woil_Status__c, Woil_First_Approver__c, 
                                            Woil_Second_Approver__c, Woil_Third_Approver__c, Woil_Outlet_Name__c, ownerId 
                                            FROM Outlet_Master__c where id= : recordId];
        
        Id Submiter_id = approval_Outlet.ownerId;
        if(approver1 !=null){
            approval_Outlet.Woil_First_Approver__c = approver1;
        }
        if(approver2 !=null){
            approval_Outlet.Woil_Second_Approver__c = approver2;
        }
        if(UrManager.ManagerId !=null){
            approval_Outlet.Woil_Third_Approver__c =  UrManager.ManagerId;
        }
        
        update approval_Outlet;
        try{
            //send Approvel request-------------
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setComments('Submitting request for approve Outlet');
            approvalRequest.setObjectId(recordId); //Outlet
            approvalRequest.setSubmitterId(Submiter_id); //LOgin User 
            approvalRequest.setNextApproverIds(new Id[] {approver1});//1st approver
            //System.debug('Approval.isLocked(recordId)======1======>'+Approval.isLocked(recordId));
            Approval.ProcessResult result = Approval.process(approvalRequest);
            
            //System.debug('Approval.isLocked(recordId)======2======>'+Approval.isLocked(recordId));
            System.debug(result.isSuccess());
            System.debug(result.getErrors());
        }
        catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            
            return  e.getMessage();
        }
        return 'success';
    }
    
    @AuraEnabled
    public static String ApprovelProccessForOrg(String recordId) {
        System.debug(recordId);
        Outlet_Master__c approval_Outlet = [SELECT Id, Name, Woil_Status__c, Woil_First_Approver__c, 
                                            Woil_Second_Approver__c, Woil_Third_Approver__c, Woil_Outlet_Name__c, ownerId 
                                            FROM Outlet_Master__c where id= : recordId];
        
        Id Submiter_id = approval_Outlet.ownerId;
        if(approver1 !=null){
            approval_Outlet.Woil_First_Approver__c = approver1;
        }
        if(approver2 !=null){
            approval_Outlet.Woil_Second_Approver__c = approver2;
        }
        update approval_Outlet;
        try{
            //send Approvel request-------------
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setComments('Submitting request for approve Outlet');
            approvalRequest.setObjectId(approval_Outlet.Id); //Outlet
            approvalRequest.setSubmitterId(Submiter_id); //LOgin User 
            approvalRequest.setNextApproverIds(new Id[] {approver1});//Id if there is any Apex approval process,else null
            Approval.ProcessResult result = Approval.process(approvalRequest);
            System.debug(result.isSuccess());
            System.debug(result.getErrors());
        }
        catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            if(approval_Outlet.Woil_First_Approver__c !=null){
                approval_Outlet.Woil_First_Approver__c = '';
            }
            if(approval_Outlet.Woil_Second_Approver__c !=null){
                approval_Outlet.Woil_Second_Approver__c = '';
            }
            update approval_Outlet;
            return  e.getMessage();
        }
        return 'success';
    }
}