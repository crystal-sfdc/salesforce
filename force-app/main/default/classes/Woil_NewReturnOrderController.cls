public with sharing class Woil_NewReturnOrderController {

     public static Map<String, String> poWrapperAPIName = new Map<String, String>{'customerName' => 'Woil_Account__c',
                                                                                    'shipToAddress' => 'Woil_ShipToAddress__c',
                                                                                    'status' => 'Woil_Status__c','division' => 'Woil_Division__c','supplierCode' => 'Woil_Supplier_Code__c','supplierName' => 'Woil_Supplier_Name__c','poExpiryDate' => 'Woil_Delivery_Date__c','reason' => 'Woil_Reason__c','deliveryAddress'=>'Woil_Delivery_Address__c','supllier'=>'Woil_Supplier__c'};

    @AuraEnabled
    public static Woil_NewPurchaseOrderControllerWrapper getPORelatedInformation(){


        Map<String,String> currentSession = Auth.SessionManagement.getCurrentSession();
        Boolean isLogCommunityUser = false;
        if(currentSession.get('LoginHistoryId')==null && currentSession.get('SourceIp')=='::' && currentSession.get('UserType')=='Standard'){
            isLogCommunityUser=true;
        }

        List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
        if(!userList.isEmpty() && userList[0].AccountId != null && userList[0].ContactId != null && isLogCommunityUser){

            List<String> recordTypeList = new List<String>{'Direct_Dealer','Distributor','Spare_Part_Distributor'};
            String ordType = '';

            Map<String,String> customerDetailMap = new Map<String,String>();
            Map<String,String> shipToAddressMap = new Map<String,String>();
            List<WOIL_Buyer_Seller_Mapping__c> buyerList = new List<WOIL_Buyer_Seller_Mapping__c>();
            Map<String,String> divisionMap = new Map<String,String>();
            Map<String,String> reasonMap = new Map<String,String>();
			reasonMap=getReasonFieldValue();
            Account address=[select BillingStreet, BillingCity, BillingState, BillingPostalCode,
                             BillingCountry from Account where id=:userList[0].AccountId];
            //String comp_addr=(address.BillingStreet!=null?address.BillingStreet+',':'')+(address.BillingCity!=null?address.BillingCity+',':'')+(address.BillingCountry!=null?address.BillingCountry:'');
            List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName, 
                                     (SELECT Id,Name FROM ShipToAddress__r),
                                     (SELECT Id,Woil_Division__c FROM Account_Plants__r), 
                                     (SELECT Id, WOIL_Seller__c, WOIL_Seller__r.Woil_Account_Number__c,WOIL_Seller__r.Woil_PAN_Number__c, WOIL_Seller__r.Name,
                                      WOIL_Seller__r.BillingStreet,WOIL_Seller__r.BillingCity,WOIL_Seller__r.BillingState,WOIL_Seller__r.BillingPostalCode,WOIL_Seller__r.BillingCountry
                                      FROM Buyer_Seller_Mappings__r) 
                                      FROM Account 
                                      Where Id =: userList[0].AccountId];

            for(Account acc : accList){
                customerDetailMap.put(acc.Name,acc.Id);
                for(Woil_ShipToAddress__c shipToAddress : acc.ShipToAddress__r){
                    shipToAddressMap.put(shipToAddress.Name,shipToAddress.Id);
                }
                for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : acc.Buyer_Seller_Mappings__r){
                    buyerList.add(buyerSellerRec);
                }
                for(Woil_Account_Plant__c accountPlantRec : acc.Account_Plants__r){
                    if(accountPlantRec.Woil_Division__c == 'AG'){
                        divisionMap.put('Finish Good', 'Finish Good');
                    }else{
                        divisionMap.put('Spare Part', 'Spare Part');
                    }
                }
            }
            
            String comp_addr=buyerList[0].WOIL_Seller__r.BillingStreet+buyerList[0].WOIL_Seller__r.BillingCity+buyerList[0].WOIL_Seller__r.BillingState+buyerList[0].WOIL_Seller__r.BillingPostalCode+buyerList[0].WOIL_Seller__r.BillingCountry;

            
			System.debug('buyerList[0].WOIL_Seller__r.Woil_Account_Number__c------'+buyerList[0].WOIL_Seller__r.Woil_Account_Number__c);
            if(recordTypeList.contains(accList[0].RecordType.developerName)){
                ordType = 'Primary Order';
            }
            /*List<Woil_Default_Value_Configuration__mdt> poDateList = [SELECT Id, Woil_Default_Value__c, Label 
                                                                    FROM Woil_Default_Value_Configuration__mdt 
                                                                    WHERE DeveloperName = 'Po_Expiry_Date'];*/
            
            Date poDate = Date.today() ;
            
            if(buyerList.size() > 0){
                return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,buyerList[0].WOIL_Seller__r.Name,buyerList[0].WOIL_Seller__r.Woil_Account_Number__c, poDate,ordType,divisionMap,true,'',reasonMap,comp_addr,buyerList[0].WOIL_Seller__c);
            }
            return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,null,null, poDate,ordType,divisionMap,true,'',reasonMap,comp_addr,null);
            
            
        }else {
            System.debug('In Else');
            Map<String,String> customerDetailMap = new Map<String,String>{'None' => 'None'};
            List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName 
                                        FROM Account];

            for(Account acc : accList){
                customerDetailMap.put(acc.Name,acc.Id);
            }

            return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,null,null,null,null, null,null,null,false,'',null,null,null); 
        }
    }
    
     @AuraEnabled
    public static Woil_NewPurchaseOrderControllerWrapper fetchPoRelatedInfo(){
		User usr=[Select ContactId from User where id=:UserInfo.getUserId()];
        List<Contact> contact=[Select AccountId from Contact where id=:usr.ContactId];
        String accId=contact[0].AccountId;
        List<String> recordTypeList = new List<String>{'Direct_Dealer','Distributor','Spare_Part_Distributor'};
        String ordType = '';

        Map<String,String> shipToAddressMap = new Map<String,String>();
        List<WOIL_Buyer_Seller_Mapping__c> buyerList = new List<WOIL_Buyer_Seller_Mapping__c>();
        Map<String,String> divisionMap = new Map<String,String>();

        Map<String,String> customerDetailMap = new Map<String,String>{'None' => 'None'};
		Map<String,String> reasonMap = new Map<String,String>();
        for(Account acc : [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName FROM Account]){
            customerDetailMap.put(acc.Name,acc.Id);
        }
        /*Account address=[select BillingStreet, BillingCity, BillingState, BillingPostalCode,
        BillingCountry from Account where id=:accId];*/
        reasonMap=getReasonFieldValue();

        List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName, 
                                    (SELECT Id,Name FROM ShipToAddress__r),
                                    (SELECT Id,Woil_Division__c FROM Account_Plants__r), 
                                    (SELECT Id, WOIL_Seller__c, WOIL_Seller__r.Woil_Account_Number__c,WOIL_Seller__r.Woil_PAN_Number__c, WOIL_Seller__r.Name, 
                                    WOIL_Seller__r.BillingStreet,WOIL_Seller__r.BillingCity,WOIL_Seller__r.BillingState,WOIL_Seller__r.BillingPostalCode,WOIL_Seller__r.BillingCountry
                                     FROM Buyer_Seller_Mappings__r) 
                                    FROM Account 
                                    Where Id =: accId];

        for(Account acc : accList){
            for(Woil_ShipToAddress__c shipToAddress : acc.ShipToAddress__r){
                shipToAddressMap.put(shipToAddress.Name,shipToAddress.Id);
            }
            for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : acc.Buyer_Seller_Mappings__r){
                buyerList.add(buyerSellerRec);
            }
            for(Woil_Account_Plant__c accountPlantRec : acc.Account_Plants__r){
                if(accountPlantRec.Woil_Division__c == 'AG'){
                    divisionMap.put('Finish Good', 'Finish Good');
                }else{
                    divisionMap.put('Spare Part', 'Spare Part');
                }
            }
        }
        
        String comp_addr=buyerList[0].WOIL_Seller__r.BillingStreet+buyerList[0].WOIL_Seller__r.BillingCity+buyerList[0].WOIL_Seller__r.BillingState+buyerList[0].WOIL_Seller__r.BillingPostalCode+buyerList[0].WOIL_Seller__r.BillingCountry;

        if(recordTypeList.contains(accList[0].RecordType.developerName)){
            ordType = 'Primary Order';
        }
        List<Woil_Default_Value_Configuration__mdt> poDateList = [SELECT Id, Woil_Default_Value__c, Label 
                                                                FROM Woil_Default_Value_Configuration__mdt 
                                                                WHERE DeveloperName = 'Po_Expiry_Date'];
        
        Date poDate = Date.today().addDays(integer.valueof(poDateList[0].Woil_Default_Value__c)) ;

        System.debug('customerDetailMap-----'+customerDetailMap);
        System.debug('shipToAddressMap------'+shipToAddressMap);
        System.debug('accList----'+accList);
        System.debug('accList[0].Woil_Account_Number__c------'+accList[0].Woil_Account_Number__c);
        System.debug('buyerList-------'+buyerList);
        System.debug('buyerList[0].WOIL_Seller__r.Name-----'+buyerList[0].WOIL_Seller__r.Name);
        System.debug('buyerList[0].WOIL_Seller__r.Woil_Account_Number__c------'+buyerList[0].WOIL_Seller__r.Woil_Account_Number__c);
        System.debug('poDate----'+poDate);
        System.debug('ordType------'+ordType);
        System.debug('divisionMap------'+divisionMap);
        System.debug('accId-----'+accId);
        System.debug('reasonMap-----'+reasonMap);

        
        if(buyerList.size() > 0){
        	return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,buyerList[0].WOIL_Seller__r.Name,buyerList[0].WOIL_Seller__r.Woil_Account_Number__c, poDate,ordType,divisionMap,false,accId,reasonMap,comp_addr,buyerList[0].WOIL_Seller__c);
        }
        return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,buyerList[0].WOIL_Seller__r.Name,buyerList[0].WOIL_Seller__r.Woil_Account_Number__c, poDate,ordType,divisionMap,false,accId,reasonMap,comp_addr,buyerList[0].WOIL_Seller__c);
                
    }
    
    public class Woil_NewPurchaseOrderControllerWrapper{
        @AuraEnabled public Map<String,String> customerDetailMap {get;set;}
        @AuraEnabled public Map<String,String> shipToAddressMap {get;set;}
        @AuraEnabled public Map<String,String> divisionMap {get;set;}
        @AuraEnabled public String billToCode {get;set;}
        @AuraEnabled public String supplierName {get;set;}
        @AuraEnabled public String supplierCode {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public Date poExpiryDate {get;set;}
        @AuraEnabled public Date effectDate {get;set;}
        @AuraEnabled public String ordType {get;set;}
        @AuraEnabled public String customerName {get;set;}
        @AuraEnabled public String shipToAddress {get;set;}
        @AuraEnabled public String division {get;set;}
        @AuraEnabled public Boolean isCommunity {get;set;}
        @AuraEnabled public Map<String,String> reasonMap {get;set;}
        @AuraEnabled public String reason {get;set;}
        @AuraEnabled public String deliveryAddress {get;set;}
        @AuraEnabled public String supllier {get;set;}
        

        public Woil_NewPurchaseOrderControllerWrapper(Map<String,String> customerDetailMap, Map<String,String> shipToAddressMap, String billToCode,String supplierName,String supplierCode, Date poExpiryDate,String ordType,Map<String,String> divisionMap, Boolean isCommunity,String customerName,Map<String,String> reasonMap,String deliveryAddress,String supllier){
            this.customerDetailMap = customerDetailMap;
            this.shipToAddressMap = shipToAddressMap;
            this.divisionMap = divisionMap;
            this.billToCode = billToCode;
            this.supplierName = supplierName;
            this.status = 'Draft';
            this.supplierCode = supplierCode;
            this.poExpiryDate = poExpiryDate;
            this.effectDate = Date.today();
            this.ordType = ordType;
            this.customerName = customerName;
            this.shipToAddress = '';
            this.division = '';
            this.isCommunity = isCommunity;
            this.reasonMap=reasonMap;
            this.reason='';
            this.deliveryAddress=deliveryAddress;
            this.supllier=supllier;

        }

        public Woil_NewPurchaseOrderControllerWrapper(){}
       
    }
    
     @AuraEnabled
    public static String createNewPoRecord(String poDetailString){
	System.debug('poDetailString::'+poDetailString);
        //try{
        
        Map<String,Object> poDetailMap =  (Map<String,Object>) JSON.deserializeUntyped(poDetailString);
        System.debug('poDetailMap::'+poDetailMap);
        Woil_SRN__c ord = new Woil_SRN__c();
        ord.Woil_Return_Order_Date__c=Date.today();
        ord.Woil_Return_Type__c= 'Return Order';
        for(String poStr : poWrapperAPIName.keySet()){              
            if(poWrapperAPIName.get(poStr)=='Woil_Delivery_Date__c'){
                ord.put(poWrapperAPIName.get(poStr),Date.valueOf(String.valueOf(poDetailMap.get(poStr))));
            }else{
                ord.put(poWrapperAPIName.get(poStr),poDetailMap.get(poStr)); 
            }	                
        }
        System.debug('ord::'+ord);
        insert ord;
        
        return 'Success#'+ord.Id;
        /*}catch(Exception ex){
            return 'error#'+ex.getMessage();
        }*/
                
    }
     @AuraEnabled 
    public static Map<String, String> getReasonFieldValue(){
        Map<String, String> options = new Map<String, String>();
        
        Schema.DescribeFieldResult fieldResult = Woil_SRN__c.Woil_Reason__c.getDescribe();
        
        List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pValues) {
            
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
}