public with sharing class Woil_NewPurchaseOrderController {

    public static Map<String, String> poWrapperAPIName = new Map<String, String>{'customerName' => 'AccountId','billToCode' => 'Woil_Bill_to_Code__c',
                                                                                    'poExpiryDate' => 'PoDate','supplierName' => 'Woil_Supplier_Name__c',
                                                                                    'supplierCode' => 'Woil_Supplier_Code__c','shipToAddress' => 'Woil_ShipToAddress__c',
                                                                                    'status' => 'Status','effectDate' => 'EffectiveDate',
                                                                                    'ordType' => 'Type','division' => 'Woil_Division__c'};

    @AuraEnabled
    public static Woil_NewPurchaseOrderControllerWrapper getPORelatedInformation(){


        Map<String,String> currentSession = Auth.SessionManagement.getCurrentSession();
        Boolean isLogCommunityUser = false;
        if(currentSession.get('LoginHistoryId')==null && currentSession.get('SourceIp')=='::' && currentSession.get('UserType')=='Standard'){
            isLogCommunityUser=true;
        }

        List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
        if(!userList.isEmpty() && userList[0].AccountId != null && userList[0].ContactId != null && isLogCommunityUser){

            List<String> recordTypeList = new List<String>{'Direct_Dealer','Distributor','Spare_Part_Distributor'};
            String ordType = '';

            Map<String,String> customerDetailMap = new Map<String,String>();
            Map<String,String> shipToAddressMap = new Map<String,String>();
            List<WOIL_Buyer_Seller_Mapping__c> buyerList = new List<WOIL_Buyer_Seller_Mapping__c>();
            Map<String,String> divisionMap = new Map<String,String>();

            List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName, 
                                     (SELECT Id,Name FROM ShipToAddress__r),
                                     (SELECT Id,Woil_Division__c FROM Account_Plants__r), 
                                     (SELECT Id, WOIL_Seller__c, WOIL_Seller__r.Woil_Account_Number__c,WOIL_Seller__r.Woil_PAN_Number__c, WOIL_Seller__r.Name 
                                      FROM Buyer_Seller_Mappings__r) 
                                      FROM Account 
                                      Where Id =: userList[0].AccountId];
            
            
        //Record Type Info    
       String reType = [ SELECT Id, Name, DeveloperName FROM RecordType where Id =: accList[0].RecordTypeId].Name;
        System.debug(reType);
            

            for(Account acc : accList){
                customerDetailMap.put(acc.Name,acc.Id);
                for(Woil_ShipToAddress__c shipToAddress : acc.ShipToAddress__r){
                    shipToAddressMap.put(shipToAddress.Name,shipToAddress.Id);
                }
                for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : acc.Buyer_Seller_Mappings__r){
                    buyerList.add(buyerSellerRec);
                }
                for(Woil_Account_Plant__c accountPlantRec : acc.Account_Plants__r){
                    if(accountPlantRec.Woil_Division__c == 'AG'){
                        divisionMap.put('Finish Good', 'Finish Good');
                    }else{
                        divisionMap.put('Spare Part', 'Spare Part');
                    }
                }
            }

            if(recordTypeList.contains(accList[0].RecordType.developerName)){
                ordType = 'Primary Order';
            }
            List<Woil_Default_Value_Configuration__mdt> poDateList = [SELECT Id, Woil_Default_Value__c, Label 
                                                                    FROM Woil_Default_Value_Configuration__mdt 
                                                                    WHERE DeveloperName = 'Po_Expiry_Date'];
            
            Date poDate = Date.today().addDays(integer.valueof(poDateList[0].Woil_Default_Value__c)) ;
            
            if(buyerList.size() > 0){
                return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,buyerList[0].WOIL_Seller__r.Name,buyerList[0].WOIL_Seller__r.Woil_Account_Number__c, poDate,ordType,divisionMap,true,reType,'');
            }
            return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,null,null, poDate,ordType,divisionMap,true,reType,'');
            
            
        }else {
            System.debug('In Else');
            Map<String,String> customerDetailMap = new Map<String,String>{'None' => 'None'};
            List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName 
                                        FROM Account];

            for(Account acc : accList){
                customerDetailMap.put(acc.Name,acc.Id);
            }

            return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,null,null,null,null, null,null,null,false,'',''); 
        }
    }

    @AuraEnabled
    public static String createNewPoRecord(String poDetailString){

        try{
        
            Map<String,Object> poDetailMap =  (Map<String,Object>) JSON.deserializeUntyped(poDetailString);
            Order ord = new Order();
            for(String poStr : poWrapperAPIName.keySet()){
                if(poWrapperAPIName.get(poStr) == 'EffectiveDate' || poWrapperAPIName.get(poStr) == 'PoDate'){
                    ord.put(poWrapperAPIName.get(poStr),Date.valueOf(String.valueOf(poDetailMap.get(poStr))));
                }else{
                    ord.put(poWrapperAPIName.get(poStr),poDetailMap.get(poStr));
                }
            }
    
            insert ord;
    
            return 'Success#'+ord.Id;
        }catch(Exception ex){
            return 'error#'+ex.getMessage();
        }
                
    }

    @AuraEnabled
    public static Woil_NewPurchaseOrderControllerWrapper fetchPoRelatedInfo(String accId){

        List<String> recordTypeList = new List<String>{'Direct_Dealer','Distributor','Spare_Part_Distributor'};
        String ordType = '';

        Map<String,String> shipToAddressMap = new Map<String,String>();
        List<WOIL_Buyer_Seller_Mapping__c> buyerList = new List<WOIL_Buyer_Seller_Mapping__c>();
        Map<String,String> divisionMap = new Map<String,String>();

        Map<String,String> customerDetailMap = new Map<String,String>{'None' => 'None'};

        for(Account acc : [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName FROM Account]){
            customerDetailMap.put(acc.Name,acc.Id);
        }

        List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName, RecordTypeId ,
                                    (SELECT Id,Name FROM ShipToAddress__r),
                                    (SELECT Id,Woil_Division__c FROM Account_Plants__r), 
                                    (SELECT Id, WOIL_Seller__c, WOIL_Seller__r.Woil_Account_Number__c,WOIL_Seller__r.Woil_PAN_Number__c, WOIL_Seller__r.Name 
                                    FROM Buyer_Seller_Mappings__r) 
                                    FROM Account 
                                    Where Id =: accId];

       String reType = [ SELECT Id, Name, DeveloperName FROM RecordType where Id =: accList[0].RecordTypeId].Name;
        System.debug(reType);
        
        for(Account acc : accList){
            for(Woil_ShipToAddress__c shipToAddress : acc.ShipToAddress__r){
                shipToAddressMap.put(shipToAddress.Name,shipToAddress.Id);
            }
            for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : acc.Buyer_Seller_Mappings__r){
                buyerList.add(buyerSellerRec);
            }
            for(Woil_Account_Plant__c accountPlantRec : acc.Account_Plants__r){
                if(accountPlantRec.Woil_Division__c == 'AG'){
                    divisionMap.put('Finish Good', 'Finish Good');
                }else{
                    divisionMap.put('Spare Part', 'Spare Part');
                }
            }
        }

        if(recordTypeList.contains(accList[0].RecordType.developerName)){
            ordType = 'Primary Order';
        }
        List<Woil_Default_Value_Configuration__mdt> poDateList = [SELECT Id, Woil_Default_Value__c, Label 
                                                                FROM Woil_Default_Value_Configuration__mdt 
                                                                WHERE DeveloperName = 'Po_Expiry_Date'];
        
        Date poDate = Date.today().addDays(integer.valueof(poDateList[0].Woil_Default_Value__c)) ;

        System.debug('customerDetailMap-----'+customerDetailMap);
        System.debug('shipToAddressMap------'+shipToAddressMap);
        System.debug('accList----'+accList);
        System.debug('accList[0].Woil_Account_Number__c------'+accList[0].Woil_Account_Number__c);
        System.debug('buyerList-------'+buyerList);
        System.debug('buyerList[0].WOIL_Seller__r.Name-----'+buyerList[0].WOIL_Seller__r.Name);
        System.debug('buyerList[0].WOIL_Seller__r.Woil_Account_Number__c------'+buyerList[0].WOIL_Seller__r.Woil_Account_Number__c);
        System.debug('poDate----'+poDate);
        System.debug('ordType------'+ordType);
        System.debug('divisionMap------'+divisionMap);
        System.debug('accId-----'+accId);

        
        if(buyerList.size() > 0){
        	return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,buyerList[0].WOIL_Seller__r.Name,buyerList[0].WOIL_Seller__r.Woil_Account_Number__c, poDate,ordType,divisionMap,false,reType,accId);
        }
        return new Woil_NewPurchaseOrderControllerWrapper(customerDetailMap,shipToAddressMap,accList[0].Woil_Account_Number__c,buyerList[0].WOIL_Seller__r.Name,buyerList[0].WOIL_Seller__r.Woil_Account_Number__c, poDate,ordType,divisionMap,false,reType,accId);
                
    }

    public class Woil_NewPurchaseOrderControllerWrapper{
        @AuraEnabled public Map<String,String> customerDetailMap {get;set;}
        @AuraEnabled public Map<String,String> shipToAddressMap {get;set;}
        @AuraEnabled public Map<String,String> divisionMap {get;set;}
        @AuraEnabled public String billToCode {get;set;}
        @AuraEnabled public String supplierName {get;set;}
        @AuraEnabled public String supplierCode {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public Date poExpiryDate {get;set;}
        @AuraEnabled public Date effectDate {get;set;}


        @AuraEnabled public String ordType {get;set;}
        @AuraEnabled public String customerName {get;set;}
        @AuraEnabled public String shipToAddress {get;set;}
        @AuraEnabled public String division {get;set;}
                //recordTypeInfo-------------------------
       @AuraEnabled public String RecodTypeName {get;set;}
        @AuraEnabled public Boolean isCommunity {get;set;}

        public Woil_NewPurchaseOrderControllerWrapper(Map<String,String> customerDetailMap, Map<String,String> shipToAddressMap, String billToCode,String supplierName,String supplierCode, Date poExpiryDate,String ordType,Map<String,String> divisionMap, Boolean isCommunity,String reType,String customerName){
            this.customerDetailMap = customerDetailMap;
            this.shipToAddressMap = shipToAddressMap;
            this.divisionMap = divisionMap;
            this.billToCode = billToCode;
            this.supplierName = supplierName;
            this.status = 'Draft';
            this.supplierCode = supplierCode;
            this.poExpiryDate = poExpiryDate;
            this.effectDate = Date.today();
            this.ordType = ordType;
            this.customerName = customerName;
            this.shipToAddress = '';
            this.division = '';
            this.RecodTypeName = reType;
            this.isCommunity = isCommunity;

        }

        public Woil_NewPurchaseOrderControllerWrapper(){}
       
    }
}