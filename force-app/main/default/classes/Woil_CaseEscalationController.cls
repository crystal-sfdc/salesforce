global class Woil_CaseEscalationController {

@AuraEnabled()
global static string getTicketCreation(String strCategory, String strSubCategory, String strDescription, String recordTypeSelect,String jsonuploadfilearray){

    System.debug('Class is called');
    String strCategoryValue = strCategory;
    String strSubCategoryValue = strSubCategory;
    String strDescriptionValue = strDescription;
    String strRecordTypeValue = recordTypeSelect;
    List<String> upload = new List<String>();
    System.debug('File Upload content:'+jsonuploadfilearray);
    List<ResponseWrapper> resp = new List<ResponseWrapper>();
    resp = (List<ResponseWrapper>)JSON.deserialize(jsonuploadfilearray,List<ResponseWrapper>.class);
    System.debug('File Upload JSON content:'+resp);
   
    /*if(!uploadedFiles.isEmpty()){
        for(String str:uploadedFiles){
            upload.add(str);
        }
    }*/
    //System.debug('Upload file str: '+upload);
    
    List<Id> userId = new List<Id>();
    List<String> userEmailList = new List<String>();
    List <String> sendTo = new List<String>();
    String resultreturn;
    String developerName;

    System.debug('strCategory: '+ strCategoryValue + ' strSubCategory:'+ strSubCategoryValue);
    if(String.isNotBlank(strCategoryValue) && String.isNotBlank(strSubCategoryValue)){

        Woil_Case_Escalation__c objCaseEscalation = [select id,Name,Woil_Department__c,Woil_Level_0_role__c,Woil_Level_1_role__c,Woil_Level_2_role__c,Woil_Level_3_role__c,Woil_Ticket_Creation__c from Woil_Case_Escalation__c where Woil_Category__c =: strCategoryValue and Woil_Sub_Category__c =: strSubCategoryValue and RecordTypeId =: strRecordTypeValue];                    
        System.debug('Ticket creation is '+ objCaseEscalation.Woil_Ticket_Creation__c);

        developerName = Schema.SObjectType.Woil_Case_Escalation__c.getRecordTypeInfosById().get(strRecordTypeValue).getDeveloperName();
        system.debug('developerName '+developerName);
        /* String userName = UserInfo.getUserName();
        User activeUser = [Select id,Email,ContactId From User where Username = : userName limit 1];
        String userEmail = activeUser.Email;
        system.debug('Active User'+activeUser);*/
        
        id userId1 = UserInfo.getUserId();
        system.debug('userId'+userId1);
        User u = [select id, contactId from User where id = : userId1];
        id getContactId = u.contactId;
        system.debug('getContactId' + getContactId);
    
        Id accID = [select Id,AccountID from contact where id =: getContactId].AccountID;
        system.debug('accID '+accID);

        //Account acc = [select Id,Name from Account where OwnerID =:activeUser.id limit 1];

        if(objCaseEscalation.Woil_Ticket_Creation__c =='No'){
            
            if(String.isNotBlank(objCaseEscalation.Woil_Level_0_role__c)){
            List<String> roleList = objCaseEscalation.Woil_Level_0_role__c.split(';');
            system.debug('Rolelist'+roleList);
            String replyObj = Label.Woil_replyTo;    
            if(!roleList.isEmpty()){
                List<AccountTeamMember> accTeamMember = [Select id,TeamMemberRole,UserId,AccountAccessLevel from AccountTeamMember where AccountId =: accID and TeamMemberRole IN: roleList];
                system.debug('AccTeamMember: '+accTeamMember);

                if(!accTeamMember.isEmpty()){

                    for(AccountTeamMember accTeam: accTeamMember){
                        userId.add(accTeam.UserId);    
                    }
                    system.debug('UserID'+userId);
                    List<User> userlist =[Select id, Email,Woil_User_Email__c from User where Id =: userId ];
                    system.debug('UserList'+userlist);    
                    if(!userlist.isEmpty()){
                        
                        for(User usr : userlist){
                            userEmailList.add(usr.Woil_User_Email__c);
                                
                        }
                        system.debug('userEmailList'+userEmailList);
                        if(!userEmailList.isEmpty()){

                            for(String emails : userEmailList){
                                sendTo.addAll(emails.split(','));
                            }
                            system.debug('sendTo '+ sendTo);
                            List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            //List <String> sendTo = userEmailList.split(',');
                            mail.setToAddresses(sendTo);
                            mail.setReplyTo(replyObj);
                            mail.setSenderDisplayName('Salesforce User');
                            mail.setSubject('Test Mail');

                            String body = 'Dear ' + ', ';
                            body += 'This is a test email from apex.';
                            
                            mail.setHtmlBody(body);
                            //mail.setHtmlBody('Test mail from apex class');
                            mails.add(mail);
                            system.debug('Email is sent');
                            resultreturn ='No';
                        }
                    }    
                    
                }
            }
        }
            

        }

        if(objCaseEscalation.Woil_Ticket_Creation__c =='Yes'){
            String replyObj = Label.Woil_replyTo; 
            system.debug('In case creation yes');
            String recName;
            Case caseObj = new Case();
            caseObj.woil_category__c = strCategoryValue;
            caseObj.woil_sub_category__c = strSubCategoryValue;
            if(String.isNotBlank(strDescriptionValue)){
                caseObj.Woil_Description__c = strDescriptionValue;            
            }
            if(developerName == 'Spares_Part'){
                recName = 'AS';
            }else if(developerName =='Finish_Good'){
                recName = 'AG';
            }
            
            caseObj.AccountId = accID;

            caseObj.Woil_Case_Escalation_RecordTypeId__c = recName;
            caseObj.Woil_Case_Escalation__c = objCaseEscalation.Id;

            System.debug('CaseObj'+caseObj);

            Database.SaveResult srList =  Database.insert(caseObj,false);
            if (srList.isSuccess()) {
                List<ContentDocumentLink> conDoc = new List<ContentDocumentLink>();
                if(!resp.isEmpty()){
                    for(ResponseWrapper rep:resp){
                        //rep.name
                        ContentDocumentLink conDocLink = New ContentDocumentLink();
                        conDocLink.LinkedEntityId = caseObj.id;
                        conDocLink.ContentDocumentId = rep.documentId;
                        conDocLink.shareType = 'V';
                        conDoc.add(conDocLink);
                    }
                    System.debug('ConDoc'+conDoc);
                    if(!conDoc.isEmpty()){
                        insert conDoc;
                    }
                }
                system.debug('In ticket creation success insert'+caseObj.Id);
                if(String.isNotBlank(objCaseEscalation.Woil_Level_0_role__c)){
                List<String> roleList = objCaseEscalation.Woil_Level_0_role__c.split(';');
                system.debug('Rolelist'+roleList);

                if(!roleList.isEmpty()){
                    List<AccountTeamMember> accTeamMember = [Select id,TeamMemberRole,UserId,AccountAccessLevel from AccountTeamMember where AccountId =: accID and TeamMemberRole IN: roleList];
                    system.debug('AccTeamMember: '+accTeamMember);

                    if(!accTeamMember.isEmpty()){

                        for(AccountTeamMember accTeam: accTeamMember){
                            userId.add(accTeam.UserId);    
                        }
                        system.debug('userId'+userId);
                        List<User> userlist =[Select id, Email,Woil_User_Email__c from User where Id =: userId ];
                        system.debug('UserList'+userlist);
                        if(!userlist.isEmpty()){
                            
                            for(User usr : userlist){
                                userEmailList.add(usr.Woil_User_Email__c);
                                
                            }
                            system.debug('userEmailList'+userEmailList);
                            if(!userEmailList.isEmpty()){    
                                for(String emails : userEmailList){
                                    sendTo.addAll(emails.split(','));
                                }
                                system.debug('sendTo '+ sendTo);
                                List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                //List <String> sendTo = userEmailList.split(',');
                                mail.setToAddresses(sendTo);
                                mail.setReplyTo(replyObj);
                                mail.setSenderDisplayName('Salesforce User');
                                mail.setSubject('Test Mail');

                                String body = 'Dear ' + ', ';
                                body += 'This is a test email from apex.';
                                
                                mail.setHtmlBody(body);
                                //mail.setHtmlBody('Test mail from apex class');
                                mails.add(mail);
                                Messaging.sendEmail(mails);
                                /*Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
                                if(results[0].isSuccess()) {
                                    List<EmailMessage> msg = [SELECT Id, ParentId FROM EmailMessage WHERE ToAddress IN:sendTo];
                                    List<EmailMessage> emailMes = new List<EmailMessage>();
                                    for(EmailMessage ms : msg){
                                        ms.ParentId = caseObj.Id;
                                        emailMes.add(ms);
                                    }

                                    if(!emailMes.isEmpty()){
                                        update emailMes;
                                    }
                                }*/

                                system.debug('Email is sent from ticket creation yes');  
                                
                                System.debug('CaseObj1'+caseObj);
                                String caseNum = [Select Id,CaseNumber from Case where ID =: caseObj.Id].CaseNumber;
                                resultreturn ='Yes' +' '+ caseNum;
                            }    
                    }
                        
                    }
                }
            }
                
            }
            else{
                for(Database.Error err : srList.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                    resultreturn = err.getMessage(); 
                }

            }
        
        }
        
    }
    return resultreturn;

}

global class ResponseWrapper{

    global string name;
    global string documentId;
    global string contentVersionId;
    
  }
}