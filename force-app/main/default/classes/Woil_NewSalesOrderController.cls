public with sharing class Woil_NewSalesOrderController {
    
    public static Map<String, String> soWrapperAPIName = new Map<String, String>{'address' => 'Woil_Outlet_Address__c',
        'customerName' => 'Woil_Acount__c','division' => 'Woil_Division__c',
        'outletStr' => 'Woil_Outlet__c','shipToAddress' => 'Woil_ShipToAddress__c',
        'status' => 'Woil_Status__c','DeliveryDate' => 'Woil_Delivery_Date__c'};
            
            @AuraEnabled
            public static Woil_NewSalesOrderControllerWrapper getSubDealerInfo(){
                List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
                if(!userList.isEmpty() && userList[0].AccountId != null && userList[0].ContactId != null){
                    List<Account> accRelatedList = [SELECT Id,  
                                                    (SELECT Id,Name FROM ShipToAddress__r), 
                                                    (SELECT Id,Woil_Division__c FROM Account_Plants__r),
                                                    (SELECT Id,Parent__r.Name,Parent__r.Woil_Account_Number__c, WOIL_Seller__r.Name, Outlet__c, Outlet__r.Name,Outlet__r.Woil_Address__c FROM Buyer_Seller_Mappings1__r WHERE (Status__c = 'Active' OR Status__c = 'Approval pending')) 
                                                    FROM Account 
                                                    WHERE Id =: userList[0].AccountId];
                    
                    Map<String,String> shipToAddressMap = new Map<String,String>();
                    Map<String,String> buyerSeller = new Map<String,String>{'None' => 'None'};
                        Map<String,String> buyerSellerCode = new Map<String,String>();
                    Map<String,String> divisionMap = new Map<String,String>();
                    Map<String,Map<String,String>> outletMap = new Map<String,Map<String,String>>();
                    Map<String,String> outletAddress = new Map<String,String>();
                    
                    for(Account acc : accRelatedList){
                        for(Woil_ShipToAddress__c shipToAddress : acc.ShipToAddress__r){
                            shipToAddressMap.put(shipToAddress.Name,shipToAddress.Id);
                        }
                        for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : acc.Buyer_Seller_Mappings1__r){
                            buyerSeller.put(buyerSellerRec.Parent__r.Name,buyerSellerRec.Parent__r.Id);
                            buyerSellerCode.put(buyerSellerRec.Parent__r.Id,buyerSellerRec.Parent__r.Woil_Account_Number__c);
                            if(!outletMap.containsKey(buyerSellerRec.Parent__r.Id)){
                                outletMap.put(buyerSellerRec.Parent__r.Id,new Map<String,String>());
                            }
                            Map<String,String> tempOutMap = outletMap.get(buyerSellerRec.Parent__r.Id);
                            tempoutMap.put(buyerSellerRec.Outlet__r.Name,buyerSellerRec.Outlet__c);
                            outletMap.put(buyerSellerRec.Parent__r.Id,tempoutMap);
                            
                            if(buyerSellerRec.Outlet__r.Woil_Address__c == null || buyerSellerRec.Outlet__r.Woil_Address__c == ''){
                                outletAddress.put(buyerSellerRec.Outlet__c,'No Address Found');
                            }else{
                                outletAddress.put(buyerSellerRec.Outlet__c,buyerSellerRec.Outlet__r.Woil_Address__c);
                            }
                            
                        }
                        for(Woil_Account_Plant__c accountPlantRec : acc.Account_Plants__r){
                            if(accountPlantRec.Woil_Division__c == 'AG'){
                                divisionMap.put('Finish Good', 'Finish Good');
                            }/*else{
divisionMap.put('Spare Part', 'Spare Part');
}*/
                        }
                    }
                    
                    return new Woil_NewSalesOrderControllerWrapper(shipToAddressMap,buyerSeller,buyerSellerCode,divisionMap,outletMap,outletAddress);
                }
                return new Woil_NewSalesOrderControllerWrapper();
            }
    
    @AuraEnabled
    public static String createNewSoRecord(String sODetailString){
        try{
            Map<String,Object> soDetailMap =  (Map<String,Object>) JSON.deserializeUntyped(sODetailString);
            Woil_Sales_Order__c salesOrder = new Woil_Sales_Order__c();
            
            for(String soStr : soWrapperAPIName.keySet()){
                if(soWrapperAPIName.get(soStr) == 'Woil_Delivery_Date__c'){
                    salesOrder.put(soWrapperAPIName.get(soStr),Date.valueOf(String.valueOf(soDetailMap.get(soStr))));
                }else{
                    salesOrder.put(soWrapperAPIName.get(soStr),soDetailMap.get(soStr));
                }
                
            }
            salesOrder.put('Woil_Order_Type__c','Secondary Order');
            System.debug('sales order information-----------------------------------------' +   salesOrder);
            Integer TotalCount = 0;
            WOIL_Buyer_Seller_Mapping__c buerSeller = [SELECT Status__c, Id, Parent__c, Outlet__c FROM WOIL_Buyer_Seller_Mapping__c where Outlet__c = :salesOrder.Woil_Outlet__c AND Parent__c = :salesOrder.Woil_Acount__c];
            if(buerSeller.Status__c == 'Approval pending'){
               TotalCount = [select count() from Woil_Sales_Order__c where Woil_Acount__c= :salesOrder.Woil_Acount__c AND Woil_Outlet__c=:salesOrder.Woil_Outlet__c];
                
            }
            if(TotalCount > 0){
                
                return 'BuyerError1234';
            }
            else{
                insert salesOrder;
                
                return 'Success#'+salesOrder.Id;
            }
            
        }catch(Exception ex){
            return 'error#'+ex.getMessage();
        }
        
    }
    
    public class Woil_NewSalesOrderControllerWrapper{
        @AuraEnabled public Map<String,String> shipToAddressMap {get;set;}
        @AuraEnabled public String shipToAddress {get;set;}
        @AuraEnabled public Map<String,String> buyerSeller {get;set;}
        @AuraEnabled public String customerName {get;set;}
        @AuraEnabled public Map<String,String> buyerSellerCode {get;set;}
        @AuraEnabled public String buyerCode {get;set;}
        @AuraEnabled public Date soDate {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public Map<String,String> divisionMap {get;set;}
        @AuraEnabled public String division {get;set;}
        @AuraEnabled public Map<String,Map<String,String>> outletMap {get;set;}
        @AuraEnabled public String outletStr {get;set;}
        @AuraEnabled public Map<String,String> outletAddress {get;set;}
        @AuraEnabled public String address {get;set;}
        @AuraEnabled public Date DeliveryDate {get;set;}
        
        public Woil_NewSalesOrderControllerWrapper(Map<String,String> shipToAddressMap, Map<String,String> buyerSeller, Map<String,String> buyerSellerCode,Map<String,String> divisionMap,Map<String,Map<String,String>> outletMap,Map<String,String> outletAddress){
            this.shipToAddressMap = shipToAddressMap;
            this.buyerSeller = buyerSeller;
            this.buyerSellerCode = buyerSellerCode;
            this.soDate = System.Today();
            this.DeliveryDate = System.Today();
            this.customerName = '';
            this.shipToAddress = '';
            this.status = 'Open';
            this.divisionMap = divisionMap;
            this.division = '';
            this.buyerCode = '';
            this.outletMap = outletMap;
            this.outletAddress = outletAddress;
            this.outletStr = '';
            this.address = '';
            
        }
        
        public Woil_NewSalesOrderControllerWrapper(){}
    }
}