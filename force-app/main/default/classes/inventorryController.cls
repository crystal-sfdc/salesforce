global class inventorryController{
    @AuraEnabled
    global static Map<String,ID> getProductList(String categoryStr) {
        system.debug('Inside Product Method');
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, contactId from User where id = : userId];
        id getContactId = u.contactId;
        system.debug('getContactId' + getContactId);
        
        Id accId = [select Id,AccountID from contact where id =: getContactId].AccountId;
        system.debug('accID'+accID);
        
        Map<String,ID> result = new Map<String,ID>();
        List<Woil_Partner_Product_Mapping__c> partnerProductMapping = [Select Woil_Product__r.name from Woil_Partner_Product_Mapping__c where Woil_Customer_Name__c=:accId and Woil_Product__r.Woil_Category__c =:categoryStr];
        for(Woil_Partner_Product_Mapping__c eachMapping : partnerProductMapping){
            
            result.put(eachMapping.Woil_Product__r.name, eachMapping.Woil_Product__c);
            // system.debug(eachMapping.Woil_Product__r.name+' '+ eachMapping.Woil_Product__c);
        }
        return result;
    }
    @AuraEnabled(cacheable = true)
    global static String getDivisionList() {
        List<Woil_Account_Plant__c> accPlantList= new List<Woil_Account_Plant__c>();
        List<String> divisionList= new List<String>();
        Set<RecordTypeInfo> recordTypeNameList = new Set<RecordTypeInfo>();
        
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, AccountId, contactId from User where id = : userId];
        
        accPlantList = [SELECT Id,Woil_CustomerName__c,Woil_Division__c FROM Woil_Account_Plant__c 
                        WHERE Woil_Trade_Partner__c=:u.AccountId];
        if(!accPlantList.isEmpty()){
            
            Schema.DescribeSObjectResult R = Product2.SObjectType.getDescribe();
            List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();   
            for(Woil_Account_Plant__c accPl: accPlantList) {
                
                if(accPl.Woil_Division__c == 'AG'){
                    divisionList.add('Finished Goods');
                    for( Schema.RecordTypeInfo recordType : RT )
                    {
                        if(recordType.isAvailable())
                        { 
                            if(recordType.Name == 'Finished Goods') {
                                recordTypeNameList.add(recordType);
                            }
                        }
                    }    
                }
                else{
                    divisionList.add('Spare Parts');
                    for( Schema.RecordTypeInfo recordType : RT )
                    {
                        if(recordType.isAvailable())
                        { 
                            if(recordType.Name == 'Spare Parts') {
                                recordTypeNameList.add(recordType);
                            }
                        }
                    }
                }
            }
        }
        
        return JSON.serialize(recordTypeNameList);
    }
    
   
    
    @AuraEnabled
    public static List<dataValueWrapper> getSerialDetails (ID productId){
        System.debug('productID---->'+productId);
        if(productID == null){
            throw new AuraHandledException('Please select any product');
            
        }
        
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, contactId from User where id = : userId];
        id getContactId = u.contactId;
        system.debug('getContactId' + getContactId);
        
        Id accId = [select Id,AccountID from contact where id =: getContactId].AccountId;
        System.debug('account id--->'+accId);
        List<dataValueWrapper> dataList = new List<dataValueWrapper>();
        AggregateResult[] arList = [select count(id) from Woil_Serial_Number_Owner__c where Woil_Product__r.Name =:productId and Woil_Customer__c =:accId and Transit_Status__c IN ('Opening Balances','Sell-In','Return') ];
        System.debug('Number of count--->'+arList.size());
        List<Woil_Serial_Number_Owner__c> serialOwnerList = [select id,Woil_WareHouse_Location__c, Woil_Product__r.Description from Woil_Serial_Number_Owner__c where Woil_Product__r.Name =:productId and Woil_Customer__c =:accId and Transit_Status__c IN ('Opening Balances','Sell-In','Return')];
        System.debug('serialOwnerList--->'+serialOwnerList);
        if(!serialOwnerList.isEmpty()){
            for(Woil_Serial_Number_Owner__c serOwner : serialOwnerList){
            	dataValueWrapper objData = new dataValueWrapper();
                objData.warehouseLocation = serOwner.Woil_WareHouse_Location__c;
                objData.materialCodeDesc = serOwner.Woil_Product__r.Description;
                objData.onHandStock = arList.size();
                dataList.add(objData);
            }    
        }
        return dataList;
        /*
        
        List<Woil_Serial_Number_Transaction__c> serialList = [Select Woil_Warehouse_Location__c,
                                                              Woil_Transaction_Date__c,
                                                              Woil_Invoice__r.name,
                                                              Product_Description__c,
                                                              Woil_Serial_Number__c,
                                                              Woil_Quantity__c,
                                                              Woil_Current_Customer_Code__c,
                                                              Woil_Serial_Transaction_Type__c
                                                              from Woil_Serial_Number_Transaction__c 
                                                              where 
                                                              Woil_Product__c =:productId ];
        
        List<Woil_Serial_Number_Transaction__c> result = new List<Woil_Serial_Number_Transaction__c>();
        double count=0;
        for(Woil_Serial_Number_Transaction__c each: serialList){
            if(each!=null && each.Woil_Quantity__c!=null){
                count+=each.Woil_Quantity__c;
            }
        }
        
        //create result list
        for(Woil_Serial_Number_Transaction__c each: serialList){
            Woil_Serial_Number_Transaction__c obj = new Woil_Serial_Number_Transaction__c();
            obj.Woil_Warehouse_Location__c= each.Woil_Warehouse_Location__c;
            
            obj.Woil_Transaction_Date__c = each.Woil_Transaction_Date__c;
            obj.Woil_Invoice__c = each.Woil_Invoice__c;
            
            obj.Woil_Serial_Transaction_Type__c = each.Woil_Serial_Transaction_Type__c;
            obj.Woil_Current_Customer_Code__c = each.Woil_Current_Customer_Code__c;
            obj.Woil_Serial_Number__c = each.Woil_Serial_Number__c;
            obj.SKU_Description__c = each.Product_Description__c;
            //obj.Woil_Product__r.name=each.Woil_Product__r.name;
            obj.Woil_Quantity__c=count;
            result.add(obj);
        }
        
        
        return result;
        
        */
    }       
    
    public class dataValueWrapper{
    	@auraEnabled public string warehouseLocation {get;set;}
        @auraEnabled public string materialCodeDesc {get;set;}
        @auraEnabled public Integer onHandStock {get;set;}
        @auraEnabled public string onOrder {get;set;}
        
    }
    
}