public class DisplayImageApex {
    
    @AuraEnabled
    public static ImageWrapper getImage(){
        ImageWrapper iw= new ImageWrapper();
        
        String sfdcModifiedBaseURL = URL.getOrgDomainUrl().toExternalForm();
        sfdcModifiedBaseURL = sfdcModifiedBaseURL.removeEndIgnoreCase('.my.salesforce.com');
        iw.baseURL= sfdcModifiedBaseURL;
        
        Id organisationId = [SELECT InstanceName, id, Name FROM Organization].id;
        iw.orgID= organisationId;
        Id cd = [SELECT Id FROM contentDocument WHERE FileType='JPG'].id;
        iw.fileID= cd;
        //ContentDocument cd= [SELECT id, Title, FileType,CreatedBy.Name,ContentSize From contentDocument WHERE FileType='JPG'];
        
        return iw;
    }
    
     public class ImageWrapper{
         @AuraEnabled
        public string baseURL { get; set;}
         @AuraEnabled
        public Id orgID  { get; set;}
         @AuraEnabled
         public Id fileID {get; set;}
    }
}