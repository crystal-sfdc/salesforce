global class Woil_inventorryController{
    @AuraEnabled
    global static Map<String,ID> getProductList(String categoryStr,String divisionStr) {
        system.debug('Inside Product Method');
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, contactId,AccountId from User where id = : userId];
        
        string recordtypename = Schema.SObjectType.Product2.getRecordTypeInfosById().get(divisionStr).getname();
        System.debug('recordtypename--->'+recordtypename);
        Map<String,ID> result = new Map<String,ID>();
        Set<Id> prodId = new Set<Id>();
        if(recordtypename == 'Finished Goods'){
            List<Woil_Partner_Product_Mapping__c> partnerProductMapping = [Select id,Woil_Product__c,Woil_Product__r.name from Woil_Partner_Product_Mapping__c where Woil_Customer_Name__c=:u.AccountId and Woil_Product__r.Woil_Category__c =:categoryStr and Woil_Product__r.RecordTypeId =:divisionStr];
            system.debug('partnerProductMapping--->'+partnerProductMapping);
            if(!partnerProductMapping.isEmpty()){
                for(Woil_Partner_Product_Mapping__c partProd: partnerProductMapping){
                    prodId.add(partProd.Woil_Product__c);       
                }    
            }
            
            List<Woil_Account_Plant__c> accPlanList = [SELECT Id,Woil_CustomerName__c,Woil_Division__c,Woil_Plant_Master__c FROM Woil_Account_Plant__c 
                                                       WHERE Woil_Trade_Partner__c=:u.AccountId and Woil_Division__c ='AG' ];
            
            System.debug('accPlanList--->'+accPlanList);
            System.debug('prodId--->'+prodId);
            
            List<Woil_Plant_Product_Mapping__c> plantProductList = [select id,Woil_Product__c,Woil_Product__r.Name,Woil_Plant_Master__c from Woil_Plant_Product_Mapping__c where Woil_Product__c IN: prodId and Woil_Plant_Master__c =:accPlanList[0].Woil_Plant_Master__c ];
            System.debug('plantProductList--->'+plantProductList);
            if(!plantProductList.isEmpty()){
                for(Woil_Plant_Product_Mapping__c eachMapping : plantProductList){
                    
                    result.put(eachMapping.Woil_Product__r.name, eachMapping.Woil_Product__c);
                    // system.debug(eachMapping.Woil_Product__r.name+' '+ eachMapping.Woil_Product__c);
                }
            }
        }
        else {
            List<Woil_Account_Plant__c> accPlanList = [SELECT Id,Woil_CustomerName__c,Woil_Division__c,Woil_Plant_Master__c FROM Woil_Account_Plant__c 
                                                       WHERE Woil_Trade_Partner__c=:u.AccountId and Woil_Division__c ='AS' ];
            
            System.debug('accPlanList AS--->'+accPlanList);
            System.debug('prodId--->'+prodId);
            
            List<Woil_Plant_Product_Mapping__c> plantProductList = [select id,Woil_Product__c,Woil_Product__r.Name,Woil_Plant_Master__c from Woil_Plant_Product_Mapping__c where Woil_Plant_Master__c =:accPlanList[0].Woil_Plant_Master__c ];
            System.debug('plantProductList--->'+plantProductList);
            if(!plantProductList.isEmpty()){
                for(Woil_Plant_Product_Mapping__c eachMapping : plantProductList){
                    
                    result.put(eachMapping.Woil_Product__r.name, eachMapping.Woil_Product__c);
                    // system.debug(eachMapping.Woil_Product__r.name+' '+ eachMapping.Woil_Product__c);
                }
            }
        }
        return result;
    }
    @AuraEnabled(cacheable = true)
    global static String getDivisionList() {
        List<Woil_Account_Plant__c> accPlantList= new List<Woil_Account_Plant__c>();
        List<String> divisionList= new List<String>();
        Set<RecordTypeInfo> recordTypeNameList = new Set<RecordTypeInfo>();
        
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, AccountId, contactId from User where id = : userId];
        
        accPlantList = [SELECT Id,Woil_CustomerName__c,Woil_Division__c FROM Woil_Account_Plant__c 
                        WHERE Woil_Trade_Partner__c=:u.AccountId];
        if(!accPlantList.isEmpty()){
            
            Schema.DescribeSObjectResult R = Product2.SObjectType.getDescribe();
            List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();   
            for(Woil_Account_Plant__c accPl: accPlantList) {
                
                if(accPl.Woil_Division__c == 'AG'){
                    divisionList.add('Finished Goods');
                    for( Schema.RecordTypeInfo recordType : RT )
                    {
                        if(recordType.isAvailable())
                        { 
                            if(recordType.Name == 'Finished Goods') {
                                recordTypeNameList.add(recordType);
                            }
                        }
                    }    
                }
                else{
                    divisionList.add('Spare Parts');
                    for( Schema.RecordTypeInfo recordType : RT )
                    {
                        if(recordType.isAvailable())
                        { 
                            if(recordType.Name == 'Spare Parts') {
                                recordTypeNameList.add(recordType);
                            }
                        }
                    }
                }
            }
        }
        
        return JSON.serialize(recordTypeNameList);
    }
    
    @AuraEnabled()
    global static Boolean checkProductisSerialized(Id productId) {
        System.debug('productID---->'+productId);   
        Boolean isSerial = true;
        isSerial = [select id,Woil_Is_Serializable__c from Product2 where Id=: productId].Woil_Is_Serializable__c;
        System.debug('isSerial---->'+isSerial);
        return isSerial;
    }
    
    
    
    @AuraEnabled
    public static List<dataValueWrapper> getSerialDetails (String productId,String categoryStr, String divisionStr){
        
        System.debug('categoryStr---->'+categoryStr);
        System.debug('divisionStr---->'+divisionStr);
        System.debug('productID---->'+productId);
        /*if(productID == null){
throw new AuraHandledException('Please select any product');

}*/
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, contactId from User where id = : userId];
        id getContactId = u.contactId;
        system.debug('getContactId' + getContactId);
        
        Id accId = [select Id,AccountID from contact where id =: getContactId].AccountId;
        System.debug('account id--->'+accId);
        List<dataValueWrapper> dataList = new List<dataValueWrapper>();
        Integer count;
        Integer countScan;
        
        Map<String,Integer> mapOfCount = new Map<String,Integer>();
        Map<String,Integer> mapOfCountonOrder = new Map<String,Integer>();
        Map<Id,Product2> mapOfProductDetails = new Map<Id,Product2>();
        Map<String,Integer> mapOfTransactionQuantity = new Map<String,Integer>();
        Map<String,String> mapOfBothCount = new Map<String,String>();//Map<String,String> mapOfWarehouse = new Map<String,String>();
        
        String squery = 'select id,Woil_WareHouse_Location__c,Woil_Product__r.Woil_Is_Serializable__c, Woil_Product__r.Description from Woil_Serial_Number_Owner__c where Woil_Product__r.RecordTypeId =:divisionStr and Woil_Customer__c =:accId and Transit_Status__c IN (\'Opening Balances\',\'Sell-In\',\'Return\')';
        if(String.isNotBlank(categoryStr) && categoryStr!= 'None'){
            squery += ' and Woil_Product__r.Woil_Category__c =:categoryStr';
        }
        if(String.isNotBlank(productID)){
            squery += ' and Woil_Product__c =:productID';
        }
        if(String.isBlank(categoryStr) || String.isBlank(productID) || productID == null){
            squery += ' limit 10000';    
        }
        
        System.debug('Full query ---->'+ squery);
        List<Woil_Serial_Number_Owner__c> serialOwnerList = Database.query(squery);
        
        //List<Woil_Serial_Number_Owner__c> serialOwnerList = [select id,Woil_WareHouse_Location__c, Woil_Product__r.Description from Woil_Serial_Number_Owner__c where Woil_Product__c =:productId and Woil_Customer__c =:accId and Transit_Status__c IN ('Opening Balances','Sell-In','Return')];
        System.debug('serialOwnerList--->'+serialOwnerList);
        
        String scanQuery ='select id,Woil_WareHouse_Location__r.Name,Woil_Product__r.Woil_Is_Serializable__c,Woil_WareHouse_Location__c,Woil_Product__r.Description from Woil_Serial_Number_Scan__c where Woil_Product__r.RecordTypeId =:divisionStr and Woil_Customer__c =:accId';
        if(String.isNotBlank(categoryStr) && categoryStr!= 'None'){
            system.debug('inside categoryStr-->'+categoryStr);
            scanQuery += ' and Woil_Product__r.Woil_Category__c =:categoryStr';
        }
        if(String.isNotBlank(productID)){
            scanQuery += ' and Woil_Product__c =:productID';
        }
        if(String.isBlank(categoryStr) || String.isBlank(productID) || productID == null || categoryStr!= 'None'){
            scanQuery += ' limit 10000';    
        }
        System.debug('Full scan query ---->'+ scanQuery);
        List<Woil_Serial_Number_Scan__c> serialScanList = Database.query(scanQuery);
        //List<Woil_Serial_Number_Scan__c> serialScanList = [select id,Woil_WareHouse_Location__r.Name,Woil_WareHouse_Location__c,Woil_Product__r.Description from Woil_Serial_Number_Scan__c where Woil_Product__c =:productId and Woil_Customer__c =:accId];
        System.debug('serialScanList--->'+serialScanList);
        
        List<Woil_Serial_Number_Transaction__c> serialTransList = [select id,Name, Woil_Customer__c, Woil_Product__c,Woil_Product__r.Description,Woil_Warehouse_Location__c,Woil_Quantity__c,Woil_Product__r.Woil_Is_Serializable__c from Woil_Serial_Number_Transaction__c where Woil_Product__c =:productId and Woil_Customer__c =:accId];
        System.debug('serialTransList--->'+serialTransList);
        
        if(!serialOwnerList.isEmpty()){
            for(Woil_Serial_Number_Owner__c serOwnID : serialOwnerList){
                
                mapOfProductDetails.put(serOwnID.Woil_Product__c,serOwnID.Woil_Product__r);
                String tempVar = serOwnID.Woil_Product__c + ','+ serOwnID.Woil_WareHouse_Location__c;
                System.debug('tempVar --->'+tempVar);
                if(mapOfCount.keySet().contains(tempVar)){
                    
                    count = mapOfCount.get(tempVar)+1;
                    mapOfCount.put(tempVar,count);
                    
                    String countStr = String.valueOf(count)+',';
                    mapOfBothCount.put(tempVar,countStr);
                    System.debug('mapOfCount-->'+mapOfCount);
                }
                else{
                    
                    String countStr = '1'+',';
                    mapOfBothCount.put(tempVar,countStr);
                    
                    mapOfCount.put(tempVar,1);
                    system.debug('mapOfCount else-->'+mapOfCount);
                }
                
                
            }
        }
        
        
        if(!serialScanList.isEmpty()){
            for(Woil_Serial_Number_Scan__c serScanID : serialScanList){
                String tempVar = serScanID.Woil_Product__c + ','+ serScanID.Woil_WareHouse_Location__r.Name;
                if(!mapOfProductDetails.keySet().contains(serScanID.Woil_Product__c)){
                    mapOfProductDetails.put(serScanID.Woil_Product__c,serScanID.Woil_Product__r);       
                }
                
                System.debug('tempVar serialowner--->'+tempVar);
                if(mapOfCountonOrder.keySet().contains(tempVar)){
                    
                    countScan = mapOfCountonOrder.get(tempVar)+1;
                    mapOfCountonOrder.put(tempVar,countScan);
                    if(mapOfBothCount.keySet().contains(tempVar)){
                        String temp = mapOfBothCount.get(tempVar).subStringBefore(',');
                        String countStr = temp+','+String.valueOf(countScan);
                        mapOfBothCount.put(tempVar,countStr); 
                    }
                    else{
                        String countStr = ','+String.valueOf(countScan);
                        mapOfBothCount.put(tempVar,countStr);
                    }
                    
                    System.debug('mapOfCountonOrder-->'+mapOfCountonOrder);        
                }
                else{
                    if(mapOfBothCount.keySet().contains(tempVar)){
                        String temp = mapOfBothCount.get(tempVar).subStringBefore(',');
                        String countStr = temp+','+'1';
                        mapOfBothCount.put(tempVar,countStr); 
                    }
                    else{
                        String countStr = ','+'1';
                        mapOfBothCount.put(tempVar,countStr);    
                    }
                    mapOfCountonOrder.put(tempVar,1);
                    system.debug('mapOfCountonOrder else-->'+mapOfCountonOrder);
                }
            }
        }
        
        if(!serialTransList.isEmpty()){
            
            for(Woil_Serial_Number_Transaction__c serTrans: serialTransList){
                if(!serTrans.Woil_Product__r.Woil_Is_Serializable__c){
                    System.debug('sertrans--->'+serTrans);    
                    String tempVar = serTrans.Woil_Product__c + ','+ serTrans.Woil_WareHouse_Location__c;
                    if(!mapOfProductDetails.keySet().contains(serTrans.Woil_Product__c)){
                        mapOfProductDetails.put(serTrans.Woil_Product__c,serTrans.Woil_Product__r);     
                    }    
                    if(mapOfTransactionQuantity.keySet().contains(tempVar)){
                        if(Integer.valueOf(mapOfTransactionQuantity.get(tempVar)+serTrans.Woil_Quantity__c)!=0){
                            Integer quant = Integer.valueOf(mapOfTransactionQuantity.get(tempVar)+serTrans.Woil_Quantity__c);
                            mapOfTransactionQuantity.put(tempVar,quant);    
                        }
                        
                    }
                    else{
                        mapOfTransactionQuantity.put(tempVar,Integer.valueOf(serTrans.Woil_Quantity__c));     
                    }
                }   
            }
        }
        
        
        System.debug('mapOfProductDetails-->'+mapOfProductDetails);
        System.debug('mapOfCount-->'+mapOfCount);
        System.debug('mapOfTransactionQuantity-->'+mapOfTransactionQuantity);
        
        
        if(!mapOfBothCount.keySet().isEmpty()){
            for(String str: mapOfBothCount.keySet()){
                Id productValue = str.subStringBefore(',');
                String warehouseValue = str.subStringAfter(','); 
                
                if(mapOfProductDetails.get(productValue).Woil_Is_Serializable__c){
                    String onHandTemp = mapOfBothCount.get(str).subStringBefore(',');
                    String onOrderTemp = mapOfBothCount.get(str).subStringAfter(','); 
                    
                    Integer onHandCount;
                    Integer onOrderCount;
                    
                    System.debug('onHandTemp---->'+onHandTemp);
                    System.debug('onOrderTemp----->'+onOrderTemp);
                    
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.warehouseLocation = warehouseValue;
                    objData.materialCodeDesc = mapOfProductDetails.get(productValue).Description;
                    if(String.isBlank(onHandTemp)){
                        onHandCount = 0;    
                    }else{
                        onHandCount = Integer.valueOf(onHandTemp);    
                    }
                    if(String.isBlank(onOrderTemp)){
                        onOrderCount = 0 ;   
                    }
                    else{
                        onOrderCount = Integer.valueOf(onOrderTemp);            
                    }
                    objData.onHandStock = onHandCount;
                    objData.onOrder = onOrderCount;
                    objData.totalAvailable = onHandCount + onOrderCount; 
                    dataList.add(objData);    
                }
            }
            
        }
        
        if(!mapOfTransactionQuantity.keySet().isEmpty()){
            for(String str: mapOfTransactionQuantity.keySet()){
                Id productValue = str.subStringBefore(',');
                String warehouseValue = str.subStringAfter(','); 
                
                
                if(!mapOfProductDetails.get(productValue).Woil_Is_Serializable__c){
                    
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.warehouseLocation = warehouseValue;
                    objData.materialCodeDesc = mapOfProductDetails.get(productValue).Description;
                    objData.onHandStock = mapOfTransactionQuantity.get(str);
                    objData.onOrder = 0;
                    objData.totalAvailable = mapOfTransactionQuantity.get(str);
                    dataList.add(objData);
                }
            }    
        }
        System.debug('dataList--->'+dataList);
        return dataList;
        
    }
    @AuraEnabled
    public static List<dataValueWrapper> getSerialOrder(String productId,String categoryStr, String divisionStr,List<dataValueWrapper> selectedDataRow){
        System.debug('productID---->'+productId);
        System.debug('selectedDataRow---->'+selectedDataRow);
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, contactId from User where id = : userId];
        id getContactId = u.contactId;
        system.debug('getContactId' + getContactId);
        
        Id accId = [select Id,AccountID from contact where id =: getContactId].AccountId;
        System.debug('account id--->'+accId);
        List<dataValueWrapper> dataList = new List<dataValueWrapper>();
        List<String> warehouseValue = new List<String>();
        Map<String,Woil_Serial_Number_Owner__c> mapOfSerialOwner = new Map<String,Woil_Serial_Number_Owner__c>();
        
        if(!selectedDataRow.isEmpty()){
            for(dataValueWrapper wrap: selectedDataRow){
                warehouseValue.add(wrap.warehouseLocation);    
            }
        }
        System.debug('warehouseValue--->'+warehouseValue);
        
        String serialQuery = 'select id,Woil_WareHouse_Location__c, Woil_Product__r.Description,Woil_Serial_Number__c,Woil_Customer__r.Woil_Account_Number__c,Transit_Status__c from Woil_Serial_Number_Owner__c where Woil_Product__r.RecordTypeId =:divisionStr and Woil_Customer__c =:accId and Transit_Status__c IN (\'Opening Balances\',\'Sell-In\',\'Return\')';
        if(String.isNotBlank(categoryStr) && categoryStr!= 'None'){
            serialQuery += ' and Woil_Product__r.Woil_Category__c =:categoryStr';
        }
        if(!warehouseValue.isEmpty()){
            serialQuery+= ' and Woil_WareHouse_Location__c IN: warehouseValue';
        }
        if(String.isNotBlank(productID)){
            serialQuery += ' and Woil_Product__c =:productID';
        }
        
        if(String.isBlank(categoryStr) || String.isBlank(productID) || productID == null || categoryStr!= 'None'){
            serialQuery += ' limit 10000';    
        }
        
        System.debug('Full serial query ---->'+ serialQuery);
        List<Woil_Serial_Number_Owner__c> serialOwnerList = Database.query(serialQuery);
        
        
        //List<Woil_Serial_Number_Owner__c> serialOwnerList = [select id,Woil_WareHouse_Location__c, Woil_Product__r.Description,Woil_Serial_Number__c,Woil_Customer__r.Woil_Account_Number__c,Transit_Status__c from Woil_Serial_Number_Owner__c where Woil_Product__c =:productId and Woil_Customer__c =:accId and Transit_Status__c IN ('Opening Balances','Sell-In','Return')];
        system.debug('serialOwnerList--->'+serialOwnerList);
        
        if(!serialOwnerList.isEmpty()){
            for(Woil_Serial_Number_Owner__c serOwn : serialOwnerList){
                mapOfSerialOwner.put(serOwn.Woil_Serial_Number__c,serOwn);    
            }
        }
        if(!mapOfSerialOwner.keySet().isEmpty()){
            for(String str : mapOfSerialOwner.keySet()){
                dataValueWrapper objData = new dataValueWrapper();
                objData.warehouseLocation = mapOfSerialOwner.get(str).Woil_WareHouse_Location__c;
                objData.materialCodeDesc = mapOfSerialOwner.get(str).Woil_Product__r.Description;
                objData.distributorCode = mapOfSerialOwner.get(str).Woil_Customer__r.Woil_Account_Number__c;
                objData.serialNumber = mapOfSerialOwner.get(str).Woil_Serial_Number__c;
                objData.transitStatus = mapOfSerialOwner.get(str).Transit_Status__c;
                dataList.add(objData);
            }    
        }
        
        return dataList;
    }
    
    @AuraEnabled
    public static List<dataValueWrapper> getTransactionDetails (String productId,String categoryStr, String divisionStr){
        System.debug('productID---->'+productId);
        
        if(productID == null || productID == ''){
            throw new AuraHandledException('Please select any product');

        }
        id userId = UserInfo.getUserId();
        system.debug('userId'+userId);
        User u = [select id, contactId,AccountId from User where id = : userId];
        id getContactId = u.contactId;
        Id accId = [select Id,AccountID from contact where id =: getContactId].AccountId;
        
        List<dataValueWrapper> dataList = new List<dataValueWrapper>();
        
        String serialTransQuery = 'select id, Woil_Product__r.Description,Woil_Product__r.Name,Woil_Order__r.OrderNumber,Woil_Purchase_Return__r.Name,Woil_Sales_Order__r.Name,Woil_Invoice__r.Name,Woil_Transaction_Date__c,Woil_Warehouse_Location__c,Woil_Serial_Number__c,Woil_Serial_Transaction_Type__c,Woil_Quantity__c,Woil_Opening_Stock__r.Name from Woil_Serial_Number_Transaction__c where Woil_Product__r.RecordTypeId =:divisionStr and Woil_Customer__c =:accId';
        if(String.isNotBlank(categoryStr) && categoryStr!= 'None'){
            serialTransQuery += ' and Woil_Product__r.Woil_Category__c =:categoryStr';
        }
        if(String.isNotBlank(productID)){
            serialTransQuery += ' and Woil_Product__c =:productID';
        }
        if(String.isBlank(categoryStr) || String.isBlank(productID) || productID == null || categoryStr!= 'None'){
            serialTransQuery += ' limit 10000';    
        }
        
        System.debug('Full serial query ---->'+ serialTransQuery);
        List<Woil_Serial_Number_Transaction__c> serTransList = Database.query(serialTransQuery);
        //List<Woil_Serial_Number_Transaction__c> serTransList = [select id, Woil_Product__r.Description,Woil_Product__r.Name,Woil_Order__r.OrderNumber,Woil_Purchase_Return__r.Name,Woil_Sales_Order__r.Name,Woil_Invoice__r.Name,Woil_Transaction_Date__c,Woil_Warehouse_Location__c,Woil_Serial_Number__c,Woil_Serial_Transaction_Type__c,Woil_Quantity__c,Woil_Opening_Stock__r.Name from Woil_Serial_Number_Transaction__c where Woil_Product__c =:productId AND Woil_Customer__c =:u.AccountId];
        system.debug('serTransList----->'+serTransList);
        
        if(!serTransList.isEmpty()){
            for(Woil_Serial_Number_Transaction__c serTrans: serTransList){
                if(serTrans.Woil_Serial_Transaction_Type__c == 'Purchase'){
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.transactionDate = serTrans.Woil_Transaction_Date__c;
                    objData.ReferenceId = serTrans.Woil_Order__r.OrderNumber;
                    objData.transactionType = serTrans.Woil_Serial_Transaction_Type__c;
                    objData.receipt = 'Stock In';
                    objData.issue = '';
                    objData.quantity = serTrans.Woil_Quantity__c;
                    dataList.add(objData);
                }
                else if(serTrans.Woil_Serial_Transaction_Type__c == 'Sales'){
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.transactionDate = serTrans.Woil_Transaction_Date__c;
                    objData.referenceId = serTrans.Woil_Invoice__r.Name;
                    objData.transactionType = serTrans.Woil_Serial_Transaction_Type__c;
                    objData.receipt = '';
                    objData.issue = 'Stock Out';
                    objData.quantity = serTrans.Woil_Quantity__c;
                    dataList.add(objData);      
                }
                else if(serTrans.Woil_Serial_Transaction_Type__c == 'Sales Return'){
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.transactionDate = serTrans.Woil_Transaction_Date__c;
                    objData.referenceId = serTrans.Woil_Invoice__r.Name;
                    objData.transactionType = serTrans.Woil_Serial_Transaction_Type__c;
                    objData.receipt = 'Stock In';
                    objData.issue = '';
                    objData.quantity = serTrans.Woil_Quantity__c;
                    dataList.add(objData);    
                }
                else if(serTrans.Woil_Serial_Transaction_Type__c == 'SellOut'){
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.transactionDate = serTrans.Woil_Transaction_Date__c;
                    objData.referenceId = serTrans.Woil_Invoice__r.Name;
                    objData.transactionType = serTrans.Woil_Serial_Transaction_Type__c;
                    objData.receipt = '';
                    objData.issue = 'Stock Out';
                    objData.quantity = serTrans.Woil_Quantity__c;
                    dataList.add(objData);    
                }
                else if(serTrans.Woil_Serial_Transaction_Type__c == 'Stock In'){
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.transactionDate = serTrans.Woil_Transaction_Date__c;
                    objData.referenceId = serTrans.Woil_Opening_Stock__r.Name;
                    objData.transactionType = serTrans.Woil_Serial_Transaction_Type__c;
                    objData.receipt = 'Stock In';
                    objData.issue = '';
                    objData.quantity = serTrans.Woil_Quantity__c;
                    dataList.add(objData);    
                }
                else if(serTrans.Woil_Serial_Transaction_Type__c == 'Purchase Return'){
                    dataValueWrapper objData = new dataValueWrapper();
                    objData.transactionDate = serTrans.Woil_Transaction_Date__c;
                    objData.referenceId = serTrans.Woil_Purchase_Return__r.Name;
                    objData.transactionType = serTrans.Woil_Serial_Transaction_Type__c;
                    objData.receipt = '';
                    objData.issue = 'Stock Out';
                    objData.quantity = serTrans.Woil_Quantity__c;
                    dataList.add(objData);    
                }
            }    
        }
        return dataList;
    }    
    
    public class dataValueWrapper{
        @auraEnabled public string warehouseLocation {get;set;}
        @auraEnabled public string materialCodeDesc {get;set;}
        @auraEnabled public Integer onHandStock {get;set;}
        @auraEnabled public Integer onOrder {get;set;}
        @auraEnabled public Integer totalAvailable {get;set;}
        @auraEnabled public string distributorCode {get;set;}
        @auraEnabled public string serialNumber {get;set;}
        @auraEnabled public string transitStatus {get;set;}
        @auraEnabled public Date transactionDate {get;set;}
        @auraEnabled public String referenceId {get;set;}
        @auraEnabled public String transactionType {get;set;}
        @auraEnabled public String receipt {get;set;}
        @auraEnabled public String issue {get;set;}
        @auraEnabled public Decimal quantity {get;set;}
    }
    
}