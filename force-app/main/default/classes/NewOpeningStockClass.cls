global class NewOpeningStockClass {
    
    @AuraEnabled
    public static List<String> getDivision() {
        
        List<Woil_Account_Plant__c> accPlantList= new List<Woil_Account_Plant__c>();
        List<String> divisionList= new List<String>();
        
          id userId = UserInfo.getUserId();
system.debug('userId'+userId);
User u = [select id, AccountId, contactId from User where id = : userId];
id getContactId = u.contactId;
system.debug('getContactId' + getContactId);

Id accId = [select Id,AccountID from contact where id =: getContactId].AccountId;
system.debug('accID'+accID);
        
       // Id accId= '0019D00000GDDYxQAP';
        
        accPlantList = [SELECT Id,Woil_CustomerName__c,Woil_Division__c FROM Woil_Account_Plant__c 
                        WHERE Woil_Trade_Partner__c=:u.AccountId];
        
        for(Woil_Account_Plant__c accPl: accPlantList) {
            
            if(accPl.Woil_Division__c == 'AG'){
                divisionList.add('Finished Good');
            }else{
                divisionList.add('Spare Part');
            }
        }
        
        return divisionList;
        
    }
    
    @AuraEnabled
    public static String saveNewRecord(String descValue, String divValue, Boolean isSeriValue, Date stockDate) {
        System.debug('Description'+descValue);
        System.debug('Division'+divValue);
        System.debug('IsSerialized'+isSeriValue);
        System.debug('stockDate'+stockDate);
        
        
        Woil_Opening_Stock__c opObj= new Woil_Opening_Stock__c();
        opObj.Woil_Description__c= descValue;
        opObj.Woil_Division__c= divValue;
        opObj.Woil_isSerialized__c= isSeriValue;
        opObj.Woil_Opening_Stock_Date__c= stockDate;
        
        try{
            insert opObj;  
             return 'Success#'+opObj.Id;
        }
        catch (Exception e){
            return 'error#'+e.getMessage();
        }    
    }
    
}