public without sharing class Woil_InvoicedSalesOrderController {
    @AuraEnabled
    public static string checkStatusController(String recordId){
        try {
            Woil_Sales_Order__c so = [SELECT Id,Woil_Status__c,(SELECT Id FROM Serial_Numbers_Scan__r) FROM Woil_Sales_Order__c WHERE Id =: recordId];
            return so.Woil_Status__c+'#'+so.Serial_Numbers_Scan__r.size();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static string createInventoryController(String recordId){
        try {

            List<Woil_Serial_Number_Transaction__c> serialNumberTransactionList = new List<Woil_Serial_Number_Transaction__c>();
            List<String> SKUCodes=new List<String>();
            Set<String> serialNos=new Set<String>();

            Woil_Sales_Order__c salesOrder = [SELECT Id,Woil_Acount__c,Woil_Outlet__r.Name,Woil_ShipToAddress__r.Name 
                                                FROM Woil_Sales_Order__c 
                                                WHERE Id =: recordId];

            User custCode = [SELECT Id, AccountId,Account.Woil_Account_Number__c,Account.Name, ContactId, Profile.Name 
                        FROM USER WHERE ID =: UserInfo.getUserId()];

            
            List<Woil_Serial_Number_Scan__c> serialNumberList = [SELECT Id,Woil_Customer__c,Woil_Product__c,Woil_Product__r.Name,Woil_Serial_Number__c,Woil_Warehouse_Location__r.Name 
                                                                    FROM Woil_Serial_Number_Scan__c 
                                                                    WHERE Woil_Sales_Order__c =: recordId];

            for(Woil_Serial_Number_Scan__c snTrans : serialNumberList){
                SKUCodes.add(snTrans.Woil_Product__r.Name);
                serialNos.add(snTrans.Woil_Serial_Number__c);
            }

            List<Woil_Serial_Number_Owner__c> serial_txn = [SELECT Id,Woil_Product__r.Name,Woil_WareHouse_Location__c,Woil_Customer__c,Woil_Serial_Number__c,Woil_Product__c,Transit_Status__c 
                                                                FROM Woil_Serial_Number_Owner__c 
                                                                WHERE Woil_Product__r.Name IN: SKUCodes AND 
                                                                        Woil_Warehouse_Location__c =: salesOrder.Woil_ShipToAddress__r.Name AND 
                                                                        (Transit_Status__c='Opening Balances' OR Transit_Status__c = 'Sell-In' OR Transit_Status__c = 'Return') AND 
                                                                        Woil_Serial_Number__c IN : serialNos AND Woil_Customer__c=:custCode.AccountId];


            for(Woil_Serial_Number_Owner__c serial : serial_txn){
                serial.Woil_Customer__c = salesOrder.Woil_Acount__c;
                serial.Transit_Status__c = 'Sell-Through';
                serial.Woil_Warehouse_Location__c = salesOrder.Woil_Outlet__r.Name;
                serial.Woil_Counter_Customer__c = custCode.Account.Name;
                serial.Woil_Transaction_Date__c = System.Today();
            }


            
            for(Woil_Serial_Number_Scan__c snTrans : serialNumberList){
                Woil_Serial_Number_Transaction__c snt = new Woil_Serial_Number_Transaction__c();
                snt.Woil_Sales_Order__c = recordId;
                snt.Woil_Customer__c = snTrans.Woil_Customer__c;
                snt.Woil_Product__c = snTrans.Woil_Product__c;
                snt.Woil_Sales_Motion__c = 'Sell Through';
                snt.Woil_Serial_Number__c = snTrans.Woil_Serial_Number__c;
                snt.Woil_Serial_Transaction_Type__c = 'Sales';
                snt.Woil_Warehouse_Location__c = snTrans.Woil_Warehouse_Location__r.Name;
                snt.Woil_Quantity__c = -1;
                snt.Woil_Transaction_Date__c = System.Today();
                serialNumberTransactionList.add(snt);
            }
            
            for(Woil_Serial_Number_Scan__c snTrans : serialNumberList){
                Woil_Serial_Number_Transaction__c snt = new Woil_Serial_Number_Transaction__c();
                //snt.Woil_Sales_Order__c = salesOrder.Woil_Acount__c;
                snt.Woil_Customer__c = salesOrder.Woil_Acount__c;
                snt.Woil_Product__c = snTrans.Woil_Product__c;
                snt.Woil_Sales_Motion__c = 'Sell Through';
                snt.Woil_Serial_Number__c = snTrans.Woil_Serial_Number__c;
                snt.Woil_Serial_Transaction_Type__c = 'Purchase';
                snt.Woil_Warehouse_Location__c = salesOrder.Woil_Outlet__r.Name;
                snt.Woil_Quantity__c = 1;
                snt.Woil_Transaction_Date__c = System.Today();
                snt.Woil_Counter_Customer__c = custCode.AccountId;
                serialNumberTransactionList.add(snt);
            }

            if(!serialNumberTransactionList.isEmpty()){
                insert serialNumberTransactionList;
            }

            if(!serial_txn.isEmpty()){
                update serial_txn;
            }

            if(!serialNumberList.isEmpty()){
                delete serialNumberList;
            }

            Woil_Sales_Order__c salesOrd = new Woil_Sales_Order__c();
            salesOrd.Id = recordId;
            salesOrd.Woil_Status__c = 'Processed';

            update salesOrd;

            return 'Success';

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}