public class Woil_ContentVersionTriggerHandler {

    public static void isUploadAllowed(List<ContentVersion> newAttachList){
        
        // Check if user can upload file or not

        Schema.DescribeSObjectResult r1 = Woil_Consumer_Finance_Master__c.sObjectType.getDescribe();
        String keyPrefix1 = r1.getKeyPrefix();
        
        Schema.DescribeSObjectResult r2 = Woil_Scheme_Master__c.sObjectType.getDescribe();
        String keyPrefix2 = r2.getKeyPrefix();      
        
        String currentUserId = UserInfo.getUserId();
        User u = [SELECT Id, Profile.Name FROM User WHERE Id = :currentUserId];
        
        Set<String> allowedProfilesSet = new Set<String>();

        for (Woil_Default_Value_Configuration__mdt allowedProfileConfig : [SELECT MasterLabel, Woil_Default_Value__c FROM Woil_Default_Value_Configuration__mdt WHERE Label = 'CFM Attachment Upload Access']) {
            if(String.isNotBlank(allowedProfileConfig.Woil_Default_Value__c)){
                allowedProfilesSet.addAll(allowedProfileConfig.Woil_Default_Value__c.split(';'));
            }
        }
        
        for(ContentVersion attch : newAttachList){
            if(attch.FirstPublishLocationId != NULL && (String.valueOf(attch.FirstPublishLocationId).startsWith(keyPrefix1) || String.valueOf(attch.FirstPublishLocationId).startsWith(keyPrefix2))&& !allowedProfilesSet.contains(String.valueOf(u.Profile.Name))){
                attch.Title.addError('Cannot upload this file.');
            }
        }
    }
}