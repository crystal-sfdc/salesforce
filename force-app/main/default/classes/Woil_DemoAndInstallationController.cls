public without sharing class Woil_DemoAndInstallationController {
    @AuraEnabled
    public static String serachRequestNumber(String requestNumber){

        try{

            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:DemoAndInstallationIntegration'+'?Name='+requestNumber);
            req.setMethod('GET');
            req.setTimeout(120000);

            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug(res.getBody());
            System.debug('res.getStatusCode()----'+res.getStatusCode());

            if(res.getStatusCode() == 200){
            if(res.getBody().contains(requestNumber)){
                    Woil_DemoAndInstallationController reponseList = (Woil_DemoAndInstallationController)JSON.deserialize(res.getBody(), Woil_DemoAndInstallationController.class);
                    return JSON.serialize(reponseList.ServiceRequest);
            }else{
                    return 'Servic Request Number Not Found.';
            }
            }else if(res.getStatusCode() == 500){
                return 'Internal Server Error.';
            }
            return 'Something went Wrong.';
        }
        catch(Exception ex){
            return ex.getMessage();
        }
    }

    public List<cls_ServiceRequest> ServiceRequest{get;set;}
    
	class cls_ServiceRequest {
        public String ID {get;set;}
		public String Status {get;set;}
		public String ObjectID {get;set;}
		public String Ticket_CreationDate {get;set;}
		public String Ticket_UpdationDate {get;set;}
		public String Ticket_Type {get;set;}
		public String ObjectServiceIssueCategoryID {get;set;}
		public String ResolutionCategory {get;set;}
		public String ObjectCategoryName {get;set;}
		public String Custom1 {get;set;}
		public String Custom2 {get;set;}
        public String Name {get;set;}
	}
}