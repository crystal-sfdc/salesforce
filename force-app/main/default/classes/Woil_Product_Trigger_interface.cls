public interface Woil_Product_Trigger_interface {
    void beforeInsert(List<sObject> newRecordsList);
    
    void afterInsert(List<sObject> newRecordsList , Map<Id, sObject> newRecordsMap);
    
    void beforeUpdate(List<sObject> newRecord, Map<Id, sObject> oldRecordsMap);
    
    void afterUpdate(Map<Id, sObject> newRecordsMap,  Map<Id, sObject> oldRecordsMap);
    
    void beforeDelete(List<sObject> oldRecordsList , Map<Id, sObject> oldRecordsMap);
    
    void afterDelete(Map<Id, sObject> oldRecordsMap);
    
    void afterUnDelete(List<sObject> newRecordsList , Map<Id, sObject> newRecordsMap);


}