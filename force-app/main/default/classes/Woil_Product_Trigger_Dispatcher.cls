public with sharing class Woil_Product_Trigger_Dispatcher {
   public static void run(Woil_Product_Trigger_interface handler, String ObjectName){
      if(Trigger.isBefore && (Trigger.isInsert)){
            handler.BeforeInsert(Trigger.New);
        }
        
        if(Trigger.isAfter && Trigger.isInsert ){
            handler.AfterInsert(Trigger.New,Trigger.NewMap);
        }
        if(Trigger.isBefore && Trigger.isUpdate ){
            handler.BeforeUpdate(Trigger.New,Trigger.oldMap);
        }

       
    }
    }