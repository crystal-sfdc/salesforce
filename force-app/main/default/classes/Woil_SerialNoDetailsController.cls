public without sharing class  Woil_SerialNoDetailsController {
    public static Map<String,Woil_GRN_Line_Item__c>  SkuVsGRNLineItems=new Map<String,Woil_GRN_Line_Item__c>();
    public static Map<String,Woil_SRN_Line_Item__c>  SkuVsSRNLineItems=new Map<String,Woil_SRN_Line_Item__c>();
    public static List<Woil_GRN_Line_Item__c>  GRNLineItems=new List<Woil_GRN_Line_Item__c>();
    public static List<Woil_SRN_Line_Item__c>  SRNLineItems=new List<Woil_SRN_Line_Item__c>();
    public static Map<String,Woil_GRN_Serial_Number__c> GRNSerialNos=new Map<String,Woil_GRN_Serial_Number__c>();
    public static Set<Id> GRN_LI_Ids=new Set<Id>();
    public static List<Woil_GRN_Line_Item__c> Lst_GRN_LI=new List<Woil_GRN_Line_Item__c>();
    public static List<Woil_SRN_Line_Item__c> Lst_SRN_LI=new List<Woil_SRN_Line_Item__c>();
    public static List<Woil_GRN_Serial_Number__c> Lst_GRN_Serials=new List<Woil_GRN_Serial_Number__c>();
    public static List<Woil_SRN_Serial_Number__c> Lst_SRN_Serials=new List<Woil_SRN_Serial_Number__c>();
    public static  Set<Id> GRN_Serial_Ids=new Set<Id>();
    public static  Set<String> GRN_Serial_Nos=new Set<String>();
    public static List<Woil_Invoice_Serial_Number__c> Lst_invoice_Serials=new List<Woil_Invoice_Serial_Number__c>();
    
    @AuraEnabled
    public static List < Woil_Invoice_Serial_Number__c > getInvoiceItems(String invoice_no) {
        return [ SELECT Woil_Invoice_Line_Item__r.Woil_Unit_Price__c,Woil_Invoice_Line_Item__r.Woil_Product__r.Woil_Product_Type__c,Id, Woil_Invoice_Line_Item__r.Woil_Invoice__r.name,Woil_Invoice_Line_Item__r.Woil_Material_Code__c,Woil_Serial_Number__c,Woil_Invoice_Line_Item__r.Woil_Invoice__r.Woil_Account__c,Woil_Invoice_Line_Item__r.Woil_Invoice__r.Woil_Order__c,Woil_Invoice_Line_Item__r.Woil_Product__c,Woil_Invoice_Line_Item__r.Woil_Product__r.Name,Woil_Invoice_Line_Item__r.Woil_Product__r.Description FROM Woil_Invoice_Serial_Number__c where Woil_Invoice_Line_Item__r.Woil_Invoice__c=:invoice_no and Woil_Accepted__c=false and Woil_Invoice_Line_Item__r.Woil_Product__r.Woil_Is_Serializable__c=true];
    }
    
     @AuraEnabled
    public static List < Woil_GRN_Line_Item__c > getLineItems(String invoice_no) {
       System.debug([ SELECT Id, Woil_GRN__r.Woil_Sales_Invoice__r.Name,Woil_Product__r.Name,Woil_GRN__r.Name,Woil_Accepted_Quantity__c,Woil_GRN__r.Woil_Posted__c ,CreatedDate  FROM Woil_GRN_Line_Item__c where Woil_GRN__r.Woil_Sales_Invoice__c=:invoice_no ]);
        return [ SELECT Id, Woil_GRN__r.Woil_Sales_Invoice__r.Name,Woil_Product__r.Name,Woil_GRN__r.Name,Woil_Accepted_Quantity__c, Woil_GRN__r.Woil_Posted__c,CreatedDate,Woil_Accepted_Quantity_formula__c,Woil_Product__r.Description  FROM Woil_GRN_Line_Item__c where Woil_GRN__r.Woil_Sales_Invoice__c=:invoice_no ];
    }
    
    @AuraEnabled
    public static List < Woil_SRN_Line_Item__c > getSRNLineItems(String invoice_no) {
        return [ SELECT Id, Woil_SRN__r.Woil_Sales_Invoice__r.Name,Woil_Product__r.Name,Woil_SRN__r.Name,Woil_Rejected_Quantity__c,Woil_SRN__r.Woil_Posted__c,Woil_SRN__r.Woil_Reason__c,CreatedDate,Woil_Product__r.Description,Woil_Ordered_Quantity__c FROM Woil_SRN_Line_Item__c where Woil_SRN__r.Woil_Sales_Invoice__c=:invoice_no ];
    }
    
    @AuraEnabled
    public static void saveGRNItems(List<Woil_Invoice_Serial_Number__c> LstSerialNos ) {
        GRN__c grn=new GRN__c();
        grn.Woil_Purchase_Order__c=LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_Invoice__r.Woil_Order__c;
        grn.Woil_Sales_Invoice__c=LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_Invoice__c;
        grn.Woil_Account__c=LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_Invoice__r.Woil_Account__c;
        grn.Woil_Receiving_Type__c ='Manual';
        insert grn;
        
        //System.debug('LstSerialNos::::'+LstSerialNos);
        for(Woil_Invoice_Serial_Number__c SerialNo:LstSerialNos){
            
            //System.debug('Product::::'+SerialNo.Woil_Invoice_Line_Item__r.Woil_Material_Code__c);
            if(!SkuVsGRNLineItems.containsKey(SerialNo.Woil_Invoice_Line_Item__r.Woil_product__r.Name) ){
                Woil_GRN_Line_Item__c GRNLineItem=new Woil_GRN_Line_Item__c();
                GRNLineItem.Woil_Product__c	=SerialNo.Woil_Invoice_Line_Item__r.Woil_Product__c;
                GRNLineItem.Woil_GRN__c	=grn.Id;
                SkuVsGRNLineItems.put(SerialNo.Woil_Invoice_Line_Item__r.Woil_product__r.Name, GRNLineItem); 
                
            } 
           
        }
        GRNLineItems =SkuVsGRNLineItems.values();
        
        
        List<Database.saveResult> lstGRNLIResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+GRNLineItems);
        lstGRNLIResult = Database.insert(GRNLineItems,false);
        
        // Iterate SaveResult array
        for (Database.SaveResult result : lstGRNLIResult) {
            if (result.isSuccess()) {
                
                System.debug('GRNLI Successfully inserted, Account Id is: ' + result.getId());
                GRN_LI_Ids.add(result.getId());
            }
            else {
                //Error ecountered              
                for(Database.Error error : result.getErrors()) {
                    //Handle error
                    System.debug(error.getStatusCode() + ': ' + error.getMessage() + 
                                 ' Fields that affected the error: ' + error.getFields());                    
                }
            }
        }
        
        
        Lst_GRN_LI=[Select Id,Woil_Product__r.ProductCode from Woil_GRN_Line_Item__c where id=:GRN_LI_Ids];
        for(Woil_Invoice_Serial_Number__c GRN_SerialNos:LstSerialNos){
            for(Woil_GRN_Line_Item__c GRN_LI:Lst_GRN_LI){
                if(GRN_SerialNos.Woil_Invoice_Line_Item__r.Woil_Product__c==GRN_LI.Woil_Product__c){
                    Woil_GRN_Serial_Number__c GRN_Serial=new Woil_GRN_Serial_Number__c();
                    GRN_Serial.Woil_Invoice_Line_Item__c =GRN_LI.Id; 
                    GRN_Serial.Woil_Serial_Number__c=GRN_SerialNos.Woil_Serial_Number__c;
                    Lst_GRN_Serials.add(GRN_Serial);
                }
                
            }
        }
        List<Database.saveResult> lstGRNSerialResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+Lst_GRN_Serials);
        lstGRNSerialResult = Database.insert(Lst_GRN_Serials,false);
        
        for (Database.SaveResult result : lstGRNSerialResult) {
            if (result.isSuccess()) {
                
                System.debug(' GRN Serial Successfully inserted, GRN Serial ID is: ' + result.getId());
                GRN_Serial_Ids.add(result.getId());
                
            }
            else {
                //Error ecountered              
                for(Database.Error error : result.getErrors()) {
                    System.debug(error.getStatusCode() + ': ' + error.getMessage() + 
                                 ' Fields that affected the error: ' + error.getFields());                    
                }
            }
        }
        Lst_GRN_Serials=[Select Id,Woil_Serial_Number__c from Woil_GRN_Serial_Number__c where id=:GRN_Serial_Ids];
        for(Woil_GRN_Serial_Number__c serial :Lst_GRN_Serials){	            
            Woil_Invoice_Serial_Number__c inv_serial= new Woil_Invoice_Serial_Number__c(Woil_Serial_Number__c = serial.Woil_Serial_Number__c);
            //system.debug('vvvv::::'+inv_serial.Id);
            GRN_Serial_Nos.add(serial.Woil_Serial_Number__c);
        }
        Lst_invoice_Serials=[Select Id, Woil_Accepted__c from Woil_Invoice_Serial_Number__c where Woil_Serial_Number__c=:GRN_Serial_Nos];
        for(Woil_Invoice_Serial_Number__c serial:Lst_invoice_Serials){
            serial.Woil_Accepted__c=true;
        }
        update Lst_invoice_Serials;
    }
    
     @AuraEnabled
    public static void saveGRNItems_reject(List<Woil_Invoice_Serial_Number__c> LstSerialNos,String reason ) {
        System.debug('reason::::'+reason);
       	List<Woil_SRN__c> srn =[select Id from Woil_SRN__c where Woil_Reason__c=:reason and Woil_Sales_Invoice__c=:LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_Invoice__c and woil_posted__c=false];
        String division;
        Woil_SRN__c grn=new Woil_SRN__c();
        if(srn.size()==0){
            List<WOIL_Buyer_Seller_Mapping__c> buyerList = new List<WOIL_Buyer_Seller_Mapping__c>();
            Map<String,String> currentSession = Auth.SessionManagement.getCurrentSession();
            Boolean isLogCommunityUser = false;
            if(currentSession.get('LoginHistoryId')==null && currentSession.get('SourceIp')=='::' && currentSession.get('UserType')=='Standard'){
                isLogCommunityUser=true;
            }
            List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            if(!userList.isEmpty() && userList[0].AccountId != null && userList[0].ContactId != null && isLogCommunityUser){
                List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName, 
                                         (SELECT Id,Name FROM ShipToAddress__r),
                                         (SELECT Id,Woil_Division__c FROM Account_Plants__r), 
                                         (SELECT Id, WOIL_Seller__c, WOIL_Seller__r.Woil_Account_Number__c,WOIL_Seller__r.Woil_PAN_Number__c, WOIL_Seller__r.Name,
                                          WOIL_Seller__r.BillingStreet,WOIL_Seller__r.BillingCity,WOIL_Seller__r.BillingState,WOIL_Seller__r.BillingPostalCode,WOIL_Seller__r.BillingCountry
                                          FROM Buyer_Seller_Mappings__r) 
                                         FROM Account 
                                         Where Id =: userList[0].AccountId];    
                for(Account acc : accList){
                    for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : acc.Buyer_Seller_Mappings__r){
                        buyerList.add(buyerSellerRec);
                    }    
                }
            }
           String comp_addr=(buyerList[0].WOIL_Seller__r.BillingStreet!=null?buyerList[0].WOIL_Seller__r.BillingStreet:'')+(buyerList[0].WOIL_Seller__r.BillingCity!=null?buyerList[0].WOIL_Seller__r.BillingCity:'')+(buyerList[0].WOIL_Seller__r.BillingState!=null?buyerList[0].WOIL_Seller__r.BillingState:'')+(buyerList[0].WOIL_Seller__r.BillingPostalCode!=null?buyerList[0].WOIL_Seller__r.BillingPostalCode:'')+(buyerList[0].WOIL_Seller__r.BillingCountry!=null?buyerList[0].WOIL_Seller__r.BillingCountry:'');

           
        grn.Woil_Purchase_Order__c=LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_Invoice__r.Woil_Order__c;
        grn.Woil_Sales_Invoice__c=LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_Invoice__c;
        grn.Woil_Account__c=LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_Invoice__r.Woil_Account__c;
        grn.Woil_Reason__c =reason;
        grn.Woil_Return_Order_Date__c =Date.today();
        grn.Woil_On_Spot_Return__c=true;  
        grn.Woil_Delivery_Date__c=Date.today();
        grn.Woil_Delivery_Address__c=comp_addr;
        grn.Woil_Return_Type__c='Return Order';
        grn.Woil_Status__c='Approved';
        //grn.Woil_Posted__c=true;
            
        insert grn;  
        }else{
        List<Woil_SRN_Line_Item__c> srnLI=[select Woil_Product__r.Name,Woil_Quantity__c from Woil_SRN_Line_Item__c where Woil_SRN__c=:srn[0].Id];
        for(Woil_SRN_Line_Item__c srn_items:srnLI){
          SkuVsSRNLineItems.put(srn_items.Woil_Product__r.Name, srn_items);
        }    
        grn=srn[0];    
        }       
        //System.debug('LstSerialNos::::'+LstSerialNos);
        decimal quantity=0;
        for(Woil_Invoice_Serial_Number__c SerialNo:LstSerialNos){
            quantity--;
            //System.debug('Product::::'+SerialNo.Woil_Invoice_Line_Item__r.Woil_Material_Code__c);
            if(!SkuVsSRNLineItems.containsKey(SerialNo.Woil_Invoice_Line_Item__r.Woil_product__r.Name) ){
                Woil_SRN_Line_Item__c GRNLineItem=new Woil_SRN_Line_Item__c();
                GRNLineItem.Woil_Product__c	=SerialNo.Woil_Invoice_Line_Item__r.Woil_Product__c;
                division=SerialNo.Woil_Invoice_Line_Item__r.Woil_Product__r.Woil_Product_Type__c;
                //GRNLineItem.Woil_Quantity__c=quantity;
                GRNLineItem.Woil_SRN__c	=grn.Id;
                GRNLineItem.Woil_Unit_Price__c=SerialNo.Woil_Invoice_Line_Item__r.Woil_Unit_Price__c;
                SkuVsSRNLineItems.put(SerialNo.Woil_Invoice_Line_Item__r.Woil_product__r.Name, GRNLineItem); 
                quantity=0;                
            } else
            {              
                Woil_SRN_Line_Item__c GRNLineItem=SkuVsSRNLineItems.get(SerialNo.Woil_Invoice_Line_Item__r.Woil_product__r.Name) ;
                //GRNLineItem.Woil_Quantity__c= GRNLineItem.Woil_Quantity__c-quantity;
                division=SerialNo.Woil_Invoice_Line_Item__r.Woil_Product__r.Woil_Product_Type__c;
                SkuVsSRNLineItems.put(SerialNo.Woil_Invoice_Line_Item__r.Woil_product__r.Name, GRNLineItem);
            }
            
        }
        SRNLineItems =SkuVsSRNLineItems.values();
        
        
        List<Database.saveResult> lstSRNLIResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+SRNLineItems);
        
        List<Database.UpsertResult> upsertAssetResult = Database.upsert(SRNLineItems,false);
        
        for(Integer index = 0; index < upsertAssetResult.size(); index++) {
            if(upsertAssetResult[index].isSuccess()) {
                System.debug('Record was created-' + SRNLineItems[index]);
                GRN_LI_Ids.add(SRNLineItems[index].Id);
            } else {
                // collect failed records here
                System.debug('Record failed-' + SRNLineItems[index]);
            }
        }
        Map<Id,decimal> SkuVsQuantity=new Map<Id,decimal>();
        decimal quantity1=1;
        Set<Id> skuCode=new Set<Id>();
        Lst_SRN_LI=[Select Id,Woil_Product__r.ProductCode from Woil_SRN_Line_Item__c where id=:GRN_LI_Ids];
        for(Woil_Invoice_Serial_Number__c GRN_SerialNos:LstSerialNos){           
            for(Woil_SRN_Line_Item__c GRN_LI:Lst_SRN_LI){
                if(GRN_SerialNos.Woil_Invoice_Line_Item__r.Woil_Product__c==GRN_LI.Woil_Product__c){
                    Woil_SRN_Serial_Number__c GRN_Serial=new Woil_SRN_Serial_Number__c();
                    GRN_Serial.Woil_SRN_Line_Item__c =GRN_LI.Id; 
                    GRN_Serial.Woil_Serial_Number__c=GRN_SerialNos.Woil_Serial_Number__c;
                    Lst_SRN_Serials.add(GRN_Serial);
                                    
                    
                    }               
            }
        }
        List<Database.saveResult> lstGRNSerialResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+Lst_SRN_Serials);
        lstGRNSerialResult = Database.insert(Lst_SRN_Serials,false);
        
        for (Database.SaveResult result : lstGRNSerialResult) {
            if (result.isSuccess()) {
                
                System.debug(' GRN Serial Successfully inserted, GRN Serial ID is: ' + result.getId());
                GRN_Serial_Ids.add(result.getId());
                
            }
            else {
                //Error ecountered              
                for(Database.Error error : result.getErrors()) {
                    System.debug(error.getStatusCode() + ': ' + error.getMessage() + 
                                 ' Fields that affected the error: ' + error.getFields());                    
                }
            }
        }
        Lst_SRN_Serials=[Select Id,Woil_Serial_Number__c from Woil_SRN_Serial_Number__c where id=:GRN_Serial_Ids];
        for(Woil_SRN_Serial_Number__c serial :Lst_SRN_Serials){	            
            Woil_Invoice_Serial_Number__c inv_serial= new Woil_Invoice_Serial_Number__c(Woil_Serial_Number__c = serial.Woil_Serial_Number__c);
            //system.debug('vvvv::::'+inv_serial.Id);
            GRN_Serial_Nos.add(serial.Woil_Serial_Number__c);
        }
        Lst_invoice_Serials=[Select Id, Woil_Accepted__c from Woil_Invoice_Serial_Number__c where Woil_Serial_Number__c=:GRN_Serial_Nos];
        for(Woil_Invoice_Serial_Number__c serial:Lst_invoice_Serials){
            serial.Woil_Accepted__c=true;
        }
        update Lst_invoice_Serials;
        grn.Woil_Division__c=division;
        grn.Woil_Posted__c=true;
        update grn;
    }
    
     @AuraEnabled 
    public static Map<String, String> getReasonFieldValue(){
        Map<String, String> options = new Map<String, String>();
        
        Schema.DescribeFieldResult fieldResult = Woil_SRN__c.Woil_Reason__c.getDescribe();
        
        List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pValues) {
            
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
      @AuraEnabled
    public static List < WrapperNonSerialProd > getNonSerializedItems(String invoice_no,String str_prod) {
        Invoice__c inv=[Select Name,Woil_Account__c,Woil_Sales_Order__c,Woil_Order__c from Invoice__c where id=:invoice_no];
        List<Woil_Invoice_Line_Item__c> invList;
        Set<String> SkuNames=new Set<String>();
        if(str_prod!=null){
      	List<WrapperNonSerialProd> wResp= (List<WrapperNonSerialProd>)JSON.deserialize(str_prod,List<WrapperNonSerialProd>.class); 

        for(WrapperNonSerialProd prod:wResp){
            SkuNames.add(prod.skuCode);
        }
        invList=[Select Woil_Unit_Price__c,Woil_Invoiced_Quantity__c,Woil_Invoice__r.Name,Woil_Product__r.Name,Woil_Product__r.Description from Woil_Invoice_Line_Item__c where Woil_Invoice__c=:invoice_no and Woil_Product__r.Woil_Is_Serializable__c!=true and Woil_Product__r.Name  in :SkuNames];
        }else{
        invList=[Select Woil_Unit_Price__c,Woil_Invoiced_Quantity__c,Woil_Invoice__r.Name,Woil_Product__r.Name,Woil_Product__r.Description from Woil_Invoice_Line_Item__c where Woil_Invoice__c=:invoice_no and Woil_Product__r.Woil_Is_Serializable__c!=true ];     
        }
        List<Woil_SRN_Line_Item__c> LstPR=[Select Woil_Quantity__c,Woil_Product__r.Name,Woil_Ordered_Quantity__c,Woil_Product__c,Woil_SRN__r.Woil_Sales_Invoice__r.Name from Woil_SRN_Line_Item__c where Woil_SRN__r.Woil_Sales_Invoice__c=:invoice_no and Woil_Product__r.Woil_Is_Serializable__c!=true];
        List<Woil_GRN_Line_Item__c> LstGRN=[Select Woil_Quantity_Non_Serialize__c,Woil_Product__r.Name,Woil_Accepted_Quantity__c,Woil_Product__c,Woil_GRN__r.Woil_Sales_Invoice__r.Name  from Woil_GRN_Line_Item__c where Woil_GRN__r.Woil_Sales_Invoice__c=:invoice_no and Woil_Product__r.Woil_Is_Serializable__c!=true];
        List<WrapperNonSerialProd> LstProd=new List<WrapperNonSerialProd>(); 
        Decimal Quantity=0;
        map<String,decimal> SKUVsQty=new  map<String,decimal>();
        map<String,decimal> SKUVsQty1=new  map<String,decimal>();
        map<String,Id> SKUVsId=new  map<String,Id>();
        map<String,String> SKUVsDesc=new  map<String,String>();
        map<String,decimal> SKUVsPrice=new  map<String,decimal>();
        
        for(Woil_Invoice_Line_Item__c invLI:invList){           
            if(SKUVsQty1.get(invLI.Woil_Product__c)==null){
                SKUVsQty1.put(invLI.Woil_Product__r.Name,invLI.Woil_Invoiced_Quantity__c);
                SKUVsId.put(invLI.Woil_Product__r.Name, invLI.Woil_Product__c);
                SKUVsPrice.put(invLI.Woil_Product__r.Name,invLI.Woil_Unit_Price__c);
                SKUVsDesc.put(invLI.Woil_Product__r.Name, invLI.Woil_Product__r.Description);
            }else{
                Quantity = SKUVsQty1.get(invLI.Woil_Product__r.Name) +invLI.Woil_Invoiced_Quantity__c;
                SKUVsQty1.put(invLI.Woil_Product__r.Name,Quantity);
                SKUVsId.put(invLI.Woil_Product__r.Name, invLI.Woil_Product__c);
                SKUVsPrice.put(invLI.Woil_Product__r.Name,invLI.Woil_Unit_Price__c);
                SKUVsDesc.put(invLI.Woil_Product__r.Name, invLI.Woil_Product__r.Description);
            }                 
        }
        
        for(Woil_GRN_Line_Item__c GrnLI:LstGRN){
            
            if(SKUVsQty.get(GrnLI.Woil_Product__r.Name)==null){
                SKUVsQty.put(GrnLI.Woil_Product__r.Name,GrnLI.Woil_Quantity_Non_Serialize__c);
            }else{
                Quantity =(GrnLI.Woil_Product__r.Name!=null? SKUVsQty.get(GrnLI.Woil_Product__r.Name):0 )+GrnLI.Woil_Quantity_Non_Serialize__c;
                SKUVsQty.put(GrnLI.Woil_Product__r.Name,Quantity);
            }                 
        }
        
        for(Woil_SRN_Line_Item__c PRLI:LstPR){
            if(SKUVsQty.get(PRLI.Woil_Product__r.Name)==null){
                SKUVsQty.put(PRLI.Woil_Product__r.Name,PRLI.Woil_Quantity__c);
            }else{
                Quantity =(PRLI.Woil_Product__r.Name!=null? SKUVsQty.get(PRLI.Woil_Product__r.Name):0) +PRLI.Woil_Quantity__c;
                SKUVsQty.put(PRLI.Woil_Product__r.Name,Quantity);
            }                 
        }
        
        for(String SKUCode:SKUVsQty1.keySet()){
            if(SKUVsQty.get(SKUCode)!=null){
                decimal qty= SKUVsQty.get(SKUCode); 
                decimal qty1=SKUVsQty1.get(SKUCode)-qty;
                SKUVsQty1.put(SKUCode, qty1);
                
            }             
        }
        System.debug('SKUVsQty::'+SKUVsQty+'::SKUVsQty1::'+SKUVsQty1);
        for(String prodId:SKUVsQty1.keyset()){
            if(SKUVsQty1.get(prodId)!=0){
            WrapperNonSerialProd wrp_Prod=new WrapperNonSerialProd();
            wrp_Prod.skuCode=  prodId;          
            wrp_Prod.Quantity=  SKUVsQty1.get(prodId);
            wrp_Prod.postedQty=  (SKUVsQty.get(prodId)!=null?SKUVsQty.get(prodId):0);
            wrp_Prod.totalQty=SKUVsQty1.get(prodId)+(SKUVsQty.get(prodId)!=null?SKUVsQty.get(prodId):0);
            wrp_Prod.InvoiceNo=inv.Name;
            wrp_Prod.AccountId=inv.Woil_Account__c;
            wrp_Prod.OrderId=inv.Woil_Order__c;
            wrp_Prod.productId=SKUVsId.get(prodId);
            wrp_Prod.InvoiceId=inv.Id;
            wrp_Prod.Unit_Price=  SKUVsPrice.get(prodId);   
            wrp_Prod.productDesc=SKUVsDesc.get(prodId);  
            LstProd.add(wrp_Prod);
            }
        }
        System.debug('LstProd::::'+LstProd);
 		return LstProd;
    }
    
     @AuraEnabled
    public static void createGRNforNonSerial(String selectedRows ) {
        
      List<WrapperNonSerialProd> wResp= (List<WrapperNonSerialProd>)JSON.deserialize(selectedRows,List<WrapperNonSerialProd>.class); 
        System.debug('wResp:::'+wResp);
         GRN__c grn=new GRN__c();
        grn.Woil_Purchase_Order__c=wResp[0].OrderId;
        grn.Woil_Sales_Invoice__c=wResp[0].InvoiceId;
        grn.Woil_Account__c=wResp[0].AccountId;
        grn.Woil_Receiving_Type__c ='Manual';
        insert grn;
        
        //System.debug('LstSerialNos::::'+LstSerialNos);
        for(WrapperNonSerialProd product:wResp){
            
            //System.debug('Product::::'+SerialNo.Woil_Invoice_Line_Item__r.Woil_Material_Code__c);
            if(!SkuVsGRNLineItems.containsKey(product.skuCode) ){
                Woil_GRN_Line_Item__c GRNLineItem=new Woil_GRN_Line_Item__c();
                GRNLineItem.Woil_Product__c	=product.productId;
                GRNLineItem.Woil_Quantity_Non_Serialize__c	=product.Quantity;
                //GRNLineItem.Woil_UnitPrice__c	=product.Unit_Price;
                GRNLineItem.Woil_GRN__c	=grn.Id;
                SkuVsGRNLineItems.put(product.skuCode, GRNLineItem); 
                
            } 
           
        }
        GRNLineItems =SkuVsGRNLineItems.values();
        
        
        List<Database.saveResult> lstGRNLIResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+GRNLineItems);
        lstGRNLIResult = Database.insert(GRNLineItems,false);
        
        // Iterate SaveResult array
        for (Database.SaveResult result : lstGRNLIResult) {
            if (result.isSuccess()) {
                
                System.debug('GRNLI Successfully inserted, Account Id is: ' + result.getId());
                GRN_LI_Ids.add(result.getId());
            }
            else {
                //Error ecountered              
                for(Database.Error error : result.getErrors()) {
                    //Handle error
                    System.debug(error.getStatusCode() + ': ' + error.getMessage() + 
                                 ' Fields that affected the error: ' + error.getFields());                    
                }
            }
        }
       
    }
    
     @AuraEnabled
    public static void createSRNforSpareParts(String str_prod){
        List<WrapperNonSerialProd> wResp= (List<WrapperNonSerialProd>)JSON.deserialize(str_prod,List<WrapperNonSerialProd>.class); 
        List<Woil_SRN__c> srn =[select Id from Woil_SRN__c where Woil_Reason__c=:wResp[0].Reject_Reason and Woil_Sales_Invoice__c=:wResp[0].InvoiceId and woil_posted__c=false];
        
        Woil_SRN__c grn=new Woil_SRN__c();
        if(srn.size()==0){
            List<WOIL_Buyer_Seller_Mapping__c> buyerList = new List<WOIL_Buyer_Seller_Mapping__c>();
            Map<String,String> currentSession = Auth.SessionManagement.getCurrentSession();
            Boolean isLogCommunityUser = false;
            if(currentSession.get('LoginHistoryId')==null && currentSession.get('SourceIp')=='::' && currentSession.get('UserType')=='Standard'){
                isLogCommunityUser=true;
            }
            List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            if(!userList.isEmpty() && userList[0].AccountId != null && userList[0].ContactId != null && isLogCommunityUser){
                List<Account> accList = [SELECT Id, Name, Woil_Account_Number__c,RecordType.developerName, 
                                         (SELECT Id,Name FROM ShipToAddress__r),
                                         (SELECT Id,Woil_Division__c FROM Account_Plants__r), 
                                         (SELECT Id, WOIL_Seller__c, WOIL_Seller__r.Woil_Account_Number__c,WOIL_Seller__r.Woil_PAN_Number__c, WOIL_Seller__r.Name,
                                          WOIL_Seller__r.BillingStreet,WOIL_Seller__r.BillingCity,WOIL_Seller__r.BillingState,WOIL_Seller__r.BillingPostalCode,WOIL_Seller__r.BillingCountry
                                          FROM Buyer_Seller_Mappings__r) 
                                         FROM Account 
                                         Where Id =: userList[0].AccountId];    
                for(Account acc : accList){
                    for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : acc.Buyer_Seller_Mappings__r){
                        buyerList.add(buyerSellerRec);
                    }    
                }
            }
            String comp_addr=(buyerList[0].WOIL_Seller__r.BillingStreet!=null?buyerList[0].WOIL_Seller__r.BillingStreet:'')+(buyerList[0].WOIL_Seller__r.BillingCity!=null?buyerList[0].WOIL_Seller__r.BillingCity:'')+(buyerList[0].WOIL_Seller__r.BillingState!=null?buyerList[0].WOIL_Seller__r.BillingState:'')+(buyerList[0].WOIL_Seller__r.BillingPostalCode!=null?buyerList[0].WOIL_Seller__r.BillingPostalCode:'')+(buyerList[0].WOIL_Seller__r.BillingCountry!=null?buyerList[0].WOIL_Seller__r.BillingCountry:'');
    	String division=[Select Woil_Product_Type__c from Product2 where Id=:wResp[0].ProductId].Woil_Product_Type__c;
        grn.Woil_Purchase_Order__c=wResp[0].OrderId;
        grn.Woil_Sales_Invoice__c=wResp[0].InvoiceId;
        grn.Woil_Account__c=wResp[0].AccountId;
        grn.Woil_Reason__c =wResp[0].Reject_Reason;
        grn.Woil_On_Spot_Return__c=true;    
        grn.Woil_Return_Order_Date__c =Date.today();         
        grn.Woil_Delivery_Date__c=Date.today();
        grn.Woil_Delivery_Address__c=comp_addr;
        grn.Woil_Return_Type__c='Return Order';
        grn.Woil_Status__c='Approved';
        //grn.Woil_Posted__c=true;
        grn.Woil_Division__c=division;
        insert grn;  
        }else{
        List<Woil_SRN_Line_Item__c> srnLI=[select Woil_Product__r.Name,Woil_Quantity__c from Woil_SRN_Line_Item__c where Woil_SRN__c=:srn[0].Id];
        for(Woil_SRN_Line_Item__c srn_items:srnLI){
          SkuVsSRNLineItems.put(srn_items.Woil_Product__r.Name, srn_items);
        }    
        grn=srn[0];    
        }       
        //System.debug('LstSerialNos::::'+LstSerialNos);
       
        for(WrapperNonSerialProd prod:wResp){
           
            if(!SkuVsSRNLineItems.containsKey(prod.skuCode) ){
                Woil_SRN_Line_Item__c GRNLineItem=new Woil_SRN_Line_Item__c();
                GRNLineItem.Woil_Product__c	=prod.ProductId;
                GRNLineItem.Woil_Quantity__c=prod.Quantity;
                GRNLineItem.Woil_SRN__c	=grn.Id;
                GRNLineItem.Woil_Unit_Price__c=prod.Unit_Price;
                
                SkuVsSRNLineItems.put(prod.skuCode, GRNLineItem); 
                              
            } else
            {              
                Woil_SRN_Line_Item__c GRNLineItem=SkuVsSRNLineItems.get(prod.skuCode) ;
                GRNLineItem.Woil_Quantity__c=GRNLineItem.Woil_Quantity__c+ prod.Quantity;
                SkuVsSRNLineItems.put(prod.skuCode, GRNLineItem);
            }
            
        }
        SRNLineItems =SkuVsSRNLineItems.values();
        
        List<Database.saveResult> lstSRNLIResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+SRNLineItems);
        
        List<Database.UpsertResult> upsertAssetResult = Database.upsert(SRNLineItems,false);
        grn.Woil_Posted__c=true;	
        update grn;  
    }
    
     public class WrapperNonSerialProd {
        @AuraEnabled public integer rowId {get;set;}
        @AuraEnabled public String skuCode {get;set;}
        @AuraEnabled public String InvoiceNo {get;set;}
        @AuraEnabled public Decimal Quantity {get;set;}
        @AuraEnabled public String productId {get;set;}
        @AuraEnabled public String AccountId {get;set;}
        @AuraEnabled public String OrderId {get;set;}
        @AuraEnabled public String InvoiceId {get;set;}
        @AuraEnabled public String Reject_Reason {get;set;} 
        @AuraEnabled public Decimal Unit_Price {get;set;} 
        @AuraEnabled public String productDesc {get;set;}
        @AuraEnabled public Decimal totalQty {get;set;}
        @AuraEnabled public Decimal postedQty {get;set;}
    }
}