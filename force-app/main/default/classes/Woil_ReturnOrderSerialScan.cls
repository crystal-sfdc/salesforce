public without sharing class  Woil_ReturnOrderSerialScan {
    
    @AuraEnabled
    public static List < WrapperSerialNos > getInvoiceItems(String retOrdNo) {
        List<String> SKUCodes=new List<String>();
        Set<String> serialNos=new Set<String>();
        List<Woil_Serial_Number_Scan__c> LstDataset=[Select Woil_Serial_Number__c from Woil_Serial_Number_Scan__c where Woil_Purchase_Return__c=:retOrdNo];
        for(Woil_Serial_Number_Scan__c data:LstDataset){
            serialNos.add(data.Woil_Serial_Number__c);
        }
        
        String whLocation,custCode;
        List<Woil_SRN_Line_Item__c> roLineItems=[Select Woil_SKU_Code__c,Woil_Warehouse_Location__c,Woil_SRN__r.Woil_Account__c from Woil_SRN_Line_Item__c where Woil_SRN__c=:retOrdNo];
        for(Woil_SRN_Line_Item__c roLI:roLineItems){           
            SKUCodes.add(roLI.Woil_SKU_Code__c);
            whLocation=roLineItems[0].Woil_Warehouse_Location__c;
            custCode=roLineItems[0].Woil_SRN__r.Woil_Account__c;
        } 
        
        List<WrapperSerialNos> wrpSerialNos=new List<WrapperSerialNos>();
        Map<String,WrapperSerialNos> Serials=new Map<String,WrapperSerialNos>();
        //List<Woil_Serial_Number_Transaction__c> serial_txn=[Select id,Woil_Product__r.Name,Woil_Warehouse_Location__c,Woil_Current_Customer_Code__c,Woil_Serial_Number__c,Woil_Product__c,Woil_Customer__c from Woil_Serial_Number_Transaction__c where Woil_Quantity__c in (1,-1) and Woil_Product__r.Name in:SKUCodes and Woil_Warehouse_Location__c=:whLocation and Woil_Serial_Transaction_Type__c in ('Purchase Return','Purchase')  and Woil_Serial_Number__c not in :serialNos and Woil_Customer__c=:custCode]; 
        List<Woil_Serial_Number_Owner__c> serial_txn=[Select Id,Woil_Product__r.Name,Woil_Product__r.Description,Woil_WareHouse_Location__c,Woil_Customer__c,Woil_Serial_Number__c,Woil_Product__c
                                                      FROM Woil_Serial_Number_Owner__c 
                                                      WHERE Woil_Product__r.Name IN: SKUCodes AND 
                                                      Woil_Warehouse_Location__c =: whLocation AND 
                                                      (Transit_Status__c in ('Opening Balances','Sell-In','Sell-Through','Return')) AND 
                                                      Woil_Serial_Number__c NOT IN : serialNos AND Woil_Customer__c=:custCode AND Woil_Product__r.Woil_Is_Serializable__c=true];
        integer rowId=1;
        System.debug('serial_txn::::'+serial_txn);
        for(Woil_Serial_Number_Owner__c serial:serial_txn){ 
            System.debug('Serials::'+Serials);
            if(Serials.get(serial.Woil_Serial_Number__c)==null){
                WrapperSerialNos wrp=new WrapperSerialNos();
                wrp.skuCode=serial.Woil_Product__r.Name;
                wrp.skuDesc=serial.Woil_Product__r.Description;
                wrp.warehouseLocation=serial.Woil_Warehouse_Location__c;
                wrp.serialNo=serial.Woil_Serial_Number__c;
                wrp.customer=serial.Woil_Customer__c;
                wrp.productId=serial.Woil_Product__c;
                wrp.rowId=rowId;
                wrpSerialNos.add(wrp);
                Serials.put(serial.Woil_Serial_Number__c,wrp);  
                rowId++;
            }else{
                Serials.remove(serial.Woil_Serial_Number__c);
            }
        }
        System.debug('Serials::'+Serials);
        return Serials.values();
    }
    @AuraEnabled 
    public static void saveScannedItems(List<WrapperSerialNos> LstSerialNos,String retOrdNo){
        System.debug('LstSerialNos=====> '+LstSerialNos);
        List<Woil_Serial_Number_Scan__c> data_set=new List<Woil_Serial_Number_Scan__c>();
        List<Woil_SRN_Serial_Number__c> serial_scan=new List<Woil_SRN_Serial_Number__c>();
        map<String,decimal> SkuVsQty=new map<String,decimal>();
        
        Set<String> warehouseName = new Set<String>();
        for(WrapperSerialNos wrpItem:LstSerialNos){
            warehouseName.add(wrpItem.warehouseLocation);
        }
        Map<String, Woil_ShipToAddress__c> shipToAddressMap = new Map<String, Woil_ShipToAddress__c>(); 
        for(Woil_ShipToAddress__c obj : [Select Name, Id From Woil_ShipToAddress__c])
            shipToAddressMap.put(obj.Name, obj);
        
        
        for(WrapperSerialNos wrpItem:LstSerialNos){
            /*Woil_SRN_Serial_Number__c srn_serial=new Woil_SRN_Serial_Number__c();
srn_serial.Woil_Serial_Number__c=wrpItem.serialNo;
srn_serial.Woil_SRN_Line_Item__c=new Woil_SRN_Line_Item__c(Woil_Product__c=wrpItem.productId).Id;*/
            Woil_Serial_Number_Scan__c dta=new Woil_Serial_Number_Scan__c();
            dta.Woil_Product__c =wrpItem.productId;
            System.debug('$$$$$warehouseLocation$$$$$ '+wrpItem.warehouseLocation);
            dta.Woil_Warehouse_Location__c =shipToAddressMap.get(wrpItem.warehouseLocation).id;
            dta.Woil_Serial_Number__c =wrpItem.serialNo;
            dta.Woil_Purchase_Return__c=retOrdNo;
            dta.Woil_Customer__c=wrpItem.customer;
            data_set.add(dta);
            if(SkuVsQty.get(dta.Woil_Product__c)!=null)
                SkuVsQty.put(dta.Woil_Product__c,SkuVsQty.get(dta.Woil_Product__c)-1);
            else
                SkuVsQty.put(dta.Woil_Product__c,-1);    
            
        }
        List <Woil_SRN_Line_Item__c> LstSRNLI=[Select id,Woil_Quantity__c,Woil_Product__c from Woil_SRN_Line_Item__c where Woil_Product__c in :SkuVsQty.keyset()];
        for(Woil_SRN_Line_Item__c data:LstSRNLI){
            data.Woil_Quantity__c=SkuVsQty.get(data.Woil_Product__c);
        }
        System.debug('LstSRNLI::'+LstSRNLI);
        insert data_set;
        update LstSRNLI;
    }
    
    @AuraEnabled
    public static List < Woil_Serial_Number_Scan__c > getScannedSerials(String pr_no) {
        
        return [Select id,name,Woil_Serial_Number__c,Woil_Product__r.Name,Woil_Warehouse_Location__c from Woil_Serial_Number_Scan__c where Woil_Purchase_Return__c=:pr_no];
    }
    public class WrapperSerialNos {
        @AuraEnabled public integer rowId {get;set;}
        @AuraEnabled public String skuCode {get;set;}
        @AuraEnabled public String skuDesc {get;set;}
        
        @AuraEnabled public String warehouseLocation {get;set;}
        @AuraEnabled public String serialNo {get;set;}
        @AuraEnabled public String customer {get;set;}
        @AuraEnabled public String productId {get;set;}
        
    }
}