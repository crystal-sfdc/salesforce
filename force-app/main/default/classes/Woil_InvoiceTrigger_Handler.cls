public class Woil_InvoiceTrigger_Handler {
    public static void isBeforeInsert(List<Invoice__c> newInvoiceItem){
        System.debug(newInvoiceItem);
        Set<String> customerCode = new Set<String>();
        Set<String> OrderNumberSet = new Set<String>();
        for(Invoice__c invo : newInvoiceItem){
            if(invo.Woil_CustomerCode__c != null && invo.Woil_SAPOrderID__c != null){
                customerCode.add(invo.Woil_CustomerCode__c);
                OrderNumberSet.add(invo.Woil_SAPOrderID__c);
            }
            
        }
        if(customerCode.size() > 0 && OrderNumberSet.size() > 0){
            Map<String,Account> CustomerInfoMap = new Map<String,Account>();
            Map<String,Order> OrderInfoMap = new Map<String,Order>();
            List<Account> acc = [select id ,Woil_Account_Number__c from Account where Woil_Account_Number__c IN : customerCode];
            List<Order> ord = [select id , Woil_SAP_Order_ID__c,OrderNumber from Order where OrderNumber IN : OrderNumberSet ];
            
            for( Account aInfo : acc){
                CustomerInfoMap.put(aInfo.Woil_Account_Number__c,aInfo);
            }
            for(Order oInfo : ord ){
                OrderInfoMap.put(oInfo.OrderNumber,oInfo);
            }
            //resolve lookUp
            
            for(Invoice__c infinal : newInvoiceItem){
                
                infinal.Woil_Account__c = CustomerInfoMap.get(infinal.Woil_CustomerCode__c).Id;
                
                infinal.Woil_Order__c = OrderInfoMap.get(infinal.Woil_SAPOrderID__c).Id;
                
            }
        }
    }
    
    //After Insert ---------------------------------------------------------------------------------------------------------------------------
    public static void isAfterInsert(List<Invoice__c> InvoiceListAfterInsert){
        System.debug(InvoiceListAfterInsert);
        Set<String> accountIdInfo = new Set<String>();
        for(Invoice__c invo : InvoiceListAfterInsert){
            accountIdInfo.add(invo.Woil_Account__c);
        }
        // user information----------------------
        Map<String , User> Userinformation = new Map<String , User>();
        for(User userRec : [SELECT Id, AccountId FROM User WHERE IsActive = true AND AccountId IN :accountIdInfo]){
            Userinformation.put(userRec.AccountId,userRec);
        }
        List<Invoice__Share> invoiceShare = new List<Invoice__Share>();
        for(Invoice__c invoice : InvoiceListAfterInsert){
            if(invoice.Woil_Account__c != null && Userinformation.containsKey(invoice.Woil_Account__c)){
                Invoice__Share inShare = new Invoice__Share();
                inShare.ParentId = invoice.Id;
                inShare.UserOrGroupId = Userinformation.get(invoice.Woil_Account__c).Id;
                inShare.AccessLevel = 'Read';
                invoiceShare.add(inShare);
            }
            System.debug(invoiceShare);
        }
        Database.SaveResult[] invoiceShareInsertResult = Database.insert(invoiceShare,false); 
        System.debug(invoiceShareInsertResult);
        
    }
    
    //After Insert ---------------------------------------------------------------------------------------------------------------------------
    public static void updateOrderLookupStatus(Map<Id,Invoice__c> newInvoiceObjMap){
        try{
            
            System.debug('newInvoiceObjList===>'+newInvoiceObjMap);
            
            Set<Id> newOrderLookupIdSet  = new Set<Id>();
            for(id idObj : newInvoiceObjMap.keySet()){
                newOrderLookupIdSet.add(newInvoiceObjMap.get(idObj).Woil_Order__c);
            }
            System.debug('newOrderLookupIdSet====> '+newOrderLookupIdSet);
            
            // Vo data jiska rec available tha.
            Map<Id,Invoice__c> orderLookupExist = new  Map<Id,Invoice__c>([SELECT Woil_Order__c, Name, Id 
                                                                           FROM Invoice__c 
                                                                           Where Woil_Order__c IN : newOrderLookupIdSet 
                                                                           And id Not IN : newInvoiceObjMap.keySet()]);
            System.debug('orderLookupExist====> '+orderLookupExist);
            
            for(Id removeId : orderLookupExist.keyset()){
                newOrderLookupIdSet.remove(orderLookupExist.get(removeId).Woil_Order__c);
            }
            System.debug('newOrderLookupIdSet====>'+newOrderLookupIdSet);
            
            List<Order> orderObjSet = New List<Order>();
            for(Id updateId : newOrderLookupIdSet){
                orderObjSet.add(new Order(Id = updateId, Status ='GRN Pending'));
            }
            System.debug('orderObjSet'+orderObjSet);
            
            if(orderObjSet.size() > 0 && orderObjSet != null)
                Update orderObjSet;
            
        }
        catch(exception ex){
            system.debug('ex '+ex.getStackTraceString());
            
        }
    }
    
    public static void isBeforeUpdate(List<Invoice__c> InvoiceListBeforeUpdate,Map<Id,Invoice__c> oldInvoiceMap){
        System.debug('inside inv update ::::::');
        for(Invoice__c inv:InvoiceListBeforeUpdate){
            if(inv.Woil_Total_Quantity__c ==((inv.Woil_Accepted_Quantity__c!=null?inv.Woil_Accepted_Quantity__c:0)+(inv.Woil_Rejected_Quantity__c!=null?inv.Woil_Rejected_Quantity__c:0))){
                inv.Woil_Status__c='Processed';  
            }    
        }        
    }
    
    //After Update 
    public static void isAfterUpdate(List<Invoice__c> InvoiceListBeforeUpdate,Map<Id,Invoice__c> oldInvoiceMap){
        set<String> orderId = new set<String>();
        for(Invoice__c invo : InvoiceListBeforeUpdate){
            if(invo.Woil_Accepted_Quantity__c != oldInvoiceMap.get(invo.Id).Woil_Accepted_Quantity__c){
                orderId.add(invo.Woil_Order__c);    
            }
        }
        if(orderId.size()>0){
            List<Order> orUpdate=new List<Order>();
            for(Order ordInfo : [select Id ,Quantity__c,Status from Order where ID IN : orderId] ){
                Decimal totalAccepctQuantity=0;
                for(Invoice__c invoInfo : [select Id ,Woil_Accepted_Quantity__c from Invoice__c where Woil_Order__c =: ordInfo.Id] ){
                    totalAccepctQuantity=invoInfo.Woil_Accepted_Quantity__c + totalAccepctQuantity;
                }  
                if(totalAccepctQuantity == ordInfo.Quantity__c){
                    ordInfo.Status = 'Full Received';
                    orUpdate.add(ordInfo);
                }
                else{
                     ordInfo.Status = 'Partial Received';
                    orUpdate.add(ordInfo);
                }
            }
            update orUpdate;
        }
        
    } 
    
}