public without sharing class Woil_SerialScanSalesOrderController {

    @AuraEnabled
    public static List < WrapperSerialNos > getInvoiceItems(String retOrdNo) {
        List<Woil_Sales_Order__c> salesOrderList = [SELECT Id, Woil_Status__c,Woil_Division__c FROM Woil_Sales_Order__c WHERE Id =: retOrdNo];
        
       	List<String> SKUCodes=new List<String>();
        Set<String> serialNos=new Set<String>();
        List<Woil_Serial_Number_Scan__c> LstDataset = [SELECT Woil_Serial_Number__c FROM Woil_Serial_Number_Scan__c WHERE Woil_Sales_Order__c =: retOrdNo];
        for(Woil_Serial_Number_Scan__c data:LstDataset){
            serialNos.add(data.Woil_Serial_Number__c);
        }

        String whLocation,custCode;

        List<Woil_Sales_Order__c> roLineItems = [SELECT Id,Woil_ShipToAddress__r.Name, 
                                                    (SELECT ID, Woil_Product__r.Name,Woil_Quantity__c FROM Sales_Order_Line_Items__r ) 
                                                    FROM Woil_Sales_Order__c WHERE id =:retOrdNo];


        custCode = [SELECT Id, AccountId,Account.Woil_Account_Number__c,  ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()].AccountId;
                                                    

        for(Woil_Sales_Order__c roLI : roLineItems){  
            whLocation = roLI.Woil_ShipToAddress__r.Name;
            for(Woil_Sales_Order_Line_Item__c soli : roLI.Sales_Order_Line_Items__r){
                SKUCodes.add(soli.Woil_Product__r.Name);
            }         
        } 

        List<WrapperSerialNos> wrpSerialNos=new List<WrapperSerialNos>();
        List<Woil_Serial_Number_Owner__c> serial_txn = [SELECT Id,Woil_Product__r.Name,Woil_WareHouse_Location__c,Woil_Customer__c,Woil_Serial_Number__c,Woil_Product__c 
                                                                FROM Woil_Serial_Number_Owner__c 
                                                                WHERE Woil_Product__r.Name IN: SKUCodes AND 
                                                                        Woil_Warehouse_Location__c =: whLocation AND 
                                                                        (Transit_Status__c='Opening Balances' OR Transit_Status__c = 'Sell-In' OR Transit_Status__c = 'Return') AND 
                                                                        Woil_Serial_Number__c NOT IN : serialNos AND Woil_Customer__c=:custCode]; 
                                                                        
        integer rowId=1;    
        for(Woil_Serial_Number_Owner__c serial:serial_txn){            
            WrapperSerialNos wrp=new WrapperSerialNos();
            wrp.skuCode=serial.Woil_Product__r.Name;
            wrp.warehouseLocation=serial.Woil_Warehouse_Location__c;
            wrp.serialNo=serial.Woil_Serial_Number__c;
            wrp.customer=serial.Woil_Customer__c;
            wrp.productId=serial.Woil_Product__c;
            wrp.rowId=rowId;
            wrp.productName = serial.Woil_Product__r.Name;
            wrp.status = salesOrderList[0].Woil_Status__c;
            wrpSerialNos.add(wrp);  
            rowId++;
        }
            
        return wrpSerialNos;
    }

    @AuraEnabled 
    public static String saveScannedItems(List<WrapperSerialNos> LstSerialNos,String retOrdNo){
        try{

            Map<String,Integer> matQuantityMap = new Map<String,Integer>();
            Map<String,Integer> productQuantMap = new Map<String,Integer>();

            List<Woil_Sales_Order__c> roLineItems = [SELECT Id,Woil_ShipToAddress__r.Name, 
                                                    (SELECT ID, Woil_Product__r.Name,Woil_Quantity__c FROM Sales_Order_Line_Items__r ) 
                                                    FROM Woil_Sales_Order__c WHERE id =:retOrdNo];

            for(Woil_Sales_Order__c roLI : roLineItems){  
                for(Woil_Sales_Order_Line_Item__c soli : roLI.Sales_Order_Line_Items__r){
                    matQuantityMap.put(soli.Woil_Product__r.Name,Integer.valueOf(soli.Woil_Quantity__c));
                }         
            }

            Map<String,String> warehousesMap = new Map<String,String>();
            Set<String> warehouse = new Set<String>();
            for(WrapperSerialNos wrpItem:LstSerialNos){
                warehouse.add(wrpItem.warehouseLocation);
                if(productQuantMap.containskey(wrpItem.productName)){
                    productQuantMap.put(wrpItem.productName,productQuantMap.get(wrpItem.productName)+1);
                }else{
                    productQuantMap.put(wrpItem.productName,1);
                }
            }

            for(Woil_ShipToAddress__c shipto : [SELECT Id,Name FROM Woil_ShipToAddress__c WHERE Name IN: warehouse]){
                warehousesMap.put(shipto.Name,shipto.Id);
            }

            for(Woil_Serial_Number_Scan__c sns : [SELECT Woil_Serial_Number__c,Woil_Product__r.Name 
                                                            FROM Woil_Serial_Number_Scan__c 
                                                            WHERE Woil_Sales_Order__c =: retOrdNo]){                                               
                if(productQuantMap.containskey(sns.Woil_Product__r.Name)){
                    productQuantMap.put(sns.Woil_Product__r.Name,productQuantMap.get(sns.Woil_Product__r.Name)+1);
                }
            }

            for(String prodName : productQuantMap.keySet()){
                if(productQuantMap.get(prodName) > matQuantityMap.get(prodName)){
                    return 'Quantity Exceeded';
                }
            }

            List<Woil_Serial_Number_Scan__c> data_set=new List<Woil_Serial_Number_Scan__c>();
            List<Woil_SRN_Serial_Number__c> serial_scan=new List<Woil_SRN_Serial_Number__c>();
            for(WrapperSerialNos wrpItem:LstSerialNos){
                Woil_Serial_Number_Scan__c dta=new Woil_Serial_Number_Scan__c();
                dta.Woil_Product__c =wrpItem.productId;
                dta.Woil_Warehouse_Location__c = warehousesMap.get(wrpItem.warehouseLocation);
                dta.Woil_Serial_Number__c =wrpItem.serialNo;
                dta.Woil_Sales_Order__c=retOrdNo;
                dta.Woil_Customer__c=wrpItem.customer;
                data_set.add(dta);
            }
            insert data_set;

            return 'Success';
        }catch(Exception ex){
            return 'error';
        }
        
        
    }

    public class WrapperSerialNos {
        @AuraEnabled public integer rowId {get;set;}
        @AuraEnabled public String skuCode {get;set;}
        @AuraEnabled public String warehouseLocation {get;set;}
        @AuraEnabled public String serialNo {get;set;}
        @AuraEnabled public String customer {get;set;}
        @AuraEnabled public String productId {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String productName {get;set;}
        
    }
}