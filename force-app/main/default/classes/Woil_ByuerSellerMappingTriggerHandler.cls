/**
* --------------------------------------------------------------------------------------------------
* Name of Class - Woil_ByuerSellerMappingTriggerHandler
* Initial Author - Shivam Baliyan (KPMG)
* Created Date - 19/8/2021
* Description -  In this class, Account is shared with the opposite Account'Contact 
* --------------------------------------------------------------------------------------------------
*/
public without sharing class Woil_ByuerSellerMappingTriggerHandler {
    public void shareRecordVisibility(Map<Id,WOIL_Buyer_Seller_Mapping__c> bsmNewMap,Map<Id,WOIL_Buyer_Seller_Mapping__c> bsmOldMap){
        if(bsmOldMap == null){
            shareRecordVisibilityHelper(bsmNewMap, null);
        }else{
            Map<Id,WOIL_Buyer_Seller_Mapping__c> newBSMMap = new Map<Id,WOIL_Buyer_Seller_Mapping__c>();
            Map<Id,WOIL_Buyer_Seller_Mapping__c> oldBSMMap = new Map<Id,WOIL_Buyer_Seller_Mapping__c>();
            for(Id bsmId : bsmNewMap.keySet()) {
                if(bsmNewMap.get(bsmId).Parent__c != bsmOldMap.get(bsmId).Parent__c) {
                    newBSMMap.put(bsmId,bsmNewMap.get(bsmId));
                    oldBSMMap.put(bsmId,bsmOldMap.get(bsmId));
                }
                if(bsmNewMap.get(bsmId).WOIL_Seller__c != bsmOldMap.get(bsmId).WOIL_Seller__c) {
                    newBSMMap.put(bsmId,bsmNewMap.get(bsmId));
                    oldBSMMap.put(bsmId,bsmOldMap.get(bsmId));
                }
                if(bsmNewMap.get(bsmId).Outlet__c != bsmOldMap.get(bsmId).Outlet__c) {
                    newBSMMap.put(bsmId,bsmNewMap.get(bsmId));
                    oldBSMMap.put(bsmId,bsmOldMap.get(bsmId));
                }
            }
            if(!newBSMMap.isEmpty()){
                shareRecordVisibilityHelper(newBSMMap,oldBSMMap);
            }
        }
    }

    public void shareRecordVisibilityHelper(Map<Id,WOIL_Buyer_Seller_Mapping__c> bsmNewMap, Map<Id,WOIL_Buyer_Seller_Mapping__c> bsmOldMap){

        if(bsmOldMap != null){

            Map<Id, List<Id>> buyerSellerMap = new Map<Id,List<Id>>();
            Map<Id,List<Id>> accConMap = new Map<Id,List<Id>>();
            List<AccountShare> accShareToInsert = new List<AccountShare>();
            Map<String,AccountShare> existingShareAccMap = new Map<String,AccountShare>();
            List<AccountShare> toBeDeleted = new List<AccountShare>();
           
            for(Id bsm : bsmOldMap.keySet()){
                
                if(!buyerSellerMap.containsKey(bsmOldMap.get(bsm).Parent__c)){
                    buyerSellerMap.put(bsmOldMap.get(bsm).Parent__c,new List<Id>());
                }
    
                List<Id> sellerList = buyerSellerMap.get(bsmOldMap.get(bsm).Parent__c);
                sellerList.add(bsmOldMap.get(bsm).WOIL_Seller__c);
                buyerSellerMap.put(bsmOldMap.get(bsm).Parent__c,sellerList);
    
                if(!buyerSellerMap.containsKey(bsmOldMap.get(bsm).WOIL_Seller__c)){
                    buyerSellerMap.put(bsmOldMap.get(bsm).WOIL_Seller__c,new List<Id>());
                }
    
                List<Id> buyerList = buyerSellerMap.get(bsmOldMap.get(bsm).WOIL_Seller__c);
                buyerList.add(bsmOldMap.get(bsm).Parent__c);
                buyerSellerMap.put(bsmOldMap.get(bsm).WOIL_Seller__c,buyerList);
                
            }
    
            for(User u : [SELECT Id, FirstName, ContactId, AccountID, Account.Name 
                            FROM User 
                            WHERE ContactId != null AND AccountID IN: buyerSellerMap.keySet()]){
                if(!accConMap.containsKey(u.AccountID)){
                    accConMap.put(u.AccountID,new List<Id>());
                }
                List<Id> userIdList = accConMap.get(u.AccountID);
                userIdList.add(u.Id);
                accConMap.put(u.AccountID,userIdList);
    
            }

            for(AccountShare accShare : [SELECT Id,AccountId,UserOrGroupId FROM AccountShare 
                                            WHERE AccountId IN: accConMap.keySet()  
                                            AND rowcause != 'Owner' ]){
                existingShareAccMap.put(accShare.AccountId+'#'+accShare.UserOrGroupId,accShare);

            }
    
            for(Id accID : buyerSellerMap.keySet()){
                for(Id accIDToShare : buyerSellerMap.get(accID)){
                    if(accConMap.containsKey(accID)){
                        for(Id userId : accConMap.get(accId)){
                            if(existingShareAccMap.containsKey(accIDToShare+'#'+userId)){
                                toBeDeleted.add(existingShareAccMap.get(accIDToShare+'#'+userId));
                            }
                        }
                    }
                }
            }

            if(!toBeDeleted.isEmpty()){
                delete toBeDeleted;
            }
        }

        Map<Id, List<Id>> buyerSellerMap = new Map<Id,List<Id>>();
        Map<Id,List<Id>> accConMap = new Map<Id,List<Id>>();
        List<AccountShare> accShareToInsert = new List<AccountShare>();
        List<Outlet_Master__Share> outletShareToInsert = new List<Outlet_Master__Share>();
        Set<String> outLetIds = new Set<String>();
       
        for(Id bsm : bsmNewMap.keySet()){

            if(!String.isBlank(bsmNewMap.get(bsm).Outlet__c)){
                outLetIds.add(bsmNewMap.get(bsm).Outlet__c);
            }
            
            if(!buyerSellerMap.containsKey(bsmNewMap.get(bsm).Parent__c)){
                buyerSellerMap.put(bsmNewMap.get(bsm).Parent__c,new List<Id>());
            }

            List<Id> sellerList = buyerSellerMap.get(bsmNewMap.get(bsm).Parent__c);
            sellerList.add(bsmNewMap.get(bsm).WOIL_Seller__c);
            buyerSellerMap.put(bsmNewMap.get(bsm).Parent__c,sellerList);

            if(!buyerSellerMap.containsKey(bsmNewMap.get(bsm).WOIL_Seller__c)){
                buyerSellerMap.put(bsmNewMap.get(bsm).WOIL_Seller__c,new List<Id>());
            }

            List<Id> buyerList = buyerSellerMap.get(bsmNewMap.get(bsm).WOIL_Seller__c);
            buyerList.add(bsmNewMap.get(bsm).Parent__c);
            buyerSellerMap.put(bsmNewMap.get(bsm).WOIL_Seller__c,buyerList);
            
        }

        for(User u : [SELECT Id, FirstName, ContactId, AccountID, Account.Name 
                        FROM User 
                        WHERE ContactId != null AND AccountID IN: buyerSellerMap.keySet()]){
            if(!accConMap.containsKey(u.AccountID)){
                accConMap.put(u.AccountID,new List<Id>());
            }
            List<Id> userIdList = accConMap.get(u.AccountID);
            userIdList.add(u.Id);
            accConMap.put(u.AccountID,userIdList);

        }
        
        List<String> sharedAccount = new List<String>();
        List<String> sharedOutlet = new List<String>();

        for(AccountShare accShare : [SELECT Id, UserOrGroupId, AccountId FROM AccountShare WHERE AccountId IN: buyerSellerMap.keySet()]){
            sharedAccount.add(accShare.UserOrGroupId+'#'+accShare.AccountId);
        }

        for(Outlet_Master__Share outShare : [SELECT Id,ParentId, UserOrGroupId, AccessLevel FROM Outlet_Master__Share WHERE ParentId IN : outLetIds]){
            sharedOutlet.add(outShare.UserOrGroupId+'#'+outShare.ParentId);
        }
    

        for(Id accID : buyerSellerMap.keySet()){
        	for(Id accIDToShare : buyerSellerMap.get(accID)){
                if(accConMap.containsKey(accID)){
                    for(Id userId : accConMap.get(accId)){
                        if(!sharedAccount.contains(userId+'#'+accIDToShare)){
                            AccountShare accShare = new AccountShare();
                            accShare.AccountId = accIDToShare;
                            accShare.OpportunityAccessLevel = 'Read';
                            accShare.AccountAccessLevel = 'Edit';
                            accShare.UserOrGroupId = userId;
                            accShareToInsert.add(accShare);
                        }
                        
                    }

                }
            }
            for(String str : outLetIds){
                if(accConMap.containsKey(accID)){
                    for(Id userId : accConMap.get(accId)){
                        if(!sharedOutlet.contains(userId+'#'+str)){
                            Outlet_Master__Share accShare = new Outlet_Master__Share();
                            accShare.ParentId = str;
                            accShare.AccessLevel = 'Edit';
                            accShare.UserOrGroupId = userId;
                            outletShareToInsert.add(accShare);
                        }
                        
                    }
                }
            }
        }
        if(!accShareToInsert.isEmpty()){
            insert accShareToInsert;
        }
        if(!outletShareToInsert.isEmpty()){
            insert outletShareToInsert;
        }
        
        
    }
}