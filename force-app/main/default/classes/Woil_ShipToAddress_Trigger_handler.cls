public class Woil_ShipToAddress_Trigger_handler {
    public static void BeforeInsert(List<Woil_ShipToAddress__c> woilShipAddress){
        Map<String, Account> accountMap = new Map<String, Account>();
        Set<String> customerName = new Set<String>();
        for(Woil_ShipToAddress__c Ap : woilShipAddress){
            customerName.add(Ap.Woil_CustomerName__c);
        }
        System.debug(customerName);
        List<Account> accList= [select Id  ,name,Woil_Account_Number__c from Account where Woil_Account_Number__c IN :customerName];     
        for(Account a :accList){
            accountMap.put(a.Woil_Account_Number__c,a);
        }
        System.debug(accountMap);
        for(Woil_ShipToAddress__c woilShipAdd : woilShipAddress){
            woilShipAdd.Woil_Customer__c = accountMap.get(woilShipAdd.Woil_CustomerName__c).Id;
        }
    }
    
    public static void BeforeUpdate(List<Woil_ShipToAddress__c> woilShipAddress){
        
    }
    
    
}