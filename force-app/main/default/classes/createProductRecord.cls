@RestResource(urlMapping='/createProductRecord/*')
global with sharing class createProductRecord {
    
    @HttpPost

      global Static void createProduct(){
          
      RestRequest req = RestContext.request;
      RestResponse res = Restcontext.response;

      string jsonString=req.requestBody.tostring();
      responseWrapper wResp=(responseWrapper) JSON.deserialize(jsonString,responseWrapper.class);

      Product2 obj=new Product2();
      obj.Name=wResp.name;
      obj.Woil_Category__c = wResp.category;
      obj.Woil_Sub_Category__c = wResp.subcategory;
      obj.Woil_Product_Type__c = wResp.type;
      obj.Woil_SAP_Created_Date__c =wResp.sapdate;

      Database.SaveResult srList =  Database.insert(obj,false); 

     
        if (srList.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            successWrapper sResp = new successWrapper();
                sResp.mapping.put('Message','Successfully record created '+srList.getId());
            
            res.responseBody = Blob.valueOf(JSON.serialize(sResp.mapping));
    	    	res.statusCode = 200;
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : srList.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Account fields that affected this error: ' + err.getFields());
                
                successWrapper sResp = new successWrapper();
                sResp.mapping.put(String.valueOf(err.getStatusCode()),err.getMessage());
                
                res.responseBody = Blob.valueOf(JSON.serialize(sResp.mapping));
    			      res.statusCode = 200;
                
            }
        }
    

     //return 'Message": "Successfully record created ';

      }

      global class responseWrapper{

        global string name;
        global string category;
        global string subcategory;
        global string type;
        global Date sapdate;


      }
    global class successWrapper{
        //global string message;
        //global string value;
        global Map<String, String> mapping = new Map<String, String>();
        
        
    }
}