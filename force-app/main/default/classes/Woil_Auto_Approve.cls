global class Woil_Auto_Approve implements Schedulable {
    global void execute(SchedulableContext SC) {
        List<ProcessInstanceWorkitem> workItems = [SELECT Id, ProcessInstanceId, OriginalActorId, ActorId, ElapsedTimeInDays, 
                                                   ElapsedTimeInHours, ElapsedTimeInMinutes, IsDeleted, CreatedDate, CreatedById,
                                                   SystemModstamp FROM ProcessInstanceWorkitem  
                                                  ];
        Set<Id> processInsId = new Set<Id>();
        
        for(ProcessInstanceWorkitem workItem : workItems){
            processInsId.add(workItem.ProcessInstanceId);
        }
        
        
        Map<Id,ProcessInstance> map_processIns=new Map<Id,ProcessInstance>(
            [SELECT Id, TargetObjectId FROM ProcessInstance where Id IN : processInsId]
        );
        
        List<ProcessInstance> pInstance_List = [SELECT Id, TargetObjectId FROM ProcessInstance where Id IN : processInsId];
        Set<Id> order_Info = new Set<Id>();
        for(ProcessInstance instance_Item : pInstance_List){
            order_Info.add(instance_Item.TargetObjectId);
        }
        
        //Active User ------remove(key)
        Map<Id,User> userActiveInfoMap =new Map<Id,User>([SELECT IsActive, Id FROM User 
                                                          where IsActive = true]);
        
        
        Map<Id,Order> totalOrder =new  Map<Id,Order>([
            select id,Approver_one__c, Approver_second__c from Order where Id IN :order_Info 
        ]);

        for(Order UserInfo : totalOrder.values()){
            if(userActiveInfoMap.containsKey(UserInfo.Approver_one__c) && userActiveInfoMap.containsKey(UserInfo.Approver_second__c)){
                
            }
            else{
                totalOrder.remove(UserInfo.Id);
            }
            
        }
        System.debug(totalOrder);
       
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
        
        for(ProcessInstanceWorkitem workItem : workItems){
            Id targetId = map_processIns.get(workItem.ProcessInstanceId).TargetObjectId;
            if(totalOrder.get(targetId).Approver_one__c == workItem.ActorId){
                if(workItem.ElapsedTimeInMinutes > Woil_Auto_Approval__mdt.getInstance('total_time').Time_To_Auto_Approve__c){
                    Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                    req.setWorkitemId(workItem.Id);
                    req.setAction('Approve');
                    req.setComments('Auto Approved');
                    requests.add(req);
                }
            }
            else if(totalOrder.get(targetId).Approver_second__c == workItem.ActorId){
                if(workItem.ElapsedTimeInMinutes > Woil_Auto_Approval__mdt.getInstance('total_time').Time_To_Auto_Approve__c){
                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                    req2.setWorkitemId(workItem.Id);
                    req2.setAction('Reject');
                    req2.setComments('Auto Rejected');
                    requests.add(req2);
                }  
            }
        }
        System.debug(requests);
        Approval.ProcessResult[] processResults = Approval.process(requests);
    }
}