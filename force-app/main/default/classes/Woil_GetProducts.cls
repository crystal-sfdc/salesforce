global class Woil_GetProducts{
    
    /**
* --------------------------------------------------------------------------------------------------
* Author - Nupur Chamaria
* Added On - 5/9/2021
* Description - Getting all Products based on Division(Finished Good, Spare Part)
* --------------------------------------------------------------------------------------------------
*/
    
    @AuraEnabled
    public static String getProductController(String recordId, String selectedRows){
        String returnString;
        List<String> selectedProdList = new List<String>();
        
        if(!String.isBlank(selectedRows)){
            List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
            for(WrapperOrderLineItem wrap : wResp){
                selectedProdList.add(wrap.prodName);
            }
        }
        
        List<ProcessInstance> orderProcess=  [SELECT Id, TargetObjectId FROM ProcessInstance where TargetObjectId=:recordId];
        List<ProcessInstanceWorkitem> workItems = new List<ProcessInstanceWorkitem>();
        if(!orderProcess.isEmpty()) {
        workItems= [SELECT Id, ProcessInstanceId, OriginalActorId, ActorId, ElapsedTimeInDays, 
                                                   ElapsedTimeInHours, ElapsedTimeInMinutes, IsDeleted, CreatedDate, CreatedById,
                                                   SystemModstamp FROM ProcessInstanceWorkitem WHERE ProcessInstanceId=:orderProcess[0].Id];
        }
        User u= [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
        
        Order orderRec= [SELECT Id, Status, Woil_Division__c FROM Order WHERE Id=:recordId];
        
        System.debug('Work Items'+workItems);
        
        if(!workItems.isEmpty()) {
        
        if(u.Id!=workItems[0].ActorId && orderRec.Status!= 'Draft') {
          /*  if(orderRec.Status!= 'Draft' && orderRec.Status == 'Approval Pending'){
                return 'You can add products only if you are the Approver';
            }*/
          
                System.debug('Inside Draft of WorkItem');
              returnString= 'You can add products only in Draft status';
                //return 'You can add products only in draft status'; 
            
        } 
        else{
            System.debug('Inside Else of Work Item');
            List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            if(orderRec.Woil_Division__c == 'Finish Good'){
                returnString= fetchFinishGood(userList,'','',selectedProdList,orderRec.Id);
            }else if(orderRec.Woil_Division__c == 'Spare Part'){
                returnString= fetchSparePart(userList,false,'','','',selectedProdList,orderRec.Id);
            }
           // return '';
            
        }
           
    }
    
    else {
       
        if( orderRec.Status!= 'Draft') {
             System.debug('Inside Else of Work Item1');
            returnString= 'You can add products only in Draft status';
        }
        else {
            System.debug('Inside Else of Work Item2');
            List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            if(orderRec.Woil_Division__c == 'Finish Good'){
                returnString= fetchFinishGood(userList,'','',selectedProdList,orderRec.Id);
            }else if(orderRec.Woil_Division__c == 'Spare Part'){
                returnString= fetchSparePart(userList,false,'','','',selectedProdList,orderRec.Id);
            }
            //return '';
            
        }
       
    }
        System.debug('Return String' +returnString);
        return returnString;
    
}


/**
* --------------------------------------------------------------------------------------------------
* Author - Nupur Chamaria
* Added On - 5/9/2021
* Description - Getting Finish Good Products
* --------------------------------------------------------------------------------------------------
*/

public static String fetchFinishGood(List<User> userList,String searchString, String catSearch, List<String> selectedProduct, Id ordId){
    
    Map<String,String> plantCodeMap = new Map<String,String>();
    Map<String,String> materialMap = new Map<String,String>();
    Set<String> finalProductSet = new Set<String>();
    Map<Id,Product2> productMap = new Map<Id,Product2>();
    Map<Id, Decimal> prodPriceMap= new Map<Id, Decimal>();
    List<WrapperProduct> wrapProdList= new List<WrapperProduct>();
    Map<String, String> sPartMap = new Map<String, String>();
    Map<String, String> prodCatMap = new Map<String, String>();
    WrapperProductList wrapList = new WrapperProductList();
    List<Product2> productList = new List<Product2> ();
    Map<Id,String> imageURLInfo= new Map<Id,String>();
    
    Id accId= [SELECT id, AccountId FROM Order WHERE id= :ordId].AccountId;
    
    List<Account> accList = [SELECT Id, (SELECT ID,Woil_Plant_Master__r.Woil_Plant_Code__c,Woil_Plant_Master__c FROM Account_Plants__r WHERE Woil_Division__c = 'AG'),
                             (SELECT Id, Woil_Product__c, Woil_Product__r.Name FROM Partner_Product_Mapping__r) 
                             FROM Account WHERE Id =: accId];
    for(Account acc : accList){
        for(Woil_Account_Plant__c accPlant : acc.Account_Plants__r){
            plantCodeMap.put(accPlant.Woil_Plant_Master__c,accPlant.Woil_Plant_Master__r.Woil_Plant_Code__c);
        }
        for(Woil_Partner_Product_Mapping__c ppm : acc.Partner_Product_Mapping__r){
            materialMap.put(ppm.Woil_Product__c, ppm.Woil_Product__r.Name);
        }
    }
    
    for(Woil_Plant_Product_Mapping__c ppm : [SELECT Woil_Product__c, Woil_Days_Of_Inventory__c 
                                             FROM Woil_Plant_Product_Mapping__c 
                                             WHERE Woil_Plant_Master__c IN:plantCodeMap.keySet() AND Woil_Product__c IN:materialMap.keySet()]){
                                                 finalProductSet.add(ppm.Woil_Product__c);
                                             }
    
    String searchStr=  '%' + searchString + '%';
    if(String.isNotBlank(catSearch)){
        productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c, Description, RecordType.Name ,
                       (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                       FROM Product2 
                       WHERE Id IN: finalProductSet AND IsActive= True AND (Name Like : searchStr OR Description Like : searchStr) AND Woil_Category__c=:catSearch LIMIT 20];
    }else{
        productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c, Description, RecordType.Name ,
                       (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                       FROM Product2 
                       WHERE Id IN: finalProductSet AND IsActive= True AND (Name Like : searchStr OR Description Like : searchStr) LIMIT 20];
    }
    
    for(Product2 prod : productList){
        productMap.put(prod.Id,prod);
        for(Woil_Product_Price__c prodprice : prod.Product_Price__r){
            prodPriceMap.put(prodprice.Woil_Product__c,prodprice.Woil_Dealer_Price__c);
        }
    }
    
    imageURLInfo= getImages(productMap);
    
    for(Id prodId: productMap.keySet()) {
        WrapperProduct wrapProd= new WrapperProduct();
        wrapProd.prodName= productMap.get(prodId).Name;
        wrapProd.prodDescription= productMap.get(prodId).Description;
        wrapProd.productId= prodId;
        wrapProd.prodRecordType=productMap.get(prodId).RecordType.Name;
        wrapProd.imageUrl= imageURLInfo.get(prodId);
        wrapProd.parentCode= productMap.get(prodId).Woil_Parent_Code__c;
        wrapProd.prodDealerPrice= prodPriceMap.get(prodId);
        wrapProd.prodCategory= productMap.get(prodId).Woil_Category__c;
        if(selectedProduct.contains(productMap.get(prodId).Name)){
            wrapProd.setCheckbox = true;
        }else{
            wrapProd.setCheckbox = false;
        }
        wrapProdList.add(wrapProd);
    }
    
    Schema.DescribeFieldResult fieldResultPart = Product2.Woil_Part_Type__c.getDescribe();
    List<Schema.PicklistEntry> pValuesPart = fieldResultPart.getPicklistValues();
    for (Schema.PicklistEntry p: pValuesPart) {
        sPartMap.put(p.getValue(), p.getLabel());
    }
    
    Schema.DescribeFieldResult fieldResultCat = Product2.Woil_Category__c.getDescribe();
    List<Schema.PicklistEntry> pValuesCat = fieldResultCat.getPicklistValues();
    for (Schema.PicklistEntry p: pValuesCat) {
        
        prodCatMap.put(p.getValue(), p.getLabel());
    }
    
    wrapList.wrapProductList = wrapProdList;
    wrapList.sPartTypeMap = sPartMap;
    wrapList.prodCategoryMap = prodCatMap;
    
    return JSON.serialize(wrapList);
}



/**
* --------------------------------------------------------------------------------------------------
* Author - Shivam Baliyan
* Added On - 19/8/2021
* Description - Getting Finish Good Products based on filter value
* --------------------------------------------------------------------------------------------------
*/

@AuraEnabled
public static string filteredOnCategory(String orderID,String filterValue, String searchString, String selectedRows){
    
    List<String> selectedProdList = new List<String>();
    
    if(!String.isBlank(selectedRows)){
        List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
        for(WrapperOrderLineItem wrap : wResp){
            selectedProdList.add(wrap.prodName);
        }
    }
    
    List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
    return fetchFinishGood(userList,searchString,filterValue,selectedProdList,orderID);
}    

/**
* --------------------------------------------------------------------------------------------------
* Author - Nupur Chamaria
* Added On - 5/9/2021
* Description - Adding OrderLineItem to Order
* --------------------------------------------------------------------------------------------------
*/

@AuraEnabled
public static string addOrderLineItemtoOrder(String selectedRows, String ordId){
    try {
        List<OrderItem> ordItemList= new List<OrderItem>();
        List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
        Set<Id> prodIdSet=new Set<Id>();
        
        Order ord= [SELECT Id, Woil_Division__c, Woil_SAP_Order_Type__c, Pricebook2Id FROM Order WHERE Id=:ordId];
        
        List<OrderItem> existingOrderItem= [SELECT Id,OrderId,Quantity, Product2Id FROM OrderItem WHERE OrderId=:ordId]; 
        
        Map<Id,OrderItem> existingPdts= new Map<Id,OrderItem>();
        
        for(OrderItem tempVar : existingOrderItem){
            existingPdts.put(tempVar.Product2Id, tempVar);
        }
        
        String getOrderType= ord.Woil_SAP_Order_Type__c;
        Boolean differentOrder = False;
        
        for(WrapperOrderLineItem tempVar : wResp){
            prodIdSet.add(tempVar.productId);
        }
        
        if(ord.Pricebook2Id == NULL) {
            ord.Pricebook2Id= [select Id, name from Pricebook2 where isStandard = true limit 1].Id;
            System.debug('Pricebook'+ord.Pricebook2Id);
        }
        
        if(String.isBlank(getOrderType)) {
            if(ord.Woil_Division__c == System.label.Woil_Finished_Goods) {
                if(wResp[0].prodCategory == 'AC') {
                    ord.Woil_SAP_Order_Type__c= System.label.Woil_AC_Order_Type;
                }
                else {
                    ord.Woil_SAP_Order_Type__c= System.label.Woil_Non_AC_Order_Type;
                }
            }
            else {
                ord.Woil_SAP_Order_Type__c= System.label.Woil_Spare_Part_Order_Type;
            }
            
            System.debug('Order Details'+Ord);
            update ord;
        }
        else{
            if(ord.Woil_Division__c == System.label.Woil_Finished_Goods) {
                if(wResp[0].prodCategory == 'AC') {
                    if(ord.Woil_SAP_Order_Type__c != System.label.Woil_AC_Order_Type) {
                        differentOrder= True;
                    }
                }
                else {
                    if(ord.Woil_SAP_Order_Type__c != System.label.Woil_Non_AC_Order_Type) {
                        differentOrder= True;
                    }
                }
            }
        }
        
        if(!differentOrder) {    
            
            List<PricebookEntry> pricebkEntryList= [SELECT Id, Product2Id FROM PricebookEntry WHERE Product2Id IN: prodIdSet];
            Map<Id,Id> prodPricebkMap= new Map<Id,Id>();
            
            for(PricebookEntry pbe:pricebkEntryList) {
                prodPricebkMap.put(pbe.Product2Id, pbe.Id);
            }
            
            for(WrapperOrderLineItem wrappObj: wResp) {
                if(existingPdts.keySet().contains(wrappObj.productId)) {
                    OrderItem ordItem = existingPdts.get(wrappObj.productId);
                    ordItem.Quantity= existingPdts.get(wrappObj.productId).Quantity+wrappObj.quantity;
                    ordItemList.add(ordItem);
                }
                else {
                    OrderItem ordItem= new OrderItem();
                    ordItem.OrderId= ordId;
                    ordItem.Product2Id= wrappObj.productId;
                    ordItem.Quantity= wrappObj.quantity;
                    ordItem.UnitPrice= WrappObj.prodDealerPrice;
                    ordItem.PricebookEntryId= prodPricebkMap.get(wrappObj.productId);
                    ordItem.Description=WrappObj.prodDescription;
                    ordItemList.add(ordItem);
                } 
            }
            upsert ordItemList;
            return 'Product added Successfully';
        }
        else {
            throw new DifferentProductException();
        }
    }
    catch(DifferentProductException e){
        System.debug('The following error has occurred.');
        return 'Error: You cannot club AC with Non-AC products ';
    }
    catch(Exception e){
        System.debug('The following error has occurred.'); 
        System.debug('Exception caught: ' + e.getMessage()); 
        throw new AuraHandledException(e.getMessage());
    }
}

/**
* --------------------------------------------------------------------------------------------------
* Author - Nupur Chamaria
* Added On - 5/9/2021
* Description - Getting Spare Part Products
* --------------------------------------------------------------------------------------------------
*/

public static String fetchSparePart(List<User> userList, Boolean tooPartType, String searchString,String partType, String categoryType, List<String> selectedProduct, Id ordId){
    Set<Id> accPlantIds= new Set<Id>();
    Set<Id> plantProdSet = new Set<Id>();
    Map<String,String> modelSPMap= new Map<String,String>();
    
    Map<String,String> plantCodeMap = new Map<String,String>();
    Map<String,String> materialMap = new Map<String,String>();
    Set<String> finalProductSet = new Set<String>();
    Map<Id,Product2> productMap = new Map<Id,Product2>();
    Map<Id, Decimal> prodPriceMap= new Map<Id, Decimal>();
    List<WrapperProduct> wrapProdList= new List<WrapperProduct>();
    Map<String, String> sPartMap = new Map<String, String>();
    Map<String, String> prodCatMap = new Map<String, String>();
    WrapperProductList wrapList = new WrapperProductList();
    List<Product2> productList = new List<Product2> ();
    Map<Id,String> imageURLInfo= new Map<Id,String>();
    String searchStr=  '%' + searchString + '%';
    
    Id accId= [SELECT id, AccountId FROM Order WHERE id= :ordId].AccountId;
    
    for(Woil_Account_Plant__c plantId: [SELECT Woil_Plant_Master__c FROM Woil_Account_Plant__c 
                                        WHERE Woil_Trade_Partner__c=:accId AND Woil_Division__c = 'AS']) {
                                            accPlantIds.add(plantId.Woil_Plant_Master__c);
                                        }
    
    for(Woil_Plant_Product_Mapping__c plantProd: [SELECT Woil_Product__c, Woil_Days_Of_Inventory__c FROM Woil_Plant_Product_Mapping__c 
                                                  WHERE Woil_Plant_Master__c IN:accPlantIds]) {
                                                      plantProdSet.add(plantProd.Woil_Product__c);
                                                  }
    
    if(searchString=='' || searchString== null) {
        for(Woil_Model_SparePart_Mapping__c str : [SELECT Id, Woil_SparePart_Code__c, Woil_Material_Code__c FROM Woil_Model_SparePart_Mapping__c]) {
            modelSPMap.put(str.Woil_SparePart_Code__c, str.Woil_Material_Code__c);
        }
    }
    
    
    
    if(String.isBlank(categoryType) && String.isBlank(partType) ){
        productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                       (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                       FROM Product2 
                       WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =:tooPartType];
    }
    else if(String.isBlank(categoryType) && String.isNotBlank(partType)) { 
        productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                       (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                       FROM Product2 
                       WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =:tooPartType AND Woil_Part_Type__c=:partType];
    }
    else if(String.isNotBlank(categoryType) && String.isBlank(partType)) {
        productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                       (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                       FROM Product2 
                       WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =:tooPartType AND Woil_Category__c=:categoryType];
    }
    else {
        productList = [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c,Woil_Top_Part__c, Woil_Part_Type__c, Description, RecordType.Name ,
                       (SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Product_Price__r WHERE Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today())
                       FROM Product2 
                       WHERE Id IN: plantProdSet AND IsActive= True AND Woil_Top_Part__c =: tooPartType AND Woil_Category__c=:categoryType AND Woil_Part_Type__c=:partType];
    }
    
    Set<String> prodSPName= new  Set<String>();
    for(Product2 prodId: productList) {
        prodSPName.add(prodId.Name);
    }
    
    if(searchString!='' || searchString!= null) {
        for(Woil_Model_SparePart_Mapping__c str : [SELECT Id, Woil_SparePart_Code__c, Woil_Material_Code__c FROM Woil_Model_SparePart_Mapping__c WHERE Woil_SparePart_Code__c IN:prodSPName AND Woil_Material_Code__c LIKE:searchString]) {
            modelSPMap.put(str.Woil_SparePart_Code__c, str.Woil_Material_Code__c);
        }
    }
    
    for(Product2 prod : productList){
        productMap.put(prod.Id,prod);
        for(Woil_Product_Price__c prodprice : prod.Product_Price__r){
            prodPriceMap.put(prodprice.Woil_Product__c,prodprice.Woil_Dealer_Price__c);
        }
    }
    
    imageURLInfo= getImages(productMap);
    
    if(searchString =='' || searchString == null) {
        for(Id prodId: productMap.keySet()) {
            WrapperProduct wrapProd= new WrapperProduct();
            wrapProd.prodName = productMap.get(prodId).Name;
            wrapProd.prodDescription = productMap.get(prodId).Description;
            wrapProd.productId = prodId;
            wrapProd.prodRecordType = productMap.get(prodId).RecordType.Name;
            wrapProd.imageUrl= imageURLInfo.get(prodId);
            wrapProd.parentCode = productMap.get(prodId).Woil_Parent_Code__c;
            wrapProd.prodDealerPrice = prodPriceMap.get(prodId);
            wrapProd.prodCategory = productMap.get(prodId).Woil_Category__c;
            wrapProd.modelName = modelSPMap.get(productMap.get(prodId).Name);
            wrapProd.partType = productMap.get(prodId).Woil_Part_Type__c;
            wrapProd.topPart = productMap.get(prodId).Woil_Top_Part__c;
            if(selectedProduct.contains(productMap.get(prodId).Name)){
                wrapProd.setCheckbox = true;
            }else{
                wrapProd.setCheckbox = false;
            }
            wrapProdList.add(wrapProd);
        }
    }
    
    else if(searchString!='' || searchString!= null) {
        
        for(String prodName: modelSPMap.keySet()) {
            for(Id prodId: productMap.keySet()) {
                if(prodName == productMap.get(prodId).Name) {
                    WrapperProduct wrapProd= new WrapperProduct();
                    wrapProd.prodName = prodName;
                    wrapProd.prodDescription = productMap.get(prodId).Description;
                    wrapProd.productId = prodId;
                    wrapProd.prodRecordType = productMap.get(prodId).RecordType.Name;
                    wrapProd.imageUrl= imageURLInfo.get(prodId);
                    wrapProd.parentCode = productMap.get(prodId).Woil_Parent_Code__c;
                    wrapProd.prodDealerPrice = prodPriceMap.get(prodId);
                    wrapProd.prodCategory = productMap.get(prodId).Woil_Category__c;
                    wrapProd.modelName = modelSPMap.get(productMap.get(prodId).Name);
                    wrapProd.partType = productMap.get(prodId).Woil_Part_Type__c;
                    wrapProd.topPart = productMap.get(prodId).Woil_Top_Part__c;
                    if(selectedProduct.contains(productMap.get(prodId).Name)){
                        wrapProd.setCheckbox = true;
                    }else{
                        wrapProd.setCheckbox = false;
                    }
                    wrapProdList.add(wrapProd);
                }
            }
        }
        
    }
    
    
    
    Schema.DescribeFieldResult fieldResultPart = Product2.Woil_Part_Type__c.getDescribe();
    List<Schema.PicklistEntry> pValuesPart = fieldResultPart.getPicklistValues();
    for (Schema.PicklistEntry p: pValuesPart) {
        sPartMap.put(p.getValue(), p.getLabel());
    }
    
    
    
    Schema.DescribeFieldResult fieldResultCat = Product2.Woil_Category__c.getDescribe();
    List<Schema.PicklistEntry> pValuesCat = fieldResultCat.getPicklistValues();
    for (Schema.PicklistEntry p: pValuesCat) {
        
        prodCatMap.put(p.getValue(), p.getLabel());
    }
    
    wrapList.wrapProductList = wrapProdList;
    wrapList.sPartTypeMap = sPartMap;
    wrapList.prodCategoryMap = prodCatMap;
    
    return JSON.serialize(wrapList);
}

@AuraEnabled
public static string filterOnSparePart(String orderID,String topPartType, String categoryType,String partType,String searchString, String selectedRows){
    List<String> selectedProdList = new List<String>();
    
    if(!String.isBlank(selectedRows)){
        List<WrapperOrderLineItem> wResp= (List<WrapperOrderLineItem>)JSON.deserialize(selectedRows,List<WrapperOrderLineItem>.class);
        for(WrapperOrderLineItem wrap : wResp){
            selectedProdList.add(wrap.prodName);
        }
    }
    
    List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
    Boolean topPartStr = false;
    if(topPartType == 'false'){
        topPartStr = false;
    }else{
        topPartStr = true; 
    }
    return fetchSparePart(userList,topPartStr,searchString,partType,categoryType,selectedProdList,orderID);
}

/**
* --------------------------------------------------------------------------------------------------
* Author - Nupur Chamaria
* Added On - 5/9/2021
* Description - Getting Alternate LFL Products
* --------------------------------------------------------------------------------------------------
*/

@AuraEnabled
public static List<WrapperProduct> getAlternateProducts(String prodtId, Id orderId) {
    System.debug('Inside getAlternateProducts');
    List<WrapperProduct> wrapProdList= new List<WrapperProduct>();
    List<Woil_Partner_Product_Mapping__c> partnerProductMapping= new List<Woil_Partner_Product_Mapping__c>();
    List<Id> partnerProdIds= new List<Id>();
    List<Woil_Account_Plant__c> accPlantMapping= new List<Woil_Account_Plant__c>();
    List<Id> accPlantIds= new List<Id>();
    List<Woil_Plant_Product_Mapping__c> plantProductMapping= new List<Woil_Plant_Product_Mapping__c>();
    Map<Id,Decimal> plantProdMap= new Map<Id,Decimal>();
    Map<Id,String> imageURLMap= new Map<Id,String>();
    List<Woil_Product_Price__c> prodPiceList= new List<Woil_Product_Price__c>();
    Map<Id, Decimal> prodPriceMap= new Map<Id, Decimal>();
    Map<Id,Product2> prodMap= new  Map<Id,Product2>();
    
    Product2 pdt= [SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c, Description, RecordType.Name FROM Product2 WHERE Id =: prodtId];
    String parentCode= pdt.Woil_Parent_Code__c;
    System.debug('Product.ParentCode'+pdt.Woil_Parent_Code__c);
    System.debug('Parent Code'+parentCode);
    if(String.isNotBlank(parentCode)) {
        System.debug('Inside Alternate If');
        
        Order ord= [SELECT AccountId, Woil_Division__c, Pricebook2Id FROM Order WHERE Id=: orderID];
        Id accId= ord.AccountId;
        String division= ord.Woil_Division__c;
        String type;
        if(division == System.label.Woil_Finished_Goods){
            type= 'AG';
            System.debug('Inside AG');
            partnerProductMapping= [SELECT Woil_Product__c FROM Woil_Partner_Product_Mapping__c WHERE Woil_Customer_Name__c=:accId]; 
            
            System.debug('Partner Product'+partnerProductMapping);
            
            for(Woil_Partner_Product_Mapping__c prodId:partnerProductMapping) {
                partnerProdIds.add(prodId.Woil_Product__c);
            }
            
            accPlantMapping= [SELECT Woil_Plant_Master__c FROM Woil_Account_Plant__c WHERE Woil_Trade_Partner__c=:accId AND Woil_Division__c=:type];
            
            System.debug('Account Plant'+accPlantMapping);
            
            
            for(Woil_Account_Plant__c plantId: accPlantMapping) {
                accPlantIds.add(plantId.Woil_Plant_Master__c);
            }
            
            plantProductMapping= [SELECT Woil_Product__c, Woil_Days_Of_Inventory__c FROM Woil_Plant_Product_Mapping__c WHERE Woil_Plant_Master__c IN:accPlantIds AND Woil_Product__c IN:partnerProdIds];
            
            System.debug('Plant Product'+plantProductMapping);
            
            for(Woil_Plant_Product_Mapping__c plantProd: plantProductMapping) {
                plantProdMap.put(plantProd.Woil_Product__c, plantProd.Woil_Days_Of_Inventory__c);
            }
            System.debug('Plant Product Map'+plantProdMap);
            
            prodMap= new Map<Id,Product2>([SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c, Description, RecordType.Name FROM Product2 WHERE Id IN: plantProdMap.keySet() AND Id !=:prodtId AND Woil_Parent_Code__c=:parentCode  LIMIT 20]);
            System.debug('Product Map'+prodMap);
            
            imageURLMap= getImages(prodMap);
            
            
            prodPiceList= [SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Woil_Product_Price__c WHERE Woil_Product__c IN:prodMap.keySet() AND Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today()];
            
            for(Woil_Product_Price__c prodPrice: prodPiceList) {
                prodPriceMap.put(prodPrice.Woil_Product__c, prodPrice.Woil_Dealer_Price__c);
            }
            
        }
        else {
            type= 'AS';
            
            accPlantMapping= [SELECT Woil_Plant_Master__c FROM Woil_Account_Plant__c WHERE Woil_Trade_Partner__c=:accId AND Woil_Division__c=:type];
            
            System.debug('Account Plant'+accPlantMapping);
            
            
            for(Woil_Account_Plant__c plantId: accPlantMapping) {
                accPlantIds.add(plantId.Woil_Plant_Master__c);
            }
            
            plantProductMapping= [SELECT Woil_Product__c, Woil_Days_Of_Inventory__c FROM Woil_Plant_Product_Mapping__c WHERE Woil_Plant_Master__c IN:accPlantIds];
            System.debug('Plant Product'+plantProductMapping);
            
            for(Woil_Plant_Product_Mapping__c plantProd: plantProductMapping) {
                plantProdMap.put(plantProd.Woil_Product__c, plantProd.Woil_Days_Of_Inventory__c);
            }
            System.debug('Plant Product Map'+plantProdMap);
            
            prodMap= new Map<Id,Product2>([SELECT Id,Name,Woil_Parent_Code__c, Woil_Category__c, Description, RecordType.Name FROM Product2 WHERE Id IN: plantProdMap.keySet() AND Id !=:prodtId AND Woil_Parent_Code__c=:parentCode  LIMIT 20]);
            System.debug('Product Map'+prodMap);
            
            
            imageURLMap= getImages(prodMap);
            
            
            prodPiceList= [SELECT Woil_Customer__c, Woil_Dealer_Price__c, Woil_Product__c FROM Woil_Product_Price__c WHERE Woil_Product__c IN:prodMap.keySet() AND Woil_Valid_From__c <=: System.today() AND Woil_Valid_To__c >=: System.today()];
            
            for(Woil_Product_Price__c prodPrice: prodPiceList) {
                prodPriceMap.put(prodPrice.Woil_Product__c, prodPrice.Woil_Dealer_Price__c);
            }
        }
        
        for(Id prodId: prodMap.keySet()) {
            WrapperProduct wrapProd= new WrapperProduct();
            wrapProd.prodName= prodMap.get(prodId).Name;
            wrapProd.prodDescription= prodMap.get(prodId).Description;
            wrapProd.productId= prodId;
            wrapProd.parentCode= prodMap.get(prodId).Woil_Parent_Code__c;
            wrapProd.imageUrl= imageURLMap.get(prodId);
            wrapProd.prodDealerPrice= prodPriceMap.get(prodId);
            wrapProd.prodCategory= prodMap.get(prodId).Woil_Category__c;
            wrapProd.setCheckbox = false;
            wrapProdList.add(wrapProd);
        }
    }
    return wrapProdList;
}

/**
* --------------------------------------------------------------------------------------------------
* Author - Nupur Chamaria
* Added On - 5/9/2021
* Description - Getting Images from Google Drive
* --------------------------------------------------------------------------------------------------
*/

public static Map<Id,String> getImages(Map<Id,Product2> prodMap){
    Map<Id,String> imageURLMap= new Map<Id,String>();
    List<String> listSKUName= new List<String>();
    String varSKUName;
    
    Http http = new Http();
    HttpRequest request= new HttpRequest();
    request.setMethod('GET');
    //271Ef0vC06C9NGxmpA7s1zLdHSgO1qpiJ1r  change to ur Grdive Folder ID
    request.setEndpoint('callout:Google_Drive/drive/v3/files/?q=%271Ef0vC06C9NGxmpA7s1zLdHSgO1qpiJ1r%27%20in%20parents&fields=files(id%2Cname%2CwebContentLink%2CwebViewLink%2ChasThumbnail%2CthumbnailLink)');
    HTTPResponse response;
    response = http.send(request);
    
    GoogleDriveFiles results  = (GoogleDriveFiles)JSON.deserialize(response.getBody(), GoogleDriveFiles.class);
    for(Files eachFile: results.files) {
        varSKUName= eachFile.name.substringBeforeLast('.');
        listSKUName.add(varSKUName);
    } 
    
    for(Files eachfile: results.files){
        for(Id pd:prodMap.keySet()) {
            if(eachFile.name.substringBeforeLast('.')==prodMap.get(pd).Name) {
                System.debug('Name'+eachFile.name.substringBeforeLast('.'));
                imageURLMap.put(pd, 'https://drive.google.com/uc?id='+eachfile.id);
            }                
        }
    }
    
    return imageURLMap;
}

public class WrapperProductList {
    @AuraEnabled public Map<String,String> prodCategoryMap {get;set;}
    @AuraEnabled public Map<String,String> sPartTypeMap {get;set;}
    @AuraEnabled public List<WrapperProduct> wrapProductList {get;set;}
    
}

public class WrapperProduct {
    @AuraEnabled public String prodName {get;set;}
    @AuraEnabled public Id productId {get;set;}
    @AuraEnabled public String prodDescription {get;set;}
    @AuraEnabled public Decimal prodDaysofInventory {get;set;}
    @AuraEnabled public Decimal prodDealerPrice {get;set;}
    @AuraEnabled public String prodCategory {get;set;}
    @AuraEnabled public string imageUrl{get;set;} 
    @AuraEnabled public string parentCode{get;set;}
    @AuraEnabled public string prodRecordType{get;set;}
    @AuraEnabled public string modelName{get;set;}
    @AuraEnabled public Boolean setCheckbox{get;set;}
    @AuraEnabled public string partType{get;set;}
    @AuraEnabled public Boolean topPart {get;set;}
}

public class WrapperOrderLineItem {
    public String prodName {get;set;}
    public String prodCategory {get;set;}
    public Decimal prodDealerPrice {get;set;}
    public String prodDescription {get;set;}
    public Decimal quantity {get;set;}
    public Id productId {get;set;}
}

public class GoogleDriveFiles{
    public Files[] files;
    
}

public class Files{
    public string id{get;set;}    
    public string name{get;set;}
    public string webViewLink{get;set;}
    public boolean hasThumbnail{get;set;}
    public string thumbnailLink{get;set;}
}
}