public class Woil_Product_Trigger_handler implements Woil_Product_Trigger_interface {
    public  void BeforeInsert(List<SObject> newItems) {
        Map<String,Id> RId=new Map<String,Id>();
        for(RecordType rt : [select id,name from RecordType where SobjectType = 'Product2']){
            RId.put(rt.Name , rt.Id);
        }
        for(Product2 p : (List<Product2>)newItems){
            if(p.Woil_Product_Type__c =='Finish Good')
            {
                p.RecordTypeId = RId.get('Finished Goods');
                p.Woil_Is_Serializable__c = true;
            }
            else if(p.Woil_Product_Type__c =='Spare Part'){
                p.RecordTypeId = RId.get('Spare Parts');
            }
        } 
        
        
    }
    
    public void AfterInsert(List<sObject> newRecordsList , Map<Id, SObject> newItems) {
        Pricebook2 SPricebook = [select name,id from Pricebook2 where name ='Standard Price Book'];
        List<PricebookEntry> PE=new List<PricebookEntry>();
        for(Product2 p: (List<Product2>)newItems.values()){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Pricebook2Id = SPricebook.id;
            pbe.Product2Id = p.id;
            pbe.UnitPrice = 0;
            pbe.IsActive = true;
            PE.add(pbe);
        }
        insert PE;
    }
    
    public void BeforeUpdate(List<SObject> newRecord, Map<Id, SObject> oldItems) {
        
        System.debug((List<Product2>)newRecord);
        
        Map<String,Id> RId=new Map<String,Id>();
        for(RecordType rt : [select id,name from RecordType where SobjectType = 'Product2']){
            RId.put(rt.Name , rt.Id);
        }
        System.debug(RId);
        
        for(Product2 uP : (List<Product2>)newRecord){
            if(uP.Woil_Product_Type__c =='Finish Good')
            {
                uP.RecordTypeId = RId.get('Finished Goods');
                  uP.Woil_Is_Serializable__c = true;
            }
            else if(uP.Woil_Product_Type__c =='Spare Part'){
                uP.RecordTypeId = RId.get('Spare Parts');
            }            
            
            
        }
        
        
    }
    
    public void BeforeDelete(List<sObject> oldRecordsList , Map<Id, SObject> oldItems) {}
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(List<sObject> newRecordsList , Map<Id, sObject> newItems) {}
}