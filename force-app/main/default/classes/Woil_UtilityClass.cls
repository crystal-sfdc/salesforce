public class Woil_UtilityClass {
    Public static Boolean firstcall=false;
    public static ID getUserContactID(ID loginUserId){
        User userObj = [select id, ContactId from User where id = : loginUserId];
        return userObj.ContactId;
    }
    
    public static ID getAccountIdFromContactID(ID contactID){
        Contact conObj = [select id, AccountId from Contact where id = : contactID];
        return conObj.AccountId;
    }
    
    public static Boolean isLogCommunityUser(){
        Map<String,String> currentSession = Auth.SessionManagement.getCurrentSession();
        Boolean CommunityUser = false;
        if(currentSession.get('LoginHistoryId')==null && currentSession.get('SourceIp')=='::' && currentSession.get('UserType')=='Standard')
            CommunityUser=true;
        return CommunityUser;
        
    }
    
    public static HttpRequest prepareRequest(String endpoint, String methodName, String reqBody, Map<String,String> headerMap){
        
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endpoint);
        req.setMethod(methodName);
        req.setBody(reqBody);
        req.setTimeout(120000);
        if(headerMap != NULL && headerMap.keySet().size() > 0){
            for(String headerId : headerMap.keySet()){
                req.setHeader(headerId, headerMap.get(headerId));
            }
        }
        return req;
    }
    
    //Method to get the content id of the document passed in the parameter
    public static String getTemplateId(String docName){
        String docid;
        
        List<Document> lstDocument = [Select Id,Name from Document where Name =:docName];
        system.debug('lstDocument'+lstDocument);
        if(!lstDocument.isEmpty()){
            string strOrgId = UserInfo.getOrganizationId();
            docid=lstDocument[0].Id;
            System.debug('docid:'+docid);
        }
        return docid;
        
    }
    
    //Method to check alphanumeric value is present in the string or not
    public static Boolean checkAlphaNumeric(String inputVal){
        Boolean check = false;
        Woil_Default_Value_Configuration__mdt customMet = Woil_Default_Value_Configuration__mdt.getInstance('Special_Character');
        System.debug('customMetada Value-->'+ customMet);
        List<String> specialCharacList = customMet.Woil_Default_Value__c.split(',');
        System.debug('customMetada Value List-->'+ specialCharacList);
        
        String[] stringCharacters = inputVal.split('');
        System.debug('stringCharacters--->'+stringCharacters);
        for(String str:stringCharacters){
            if(specialCharacList.contains(str)){
                check =true;
                break;
            }
        }
        return check;
    }
    
    //To Check The Stock availability Woil_UtilityClass.checkStockAvailability
    public static String checkStockAvailability(String recordId){
        
        String productString = '';
        List<Woil_SRN__c> purchaseReturnRec  = [SELECT Id, Woil_On_Spot_Return__c, Woil_Account__c, 
                                                Woil_ShipToAddress__r.Name, Woil_Supplier__c FROM Woil_SRN__c 
                                                Where Woil_On_Spot_Return__c = false And Id = : recordID];
        if(purchaseReturnRec.size() <= 0)
            throw new AuraHandledException('No check In case of On spot return');
        System.debug('purchaseReturnRec===> '+purchaseReturnRec);
        
        List<Woil_SRN_Line_Item__c> srnLineItemObjList = [SELECT Id, Woil_SRN__c, Woil_SKU_Code__c, 
                                                          Woil_Ordered_Quantity__c 
                                                          FROM Woil_SRN_Line_Item__c 
                                                          Where Woil_SRN__c  = : recordID
                                                          AND Woil_SRN__r.Woil_On_Spot_Return__c = false];
        List<String> skuCodeList = new List<String>();
        System.debug('srnLineItemObjList===> '+srnLineItemObjList);
        
        Map<String,Integer> returnItemMap = new Map<String,Integer>();
        for(Woil_SRN_Line_Item__c obj : srnLineItemObjList){
            returnItemMap.Put(obj.Woil_SKU_Code__c, Integer.valueOf(obj.Woil_Ordered_Quantity__c));
        }
        System.debug('returnItemMap===> '+returnItemMap);
        
        System.debug('skuCodeList===> '+returnItemMap.keySet());
        System.debug('Warehouse Location===> '+purchaseReturnRec[0].Woil_ShipToAddress__r.Name );
        System.debug('Woil_Supplier__c===> '+purchaseReturnRec[0].Woil_Account__c);
        List<Woil_Serial_Number_Transaction__c> serialNumberTransactionList = [SELECT Id, Woil_Product__r.Name, Woil_Quantity__c, 
                                                                               Woil_Warehouse_Location__c, Woil_Serial_Transaction_Type__c 
                                                                               FROM Woil_Serial_Number_Transaction__c 
                                                                               Where Woil_Warehouse_Location__c = :
                                                                               purchaseReturnRec[0].Woil_ShipToAddress__r.Name 
                                                                               And Woil_Product__r.Name IN : returnItemMap.keySet()
                                                                               And Woil_Customer__c  = : purchaseReturnRec[0].Woil_Account__c];
        System.debug('serialNumberTransactionList===> '+serialNumberTransactionList);
        
        if(serialNumberTransactionList.size() > 0){
            Map<String,Integer> inventoryMap = new Map<String,Integer>();
            for(Woil_Serial_Number_Transaction__c obj : serialNumberTransactionList){
                Integer valueofMap = 0;
                if((obj.Woil_Quantity__c != null) &&
                   (obj.Woil_Serial_Transaction_Type__c == 'Stock In' ||
                    obj.Woil_Serial_Transaction_Type__c == 'Purchase' ||
                    obj.Woil_Serial_Transaction_Type__c == 'Purchase Return')){
                        if(inventoryMap.containsKey(obj.Woil_Product__r.Name)){
                            if(obj.Woil_Serial_Transaction_Type__c == 'Purchase Return')
                                valueofMap = inventoryMap.get(obj.Woil_Product__r.Name) - Integer.valueOf(obj.Woil_Quantity__c) ;
                            else
                                valueofMap = inventoryMap.get(obj.Woil_Product__r.Name) + Integer.valueOf(obj.Woil_Quantity__c) ;
                            inventoryMap.remove(obj.Woil_Product__r.Name);
                            inventoryMap.Put(obj.Woil_Product__r.Name, valueofMap);
                        }
                        else{
                            inventoryMap.Put(obj.Woil_Product__r.Name, Integer.valueOf(obj.Woil_Quantity__c));
                        }   
                    }
            }
            System.debug('inventoryMap===> '+inventoryMap);
            if(returnItemMap.size() > 0 && returnItemMap != null){
                for(String skuCode :returnItemMap.keySet()){
                    Integer tempReturnValue = returnItemMap.get(skuCode) * (-1);
                    System.debug('return > total Error===> '+tempReturnValue+' > '+ inventoryMap.get(skuCode));
                    if(tempReturnValue > inventoryMap.get(skuCode)){
                        if(productString == '')
                            productString += skuCode;
                        else
                            productString += ','+skuCode;
                    }
                }   
            }
            
            if(productString == '')
                productString = 'Pass';
        }
        
        if(productString == '')
            return 'notBelong'; //Stock not belong to your inventory
        else if(productString == 'Pass') // stock available And valid purchase return.
            return productString;
        else
            return 'notAvailable '+productString; //Stock not available for selected products 
    }
    
    public static String generateRandomString(Integer lengthOfRandomString) {
        
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randomStr = '';
        while (randomStr.length() < lengthOfRandomString) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randomStr += chars.substring(idx, idx+1);
        }
        return randomStr;
    }
    
}