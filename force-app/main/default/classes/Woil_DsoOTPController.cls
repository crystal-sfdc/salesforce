public with sharing class  Woil_DsoOTPController {
 
    @AuraEnabled(cacheable=true) 
    public static Contact doInitAction (String recordId1) {
        System.debug('information'+ recordId1);
            Contact con = [select id , Woil_Active__c from Contact where Id = : recordId1];
                return con;

        
    }
    
    @AuraEnabled
    public static void SubmitAction (String recordId1) {
        System.debug('hello '+ recordId1);
        Contact con = [select id , Woil_Active__c from Contact where Id = : recordId1];
        con.Woil_Active__c = true;
        update con;
    }
    @AuraEnabled
    public static String DsoDeactivate (String recordId1) {
        Contact con = [select id , Woil_Active__c from Contact where Id = : recordId1];
        System.debug(con);
        if(con.Woil_Active__c == true){
              con.Woil_Active__c = false;
                update con;
            return 'true';
        }
        else{
            return 'false';
            
        }
        
    }
    
}