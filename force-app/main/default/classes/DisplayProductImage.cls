public class DisplayProductImage {
    @AuraEnabled
    public static list<Product2> fetchProductRecords(){
		return [select Name,ProductCode, TestProductImage__c from Product2 limit 20];        
    }

}