public class Woil_InvoiceSerial_Trigger_Handler {
    public static void  isBeforeInsert(List<Woil_Invoice_Serial_Number__c> newInvoiceSerialNumber){
        Set<String> invoiceInfo=new Set<String>();
        Set<String> invoiceLineInfo=new Set<String>();
    
        for(Woil_Invoice_Serial_Number__c inSerial : newInvoiceSerialNumber){
if(inSerial.Woil_SAP_Invoice_Id__c != null && inSerial.Woil_SAP_Invoice_Line_Id__c != null){

            invoiceInfo.add(inSerial.Woil_SAP_Invoice_Id__c);
            invoiceLineInfo.add(inSerial.Woil_SAP_Invoice_Line_Id__c);
}
        }
        
        
        if(invoiceInfo.size() > 0 && invoiceLineInfo.size() > 0){
        List<invoice__c> invo = [SELECT Id, Woil_SAP_Invoice_ID__c,
                                 (SELECT Id, Woil_SAP_INV_Line_ID__c FROM Invoice_Line_Items__r where Woil_SAP_INV_Line_ID__c IN : invoiceLineInfo)
                                 FROM Invoice__c where Woil_SAP_Invoice_ID__c IN : invoiceInfo];
        
        
        Map<String,invoice__c > invoInfoMap = new  Map<String,invoice__c>();
        for(invoice__c inc : invo){
            invoInfoMap.put(inc.Woil_SAP_Invoice_ID__c,inc);
        }
        
        
     
        //lookUp resolve--------------------------------------------------
        for(Woil_Invoice_Serial_Number__c invoSerialfinal : newInvoiceSerialNumber ){
            
            for(Woil_Invoice_Line_Item__c inLine : invoInfoMap.get(invoSerialfinal.Woil_SAP_Invoice_Id__c).Invoice_Line_Items__r ){
                if(invoSerialfinal.Woil_SAP_Invoice_Line_Id__c == inLine.Woil_SAP_INV_Line_ID__c){
                    invoSerialfinal.Woil_Invoice_Line_Item__c = inLine.Id;
                    
                }
            }
            
        }
        
        }
    }
    
}