public without sharing class Woil_PostGRNController {
    public static List<Woil_GRN_Serial_Number__c> serials=new List<Woil_GRN_Serial_Number__c>();
    public static List<Woil_Serial_Number_Transaction__c> Lstserials_txn=new List<Woil_Serial_Number_Transaction__c>();
    public static List<Woil_Serial_Bank__c> Lstserials_bank=new List<Woil_Serial_Bank__c>();
    public static List<Woil_Serial_Number_Owner__c> Lstserials_owner=new List<Woil_Serial_Number_Owner__c>();
    public static map<Id,decimal> ProdVsQuantity= new map<Id,decimal>();
    public static List<OrderItem> LstOrderItem=new List<OrderItem>();
    public static Map<String,Woil_Return_Order_Line_Item__c>  SkuVsROLineItems=new Map<String,Woil_Return_Order_Line_Item__c>();
    public static List<Woil_Return_Order_Line_Item__c>  ROLineItems=new List<Woil_Return_Order_Line_Item__c>();
    public static Set<String> RO_LI_Ids=new Set<String>();
    public static List <Woil_Return_Order_Serial_Number__c> Lst_RO_Serials=new List <Woil_Return_Order_Serial_Number__c>();
    public static List <Woil_GRN_Line_Item__c> Lst_GRN_Items=new List <Woil_GRN_Line_Item__c>();
    public static List <Woil_SRN_Line_Item__c> Lst_SRN_Items=new List <Woil_SRN_Line_Item__c>();

    @AuraEnabled
    public static String checkValidPostReturnEntry(String recordId){
        String status = Woil_UtilityClass.checkStockAvailability(recordId);
        system.debug('Woil_UtilityClass.checkStockAvailability()===> '+status);
        return status;
    }
    
    @AuraEnabled
    public static void PostGrn(String GrnId){
        try{
            List<WOIL_Buyer_Seller_Mapping__c> buyerList = new List<WOIL_Buyer_Seller_Mapping__c>();
            System.debug('GrnId::'+GrnId);
            List<User> userList = [SELECT Id, AccountId, ContactId, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            
             List<Account> accList = [SELECT Id ,(SELECT Id, WOIL_Seller__c, WOIL_Seller__r.Woil_Account_Number__c,WOIL_Seller__r.Woil_PAN_Number__c, WOIL_Seller__r.Name 
                                      FROM Buyer_Seller_Mappings__r) 
                                      FROM Account 
                                      Where Id =: userList[0].AccountId];
            for(WOIL_Buyer_Seller_Mapping__c buyerSellerRec : accList[0].Buyer_Seller_Mappings__r){
                buyerList.add(buyerSellerRec);
            }
            GRN__c grn=[Select Woil_Posted__c from GRN__c where Id=:GrnId];
            Lst_GRN_Items=[select Woil_Quantity_Non_Serialize__c,Woil_Product__c,Woil_GRN__r.Woil_Account__c,Woil_GRN__r.Woil_Purchase_Order__c,Woil_GRN__r.Woil_Sales_Invoice__c,Woil_GRN__r.Woil_Posted__c,Woil_GRN__r.Woil_Ship_To_Code__c from Woil_GRN_Line_Item__c where Woil_GRN__c =:GrnId and Woil_Product__r.Woil_Is_Serializable__c=false];
            
            serials=[select Woil_Invoice_Line_Item__r.Woil_Product__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Account__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Purchase_Order__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Sales_Invoice__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Account__r.Woil_Geolocation__c,Woil_Serial_Number__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Posted__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Ship_To_Code__c from Woil_GRN_Serial_Number__c where Woil_Invoice_Line_Item__r.Woil_GRN__c =:GrnId];
            if(serials.size()!=0){
                LstOrderItem= [select Product2Id,Woil_Received_Quantity__c from OrderItem where OrderId=:serials[0].Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Purchase_Order__c];
            }
            for(OrderItem ord:LstOrderItem){
                ProdVsQuantity.put(ord.Product2Id, ord.Woil_Received_Quantity__c!=null?ord.Woil_Received_Quantity__c:0);
            }
            System.debug('ProdVsQuantity::'+ProdVsQuantity);
        
        if(grn.Woil_Posted__c!=true){
            for(Woil_GRN_Serial_Number__c serial:serials)   {
                decimal quantity=ProdVsQuantity.get(serial.Woil_Invoice_Line_Item__r.Woil_Product__c)!=null?ProdVsQuantity.get(serial.Woil_Invoice_Line_Item__r.Woil_Product__c):0;
                ProdVsQuantity.put(serial.Woil_Invoice_Line_Item__r.Woil_Product__c, quantity+1);
                Woil_Serial_Bank__c serial_bank=new Woil_Serial_Bank__c();
                serial_bank.Woil_Customer__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Account__c;
                serial_bank.Woil_Invoice__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Sales_Invoice__c;
                serial_bank.Woil_Product__c=serial.Woil_Invoice_Line_Item__r.Woil_Product__c;
                serial_bank.Woil_Serial_Number__c=serial.Woil_Serial_Number__c;
                serial_bank.Woil_Order__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Purchase_Order__c;
                serial_bank.Woil_Current_Sales_Motion__c='Sell-In';
                serial_bank.Woil_Serial_Transaction_Type__c='Purchase';
                serial_bank.Woil_Transaction_Date__c=date.today();
                serial_bank.Woil_Warehouse_Code__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Ship_To_Code__c;
                Lstserials_bank.add(serial_bank);
                Woil_Serial_Number_Owner__c serial_owner=new Woil_Serial_Number_Owner__c();
                serial_owner.Woil_Customer__c=  serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Account__c;
                serial_owner.Woil_Product__c=serial.Woil_Invoice_Line_Item__r.Woil_Product__c;
                serial_owner.Woil_Serial_Number__c=serial.Woil_Serial_Number__c;
                serial_owner.Transit_Status__c='Sell-In';
                serial_owner.Woil_Counter_Customer1__c=buyerList[0].WOIL_Seller__c;
                serial_owner.Woil_Order__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Purchase_Order__c;
                serial_owner.Woil_Invoice__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Sales_Invoice__c;
                serial_owner.Woil_WareHouse_Location__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Ship_To_Code__c;
                serial_owner.Woil_Transaction_Date__c=Date.today();
                Lstserials_owner.add(serial_owner);
                Woil_Serial_Number_Transaction__c serial_txn=new Woil_Serial_Number_Transaction__c();
                serial_txn.Woil_Customer__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Account__c;
                serial_txn.Woil_Counter_Customer__c=buyerList[0].WOIL_Seller__c;
                serial_txn.Woil_Product__c=serial.Woil_Invoice_Line_Item__r.Woil_Product__c;
                serial_txn.Woil_Serial_Number__c=serial.Woil_Serial_Number__c;
                serial_txn.Woil_Sales_Motion__c='Sell-In';
                serial_txn.Woil_Quantity__c=1;
                serial_txn.Woil_Serial_Transaction_Type__c='Purchase';
                serial_txn.Woil_Order__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Purchase_Order__c;
                serial_txn.Woil_Invoice__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Sales_Invoice__c;
                serial_txn.Woil_Transaction_Date__c=Date.today();
                serial_txn.Woil_Warehouse_Location__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Ship_To_Code__c;
                Lstserials_txn.add(serial_txn);
            }
            /*Lstserials_txn.clear();
            Lstserials_owner.clear();
            Lstserials_bank.clear();*/
            System.debug('Lst_GRN_Items::'+Lst_GRN_Items);
            for(Woil_GRN_Line_Item__c grnItem:Lst_GRN_Items){
              Woil_Serial_Bank__c serial_bank=new Woil_Serial_Bank__c();
                serial_bank.Woil_Customer__c=grnItem.Woil_GRN__r.Woil_Account__c;
                serial_bank.Woil_Invoice__c=grnItem.Woil_GRN__r.Woil_Sales_Invoice__c;
                serial_bank.Woil_Product__c=grnItem.Woil_Product__c;
                serial_bank.Woil_Order__c=grnItem.Woil_GRN__r.Woil_Purchase_Order__c;
                serial_bank.Woil_Current_Sales_Motion__c='Sell-In';
                serial_bank.Woil_Serial_Transaction_Type__c='Purchase';
                serial_bank.Woil_Transaction_Date__c=date.today();
                serial_bank.Woil_Warehouse_Code__c=grnItem.Woil_GRN__r.Woil_Ship_To_Code__c;
                Lstserials_bank.add(serial_bank);
                Woil_Serial_Number_Owner__c serial_owner=new Woil_Serial_Number_Owner__c();
                serial_owner.Woil_Customer__c=  grnItem.Woil_GRN__r.Woil_Account__c;
                serial_owner.Woil_Product__c=grnItem.Woil_Product__c;
                serial_owner.Transit_Status__c='Sell-In';
                serial_owner.Woil_Order__c=grnItem.Woil_GRN__r.Woil_Purchase_Order__c;
                serial_owner.Woil_Invoice__c=grnItem.Woil_GRN__r.Woil_Sales_Invoice__c;
                serial_owner.Woil_Counter_Customer1__c=buyerList[0].WOIL_Seller__c;
                serial_owner.Woil_WareHouse_Location__c=grnItem.Woil_GRN__r.Woil_Ship_To_Code__c;
                serial_owner.Woil_Transaction_Date__c=Date.today();
                //serial_owner.Woil_Location__c=serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Account__r.Woil_Geolocation__c;
                Lstserials_owner.add(serial_owner);
                Woil_Serial_Number_Transaction__c serial_txn=new Woil_Serial_Number_Transaction__c();
                serial_txn.Woil_Customer__c=grnItem.Woil_GRN__r.Woil_Account__c;
                serial_txn.Woil_Counter_Customer__c=buyerList[0].WOIL_Seller__c;
                serial_txn.Woil_Product__c=grnItem.Woil_Product__c;
                serial_txn.Woil_Sales_Motion__c='Sell-In';
                serial_txn.Woil_Serial_Transaction_Type__c='Purchase';
                serial_txn.Woil_Order__c=grnItem.Woil_GRN__r.Woil_Purchase_Order__c;
                serial_txn.Woil_Invoice__c=grnItem.Woil_GRN__r.Woil_Sales_Invoice__c;
                serial_txn.Woil_Transaction_Date__c=Date.today();
                serial_txn.Woil_Warehouse_Location__c=grnItem.Woil_GRN__r.Woil_Ship_To_Code__c;
                serial_txn.Woil_Quantity__c=grnItem.Woil_Quantity_Non_Serialize__c;
                Lstserials_txn.add(serial_txn);  
            }
                       
            List<Database.saveResult> lstGRNLIResult =Database.insert(Lstserials_bank,false);
           List<Database.saveResult> lstGRNLIResult1= Database.insert(Lstserials_owner,false);
           List<Database.saveResult> lstGRNLIResult2 =Database.insert(Lstserials_txn,false); 
            System.debug('lstGRNLIResult::'+lstGRNLIResult+'::lstGRNLIResult1::'+lstGRNLIResult1+'::lstGRNLIResult2::'+lstGRNLIResult2);
            grn.Woil_Posted__c=true;
            grn.Woil_Transaction_Date__c=Date.today();
            update grn;
        }else{
            grn.addError('GRN Already Posted!');
        }
        for(OrderItem ord:LstOrderItem){
           ord.Woil_Received_Quantity__c= ProdVsQuantity.get(ord.Product2Id);
        }
        System.debug('LstOrderItem:'+LstOrderItem); 
        update LstOrderItem;
        }catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage()+'::At::'+e.getLineNumber());
        }
    }
    
    @AuraEnabled
    public static void ReleaseGrn(String serial_no){
        
        Woil_GRN_Serial_Number__c serial =[select Woil_Serial_Number__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Posted__c from Woil_GRN_Serial_Number__c where id=:serial_no];
        if(serial.Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Posted__c!=true){
            Woil_Invoice_Serial_Number__c inv_serial=[select id,Woil_Accepted__c from Woil_Invoice_Serial_Number__c where Woil_Serial_Number__c=:serial.Woil_Serial_Number__c ];
            inv_serial.Woil_Accepted__c=false;
            update inv_serial;
            delete serial;
            List<Woil_GRN_Serial_Number__c> all_serial =[select id from Woil_GRN_Serial_Number__c where Woil_Invoice_Line_Item__r.Woil_GRN__c=:serial.Woil_Invoice_Line_Item__r.Woil_GRN__c];
            if(all_serial.size()==0){
                GRN__c grn=new GRN__C(id=serial.Woil_Invoice_Line_Item__r.Woil_GRN__c);
                delete grn;
            }
        }else{
            serial.addError('GRN Posted!');
        }        
    }
    
    @AuraEnabled
    public static List < Woil_GRN_Serial_Number__c > getGRNItems(String grn_no) {
        
        return [ SELECT Id, Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Sales_Invoice__r.Name,Woil_Invoice_Line_Item__r.Woil_Product__r.Name,Woil_Serial_Number__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Account__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Purchase_Order__c,Woil_Invoice_Line_Item__r.Woil_Product__c,Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Posted__c FROM Woil_GRN_Serial_Number__c where Woil_Invoice_Line_Item__r.Woil_GRN__c=:grn_no  ];
    }
    
    @AuraEnabled
    public static void ReleaseGRNItems(List<Woil_GRN_Serial_Number__c> LstSerialNos) {
        System.debug('LstSerialNos:::::'+LstSerialNos);
        Set<String> serial_nos=new Set<String>();
        Set<String> serial_items=new Set<String>();
        map<string, List<String>> mapSerial=new map<string,List<String>>();
        List<string> serials=new List<string>();
        for(Woil_GRN_Serial_Number__c serial:LstSerialNos){
            String serialNo=serial.Woil_Serial_Number__c;
            serial_nos.add(serialNo);           
            serial_items.add(serial.Woil_Invoice_Line_Item__c);
        }        
        List<Woil_Invoice_Serial_Number__c> LstInvSerial=  [Select Id,Woil_Serial_Number__c,Woil_Accepted__c from Woil_Invoice_Serial_Number__c where Woil_Serial_Number__c in :serial_nos];
        for(Woil_Invoice_Serial_Number__c serial: LstInvSerial){
            serial.Woil_Accepted__c=false;
        }
        update LstInvSerial;
        delete LstSerialNos;
        Set<String> updt_serial_items=new Set<String>();
        Set<String> delete_serial_items=new Set<String>();
        List<Woil_GRN_Serial_Number__c> all_serial =[select id,Woil_Invoice_Line_Item__r.Woil_Product__r.Name,Woil_Invoice_Line_Item__c from Woil_GRN_Serial_Number__c where Woil_Invoice_Line_Item__r.Woil_GRN__c=:LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_GRN__c];
        List<Woil_GRN_Serial_Number__c> all_serial1 =[select id,Woil_Invoice_Line_Item__r.Woil_Product__r.Name,Woil_Invoice_Line_Item__c from Woil_GRN_Serial_Number__c where Woil_Invoice_Line_Item__r.Woil_GRN__c=:LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_GRN__c and Woil_Invoice_Line_Item__c in :serial_items];
        
        if(all_serial.size()==0){
            GRN__c grn=new GRN__C(id=LstSerialNos[0].Woil_Invoice_Line_Item__r.Woil_GRN__c);
            delete grn;
        }else{
            for(Woil_GRN_Serial_Number__c serial:all_serial1){
                updt_serial_items.add(serial.Woil_Invoice_Line_Item__c);
            }
            for(String updt:serial_items){
                if(!updt_serial_items.contains(updt)){
                    delete_serial_items.add(updt)  ;
                }                
            }
            List<Woil_GRN_Line_Item__c> grn_lineItems=new List<Woil_GRN_Line_Item__c>();
            for(String ids:delete_serial_items){
                Woil_GRN_Line_Item__c grnLI=new  Woil_GRN_Line_Item__c(id=ids);
                grn_lineItems.add(grnLI); 
            }
            System.debug('delete GRN Line Item::::'+delete_serial_items);
            delete  grn_lineItems;            
        }        
    }
    
        @AuraEnabled
    public static void postSRN(String recordId ) {
        system.debug('Woil_UtilityClass.checkStockAvailability()===> '
                     +Woil_UtilityClass.checkStockAvailability(recordId));
        
        Woil_SRN__c srn1=[Select Woil_Posted__c,Woil_On_Spot_Return__c from Woil_SRN__c where Id=:recordId];
        Lst_SRN_Items=[select Woil_Quantity__c,Woil_Product__r.Name,Woil_Product__c,Woil_SRN__r.Woil_Account__c,Woil_SRN__r.Woil_Purchase_Order__c,Woil_SRN__r.Woil_Sales_Invoice__c,Woil_Serial_Number__c,Woil_SRN__r.Woil_Posted__c,Woil_SRN__r.Woil_Purchase_Order__r.Woil_ShipToAddress__c,Woil_SRN__r.Woil_reason__c from Woil_SRN_Line_Item__c where Woil_SRN__c =:recordId and Woil_Product__r.Woil_Is_Serializable__c=false];
        List<Woil_SRN_Serial_Number__c> LstSerialNos=[select Woil_SRN_Line_Item__r.Woil_Product__r.Name,Woil_SRN_Line_Item__r.Woil_Product__c,Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Account__c,Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Purchase_Order__c,Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Sales_Invoice__c,Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Account__r.Woil_Geolocation__c,Woil_Serial_Number__c,Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Posted__c,Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Purchase_Order__r.Woil_ShipToAddress__c,Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_reason__c from Woil_SRN_Serial_Number__c where Woil_SRN_Line_Item__r.Woil_SRN__c =:recordId ];
        if((LstSerialNos.size()>0  || Lst_SRN_Items.size()>0) && srn1.Woil_On_Spot_Return__c==true) {
            if(srn1.Woil_Posted__c!=true){
               /* 
                Woil_Return_Order__c ro=new Woil_Return_Order__c();
                if( LstSerialNos.size()>0){
                    List<Woil_Return_Order__c> ro_existing =[select id from Woil_Return_Order__c where Woil_Reason__c=:LstSerialNos[0].Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_reason__c and Woil_Parent_Order__c=:LstSerialNos[0].Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Purchase_Order__c];
                    //ro.Woil_Parent_Order__c=LstSerialNos[0].Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Purchase_Order__c;
                    if(ro_existing.size()==0 ){
                        ro.Woil_ShipToAddress__c=LstSerialNos[0].Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Purchase_Order__r.Woil_ShipToAddress__c;
                        ro.Woil_Account__c=LstSerialNos[0].Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_Account__c;
                        ro.Status__c ='Submitted';
                        ro.Woil_Reason__c=LstSerialNos[0].Woil_SRN_Line_Item__r.Woil_SRN__r.Woil_reason__c;
                    }else if(ro_existing.size()>0){
                        ro.Id= ro_existing[0].Id;   
                    }
                }else if(Lst_SRN_Items.size()>0){
                    List<Woil_Return_Order__c> ro_existing1 =[select id from Woil_Return_Order__c where Woil_Reason__c=:Lst_SRN_Items[0].Woil_SRN__r.Woil_reason__c and Woil_Parent_Order__c=:Lst_SRN_Items[0].Woil_SRN__r.Woil_Purchase_Order__c];
                    if(ro_existing1.size()==0){
                        ro.Woil_ShipToAddress__c=Lst_SRN_Items[0].Woil_SRN__r.Woil_Purchase_Order__r.Woil_ShipToAddress__c;
                        ro.Woil_Account__c=Lst_SRN_Items[0].Woil_SRN__r.Woil_Account__c;
                        ro.Status__c ='Submitted';
                        ro.Woil_Reason__c=Lst_SRN_Items[0].Woil_SRN__r.Woil_reason__c;
                    } else if(ro_existing1.size()>0){
                        ro.Id= ro_existing1[0].Id;    
                    }
                }           
        upsert ro;      
        //System.debug('LstSerialNos::::'+LstSerialNos);
        for(Woil_SRN_Serial_Number__c SerialNo:LstSerialNos){            
            
            if(!SkuVsROLineItems.containsKey(SerialNo.Woil_SRN_Line_Item__r.Woil_Product__r.Name) ){
                Woil_Return_Order_Line_Item__c GRNLineItem=new Woil_Return_Order_Line_Item__c();
                GRNLineItem.Woil_Product__c	=SerialNo.Woil_SRN_Line_Item__r.Woil_Product__c;
                GRNLineItem.Woil_Return_Order__c	=ro.Id;
                SkuVsROLineItems.put(SerialNo.Woil_SRN_Line_Item__r.Woil_Product__r.Name, GRNLineItem);                
            }           
        }
        
        for(Woil_SRN_Line_Item__c item:Lst_SRN_Items){
            if(!SkuVsROLineItems.containsKey(item.Woil_Product__r.Name) ){
                Woil_Return_Order_Line_Item__c GRNLineItem=new Woil_Return_Order_Line_Item__c();
                GRNLineItem.Woil_Product__c	=item.Woil_Product__c;
                GRNLineItem.Woil_Return_Order__c	=ro.Id;
                GRNLineItem.Woil_Quantity__c	=item.Woil_Quantity__c;
                SkuVsROLineItems.put(item.Woil_Product__r.Name, GRNLineItem);                
            }           
        }
            
        ROLineItems =SkuVsROLineItems.values();
        List<Database.saveResult> lstGRNLIResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+ROLineItems);
        lstGRNLIResult = Database.insert(ROLineItems,false);
         // Iterate SaveResult array
        for (Database.SaveResult result : lstGRNLIResult) {
            if (result.isSuccess()) {
                
                System.debug('GRNLI Successfully inserted, Account Id is: ' + result.getId());
                RO_LI_Ids.add(result.getId());
            }
            else {
                //Error ecountered              
                for(Database.Error error : result.getErrors()) {                 
                    System.debug(error.getStatusCode() + ': ' + error.getMessage() + 
                                 ' Fields that affected the error: ' + error.getFields());                    
                }
            }
        }
        List<Woil_Return_Order_Line_Item__c>  Lst_RO_LI=[Select Id,Woil_Product__r.ProductCode from Woil_Return_Order_Line_Item__c where id=:RO_LI_Ids];
        for(Woil_SRN_Serial_Number__c RO_SerialNos:LstSerialNos){
            for(Woil_Return_Order_Line_Item__c GRN_LI:Lst_RO_LI){
                if(RO_SerialNos.Woil_SRN_Line_Item__r.Woil_Product__c==GRN_LI.Woil_Product__c){
                    Woil_Return_Order_Serial_Number__c RO_Serial=new Woil_Return_Order_Serial_Number__c();
                    RO_Serial.Woil_Return_Order_Line_Item__c =GRN_LI.Id; 
                    RO_Serial.Woil_Serial_Number__c=RO_SerialNos.Woil_Serial_Number__c;
                    Lst_RO_Serials.add(RO_Serial);
                }
                
            }
        }
        
        List<Database.saveResult> lstGRNSerialResult = new List<Database.saveResult>();
        system.debug('Before Updating Account -- '+Lst_RO_Serials);
        lstGRNSerialResult = Database.insert(Lst_RO_Serials,false);	*/
       
        Woil_SRN__c srn=[select Woil_Posted__c from Woil_SRN__c where id=:recordId];
        srn.Woil_Posted__c=true;
        srn.Woil_Return_Order_Date__c=Date.today();
        update srn;
        }else{
            LstSerialNos[0].addError('Return Already Posted!');
        }
        }else{
            Woil_SRN__c srn=[select Woil_Posted__c from Woil_SRN__c where id=:recordId];
            if(srn.Woil_Posted__c!=true){
                List<String> SKUCodes=new List<String>();
                Set<String> serialNos=new Set<String>();
                List<Woil_Serial_Number_Scan__c> serial_scans=[select Woil_Product__r.Name,Woil_Purchase_Return__c,id,Woil_Serial_Number_Scan__c.Woil_Product__c,Woil_Serial_Number__c,Woil_Warehouse_Location__c,Woil_Customer__c,Woil_Purchase_Return__r.Woil_Supplier_Code__c,Woil_Purchase_Return__r.Woil_Supplier__c,Woil_Purchase_Return__r.Woil_ShipToAddress__r.Woil_Code__c,Woil_Purchase_Return__r.Woil_ShipToAddress__r.Name from Woil_Serial_Number_Scan__c where Woil_Purchase_Return__c=:recordId];
                List<Woil_Serial_Number_Transaction__c> Lstrnx=new List<Woil_Serial_Number_Transaction__c>();
                List<Woil_SRN_Line_Item__c> LstPR=[Select Woil_Quantity__c,Woil_Product__c from Woil_SRN_Line_Item__c where  id=:recordId];
                Map<Id,decimal> SKUVsQty=new Map<Id,decimal>(); 
                Map<string,Woil_Serial_Number_Transaction__c> SerialVsTxn =new Map<string,Woil_Serial_Number_Transaction__c>();
                Woil_SRN__c salesOrder = [SELECT Id,Woil_Account__c,Woil_Supplier__c,Woil_ShipToAddress__r.Name,Woil_Supplier_Name__c 
                                                  FROM Woil_SRN__c 
                                                  WHERE Id =: recordId];
                
                User custCode = [SELECT Id, AccountId,Account.Woil_Account_Number__c,Account.Name, ContactId, Profile.Name 
                                 FROM USER WHERE ID =: UserInfo.getUserId()];
                
                for(Woil_Serial_Number_Scan__c snTrans : serial_scans){
                    SKUCodes.add(snTrans.Woil_Product__r.Name);
                    serialNos.add(snTrans.Woil_Serial_Number__c);
                }
                List<Woil_Serial_Number_Owner__c> serial_txn = [SELECT Id,Woil_Product__r.Name,Woil_WareHouse_Location__c,Woil_Customer__c,Woil_Serial_Number__c,Woil_Product__c,Transit_Status__c 
                                                                FROM Woil_Serial_Number_Owner__c 
                                                                WHERE Woil_Product__r.Name IN: SKUCodes AND 
                                                                        Woil_Warehouse_Location__c =: salesOrder.Woil_ShipToAddress__r.Name AND 
                                                                        (Transit_Status__c='Opening Balances' OR Transit_Status__c = 'Sell-In') AND 
                                                                        Woil_Serial_Number__c IN : serialNos AND Woil_Customer__c=:custCode.AccountId];
				
                for(Woil_Serial_Number_Owner__c serial : serial_txn){
                    serial.Woil_Customer__c = salesOrder.Woil_Account__c;
                    serial.Transit_Status__c = 'Return';
                    serial.Woil_Warehouse_Location__c = salesOrder.Woil_ShipToAddress__r.Name;
                    serial.Woil_Counter_Customer__c=salesOrder.Woil_Supplier_Name__c;
                    serial.Woil_Counter_Customer1__c = custCode.Account.Id;
                    serial.Woil_Transaction_Date__c = System.Today();
                }
                
            for(Woil_Serial_Number_Scan__c scan:serial_scans){
                Woil_Serial_Number_Transaction__c trnx=new Woil_Serial_Number_Transaction__c();
                trnx.Woil_Product__c=scan.Woil_Product__c;
                trnx.Woil_Serial_Number__c=scan.Woil_Serial_Number__c;
               	trnx.Woil_Quantity__c=-1;
                trnx.Woil_Transaction_Date__c=Date.today();
                trnx.Woil_Sales_Motion__c='Return';
                trnx.Woil_Serial_Transaction_Type__c='Purchase Return';
                trnx.Woil_Warehouse_Location__c=scan.Woil_Purchase_Return__r.Woil_ShipToAddress__r.Name;
                trnx.Woil_Customer__c=scan.Woil_Customer__c;
                trnx.Woil_Counter_Customer__c=scan.Woil_Purchase_Return__r.Woil_Supplier__c;
                trnx.Woil_Purchase_Return__c=scan.Woil_Purchase_Return__c;
                SKUVsQty.put(scan.Woil_Product__c,SKUVsQty.get(scan.Woil_Product__c)==null?0:-1-1);
                SerialVsTxn.put(scan.Woil_Serial_Number__c,trnx);
            }
                for(Woil_SRN_Line_Item__c prLI:LstPR){
                    decimal qty	= SKUVsQty.get(prLI.Woil_Product__c);
                    prLI.Woil_Quantity__c =qty;
                }
                System.debug('SerialVsTxn::::'+SerialVsTxn);
                if(!serial_txn.isEmpty()){
                    update serial_txn;
                }
                if(!SerialVsTxn.values().isEmpty()){
                    insert SerialVsTxn.values();   
                }
                          
            update LstPR;
            delete serial_scans;
            List<Woil_SRN_Line_Item__c> LstSparePart=[Select Woil_Quantity__c,Woil_SRN__r.Woil_ShipToAddress__r.Name,Woil_SRN__r.Woil_Account__c,Woil_SRN__r.Woil_Supplier__c,Woil_SRN__c,Woil_Product__r.Name,Woil_Product__c from Woil_SRN_Line_Item__c where Woil_SRN__c=:recordId and Woil_Product__r.Woil_Is_Serializable__c=false];
             Map<string,Woil_Serial_Number_Transaction__c> SKuVsTxn =new Map<string,Woil_Serial_Number_Transaction__c>();
                Set<String> SkuCodes1=new Set<String>();
                for(Woil_SRN_Line_Item__c srn_item:LstSparePart){
                   SkuCodes1.add(srn_item.Woil_Product__r.Name); 
                }
            List<Woil_Serial_Number_Owner__c> serial_txn1 = [SELECT Id,Woil_Product__r.Name,Woil_WareHouse_Location__c,Woil_Customer__c,Woil_Serial_Number__c,Woil_Product__c,Transit_Status__c 
                                                                FROM Woil_Serial_Number_Owner__c 
                                                                WHERE Woil_Product__r.Name IN: SkuCodes1 AND 
                                                                        Woil_Warehouse_Location__c =: salesOrder.Woil_ShipToAddress__r.Name AND 
                                                                        (Transit_Status__c='Opening Balances' OR Transit_Status__c = 'Sell-In') AND 
                                                                         Woil_Customer__c=:custCode.AccountId];
                System.debug('serial_txn1::'+serial_txn1);
                 for(Woil_Serial_Number_Owner__c serial : serial_txn1){
                    serial.Woil_Customer__c = salesOrder.Woil_Account__c;
                    serial.Transit_Status__c = 'Purchase Return';
                    serial.Woil_Warehouse_Location__c = salesOrder.Woil_ShipToAddress__r.Name;
                    serial.Woil_Counter_Customer__c=salesOrder.Woil_Supplier_Name__c;
                    serial.Woil_Counter_Customer1__c = custCode.Account.Name;
                    serial.Woil_Transaction_Date__c = System.Today();
                }
                 for(Woil_SRN_Line_Item__c sparepart:LstSparePart){
                Woil_Serial_Number_Transaction__c trnx=new Woil_Serial_Number_Transaction__c();
                trnx.Woil_Product__c=sparepart.Woil_Product__c;
               	trnx.Woil_Quantity__c=sparepart.Woil_Quantity__c;
                trnx.Woil_Transaction_Date__c=Date.today();
                trnx.Woil_Sales_Motion__c='Return';
                trnx.Woil_Serial_Transaction_Type__c='Purchase Return';
                trnx.Woil_Warehouse_Location__c=sparepart.Woil_SRN__r.Woil_ShipToAddress__r.Name;
                trnx.Woil_Customer__c=sparepart.Woil_SRN__r.Woil_Account__c;
                trnx.Woil_Counter_Customer__c=sparepart.Woil_SRN__r.Woil_Supplier__c;
                trnx.Woil_Purchase_Return__c=sparepart.Woil_SRN__c;
                SKuVsTxn.put(sparepart.Woil_Product__c,trnx);
            }
                if(!SKuVsTxn.isEmpty()){
                    insert SKuVsTxn.values();
                }
                //srn.Woil_Posted__c=true;
                //update srn;
                
            }else{
                srn.addError('Return Already Posted!'); 
            }
        }
    }
}