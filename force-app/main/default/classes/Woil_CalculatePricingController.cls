public without sharing class Woil_CalculatePricingController {
    @AuraEnabled
    public static String getPriceController(String recordId){
        try{
            Map<String, String> lineItemAPIName = new Map<String, String>{'ZR01' => 'Woil_RDM__c','ZE01' => 'Woil_ETD__c',
                                                                        'ZA02' => 'Woil_Additional_PU__c','ZA01' => 'Woil_Additional__c',
                                                                        'ZC01' => 'Woil_Discount__c','ZF01' => 'Woil_FOC_Discount__c',
                                                                        'ZDP1' => 'UnitPrice'};

            Map<String,String> productIdMap = new Map<String,String>();
            Map<String,Map<String,String>> responseMap = new Map<String,Map<String,String>>();
            Map<String,String> responseErrorMap = new Map<String,String>();
            List<OrderItem> ordItemlist = new List<OrderItem>();

            List<ItemDetails> itemDetailList = new List<ItemDetails>();
            String divisonfield = '';
            String pricingProcedure = '';

            Order ord = [SELECT Id, Account.Woil_Distribution_Channel__c, Account.Woil_Account_Number__c, Woil_Division__c, Type, Status, AccountId,
                            (SELECT Product2.Name,Quantity,Product2.Woil_Control_Code__c FROM OrderItems) 
                            FROM Order WHERE Id = : recordId];

            if(ord.Status != 'Draft'){
                return 'Pricing can be calculate only on Draft status';
            }

            if(ord.OrderItems.size() == 0){
                return 'Add atleast one product for pricing calculation';
            }

            
            if(ord.Woil_Division__c == 'Finish Good'){
                divisonfield = 'AG';
            }
            else if(ord.Woil_Division__c == 'Spare Part'){
                divisonfield = 'AS';
            }

            Integer lineNumber = 1;
            for(OrderItem orderItemObj : ord.OrderItems){
                productIdMap.put(orderItemObj.Product2.Name,orderItemObj.Id);
                itemDetailList.add(new ItemDetails(String.valueOf(lineNumber),orderItemObj.Product2.Name, 
                                                    String.valueOf(orderItemObj.Quantity),orderItemObj.Product2.Woil_Control_Code__c));
                lineNumber++;
            }

            List<Woil_Account_Plant__c> listOfAccountPlant = [SELECT Id, Woil_Plant_Master__r.Woil_Plant_Code__c 
                                                                FROM Woil_Account_Plant__c 
                                                                WHERE Woil_Trade_Partner__c = : ord.AccountID AND Woil_Division__c = : divisonfield];

            PricingHeaderwrap pH = new PricingHeaderwrap(listOfAccountPlant[0].Woil_Plant_Master__r.Woil_Plant_Code__c,ord.Account.Woil_Distribution_Channel__c,divisonfield,ord.Account.Woil_Account_Number__c,ord.Type, itemDetailList);

            

            PricingWrap pricingWrapObj = new PricingWrap(pH);

            System.debug('yyyyyyyyyyyyyyyyyyyyyy'+json.serialize(pricingWrapObj));

            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:SAP_Pricing_Integration');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setBody(json.serialize(pricingWrapObj));
            req.setTimeout(120000);
            //HttpRequest req = Woil_UtilityClass.prepareRequest('callout:SAP_Pricing_Integration', 'POST', json.serialize(pricingWrapObj), new Map<String,String>());
            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug(res.getBody());
            System.debug('res.getStatusCode()----'+res.getStatusCode());
            if(res.getStatusCode() == 200){
                PricingHeaderResp responseList = (PricingHeaderResp)JSON.deserialize(res.getBody(), PricingHeaderResp.class);
                System.debug('responseList---'+responseList.PricingHeaderResponse);
                for(PricingResponse pr : responseList.PricingHeaderResponse){
                    if(pr.Status == 'E'){
                        responseErrorMap.put(pr.MaterialCode,pr.Message);
                    }else{
                        if(!responseMap.containsKey(pr.MaterialCode)){
                        	responseMap.put(pr.MaterialCode, new Map<String,String>());
                    	}
                        Map<String,String> tempMap = responseMap.get(pr.MaterialCode);
                        tempMap.put(pr.ConditionType,pr.Rate);
                        responseMap.put(pr.MaterialCode,tempMap);
                    }
                }
				
                System.debug('responseErrorMap---'+responseErrorMap);
                System.debug('responseMap---'+responseMap);

                for(String matId : responseMap.keySet()){
                    OrderItem ordItem = new OrderItem();
                    ordItem.Id = productIdMap.get(matId);
                    ordItem.Woil_Error_Message__c = '';
                    for(String respo : responseMap.get(matId).keySet()){
                        System.debug('lineItemAPIName.get(respo)---'+lineItemAPIName.get(respo));
                        if(lineItemAPIName.get(respo) == 'UnitPrice' || lineItemAPIName.get(respo) == 'Woil_Additional_PU__c' || 
                            lineItemAPIName.get(respo) == 'Woil_Discount__c' || lineItemAPIName.get(respo) == 'Woil_FOC_Discount__c'){
                            String unitPriceStr = responseMap.get(matId).get(respo).trim();
                            ordItem.put(lineItemAPIName.get(respo),Decimal.valueOf(unitPriceStr));
                        }else if(lineItemAPIName.get(respo) == 'Woil_Additional__c' || lineItemAPIName.get(respo) == 'Woil_RDM__c' || 
                                lineItemAPIName.get(respo) == 'Woil_ETD__c'){
                            ordItem.put(lineItemAPIName.get(respo),Integer.valueOf(responseMap.get(matId).get(respo).trim()));
                        }else{
                            ordItem.put(lineItemAPIName.get(respo),responseMap.get(matId).get(respo));
                        }
                    }
                    ordItemlist.add(ordItem);
                }
                
                for(String matId : responseErrorMap.keySet()){
                    OrderItem ordItem = new OrderItem();
                    ordItem.Id = productIdMap.get(matId);
                    ordItem.Woil_Error_Message__c = responseErrorMap.get(matId);
                    ordItemlist.add(ordItem);
                }

                System.debug('or----'+ordItemlist); 

                if(ordItemlist.size() > 0){
                    update ordItemlist;
                }
                return 'Success#'+recordId;
            }else{
                return 'Error#Something went wrong. Please contact to admin.';
            }
            
        }catch(Exception ex){
            return 'Error#'+ex.getMessage();
        }
        
        
        
    }

    public class PricingHeaderwrap {
        public String Plant {get;set;} 
        public String Pricing_Date {get;set;} 
        public String Sales_Org {get;set;} 
        public String Distribution_Channel {get;set;} 
        public String Order_Type {get;set;}
        public String Division {get;set;} 
        public String Sold_to_Party {get;set;}
        public List<ItemDetails> MaterialList{get;set;}
        Public PricingHeaderwrap(String plant, String distribution_Channel, String division, String sold_to_Party, String order_type, List<ItemDetails> itemdetails){
            this.Plant = plant;
            this.Sales_Org = 'IN01'; 
            this.Pricing_Date = String.valueOf(System.Today());
            this.Distribution_Channel = distribution_Channel; 
            this.Division = division; 
            this.Sold_to_Party = sold_to_Party;
            this.Order_Type = order_type;
            this.MaterialList = itemdetails;                    
        }
    }

    public class ItemDetails {
        public String Material_Code {get;set;} 
        public String Mat_Pricing_Group {get;set;} 
        public String Control_Code {get;set;}  
        public String Quantity {get;set;}
        public String Line_Item_no {get;set;}
        public ItemDetails(String Line_Item_no, String material_Code, String quantity, String control_code){
            this.Line_Item_no = Line_Item_no;
            this.Material_Code = material_Code; 
            this.Mat_Pricing_Group = '';
            this.Quantity = quantity;
            this.Control_Code = control_code;                        
        }
    }

    public class PricingWrap {
        public PricingHeaderwrap PricingHeader {get;set;} 
       // public List<ItemDetails> MaterialList {get;set;}
        public PricingWrap(PricingHeaderwrap headerObj){
            this.PricingHeader = headerObj;
          //  this.MaterialList = itemDetailsList;
        }
    }


    public class PricingHeaderResp{
        public List<PricingResponse> PricingHeaderResponse{get;set;}
    }

    public class PricingResponse{
        public String MaterialCode {get;set;}
        public String ConditionType {get;set;}
        public String Line_Item {get;set;}
        public String Rate {get;set;}
        public String UoM {get;set;}
        public String Amount {get;set;}
        public String Status {get;set;}
        public String Message {get;set;}
    }
}