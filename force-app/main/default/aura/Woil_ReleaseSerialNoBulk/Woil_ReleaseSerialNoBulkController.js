({
    fetchItems : function(component, event, helper) {
        console.log('recordID:::'+component.get("v.recordId"));
        component.set("v.spinner", false);
        
        component.set('v.mycolumns', [
            {label: 'Sales Invoice', fieldName: 'InvoiceName', type: 'text'},
            {label: 'SKU Code', fieldName: 'SKUCode', type: 'text'},
            {label: 'Serial Number', fieldName: 'Woil_Serial_Number__c', type: 'Text'}
        ]);
        var action = component.get("c.getGRNItems");
        action.setParams({"grn_no":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            var rows=response.getReturnValue();
            var selectedValues=[];
            component.set('v.selectedValues',[]); 
            component.set("v.invList", []);
            component.set("v.filteredList", []);
            
            console.log('rows::::'+rows+'::state::'+state);
            if (state === "SUCCESS") {
                if( rows===null || rows.length===0){
                    component.set("v.Noresults",true);
                }
                else if(rows[0].Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Posted__c!=true){
                    component.set("v.Noresults",false);
                    for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    row.InvoiceName = row. Woil_Invoice_Line_Item__r.Woil_GRN__r.Woil_Sales_Invoice__r.Name;
                    row.SKUCode=row.Woil_Invoice_Line_Item__r.Woil_Product__r.Name;                    
                    selectedValues.push(row.Id);
                }
                // setting formatted data to the datatable
                // component.set("v.data", rows);
                console.log('selectedValues::::'+JSON.stringify(selectedValues));
                component.set('v.selectedValues',selectedValues);
                component.set("v.invList", rows);
                component.set("v.filteredList", rows);
                console.log('rows2222::::'+rows);
               /* if(rows.length===0 || rows===null ){
                    component.set("v.Noresults",true);
                }else{
                    component.set("v.Noresults",false);
                }*/
                var SKUList=[];
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if(SKUList!=null)
                        var index=SKUList.indexOf(row.Woil_Invoice_Line_Item__r.Woil_Product__r.Name);                   
                    if(index===-1){
                        SKUList.push(row.Woil_Invoice_Line_Item__r.Woil_Product__r.Name);
                    }
                }                                         
                component.set("v.SKUList", SKUList);
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        message:'GRN is already posted!',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire()
                }
            }else if(state === "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        message:'GRN is deleted!',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire()
            }
        });
        $A.enqueueAction(action);
    },
    getInvID : function(component, event, helper) {
        
        component.set("v.InvoiceId",event.getParam("InvoiceID"));
    }
    ,
    SearchInvItems : function(component, event, helper) {
        var srno =       component.find("srno").get("v.value");
        component.set("v.invList",component.get("v.filteredList"));
        var allRecords = component.get('v.invList');
        for(let j=0; j < allRecords.length; j++){
            console.log('inside lklklkll::::');
            
        }
        
        var srnolist =[];
        var tempArray = [];
        var SKUCode="";
        if(srno != null && srno != '')
        {
            srnolist =   srno.split(','); 
            //(srnolist.length === 0)
            //srnolist.push(srno);
        }else{
            SKUCode= component.get("v.SKU")  
        }
        console.log('srnolist::::'+srnolist+'::srno::'+srno+'::allRecords::'+allRecords+'::SKUCode::'+SKUCode);
        for(let i=0; i < allRecords.length; i++){
            console.log('allRecords::::');
            for(let j=0; j < srnolist.length; j++){
                console.log('srnolist::::');
                if(allRecords[i].Woil_Serial_Number__c && allRecords[i].Woil_Serial_Number__c.toLowerCase()===srnolist[j].toLowerCase() )                   
                {
                    console.log('srnolist11::::');
                    tempArray.push(allRecords[i]);
                }
            }
            console.log('SKUCode::::'+SKUCode+'::srnolist::'+srnolist.length);
            if((SKUCode!="" && allRecords[i].Woil_Invoice_Line_Item__r.Woil_Product__r.Name===SKUCode ) ){
                tempArray.push(allRecords[i]);
            }else if((SKUCode==='---Select---') && srnolist.length===0){	
                tempArray.push(allRecords[i]);
            }
        }
        
        console.log('tempArray::::'+tempArray+'::FILTER LIST::'+component.get("v.filteredList"));
        var selectedValues=[];
        component.set('v.selectedValues',[]); 
        console.log('SKUCode::::'+SKUCode);
        if(SKUCode==='---Select---'){
            console.log('inside SKUCode::::');
            //var invLst=component.get("v.filteredList");          
            for(let j=0; j < tempArray.length; j++){
                selectedValues.push(tempArray[j].Id) ;
                
            }
            
            component.set('v.selectedValues',selectedValues);   
            console.log('tempArray11::::'+selectedValues);
        }
        
        else{
            console.log('inside tempArray::::'+tempArray);
            //component.set('v.invList',tempArray);
            
            for(let j=0; j < tempArray.length; j++){
                console.log('inside tempsafadArray::::');
                selectedValues.push(tempArray[j].Id);
                
            }
            /*if(selectedValues.length===0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message:'No Records Found',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }*/
             
            console.log('tempArray ggg::::'+selectedValues);
            component.set('v.selectedValues',selectedValues);
        }
        if(selectedValues.length===0 || selectedValues===null ){
                    component.set("v.Noresults",true);
                }else{
                    component.set("v.Noresults",false);
                }
        
    },
    
    handleSelect: function(component, event, helper) 
    {
        component.find("selectAll").set("v.value", false); 
        var selectedRows = event.getParam('selectedRows');
        console.log('selectedRows:::'+JSON.stringify(selectedRows));
        var selectedinv=  component.get("v.invSelected");
        //component.set("v.RowsSelected",selectedRows);
        var filteredList=component.get("v.filteredList");
        var invListAll=component.get("v.invSelected");
        var invList=component.get("v.invList");
        
        for(let row=0;row<=invList.length;row++)
        {
            for(let Srow=0;Srow<=selectedRows.length;Srow++){
                const index = invList.indexOf(selectedRows[Srow]);
                const index1 = filteredList.indexOf(selectedRows[Srow]);
                if (index > -1) {
                    //invList.splice(index, 1);
                    invListAll.push(selectedRows[Srow]);
                }  
                if(index1 >-1){
                    filteredList.splice(index1,1);
                }
            }           
        }
        //var RowsSelected=component.get("v.RowsSelected");
        //RowsSelected.push(selectedRows);
        
        
        //component.set("v.invList",invList);
        component.set("v.filteredList",filteredList);
        component.set("v.invSelected",invListAll);
        
        
    },
    changeSelect: function (cmp, event, helper) {
        //Press button to change the selected option
        console.log('selected::'+cmp.find("select").get("v.value")+':::'+cmp.get("v.SKU"));
        cmp.find("srno").set("v.value","");
    },
    closeModel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire()
    },
    accept:function(component, event, helper) {
        
        console.log('inside accept:::::');
             
        var getAllId = component.find("boxPack"); 
        var totalItems=component.get("v.invList");
        var acceptLst=[];
        var rejectLst=[];
        console.log('jcvdbvjjcvda::'+getAllId+'::totalItems::'+totalItems);   
        for (var k = 0; k < totalItems.length; k++) {
            console.log('11122::'+getAllId.length);
            if(getAllId.length!=undefined){
                for (var i = 0; i < getAllId.length; i++) {
                    console.log('11133::');
                    if (getAllId[i].get("v.value") === true) {
                        var selectedId =getAllId[i].get("v.text");
                        console.log('selectedId::'+selectedId+'::tot id::'+totalItems[k].Id);
                        if(selectedId===totalItems[k].Id){
                            
                                acceptLst.push(totalItems[k]);
                           
                        }
                    }               
                }
            }else{
                	var selectedId =getAllId.get("v.text");
                 if(selectedId===totalItems[k].Id && getAllId.get("v.value") === true){
                    acceptLst.push(totalItems[k]);
                 }
               
            }
        }
        console.log('acceptLst::::'+acceptLst);
        if(acceptLst.length!=0 || rejectLst.length!=0){
               var msg ='Are you sure you want to release selected serial Numbers?';
        if (!confirm(msg)) {
            console.log('No');
            return false;
        } else {
            console.log('Yes');
              var action = component.get("c.ReleaseGRNItems");
            action.setParams({"LstSerialNos":acceptLst
                             });
            action.setCallback(this, function(response){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success',
                    message: 'Selected Serial Nos released',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                var a = component.get('c.fetchItems');
                $A.enqueueAction(a);
                
            });
            $A.enqueueAction(action);
        }
           
        }else{
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Info',
                message: 'Select Serial Numbers to Release',
                duration:' 5000',
                key: 'info_alt',
                type: 'info',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        
    },
    reject:function(component, event, helper) {
        /*component.set("v.RejectList",component.get("v.RowsSelected"));
        var totalItems=component.get("v.invList");
        var RejectItems=component.get("v.RejectList");
        console.log("RejectList LI::"+JSON.stringify(component.get("v.RejectList")));
        for(let row=0;row<=totalItems.length;row++)
        {
            var index=RejectItems.indexOf(totalItems[row]);
            if (index > -1) {
                totalItems.splice(row,1);
            }
        }
        component.set("v.invList",totalItems);*/
        
        
        var invSelected=component.get("v.invSelected");
        component.set("v.RejectList",invSelected);
        component.set("v.invSelected",null);       
        console.log("RejectList LI11::"+JSON.stringify(component.get("v.RejectList")));
        
    },
    
    /*************************************************/
    handleSelectAllContact:function(component, event){
        
        var selectedHeaderCheck = component.find("selectAll").get("v.value");
        var getAllId = component.find("boxPack"); 
        console.log('getAllId:::'+getAllId);
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("boxPack").set("v.value", true);    
            }else{
                component.find("boxPack").set("v.value", false);
            }
        }else{
            // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
            // and set the all selected checkbox length in selectedCount attribute.
            // if value is false then make all checkboxes false in else part with play for loop 
            // and select count as 0
            
            var selectedvalues=component.get("v.selectedValues");
           console.log('selectedHeaderCheck:::'+selectedHeaderCheck+'::getAllId::'+getAllId+'::selectedvalues::'+selectedvalues);

            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", true);    
                    }
                    
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", false);   
                    }
                    
                }
            } 
        }  
    },
    updateFields: function(component, event, helper) {
        var updateId = [];
        var getAllId = component.find("boxPack");
        if(! Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                updateId.push(getAllId.get("v.text"));
            }else{
                index=updateId.indesxOf(getAllId.get("v.text")); 
                updateId.splice(index);
            }
        }else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    updateId.push(getAllId[i].get("v.text"));
                }else{
                    index=updateId.indesxOf(getAllId.get("v.text")); 
                    updateId.splice(index);
                }
            }
        } 
        component.set("v.SelectedIds",updateId) ; 
    },
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    },
    showorhiderows:function(component,event,helper){
        console.log('inside func:::');
    },
    
    handleConfirmDialog : function(component, event, helper) {
        var selectedEventId = event.target.id;
     
    }
    
})