({
    getPrice : function(component,event) {
        component.set("v.spinner", true);
        var parentId = component.get('v.recordId');
        var action = component.get("c.getPriceController");
        action.setParams({
            recordId : parentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var respo = response.getReturnValue();
            if (state === "SUCCESS") {
                if(respo.includes("#")){
                    if(respo.split("#")[0] === 'Success'){
                        $A.get("e.force:closeQuickAction").fire();
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": respo.split("#")[1],
                            "slideDevName": "related"
                        });
                        navEvt.fire();
                    }else if(respo.split("#")[0] === 'Error'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: respo.split("#")[1],
                            type: 'error',
                        });
                        toastEvent.fire();
    
                        $A.get("e.force:closeQuickAction").fire();
                    }
                }else{
                    if(respo === 'Pricing can be calculate only on Draft status'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: 'Pricing can be calculate only on Draft status.',
                            type: 'error',
                        });
                        toastEvent.fire();
    
                        $A.get("e.force:closeQuickAction").fire();
                    }else if(respo === 'Add atleast one product for pricing calculation'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: 'Add atleast one product for pricing calculation.',
                            type: 'error',
                        });
                        toastEvent.fire();
    
                        $A.get("e.force:closeQuickAction").fire();
                    }
                }
                
                
            }if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.spinner", false);           
        });
        $A.enqueueAction(action); 
    }
})