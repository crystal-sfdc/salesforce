({
    
	getLineItemDetails : function(component, event, helper) {
        console.log('name comp::::'+component.getName());
		component.set('v.mycolumns', [
            
            {label: 'Product Code', fieldName: 'SKUCode', type: 'text'},
            {label: 'Product Description', fieldName: 'SKUDesc', type: 'text'},
            
            {label: 'Quanity', fieldName: 'Woil_Ordered_Quantity__c', type: 'Text'},
            
            {label: 'Reject Reason', fieldName: 'reason', type: 'Text'},
            {label: 'Action', fieldName: 'Rejected', type: 'Text'},
            { 
                label: 'SRN No', fieldName: 'SRNUrl', wrapText: true,
                type: 'url',
                typeAttributes: {
                    label: { 
                        fieldName: 'SRNNo' 
                    },
                    target : '_blank'
                }
            },
            {label: 'Posted', fieldName: 'Posted', type: 'boolean'},
              {label: 'Transaction Date', fieldName: 'CreatedDate', type: 'date', typeAttributes: {  
                                                                            day: 'numeric',  
                                                                            month: 'short',  
                                                                            year: 'numeric',  
                                                                         }}
            
        ]);
        
         var action = component.get("c.getSRNLineItems");
        action.setParams({"invoice_no":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            var data=response.getReturnValue();
            var selectedValues=[];
              
            
            console.log('rows::::'+data);
            let baseUrl = 'https://'+location.host+'/';
            if (state === "SUCCESS") {
                console.log('inside success::::');
                data.forEach(Rec => {
                    Rec.Rejected='Rejected';
                    if(Rec.Woil_SRN__r.Woil_Sales_Invoice__r.Name){
                    Rec.InvoiceName = Rec.Woil_SRN__r.Woil_Sales_Invoice__r.Name;
                }
                    if(Rec.Woil_Product__r.Name){
                    Rec.SKUCode = Rec.Woil_Product__r.Name;
                }
                    if(Rec.Woil_Product__r.Description){
                    Rec.SKUDesc =Rec.Woil_Product__r.Description;
                }
                	if(Rec.Woil_SRN__r.Name){
                        Rec.SRNNo = Rec.Woil_SRN__r.Name;
                    	Rec.SRNUrl = baseUrl+Rec.Woil_SRN__c;
                    	Rec.Posted = Rec.Woil_SRN__r.Woil_Posted__c;
                    	Rec.reason =Rec.Woil_SRN__r.Woil_Reason__c;
                    }
                });
                component.set("v.result",data);  
                console.log('resultt'+component.get("v.result"));
            }
        });
        $A.enqueueAction(action);
	},
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    }
})