({
    fetchItemsHelper : function(component, event) {
        component.set("v.spinner", false);
        
        component.set('v.mycolumns', [
            {label: 'SKU Code', fieldName: 'skuCode', type: 'text'},
            {label: 'Warehouse Location', fieldName: 'warehouseLocation', type: 'text'},
            {label: 'Serial Number', fieldName: 'serialNo', type: 'Text'}
        ]);
        var action = component.get("c.getInvoiceItems");
        action.setParams({
            "retOrdNo":component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var rows=response.getReturnValue();
            var selectedValues=[];
            component.set('v.selectedValues',[]); 
            component.set("v.invList", []);
            if (state === "SUCCESS") {
                if(rows.length > 0 ){
                    if(rows[0].status === 'Processed'){
                        component.set("v.isModalOpen",false);
                        component.set("v.error",true);
                        component.set("v.message","Order is already processed");
                    }else{
                        for (var i = 0; i < rows.length; i++) {
                            var row = rows[i];
                            row.skuCode = row.skuCode;
                            row.warehouseLocation=row.warehouseLocation; 
                            row.serialNo=row.serialNo
                            selectedValues.push(row.rowId);
                        }
                        component.set('v.selectedValues',selectedValues);
                        component.set("v.invList", rows);
                        component.set("v.filteredList", rows);
                        if(rows.length===0 || rows===null ){
                            component.set("v.Noresults",true);
                        }else{
                            component.set("v.Noresults",false);
                        }
                        var SKUList=[];
                        for (var i = 0; i < rows.length; i++) {
                            var row = rows[i];
                            if(SKUList!=null)
                                var index=SKUList.indexOf(row.skuCode);                   
                            if(index===-1){
                                SKUList.push(row.skuCode);
                            }
                        }                                         
                        component.set("v.SKUList", SKUList);
                    }
                }else{
                    component.set("v.Noresults",true);
                }
                                                
            }
        });
        $A.enqueueAction(action);
    }
})