({
    fetchItems : function(component, event, helper) {
        helper.fetchItemsHelper(component, event);
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    },

    handleSelectAllContact:function(component, event){
        
        var selectedHeaderCheck = component.find("selectAll").get("v.value");
        var getAllId = component.find("boxPack");
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("boxPack").set("v.value", true);    
            }else{
                component.find("boxPack").set("v.value", false);
            }
        }else{
            // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
            // and set the all selected checkbox length in selectedCount attribute.
            // if value is false then make all checkboxes false in else part with play for loop 
            // and select count as 0
            
            var selectedvalues=component.get("v.selectedValues");

            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", true);    
                    }
                    
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", false);   
                    }
                    
                }
            } 
        }  
    },

    closeModel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    changeSelect: function (cmp, event, helper) {
        //Press button to change the selected option
        cmp.find("srno").set("v.value","");
    },

    clearSKUCode : function(component, event, helper){
        component.find("select").set("v.value","");
    },
    SearchInvItems : function(component, event, helper) {
        var srno =       component.find("srno").get("v.value");
        component.set("v.invList",component.get("v.filteredList"));
        var allRecords = component.get('v.invList');       
        var srnolist =[];
        var tempArray = [];
        var SKUCode="";
        if(srno != null && srno != '')
        {
            srnolist =   srno.split(','); 
        }else{
            SKUCode= component.get("v.SKU")  
        }
       
        for(let i=0; i < allRecords.length; i++){
            for(let j=0; j < srnolist.length; j++){
                
                if(allRecords[i].serialNo && allRecords[i].serialNo.toLowerCase()===srnolist[j].toLowerCase() )                   
                {                    
                    tempArray.push(allRecords[i]);
                }
            }         
            if((SKUCode!="" && allRecords[i].skuCode===SKUCode ) ){
                tempArray.push(allRecords[i]);
            }else if((SKUCode==='---Select---') && srnolist.length===0){	
                tempArray.push(allRecords[i]);
            }
        }
        
        var selectedValues=[];
        component.set('v.selectedValues',[]); 
       
        if(SKUCode==='---Select---'){
            //var invLst=component.get("v.filteredList");          
            for(let j=0; j < tempArray.length; j++){
                selectedValues.push(tempArray[j].rowId) ;               
            }            
            component.set('v.selectedValues',selectedValues);               
        }
        
        else{
            for(let j=0; j < tempArray.length; j++){               
                selectedValues.push(tempArray[j].rowId);                
            }         
            component.set('v.selectedValues',selectedValues); 
        }
        if(selectedValues.length===0 || selectedValues===null ){
                    component.set("v.Noresults",true);
                }else{
                    component.set("v.Noresults",false);
                }
        
    },

    accept:function(component, event, helper) {
       
        var getAllId = component.find("boxPack"); 
        var totalItems=component.get("v.invList");
        var acceptLst=[];
        var rejectLst=[];
        if(getAllId.length!=undefined && getAllId.length>0  ){
            for (var k = 0; k < getAllId.length; k++) {                
                if (getAllId[k].get("v.value") === true) {
                    var selectedId =getAllId[k].get("v.text");              
                    acceptLst.push(totalItems[k]);                                      
                }
            }
        }
        else{
            if(getAllId.get("v.value")===true)
                acceptLst.push(totalItems[0]);
        }

        if(acceptLst.length!=0 || rejectLst.length!=0){
            var action = component.get("c.saveScannedItems");
            action.setParams({"LstSerialNos":acceptLst,
                              "retOrdNo":component.get("v.recordId")
                             });
            action.setCallback(this, function(response){
                var state = response.getState();
                var reso = response.getReturnValue();

                if (state === "SUCCESS") {
                    if(reso === 'Success'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Success',
                            message: 'Serial No scanned',
                            key: 'info_alt',
                            type: 'success',
                        });
                        toastEvent.fire();

                        $A.get("e.force:closeQuickAction").fire();
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                        "recordId": component.get("v.recordId"),
                        "slideDevName": "related"
                        });
                        navEvt.fire();
                        /*var a = component.get('c.fetchItems');
                        $A.enqueueAction(a);*/
                    }
                    if(reso === 'Quantity Exceeded'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: 'You can not scan more than Ordered Quantity',
                            type: 'error',
                        });
                        toastEvent.fire();

                        $A.get("e.force:closeQuickAction").fire();
                    }
                    
                // $A.get("e.force:closeQuickAction").fire();
                }else if (state === "ERROR") { 
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
                component.set("v.Spinner1", false);
                
            });
            $A.enqueueAction(action);
        }else{
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Info',
                message: 'Select Serial Numbers to Scan',
                duration:' 5000',
                key: 'info_alt',
                type: 'info',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        
    },
    handleSelect: function(component, event, helper) 
    {
        //var selectedRows = event.getParam('selectedRows');
        var selectedRows=component.get("v.selectedValues");
        var selectedinv=  component.get("v.invSelected");
        component.set("v.RowsSelected",selectedRows);
        var filteredList=component.get("v.filteredList");
        var invListAll=component.get("v.invSelected");
        var invList=component.get("v.invList");
        //component.set("selectAll",false);
        component.find("selectAll").set("v.value", false); 
        for(let row=0;row<=invList.length;row++)
        {
            for(let Srow=0;Srow<=selectedRows.length;Srow++){
                const index = invList.indexOf(selectedRows[Srow]);
                const index1 = filteredList.indexOf(selectedRows[Srow]);
                if (index > -1) {
                    //invList.splice(index, 1);
                    invListAll.push(selectedRows[Srow]);
                }  
                if(index1 >-1){
                    filteredList.splice(index1,1);
                }
            }           
        }
        //var RowsSelected=component.get("v.RowsSelected");
        //RowsSelected.push(selectedRows);
        
        
        //component.set("v.invList",invList);
        component.set("v.filteredList",filteredList);
        component.set("v.invSelected",invListAll);
        
        
    }
})