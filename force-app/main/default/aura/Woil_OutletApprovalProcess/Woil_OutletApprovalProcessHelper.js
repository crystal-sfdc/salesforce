({
    init: function(component, event, helper) {
         //Delay to make the processing happen at server
       // window.setTimeout(
       //     $A.getCallback(function() {
       //         cmp.set("v.visible", true);
       //     }), 5000
       // );

        window.setTimeout(
            $A.getCallback(function () {
                component.find("statusPicklist").focus();
            }), 1
        );
        
        var toastEvent = $A.get("e.force:showToast");
        console.log("component.get v.recordId=======>" +component.get("v.recordId"));
        var action = component.get("c.getSupportUser");
        action.setParams({recordId :component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.secondApprover",response.getReturnValue().Woil_Support_User__c);
                component.set("v.secondApproverName",response.getReturnValue().Woil_Support_User__r.Name);
                
                var action = component.get("c.getApprovers");
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log("pppppppp");
                        console.log( 'Response date=======>' + JSON.stringify( response.getReturnValue()));
                        component.set("v.asmList",response.getReturnValue() );
                        console.log("=========>"+component.get("v.asmList", asmList));
                    }
                    else{
                        console.log("getApprovers Fail");
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                // log the error passed in to AuraHandledException
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
                $A.enqueueAction(action);
            } else{
                console.log("getSupportUser Fail");
                var errors = response.getError();
                if (errors) {
                    $A.get("e.force:closeQuickAction").fire();
                    
                    if (errors[0] && errors[0].message) {
                        // log the error passed in to AuraHandledException
                        console.log("Error message: " + errors[0].message);
                        if(errors[0].message === 'Approval Pending' || 
                                 errors[0].message === 'Rejected' || 
                                 errors[0].message ===  'Active'){
                            toastEvent.setParams({
                                "title": "Can not initiate Approval process!",
                                "message": "Approval is in "+errors[0].message+" stage. Please create new record, to initiate approval process",
                                "type" : "warning",
                                "duration" : "7000"
                            });
                            toastEvent.fire();
                        }
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    handleChange: function (component, event, helper) {
        var selPickListValue = event.getSource().get("v.value");
        component.set("v.firstApprover",selPickListValue);
        console.log("firstApprover ========> "+selPickListValue);
        
        var firstApprover=component.get("v.firstApprover");
        
        console.log("secondApprover ========> "+component.get("v.secondApprover"));
        
        console.log("thirdApprover ========> "+component.get("v.thirdApprover"));
        
        var UserArr = component.get("v.asmList");
        for(var i=0;i<UserArr.length;i++){
            if(UserArr[i].Id === selPickListValue){
                if(UserArr[i].Manager !=null){
                    console.log("Third Approver ID===========>"+UserArr[i].Manager.ID); 
                    component.set("v.thirdApprover", UserArr[i].Manager.Name) ;
                }
                else {
                    component.set("v.thirdApprover", "") ;
                }
            }
        }
    },
    
    //////////////////////////////////////////////////////////////////////////////////////////////////
    handleSubmit: function(component, event, helper) {
        console.log("Contoller");
        var firstApprover=component.get("v.firstApprover");
        var secondApprover=component.get("v.secondApprover");
        var thirdApprover = component.get("v.thirdApprover");
        var recId=component.get("v.recordId");
        var toastEvent = $A.get("e.force:showToast");
        
        if (typeof(thirdApprover) != 'undefined' && thirdApprover != null)
        {
            //bugs
            console.log("firstApprover==> "+firstApprover+ 
                        " secondApprover==> " +secondApprover+ 
                        " thirdApprover==>" +thirdApprover+
                        " recId==> "+recId);
            var action = component.get("c.ApprovelProccessForTP");
            action.setParams({
                "firstApprover" : firstApprover ,
                "secondApprover" : secondApprover ,
                "thirdApprover" : thirdApprover ,
                "RecordId":recId,
            });
            action.setCallback(this, function(a){
                var state = a.getState(); 
                console.log(state);
                if(state == 'SUCCESS') {
                    console.log(a.getReturnValue());
                    if(a.getReturnValue() != null){
                        console.log(a.getReturnValue());
                        component.find('notifyId').showToast({
                            "variant": "Success",
                            "title": "Success!",
                            "message": "Approval Submitted."
                        });
                        $A.get("e.force:closeQuickAction").fire();  
                        window.location.reload()
                    }
                }
                else{
                    var errors = a.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    component.find('notifyId').showToast({
                        "variant": "error",
                        "title": "error!",
                        "message": a.getReturnValue()
                    });
                }
            });
            $A.enqueueAction(action);
        }
        else{
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please fill all mandatory fields.",
                "type" : "warning"
            });
            toastEvent.fire();
        }
    },
    gethandleClose: function(component, event, helper) {
     $A.get("e.force:closeQuickAction").fire();
    }
    
})