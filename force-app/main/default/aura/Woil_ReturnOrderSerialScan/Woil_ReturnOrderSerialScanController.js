({
    fetchItems : function(component, event, helper) {
      
        component.set("v.spinner", false);
        
        component.set('v.mycolumns', [
            {label: 'SKU Code', fieldName: 'skuCode', type: 'text'},
            {label: 'SKU Code', fieldName: 'skuDesc', type: 'text'},
            
            {label: 'Warehouse Location', fieldName: 'warehouseLocation', type: 'text'},
            {label: 'Serial Number', fieldName: 'serialNo', type: 'Text'}
        ]);
        var action = component.get("c.getInvoiceItems");
        action.setParams({"retOrdNo":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            var rows=response.getReturnValue();
            var selectedValues=[];
            component.set('v.selectedValues',[]); 
            component.set("v.invList", []);
            console.log('rows::::'+rows);
            if (state === "SUCCESS") {
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    row.skuCode = row.skuCode;
                    row.skuDesc = row.skuDesc;
                    row.warehouseLocation=row.warehouseLocation; 
                    row.serialNo=row.serialNo
                    selectedValues.push(row.rowId);
                }
                
                component.set('v.selectedValues',selectedValues);
                component.set("v.invList", rows);
                component.set("v.filteredList", rows);
                if(rows.length===0 || rows===null ){
                    component.set("v.Noresults",true);
                }else{
                    component.set("v.Noresults",false);
                }
                var SKUList=[];
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if(SKUList!=null)
                        var index=SKUList.indexOf(row.skuCode);                   
                    if(index===-1){
                        SKUList.push(row.skuCode);
                    }
                }                                         
                component.set("v.SKUList", SKUList);                                
            }
        });
        $A.enqueueAction(action);
    },
    getInvID : function(component, event, helper) {
        
        component.set("v.InvoiceId",event.getParam("InvoiceID"));
    }
    ,
    SearchInvItems : function(component, event, helper) {
        var srno =       component.find("srno").get("v.value");
        component.set("v.invList",component.get("v.filteredList"));
        var allRecords = component.get('v.invList');       
        var srnolist =[];
        var tempArray = [];
        var SKUCode="";
        if(srno != null && srno != '')
        {
            srnolist =   srno.split(','); 
        }else{
            SKUCode= component.get("v.SKU")  
        }
       
        for(let i=0; i < allRecords.length; i++){
            for(let j=0; j < srnolist.length; j++){
                
                if(allRecords[i].serialNo && allRecords[i].serialNo.toLowerCase()===srnolist[j].toLowerCase() )                   
                {                    
                    tempArray.push(allRecords[i]);
                }
            }         
            if((SKUCode!="" && allRecords[i].skuCode===SKUCode ) ){
                tempArray.push(allRecords[i]);
            }else if((SKUCode==='---Select---') && srnolist.length===0){	
                tempArray.push(allRecords[i]);
            }
        }
        
        var selectedValues=[];
        component.set('v.selectedValues',[]); 
       
        if(SKUCode==='---Select---'){
            //var invLst=component.get("v.filteredList");          
            for(let j=0; j < tempArray.length; j++){
                selectedValues.push(tempArray[j].rowId) ;               
            }            
            component.set('v.selectedValues',selectedValues);               
        }
        
        else{
            for(let j=0; j < tempArray.length; j++){               
                selectedValues.push(tempArray[j].rowId);                
            }         
            component.set('v.selectedValues',selectedValues); 
        }
        if(selectedValues.length===0 || selectedValues===null ){
                    component.set("v.Noresults",true);
                }else{
                    component.set("v.Noresults",false);
                }
        
    },
    
    handleSelect: function(component, event, helper) 
    {
        var selectedRows = event.getParam('selectedRows');        
        var selectedinv=  component.get("v.invSelected");
        component.set("v.RowsSelected",selectedRows);
        var filteredList=component.get("v.filteredList");
        var invListAll=component.get("v.invSelected");
        var invList=component.get("v.invList");
       
        component.find("selectAll").set("v.value", false); 
        for(let row=0;row<=invList.length;row++)
        {
            for(let Srow=0;Srow<=selectedRows.length;Srow++){
                const index = invList.indexOf(selectedRows[Srow]);
                const index1 = filteredList.indexOf(selectedRows[Srow]);
                if (index > -1) {
                    //invList.splice(index, 1);
                    invListAll.push(selectedRows[Srow]);
                }  
                if(index1 >-1){
                    filteredList.splice(index1,1);
                }
            }           
        }
       
        component.set("v.filteredList",filteredList);
        component.set("v.invSelected",invListAll);
                
    },
    changeSelect: function (cmp, event, helper) {
        //Press button to change the selected option
        cmp.find("srno").set("v.value","");
    },
    closeModel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire()
    },
    accept:function(component, event, helper) {
       
        var getAllId = component.find("boxPack"); 
        var totalItems=component.get("v.invList");
        var acceptLst=[];
        var rejectLst=[];
        console.log('getAllId.length::::'+getAllId.length);
        if(getAllId.length!=undefined && getAllId.length>0  ){
            for (var k = 0; k < getAllId.length; k++) {                
                if (getAllId[k].get("v.value") === true) {
                    var selectedId =getAllId[k].get("v.text");
                    console.log('selectedId::'+selectedId);                         
                    acceptLst.push(totalItems[k]);                                      
                }
            }
        }
            else{
                console.log('inside else::::'+getAllId.get("v.value"));
                if(getAllId.get("v.value")===true)
                    acceptLst.push(totalItems);
            }
        
        console.log('acceptLst::::'+acceptLst);
        if(acceptLst.length!=0 || rejectLst.length!=0){
            var action = component.get("c.saveScannedItems");
            action.setParams({"LstSerialNos":acceptLst,
                              "retOrdNo":component.get("v.recordId")
                             });
            action.setCallback(this, function(response){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success',
                    message: 'Serial No scanned',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                var a = component.get('c.fetchItems');
                $A.enqueueAction(a);
                
            });
            $A.enqueueAction(action);
        }else{
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Info',
                message: 'Select Serial Numbers to Scan',
                duration:' 5000',
                key: 'info_alt',
                type: 'info',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        
    },
    
    /*************************************************/
    handleSelectAllContact:function(component, event){
        
        var selectedHeaderCheck = component.find("selectAll").get("v.value");
        var getAllId = component.find("boxPack"); 
        console.log('getAllId:::'+getAllId);
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("boxPack").set("v.value", true);    
            }else{
                component.find("boxPack").set("v.value", false);
            }
        }else{
            // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
            // and set the all selected checkbox length in selectedCount attribute.
            // if value is false then make all checkboxes false in else part with play for loop 
            // and select count as 0
            
            var selectedvalues=component.get("v.selectedValues");
           console.log('selectedHeaderCheck:::'+selectedHeaderCheck+'::getAllId::'+getAllId+'::selectedvalues::'+selectedvalues);

            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", true);    
                    }
                    
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", false);   
                    }
                    
                }
            } 
        }  
    },
    updateFields: function(component, event, helper) {
        var updateId = [];
        var getAllId = component.find("boxPack");
        if(! Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                updateId.push(getAllId.get("v.text"));
            }else{
                index=updateId.indesxOf(getAllId.get("v.text")); 
                updateId.splice(index);
            }
        }else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    updateId.push(getAllId[i].get("v.text"));
                }else{
                    index=updateId.indesxOf(getAllId.get("v.text")); 
                    updateId.splice(index);
                }
            }
        } 
        component.set("v.SelectedIds",updateId) ; 
    },
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    },
    showorhiderows:function(component,event,helper){
        console.log('inside func:::');
    }
    
})