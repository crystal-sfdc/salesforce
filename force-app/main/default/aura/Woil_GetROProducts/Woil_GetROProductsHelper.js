({
    getProduct : function(component,event) {
        var parentId = component.get('v.recordId');
        var action = component.get("c.getProductController");
        action.setParams({
            recordId : parentId,
            selectedRows: ''
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(!response.getReturnValue().startsWith("Cannot add Products")){
                    
                
                if(response.getReturnValue() === 'Purchase Return is already posted'){
                    //component.set("v.showModal","false");
                    component.set("v.error","true");
                    component.set("v.message","Purchase Return is already posted");
                    console.log('already posted::::')
                    $A.util.addClass(component.find("showProduct"), "slds-hide");
                }
                else{
                    var reponseList = JSON.parse(response.getReturnValue());
                    console.log('response.getReturnValue()::'+response.getReturnValue()+'::reponseList::'+JSON.stringify(reponseList));
                    if(reponseList.wrapProductList.length > 0){
                         if(reponseList.wrapProductList[0].prodRecordType != 'Finished Goods') {
                            component.set("v.isFG",false);
                    
                        }
                        var categoryMap = [];
                        for(var key in reponseList.prodCategoryMap){
                            categoryMap.push({key: key, value: reponseList.prodCategoryMap[key]});
                        }
                        var partTypeMapList = [];
                        for(var key in reponseList.sPartTypeMap){
                            partTypeMapList.push({key: key, value: reponseList.sPartTypeMap[key]});
                        }
                        component.set("v.partTypeMap", partTypeMapList);
                        component.set("v.fieldMap", categoryMap);                  
                        component.set("v.productList", reponseList.wrapProductList);
                    }else{
                        component.set("v.noProductFound","false");
                    }
                }
                }else{
                    //component.set("v.showModal","false");
                    component.set("v.error","true");
                    component.set("v.message",response.getReturnValue() ); 
                    $A.util.addClass(component.find("showProduct"), "slds-hide");
                }
                
            }if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            $A.util.addClass(component.find("addQuantity"), "slds-hide");
            
            
        });
        $A.enqueueAction(action); 
    },

    handleSelectAllProductHelper : function(component,event){

        var checkvalue = component.find("selectAll").get("v.value");  
        var checkProductRecords = component.find("checkProduct");
        var checkProductRecordsMap = component.get("v.productList");
        var RowsSelectedProduct = component.get("v.RowsSelected");
        
        if(checkvalue == true){
            for(var i=0; i<checkProductRecordsMap.length; i++){
                checkProductRecordsMap[i].setCheckbox = true;
                RowsSelectedProduct.push(checkProductRecordsMap[i]);
            }
        }
        else{ 
            for(var i=0; i<checkProductRecordsMap.length; i++){
                for(var j=0; j<RowsSelectedProduct.length; j++){
                    if(checkProductRecordsMap[i].prodName == RowsSelectedProduct[j].prodName){
                        RowsSelectedProduct.splice(j,1);
                        break;
                    }
                }
                checkProductRecordsMap[i].setCheckbox = false;
            }
        }
        component.set('v.RowsSelected',RowsSelectedProduct);
        component.set('v.productList',checkProductRecordsMap);
    },

    handleSelectedProductHelper : function(component,event){

        var checkvalue = event.getSource().get('v.text').setCheckbox;
        var productName = event.getSource().get('v.text').prodName;
        var RowsSelectedProduct = component.get("v.RowsSelected");
        var checkProductRecords = component.get("v.productList");
        if(checkvalue == false) {
            component.set("v.isSelectAll","false");
            for(var j=0; j<RowsSelectedProduct.length; j++){
                if(productName == RowsSelectedProduct[j].prodName){
                    RowsSelectedProduct.splice(j,1);
                    break;
                }
            }
        }else{
            var selected = true;
            RowsSelectedProduct.push(event.getSource().get('v.text'));
            for(var i=0; i<checkProductRecords.length; i++){
                if(!checkProductRecords[i].setCheckbox){
                    selected = false;
                    break;
                }
            }
            component.set("v.isSelectAll",selected);
        }
        component.set('v.RowsSelected',RowsSelectedProduct);
    },

    addQuantityHelper : function(component, event) {

        var selectedProducts = component.get("v.RowsSelected");

        var diffType= 'False';
        var isValid= 'True';

        if(selectedProducts.length === 0){
            isValid= 'False';
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select atleast one product",
                "type": "error"
            });
            toastEvent.fire();
        }else if (selectedProducts.length > 1) {
            for(let i=0; i<selectedProducts.length-1; i++){
                if((selectedProducts[i].prodCategory != selectedProducts[i+1].prodCategory) && (selectedProducts[i].prodCategory == 'AC' || selectedProducts[i+1].prodCategory == 'AC') ) {
                    isValid= 'False';
                    diffType= 'True';
                    break;
                }
            }
        }

        if(diffType=='True') {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "You cannot club AC with Non-AC Products. Please select Products of same category.",
                "type": "error"
            });
            toastEvent.fire();
        }

        if(isValid=='True'){
            $A.util.addClass(component.find("showProduct"), "slds-hide");
            $A.util.removeClass(component.find("addQuantity"), "slds-hide");    
            component.set('v.columns', [
                {label: 'Product Code', fieldName: 'prodName', type: 'text'},
                {label: 'Product Description', fieldName: 'prodDescription', type: 'text'},
                {label: 'Dealer Price', fieldName: 'prodDealerPrice', type: 'Decimal'},
                {label: 'Quantity', fieldName: 'quantity', type: 'decimal', editable: "true", hideDefaultActions: true}
            ]);
        }
    },

    backtoProductHelper : function(component, event) {
        $A.util.removeClass(component.find("showProduct"), "slds-hide");
        $A.util.addClass(component.find("addQuantity"), "slds-hide");
        var recordInfo= component.get("v.RowsSelected");
        var recordInfoListString=JSON.stringify(recordInfo);

        var parentId = component.get('v.recordId');
        var action = component.get("c.getProductController");
        action.setParams({
            recordId : parentId,
            selectedRows: recordInfoListString
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var reponseList = JSON.parse(response.getReturnValue());
                if(reponseList.wrapProductList.length > 0){

                    if(reponseList.wrapProductList[0].prodRecordType != 'Finished Goods') {
                        component.set("v.isFG",false);
                        
                    }
                    var selected = true;
                    for(var i=0; i<reponseList.wrapProductList.length; i++){
                        if(!reponseList.wrapProductList[i].setCheckbox){
                            selected = false;
                            break;
                        }
                    }
                    component.set("v.isSelectAll",selected);

                    var categoryMap = [];
                    for(var key in reponseList.prodCategoryMap){
                        categoryMap.push({key: key, value: reponseList.prodCategoryMap[key]});
                    }
                    component.set("v.fieldMap", categoryMap); 
                    var partTypeMapList = [];
                        for(var key in reponseList.sPartTypeMap){
                            partTypeMapList.push({key: key, value: reponseList.sPartTypeMap[key]});
                        }
                        component.set("v.partTypeMap", partTypeMapList);
                    component.set("v.productList", reponseList.wrapProductList);
                }else{
                    component.set("v.noProductFound","false");
                }
            }if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }           
            
        });
        $A.enqueueAction(action);
    },

    searchProductHelper : function(component, event){

        component.set("v.noSearchedProductFound","true");
        var checkBoolean= component.get("v.isFG");
        if(checkBoolean==true){
         
            var recordInfoListString;
            var searchStringId= component.find("SearchBox").get("v.value");
            var filterCategoryId= component.find("filterBox").get("v.value");
            var recordInfo= component.get("v.RowsSelected");
            
            if(searchStringId.substring(0,1) == '_' || searchStringId.substring(0,1) == '%'){
                console.log('inside else if');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Please enter correct input value.",
                    "type": "error"
                });
                toastEvent.fire();
            }else{
                if(recordInfo.length > 0){
                    recordInfoListString=JSON.stringify(recordInfo);
                }else{
                    recordInfoListString = '';
                }
                
                var action = component.get("c.filteredOnCategory");
                action.setParams({
                    orderID : component.get("v.recordId"),
                    filterValue : filterCategoryId,
                    searchString : searchStringId,
                    selectedRows : recordInfoListString
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    var output= response.getReturnValue();
                    if (state === "SUCCESS") {
                        var reponseList = JSON.parse(response.getReturnValue());
                        if(reponseList.wrapProductList.length > 0) {
                            
                            var selected = true;
                            for(var i=0; i<reponseList.wrapProductList.length; i++){
                                if(!reponseList.wrapProductList[i].setCheckbox){
                                    selected = false;
                                    break;
                                }
                            }
                            component.set("v.isSelectAll",selected);
                            
                            component.set("v.productList", reponseList.wrapProductList);
                        }
                        else {
                            component.set("v.noSearchedProductFound","false");
                        }
                    }
                    else if (state === "ERROR") { 
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
                $A.enqueueAction(action); 
            }  
        }else{
            var topPartTypeValue= component.find("topPart").get("v.checked");
            var categoryValue= component.find("categorySP").get("v.value");
            var partTypeValue= component.find("partType").get("v.value");
            var searchMatString= component.find("modelName").get("v.value");
			 var recordInfoListString;
            var recordInfo= component.get("v.RowsSelected");
            console.log('searchMatString::'+searchMatString);
             if(searchMatString.substring(0,1) == '_' || searchMatString.substring(0,1) == '%'){
                console.log('inside else if');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Please enter correct input value.",
                    "type": "error"
                });
                toastEvent.fire();
             }else{
                 if(recordInfo.length > 0){
                     recordInfoListString=JSON.stringify(recordInfo);
                 }else{
                     recordInfoListString = '';
                 }
                 
                 var action = component.get("c.filterOnSparePart");
                 action.setParams({
                     "orderID": component.get("v.recordId"),
                    "topPartType":topPartTypeValue,
                    "categoryType": categoryValue,
                    "partType":partTypeValue,
                    "searchString": searchMatString,
                     "selectedRows" : recordInfoListString
                 });
                 action.setCallback(this, function(response){
                     var state = response.getState();
                    
                     var output= response.getReturnValue();
                     if (state === "SUCCESS") {
                         var reponseList = JSON.parse(response.getReturnValue());
                          console.log(reponseList);
                         if(reponseList.wrapProductList.length > 0) {
                             
                             var selected = true;
                             for(var i=0; i<reponseList.wrapProductList.length; i++){
                                 if(!reponseList.wrapProductList[i].setCheckbox){
                                     selected = false;
                                     break;
                                 }
                             }
                             component.set("v.isSelectAll",selected);
                             
                             component.set("v.productList", reponseList.wrapProductList);
                         }
                         else {
                             component.set("v.noSearchedProductFound","false");
                         }
                     }
                     else if (state === "ERROR") { 
                         var errors = response.getError();
                         if (errors) {
                             if (errors[0] && errors[0].message) {
                                 console.log("Error message: " + 
                                             errors[0].message);
                             }
                         } else {
                             console.log("Unknown error");
                         }
                     }
                 });
                 $A.enqueueAction(action); 
             }
        }
    },

    addSOLineItemHelper : function(component, event){
        var updatedRecords = component.find("saveQuantity").get("v.draftValues");
        var recordInfo= component.get("v.RowsSelected");
        
        for (var i=0; i < updatedRecords.length; i++) {
            var str= updatedRecords[i].Id;
            var rowIndex=str.substring(4);
            recordInfo[rowIndex].quantity=updatedRecords[i].quantity;
        }

        var recordInfoListString=JSON.stringify(recordInfo);

        for(var i = 0; i < recordInfo.length; i++) {

            if(recordInfo[i].prodDealerPrice === null || recordInfo[i].prodDealerPrice === ''){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Dealer Price can not be blank",
                    "type": "error"
                });
                toastEvent.fire();
                return;
            }else if(typeof recordInfo[i].quantity === 'undefined'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Please enter Quantity",
                    "type": "error"
                });
                toastEvent.fire();
                return;
            }
            else if(recordInfo[i].quantity == 0) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Please enter Quantity",
                    "type": "error"
                });
                toastEvent.fire();
                return;
            }
            else if(recordInfo[i].quantity == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Quantity entered cannot be empty",
                    "type": "error"
                });
                toastEvent.fire();
                return;
            }
            else if(recordInfo[i].quantity % 1 != 0){
                if(isNaN(recordInfo[i].quantity)) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error",
                        "message": "Please enter a valid number",
                        "type": "error"
                    });
                    toastEvent.fire();
                    return;
                }
                else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error",
                        "message": "Please Enter a whole number in quantity",
                        "type": "error"
                    });
                    toastEvent.fire();
                    return;
                    
                }
            }
        }

        var action = component.get("c.addSOLineItemtoOrder");
        action.setParams({
            "selectedRows": recordInfoListString,
            "ordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var output= response.getReturnValue();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                if (output.startsWith("Error")) {
                    toastEvent.setParams({
                        "title": "Error",
                        "message": output,
                        "type": "error"
                    });
                    toastEvent.fire();
                }
                else {
                    toastEvent.setParams({
                        "title": "Success",
                        "message": output,
                        "type": "success"
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    $A.get("e.force:refreshView").fire(); 
                }
                
                
            }
            else {
                var errors = response.getError();
                if (errors) {
                   if (errors[0] && errors[0].message) {
                       var toastEvent = $A.get("e.force:showToast");
                       toastEvent.setParams({
                           "title": "Error",
                           "message": "Error message: " + errors[0].message,
                           "type": "error"
                       });
                       toastEvent.fire();
                    }
               } else {
                   console.log("Unknown error");
               }
            }
        });
        $A.enqueueAction(action);
    },

    closeQuickActionHelper : function(component,event) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})