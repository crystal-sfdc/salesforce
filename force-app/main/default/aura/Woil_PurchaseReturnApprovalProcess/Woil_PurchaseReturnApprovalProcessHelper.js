({
    init: function(component, event, helper) { 
        
        var toastEvent = $A.get("e.force:showToast");
        console.log("component.get v.recordId=======>" +component.get("v.recordId"));
        
        //Valid Stock return AND Status check to restrict invalid entry 
        var action = component.get("c.validEntrycheck");
        action.setParams({recordId :component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            var errors = response.getError();
            if (state === "SUCCESS") {
                console.log("From server: " + response.getReturnValue());
                
            }
            else if (state === "ERROR") {
                $A.get("e.force:closeQuickAction").fire();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: " + errors[0].message);
                        if(errors[0].message === "notBelong"){
                            toastEvent.setParams({
                                "title": "Error",
                                "message": "Stock not belong to your inventory!",
                                "type" : "warning",
                                "duration" : "7000"
                            });
                            toastEvent.fire();
                        }else if(errors[0].message.substring(0,12) === "notAvailable"){
                            toastEvent.setParams({
                                "title": "Error",
                                "message": 'Stock not available for selected products '+errors[0].message.substring(13),
                                "type" : "warning",
                                "duration" : "7000"
                            });
                            toastEvent.fire();
                        }else if(errors[0].message === 'Approval Pending' || 
                                 errors[0].message === 'Rejected' || 
                                 errors[0].message ===  'Active'){
                            toastEvent.setParams({
                                "title": "Can not initiate Approval process!",
                                "message": "Approval is in "+errors[0].message+" stage. Please create new record, to initiate approval process",
                                "type" : "warning",
                                "duration" : "7000"
                            });
                            toastEvent.fire();
                        }else if(errors[0].message === 'Please add the product first'){
                            toastEvent.setParams({
                                "title": "Error",
                                "message": "Please add the product first",
                                "type" : "warning",
                                "duration" : "7000"
                            });
                            toastEvent.fire();
                        }else if(errors[0].message === "Please serialized the product first"){
                            toastEvent.setParams({
                                "title": "Not allowed",
                                "message": "Please do the serialization of the product first.",
                                "type" : "warning",
                                "duration" : "7000"
                            });
                            toastEvent.fire();
                        }else if(errors[0].message === 'Case is of OnSpot, AutoApproval and AutoPostReturn done'){
                            toastEvent.setParams({
                                "title": "Success",
                                "message": "Case is of OnSpot, AutoApproval and AutoPostReturn done.!",
                                "type" : "success",
                                "duration" : "7000"
                            });
                            toastEvent.fire();
                        }
                            else{
                                console.log("Unknown error");
                            }       
                    }
                }               
            }
        });
        $A.enqueueAction(action);
        
        
        //function to fetch the team member list and their respective BM's
        var action = component.get("c.getApprovers");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.asmList",response.getReturnValue() );
                console.log("=========>"+component.get("v.asmList", asmList));
            }
            else{
                console.log("getApprovers Fail");
                var errors = response.getError();
                if (errors){
                    
                    if (errors[0] && errors[0].message) 
                        console.log("Error message: " + errors[0].message);
                    else{
                        console.log("Unknown error");
                    } 
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    // when user select the 1st Approver, 2nd Approver get auto selected.
    handleChange: function (component, event, helper) {
        var selPickListValue = event.getSource().get("v.value");
        component.set("v.firstApprover",selPickListValue);
        console.log("firstApprover ========> "+selPickListValue);
        console.log("secondApprover ========> "+component.get("v.secondApprover"));
        var firstApprover=component.get("v.firstApprover");
        
        var UserArr = component.get("v.asmList");
        for(var i=0;i<UserArr.length;i++){
            if(UserArr[i].Id === selPickListValue){
                if(UserArr[i].Manager !=null){
                    console.log("secondApprover===========>"+UserArr[i].Manager.Id); 
                    component.set("v.secondApproverName", UserArr[i].Manager.Name) ;
                    component.set("v.secondApprover", UserArr[i].Manager.Id) ;
                    console.log("secondApprover ========> "+component.get("v.secondApprover"));
                    //console.log(component.get("v.secondApprover")+ " " +component.get("v.secondApproverName"));
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    component.set("v.secondApprover", "") ;
                    toastEvent.setParams({
                        "title": "Manager not assigned",
                        "message": "Contact your adminstrator",
                        "type" : "warning",
                        "duration" : "7000"
                    });
                    toastEvent.fire();
                } 
            }
        }
    },
    
    //Final Submission of Approval
    handleSubmit: function(component, event, helper) {
        var firstApprover=component.get("v.firstApprover");
        var secondApprover=component.get("v.secondApprover");
        var recId=component.get("v.recordId");
        var toastEvent = $A.get("e.force:showToast");
        
        if (typeof(secondApprover) != 'undefined' && secondApprover != null){
            console.log(" recId==> "+recId+
                        "firstApprover==> "+firstApprover+ 
                        " secondApprover==> " +secondApprover);
            var action = component.get("c.submitApprovalProcess");
            action.setParams({
                "recordId":recId,
                "firstApprover" : firstApprover,
                "secondApprover" : secondApprover
            });
            action.setCallback(this, function(response){
                var state = response.getState(); 
                console.log(state);
                //(state);
                if(state == 'SUCCESS') {
                    console.log(response.getReturnValue());
                    if(response.getReturnValue() === 'success'){
                        component.find('notifyId').showToast({
                            "variant": "Success",
                            "title": "Success!",
                            "message": "Approval Submitted."
                        });
                        $A.get("e.force:closeQuickAction").fire();  
                        window.location.reload()
                    }
                }
                else{
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    component.find('notifyId').showToast({
                        "variant": "error",
                        "title": "error!",
                        "message": response.getReturnValue()
                    });
                }
            });
            $A.enqueueAction(action);
        }
        else{
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please fill all mandatory fields.",
                "type" : "warning"
            });
            toastEvent.fire();
        }
    },
    
    gethandleClose: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})