({
    doInit: function(component, event, helper) {
        var recId=component.get("v.recordId");
        console.log(recId);
        var action = component.get('c.newScreenControl'); 
        action.setParams({
            "recordId" : recId 
        });
        action.setCallback(this, function(a){
            var state = a.getState(); 
            var ScreenInformation = [];
            if(state == 'SUCCESS') {
                ScreenInformation = a.getReturnValue();
                console.log(ScreenInformation);
                if(ScreenInformation.OrderItemInfo != null){
                    component.set("v.displayerror",true);
                    component.set("v.ScreencontrollApproved",false);
                    component.set("v.ScreencontrollAsm",false);
                    component.set("v.ScreencontrollTeamMember",false);
                    
                }
                else{
                    if(ScreenInformation.OrderStatus != null){
                        component.set("v.ScreencontrollApproved",true);
                        component.set("v.displayerror",false);
                        component.set("v.ScreencontrollAsm",false);
                        component.set("v.ScreencontrollTeamMember",false);
                    }
                    
                    else if(ScreenInformation.UserAsmInfo != null){
                        component.set("v.ScreencontrollAsm",true);
                        component.set("v.ScreencontrollApproved",false);
                        component.set("v.displayerror",false);
                        component.set("v.ScreencontrollTeamMember",false);
                        
                    }
                        else if(ScreenInformation.SelectApprover != null && ScreenInformation.UserAsmInfo == null){
                            component.set("v.ScreencontrollTeamMember",true);
                            component.set("v.ScreencontrollApproved",false);
                            component.set("v.displayerror",false);
                            component.set("v.ScreencontrollAsm",false);
                            //second Action
                            var action2 = component.get('c.newTeamMemberRole'); 
                            action2.setParams({
                                "recordId" : recId 
                            });
                            action2.setCallback(this, function(response){
                                var state = response.getState(); 
                                if(state == 'SUCCESS') {
                                    component.set("v.options",response.getReturnValue());
                                }
                            });
                            $A.enqueueAction(action2);
                        }
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    handleApproved : function (component, event, helper) {
        var recId=component.get("v.recordId");
        var actionStatus = component.get("c.changeOrderStatus"); 
        actionStatus.setParams({
            "recordId" : recId 
        });
        actionStatus.setCallback(this, function(response){
            var state = response.getState(); 
            if(state == 'SUCCESS') {
                component.find('notifyId').showToast({
                    "variant": "Success",
                    "title": "Success!",
                    "message": "Order successfully Approved."
                });
                $A.get("e.force:closeQuickAction").fire();  
                window.location.reload()   
            }
        });
        $A.enqueueAction(actionStatus);
    },
    
    onChange: function (component, event, helper) {
        var selPickListValue = event.getSource().get("v.value");
        component.set("v.OneUserId",selPickListValue);
        console.log(selPickListValue);
        var UserArr = component.get("v.options");
        for(var i=0;i<UserArr.length;i++){
            if(UserArr[i].Id === selPickListValue){
                if(UserArr[i].Manager !=null){
                    console.log(UserArr[i].Manager.Name); 
                    component.set("v.SecondApp", UserArr[i].Manager.Name) ;
                }
                else {
                    component.set("v.SecondApp", "") ;
                }
                
            }
            
        }
            
        
        /*  var action = component.get("c.Managerinfo"); 
        action.setParams({
            "UserId" : selPickListValue
        });
        action.setCallback(this, function(a){
            var state = a.getState(); 
            if(state == 'SUCCESS') {
                console.log(a.getReturnValue());
                if(a.getReturnValue() !=null){
                    component.set("v.SecondApp", a.getReturnValue()) ;
                }
                else{
                    component.set("v.SecondApp",'') ;
                }
            }
        });
        $A.enqueueAction(action);
        */
        
    },
    CancleClick: function (component, event, helper) {
                
                $A.get("e.force:closeQuickAction").fire();
                
            },
    handleSubmit: function(component, event, helper) { 
        var Manager = component.get("v.SecondApp");
        var UId=component.get("v.OneUserId");
        var recId=component.get("v.recordId");
        //bugs
        console.log(Manager+"	"+UId);
        if(Manager == undefined || Manager == ""){
            console.log("enter both");
            component.set("v.FillDataInfo",true);
        }
        else{
            component.set("v.FillDataInfo",false);
            var action = component.get('c.ApprovelProccess'); 
            action.setParams({
                "UserId" : UId ,
                "RecordId":recId,
            });
            action.setCallback(this, function(a){
                var state = a.getState(); 
                console.log(state);
                if(state == 'SUCCESS') {
                    console.log(a.getReturnValue());
                    if(a.getReturnValue() != null){
                        // console.log(a.getReturnValue());
                        component.find('notifyId').showToast({
                            "variant": "Success",
                            "title": "Success!",
                            "message": "Order has successfully Submited."
                        });
                        $A.get("e.force:closeQuickAction").fire();  
                        window.location.reload()
                    }
                }
                else{
                    var errors = a.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    component.find('notifyId').showToast({
                        "variant": "error",
                        "title": "error!",
                        "message": a.getReturnValue()
                    });
                    // $A.get("e.force:closeQuickAction").fire();  
                    // window.location.reload()
                }
            });
            $A.enqueueAction(action);
        }
    }
    
    
    
    
})