({
    doInit: function(component, event, helper){
        helper.getProduct(component,event);
    },
    handleSelectAllProduct : function(component, event, helper) {
        helper.handleSelectAllProductHelper(component,event);
    },
    handleSelectedProduct : function(component, event, helper){
        helper.handleSelectedProductHelper(component,event);
    },

    addQuantity : function(component, event, helper){
        helper.addQuantityHelper(component,event);
    },

    backtoProduct : function(component, event, helper){
        helper.backtoProductHelper(component,event)
    },

    searchProduct : function(component, event, helper){
        helper.searchProductHelper(component,event)
    },

    addSOLineItem : function(component, event, helper){
        helper.addSOLineItemHelper(component,event)
    },
    closeQuickAction : function(component, event, helper) {
		helper.closeQuickActionHelper(component,event);
	}
})