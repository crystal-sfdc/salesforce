({
    oninit : function(component, event, helper) {
        //alert('inside SKUCode::::'+component.get("v.recordId"));
        helper.checkValidEntry(component, event, helper);
        component.set('v.showConfirmDialog', true);
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    },
    handleConfirmDialogYes : function(component, event, helper) {
        console.log('Yes');
        component.set('v.showConfirmDialog', false);
        helper.PostGRN(component, event, helper);
    },
    
    handleConfirmDialogNo : function(component, event, helper) {
        console.log('No');
        component.set('v.showConfirmDialog', false);
        $A.get("e.force:closeQuickAction").fire();  
    },
})