({
    PostGRN : function(component, event, helper) {
        var msg ='Are you sure you want to Post SRN?';
        var action = component.get("c.postSRN");
        action.setParams({"recordId":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success',
                    message: 'SRN is Posted',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message:'Already Posted!',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();  
            }
        });
        $A.enqueueAction(action);
        
    },
    checkValidEntry: function(component, event, helper) {
        var action = component.get("c.checkValidPostReturnEntry");
        action.setParams({"recordId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert("From server SUCCESS: " + response.getReturnValue());
                var toastEvent = $A.get("e.force:showToast");
                if(response.getReturnValue() === "notBelong"){
                    toastEvent.setParams({
                        title : 'Error',
                        message:'Stock not belong to your inventory!',
                        duration:' 4000',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    
                }
                else if(response.getReturnValue().substring(0,12) === "notAvailable"){
                    toastEvent.setParams({
                        title : 'Error',
                        message : 'Stock not available for selected products '+response.getReturnValue().substring(13),
                        duration :' 4000',
                        type : 'error',
                        mode : 'pester'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
            else {
                //
                var errors = response.getError();
                if (errors) {
                    if(errors[0].message === 'No check In case of On spot return'){
                        console.log('No check In case of On spot return');
                    }
                    else if (errors[0] && errors[0].message) {
                        alert("From server Error: " + errors[0].message);
                        console.log("From server Error: " + errors[0].message);
                        $A.get("e.force:closeQuickAction").fire();
                    }
                } 
                else{
                    //alert("Unknown error");
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})