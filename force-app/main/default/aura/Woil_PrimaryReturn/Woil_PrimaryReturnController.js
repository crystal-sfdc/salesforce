({
    
	getLineItemDetails : function(component, event, helper) {
        console.log('name comp::::'+component.getName());
		component.set('v.mycolumns', [
            
            {label: 'Product Code', fieldName: 'SKUCode', type: 'text'},
             {label: 'Product Description', fieldName: 'SKUDesc', type: 'text'},

            {label: 'Quanity', fieldName: 'Woil_Accepted_Quantity_formula__c', type: 'Text'},
            {label: 'Action', fieldName: 'Accepted', type: 'Text'},
            
            { 
                label: 'GRN No', fieldName: 'GRNUrl', wrapText: true,
                type: 'url',
                typeAttributes: {
                    label: { 
                        fieldName: 'GRNNo' 
                    },
                    target : '_blank'
                }
            },
            {label: 'Posted', fieldName: 'Posted', type: 'boolean'},
            {label: 'Transaction Date', fieldName: 'CreatedDate', type: 'date', typeAttributes: {  
                                                                            day: 'numeric',  
                                                                            month: 'short',  
                                                                            year: 'numeric',  
                                                                         }}
            
        ]);
        
         var action = component.get("c.getLineItems");
        action.setParams({"invoice_no":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            var data=response.getReturnValue();
            var selectedValues=[];
              
            
            console.log('rows::::'+data);
            let baseUrl = 'https://'+location.host+'/';
            if (state === "SUCCESS") {
                console.log('inside success::::');
                data.forEach(Rec => {
                    console.log(Rec.CreatedDate);
                    Rec.Accepted='Accepted';
                    if(Rec.Woil_GRN__r.Woil_Sales_Invoice__r.Name){
                        Rec.InvoiceName = Rec.Woil_GRN__r.Woil_Sales_Invoice__r.Name;
                    }
                    if(Rec.Woil_Product__r.Name){
                        Rec.SKUCode = Rec.Woil_Product__r.Name;
                    }
                    if(Rec.Woil_Product__r.Description){
                        Rec.SKUDesc = Rec.Woil_Product__r.Description;
                    }
                    
                	if(Rec.Woil_GRN__r.Name){
                        Rec.GRNNo = Rec.Woil_GRN__r.Name;
                    	Rec.GRNUrl = baseUrl+Rec.Woil_GRN__c;
                    	Rec.Posted = Rec.Woil_GRN__r.Woil_Posted__c;
                    }
                });
                component.set("v.result",data);  
                console.log('resultt'+component.get("v.result"));
            }
        });
        $A.enqueueAction(action);
	},
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    }
})