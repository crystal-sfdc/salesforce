({
    doInit : function(component, event, helper) {
		helper.checkStatus(component,event);
	},
    closeQuickAction : function(component, event, helper) {
		helper.closeQuickActionHelper(component,event);
	},
    createInventory : function(component, event, helper) {
		helper.createInventoryHelper(component,event);
	}
})