({
    checkStatus : function(component,event) {
        component.set("v.spinner", true);
		var parentId = component.get('v.recordId');
        var action = component.get("c.checkStatusController");
        action.setParams({
            recordId : parentId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	var details = response.getReturnValue();
                if(details.split("#")[0] === 'Open'){
                    if(details.split("#")[1] == '0'){
                        component.set("v.message",'Please Scan material first.');
                        component.set("v.processed", true);
                        component.set("v.showDisclaimer", false);
                        component.set("v.error", false);
                    }else{
                        component.set("v.message",'Do you want to POST the Invoice ?');
                        component.set("v.processed", false);
                        component.set("v.showDisclaimer", true);
                        component.set("v.error", false);
                    }
                }else{
                    component.set("v.message",'This Order is already processed.');
                    component.set("v.processed", true);
                    component.set("v.showDisclaimer", false);
                    component.set("v.error", false);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }else{
                        console.log("Unknown error");
                    }
                    component.set("v.message",errors[0].message);
                    component.set("v.processed", false);
                    component.set("v.showDisclaimer", false);
                    component.set("v.error", true);
                } 
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
	},

    createInventoryHelper : function(component,event){
        var parentId = component.get('v.recordId');
        var action = component.get("c.createInventoryController");
        action.setParams({
            recordId : parentId
        });
        
        action.setCallback(this, function(response) {
            component.set("v.spinner", true);
            var state = response.getState();
            if (state === "SUCCESS") {
            	var details = response.getReturnValue();
                if(details === 'Success'){
                    $A.get("e.force:closeQuickAction").fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": parentId,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }else{
                    component.set("v.message",response.getReturnValue());
                    component.set("v.processed", false);
                    component.set("v.showDisclaimer", false);
                    component.set("v.error", true);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }else{
                        console.log("Unknown error");
                    }
                    component.set("v.message",errors[0].message);
                    component.set("v.processed", false);
                    component.set("v.showDisclaimer", false);
                    component.set("v.error", true);
                } 
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    },

    closeQuickActionHelper : function(component,event) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})