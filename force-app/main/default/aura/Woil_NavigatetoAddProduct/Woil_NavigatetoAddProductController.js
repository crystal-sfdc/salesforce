({
	navigateToGetProducts : function(component, event, helper) {
        var sPageURL = window.location.href;
      	$A.get("e.force:closeQuickAction").fire();
  
        var evt= $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:Woil_GetProductsComponent",
            componentAttributes: {
                orderId : component.get("v.recordId"),
                returnToUrl: sPageURL
            }
        })
        evt.fire();
	}
})