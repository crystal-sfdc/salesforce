({
    PostGRN : function(component, event, helper) {
      
        var action = component.get("c.PostGrn");
        action.setParams({"GrnId":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if (state === "SUCCESS") {
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success',
                    message: 'GRN is Posted',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire();
            }else{
               var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message:'Already Posted!',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
              $A.get("e.force:closeQuickAction").fire();  
            }
        });
        $A.enqueueAction(action);
    }
})