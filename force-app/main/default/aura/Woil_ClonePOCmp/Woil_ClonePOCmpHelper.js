({
    checkStatus : function(component,event){
        component.set("v.spinner", true);
        var parentId = component.get('v.recordId');
        var action = component.get("c.checkStatusController");
        action.setParams({
            recordId : parentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var respo = response.getReturnValue();
            console.log('respo---'+respo);
            if (state === "SUCCESS") {
                if(respo === 'Not a Community user'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        message: 'You dont have permission to clone this PO',
                        type: 'error',
                    });
                    toastEvent.fire();

                    $A.get("e.force:closeQuickAction").fire();
                }else if(respo === 'Community User'){
                    component.set("v.showDisclaimer", true);
                    component.set("v.message",'Do you want to clone this Order?');
                }               
                
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    },

    ClonePoHelper : function(component,event){
        component.set("v.spinner", true);
        var parentId = component.get('v.recordId');
        var action = component.get("c.clonePOController");
        action.setParams({
            recordId : parentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var respo = response.getReturnValue();
            console.log('state---'+state);
            console.log('respo---'+respo);
            if (state === "SUCCESS") {
                if(respo.split("#")[0] != 'error'){
                    $A.get("e.force:closeQuickAction").fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                    "recordId": respo.split("#")[1],
                    "slideDevName": "detail"
                    });
                    navEvt.fire();
                }else if(respo.split("#")[1].includes('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "message": respo.split("#")[1].split("FIELD_CUSTOM_VALIDATION_EXCEPTION, ")[1]
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "message": "Some Error occured. Please Contact to your System Admin."
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                
            }if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    }
})