({
    doInit: function(component, event, helper){
        helper.checkStatus(component,event);
    },
    closeQuickAction: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    ClonePo: function(component, event, helper){
        helper.ClonePoHelper(component,event);
    }
})