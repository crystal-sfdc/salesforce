({
    
	getLineItemDetails : function(component, event, helper) {
        console.log('name comp::::'+component.getName());
		component.set('v.mycolumns', [          
            {label: 'SKU Code', fieldName: 'SKUCode', type: 'text'}, 
            {label: 'Warehouse Location', fieldName: 'location1', type: 'text'}, 
            { 
                label: 'Serial No', fieldName: 'SRNUrl', wrapText: true,
                type: 'url',
                typeAttributes: {
                    label: { 
                        fieldName: 'SRNNo' 
                    },
                    target : '_blank'
                }
            }           
        ]);
        
         var action = component.get("c.getScannedSerials");
        action.setParams({"pr_no":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            var data=response.getReturnValue();
            var selectedValues=[];
            console.log('data:::'+JSON.stringify(data));
            let baseUrl = 'https://'+location.host+'/';
            if (state === "SUCCESS") {
                
                data.forEach(Rec => {
                    
                    if(Rec.Woil_Warehouse_Location__c!=null && Rec.Woil_Warehouse_Location__c!='undefined'){
                        Rec.location1 = Rec.Woil_Warehouse_Location__r.Woil_Code__c;
                    }
                    if(Rec.Woil_Product__r.Name){
                        Rec.SKUCode = Rec.Woil_Product__r.Name;
                    }
                	if(Rec.Woil_Serial_Number__c){
                        Rec.SRNNo = Rec.Woil_Serial_Number__c;
                    	Rec.SRNUrl = baseUrl+Rec.Id;
                    	
                    }
                });
                component.set("v.result",data);  
                console.log('resultt'+component.get("v.result"));
            }
        });
        $A.enqueueAction(action);
	},
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    }
})