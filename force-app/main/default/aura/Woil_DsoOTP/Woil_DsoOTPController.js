({
    
    doInit:function (component, event, helper) {
        var rec = component.get("v.recordId");
       // console.log('hello');
        
        var action = component.get('c.doInitAction'); 
        action.setParams({
            "recordId1" : rec
        });
        action.setCallback(this, function(response){
            var state = response.getState(); 
            if(state == 'SUCCESS') {   
                console.log(response.getReturnValue().Woil_Active__c);
                component.set("v.DisplayInfo",response.getReturnValue());
            } 
            
        });
        $A.enqueueAction(action);
        
    },
    
    CancleClick : function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire(); 
        
    },
    ChangeInfo:function (component, event, helper) {
        component.set("v.FillDataInfo","");
      //  console.log("hello");
        
    },
    SubmitActionforRecordUpdate : function (component, event, helper) {
        var rec = component.get("v.recordId");
        if(!$A.util.isEmpty(component.get("v.Input")) &&  component.get("v.Input").match(/^\d{5}$/)){
            var action = component.get('c.SubmitAction'); 
            action.setParams({
                "recordId1" : rec
            });
            action.setCallback(this, function(a){
                var state = a.getState(); 
                if(state == 'SUCCESS') {   
                    component.find('notifyId').showToast({
                        "variant": "Success",
                        "title": "Success!",
                        "message": "OTP has been validate successfully "
                    });
                    $A.get("e.force:closeQuickAction").fire();  
                }
            });
            $A.enqueueAction(action);
            window.location.reload() ; 
            
        }
        else{
                component.set("v.FillDataInfo",'please enter correct OTP');
           
        }
    }
})