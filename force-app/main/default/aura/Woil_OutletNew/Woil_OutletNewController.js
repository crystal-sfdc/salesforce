({
    handleCloseModal: function(component, event, helper){
        helper.getHandleClose(component,event);
    },
    handleSearchClick: function(component, event, helper){
        helper.getHandleSearchClick(component,event);
    },
    handleCreateNew: function(component, event, helper){
        helper.getHandleCreateNew(component,event);
    },
    handleCancel: function(component, event, helper){
        helper.getHandleClose(component,event);
    },
    handleSave: function(component, event, helper){
        helper.getHandleSave(component,event);
    },
    handlePinCodeChange: function(component, event, helper){
        helper.getHandlePinCodeChange(component,event);
    }
})