({
    // Function to close the search page Window
    getHandleClose: function(component, event, helper) {  
        component.set("v.openModal", false);
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "Outlet_Master__c"
        });
        homeEvt.fire();
    },
    
    
    // Function to perform search logic
    getHandleSearchClick: function(component, event, helper) {
        var searchtext = component.get("v.searchText");
        var regexGst = /^([0-9]){2}([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([0-9]){1}([a-zA-Z]){1}([0-9]){1}?$/;
        var regexPan = /([A-Z]){5}([0-9]){4}([A-Z]){1}$/;
        var toastEvent = $A.get("e.force:showToast");
        
        if(regexGst.test(searchtext) || regexPan.test(searchtext)){
            component.set("v.disableActive", false);
            
            component.set('v.mycolumns', [
                {label: 'Outlet Name', fieldName: 'Woil_Outlet_Name__c', type: 'text'},
                {label: 'Outlet Code', fieldName: 'Name', type: 'text'},
                {label: 'PAN', fieldName: 'Woil_PAN__c', type: 'Phone'},
                {label: 'GST', fieldName: 'Woil_GST__c', type: 'text '},
                {label: 'Pin Code', fieldName: 'Woil_Pin_Code__c', type: 'text '},
                {label: 'City', fieldName: 'Woil_City__c', type: 'text '},
                {label: 'State', fieldName: 'Woil_State__c', type: 'text '},
                {label: 'Contact Person', fieldName: 'Woil_Contact_Person__c', type: 'text '},
                {label: 'Mobile Number', fieldName: 'Woil_Mobile_Number__c', type: 'text '}
            ]);
            
            var action = component.get("c.getOutletList");
            action.setParams({ gstOrPan : searchtext });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == 'SUCCESS' && response.getReturnValue().length > 0) {
                    component.set("v.recordExist", true);
                    component.set('v.serachResults', response.getReturnValue());
                    
                    var rows = response.getReturnValue();     //storing the response in a temporary variable 
                    //looping through each row of the result 
                    for (var i = 0; i < rows.length; i++) { 
                        var row = rows[i]; 
                        //as data columns with relationship __r can not be displayed directly in data table, so generating dynamic columns 
                        if (row.Outlet__r) { 
                            //here you assign the related data to new variables
                            row.Woil_Outlet_Name__c = row.Outlet__r.Woil_Outlet_Name__c; 
                            row.Name = row.Outlet__r.Name; 
                            row.Woil_PAN__c = row.Outlet__r.Woil_PAN__c; 
                            row.Woil_GST__c = row.Outlet__r.Woil_GST__c;  
                            row.Woil_Pin_Code__c = row.Outlet__r.Woil_Pin_Code__c; 
                            row.Woil_City__c = row.Outlet__r.Woil_City__c;  
                            row.Woil_State__c = row.Outlet__r.Woil_State__c; 
                            row.Woil_Mobile_Number__c = row.Outlet__r.Woil_Mobile_Number__c;  
                            row.Woil_Contact_Person__c = row.Outlet__r.Woil_Contact_Person__c; 
                        } 
                    }
                    component.set("v.serachResults", rows); 
                    
                    toastEvent.setParams({
                        "title": "Record Found",
                        "message": "GST or PAN number found in records.",
                        "type" : "success"});
                    toastEvent.fire();
                }
                else{
                    var rows = response.getReturnValue();     //storing the response in a temporary variable 
                    //looping through each row of the result 
                    for (var i = 0; i < rows.length; i++) { 
                        var row = rows[i]; 
                        //as data columns with relationship __r can not be displayed directly in data table, so generating dynamic columns 
                        if (row.Outlet__r) { 
                            //here you assign the related data to new variables
                            row.Woil_Outlet_Name__c = row.Outlet__r.Woil_Outlet_Name__c; 
                            row.Name = row.Outlet__r.Name; 
                            row.Woil_PAN__c = row.Outlet__r.Woil_PAN__c; 
                            row.Woil_GST__c = row.Outlet__r.Woil_GST__c;  
                            row.Woil_Pin_Code__c = row.Outlet__r.Woil_Pin_Code__c; 
                            row.Woil_City__c = row.Outlet__r.Woil_City__c;  
                            row.Woil_State__c = row.Outlet__r.Woil_State__c; 
                            row.Woil_Mobile_Number__c = row.Outlet__r.Woil_Mobile_Number__c;  
                            row.Woil_Contact_Person__c = row.Outlet__r.Woil_Contact_Person__c; 
                        } 
                    }
                    component.set("v.serachResults", rows); 
                    
                    toastEvent.setParams({
                        "title": "No Record Found",
                        "message": "No Record Found with this GST or PAN number.",
                        "type" : "info"});
                    toastEvent.fire();
                }
                
            });
            $A.enqueueAction(action);
        }
        else{
            if(searchtext.length != 10 || searchtext.length != 15){
                toastEvent.setParams({
                    "title": "Not Valid!",
                    "message": "GST/PAN Identification Number is not valid.",
                    "type" : "warning"
                });
                toastEvent.fire();
            }
            else if(!regexGst.test(searchtext) && searchtext!=''){
                toastEvent.setParams({
                    "title": "Not Valid!",
                    "message": "GST Identification Number is not valid.",
                    "type" : "warning"
                });
                toastEvent.fire();
            }
                else if(!regexPan.test(searchtext) && searchtext!=''){
                    toastEvent.setParams({
                        "title": "Not Valid!",
                        "message": "PAN Identification Number is not valid.",
                        "type" : "warning"
                    });
                    toastEvent.fire();
                }
        }
    },
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Function to show the create New form
    getHandleCreateNew: function(component, event, helper) {
        
        if(component.get("v.searchText").length == 10){
            component.set("v.outletObj.Woil_PAN__c", component.get("v.searchText"));
            component.set("v.panDeactivate", true);
        }
        else if(component.get("v.searchText").length == 15){
            component.set("v.outletObj.Woil_GST__c", component.get("v.searchText"));
            component.set("v.gstDeactivate", true);  
            component.set("v.outletObj.Woil_PAN__c", component.get("v.searchText").substring(2,12));
            component.set("v.panDeactivate", true);
        }
        component.set("v.searchPage", false);
        
    },
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    getHandleSave: function(component, event, helper) {
        
        console.log(component.get("v.outletObj"));
        var outletObj = component.get("v.outletObj");
        var toastEvent = $A.get("e.force:showToast");
        var validation = false;
        //Form validation
        
        var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if (allValid == false) {
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please fill all mandatory fields.",
                "type" : "warning"
            });
            toastEvent.fire();
        } 
        else {
            
            var mobileNumber = component.get("v.outletObj.Woil_Mobile_Number__c");
            component.set("v.outletObj.Woil_Mobile_Number__c",mobileNumber.replace(/^0+/, ''));
            var pinCode = component.get("v.outletObj.Woil_Pin_Code__c");
            component.set("v.outletObj.Woil_Pin_Code__c",pinCode.replace(/^0+/, ''));
           
            if(!(component.get("v.outletObj").Woil_PAN__c.match(/([A-Z]){5}([0-9]){4}([A-Z]){1}$/))){
                toastEvent.setParams({
                    "title": "Not Valid!",
                    "message": "Enter valid PAN number",
                    "type" : "warning"
                });
                toastEvent.fire();
            }
            else{
                if(!(component.get("v.outletObj").Woil_GST__c.match(/^([0-9]){2}([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([0-9]){1}([a-zA-Z]){1}([0-9]){1}?$/))){
                    toastEvent.setParams({
                        "title": "Not Valid!",
                        "message": "Enter valid GST number",
                        "type" : "warning"
                    });
                    toastEvent.fire();
                }
                else{
                    if(!(component.get("v.outletObj").Woil_Mobile_Number__c.match(/^\d{10}$/))){
                        toastEvent.setParams({
                            "title": "Not Valid!",
                            "message": "Enter only 10 digit mobile number",
                            "type" : "warning"
                        });
                        toastEvent.fire();
                    }
                    else{
                        if(!(component.get("v.outletObj").Woil_Pin_Code__c.match(/^\d{6}$/))){
                            toastEvent.setParams({
                                "title": "Not Valid!",
                                "message": "Enter 6 digit Pin Code only!",
                                "type" : "warning"
                            });
                            toastEvent.fire();
                        }
                        else{
                            validation = true;
                        }
                    }
                }
            }
            
        }        
        if(validation == true){
            //Outlet Record Creation
            console.log("Retail_Outlet==========> "+component.get("v.outletObj.Woil_Is_Retail_Outlet__c"));
            console.log("retailVaue==========> "+component.get("v.retailVaue"));
            component.set("v.outletObj.Woil_Is_Retail_Outlet__c",component.get("v.retailVaue"));
            console.log("Retail_Outlet====Final======> "+component.get("v.outletObj.Woil_Is_Retail_Outlet__c"));
            var action = component.get("c.createOutletRec");
            action.setParams({ 
                outletObj : component.get("v.outletObj")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set('v.outletObj', response.getReturnValue());
                    
                    // Successfully record creation message
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Outlet "+response.getReturnValue().Woil_Outlet_Name__c+" has been created successfully.",
                        "type" : "success"
                    });
                    toastEvent.fire();
                    
                    //Navigate to newly created record 
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": response.getReturnValue().Id,
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                    
                }
                else if (state === "ERROR"){
                    var homeEvt = $A.get("e.force:navigateToObjectHome");
                    homeEvt.setParams({
                        "scope": "Outlet_Master__c"
                    });
                    homeEvt.fire();
                    toastEvent.setParams({
                        "title": "Failed!",
                        "message": "Record not created successfully.",
                        "type" : "error"
                    });
                    toastEvent.fire();
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    
    getHandlePinCodeChange: function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        if(component.get("v.outletObj").Woil_Pin_Code__c.length === 6){
            if(component.get("v.outletObj").Woil_Pin_Code__c.match(/^\d{6}$/)){
                var action = component.get("c.searchByPinCode");
                action.setParams({ pinCode : component.get("v.outletObj").Woil_Pin_Code__c });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        component.set("v.outletObj.Woil_City__c",(response.getReturnValue()).Woil_City__c);
                        component.set("v.outletObj.Woil_State__c",(response.getReturnValue()).Woil_State__c);
                        component.set("v.disableCityAndState",true);
                    }
                    else{
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
                $A.enqueueAction(action);
            }
            else{
                toastEvent.setParams({
                    "title": "Not Valid!",
                    "message": "Enter 6 digit Pin Code only!",
                    "type" : "warning"
                });
                toastEvent.fire();
            }
        }
        if(component.get("v.disableCityAndState")){
            component.set("v.disableCityAndState",false);
            component.set("v.outletObj.Woil_City__c",'');
            component.set("v.outletObj.Woil_State__c",'');
            
        }
        
    }   
})