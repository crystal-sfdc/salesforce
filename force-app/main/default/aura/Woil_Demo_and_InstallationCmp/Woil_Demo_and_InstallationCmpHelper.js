({
    searchHelper : function(component, event) {
        
        component.set('v.mycolumns', [    
            {label: 'Service Request No.', fieldName: 'Name', type: 'text'},    
            {label: 'Product', fieldName: 'ObjectCategoryName', type: 'text'},        
            {label: 'Service Request Status', fieldName: 'Status', type: 'Text'},
            {label: 'Service Request Type', fieldName: 'Ticket_Type', type: 'Text'},
            {label: 'Service Request Register Date', fieldName: 'Ticket_CreationDate', type: 'date', typeAttributes: {  
                                                                            day: 'numeric',  
                                                                            month: 'short',  
                                                                            year: 'numeric'}},
            {label: 'Service Request Updated Date', fieldName: 'Ticket_UpdationDate', type: 'date', typeAttributes: {  
                                                                            day: 'numeric',  
                                                                            month: 'short',  
                                                                            year: 'numeric'}},
            
        ]);
        
        var serviceNumber = component.find('inpBox').get("v.value");
        if(serviceNumber === '' || serviceNumber === undefined || serviceNumber === null){
            component.set("v.showData", false);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message: 'Please Enter Service request Number.',
                type: 'error',
            });
            toastEvent.fire();
        }else{
            component.set("v.spinner", true);
            var action = component.get("c.serachRequestNumber");
            action.setParams({
                requestNumber : serviceNumber
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var respo = response.getReturnValue();
                console.log('respo---'+respo);
                if (state === "SUCCESS") {
                    if(respo.includes(serviceNumber)){
                        var parsevalue = JSON.parse(respo);
                        component.set("v.parseList",parsevalue);
                        component.set("v.showData", true);
                    }else if(respo === 'Servic Request Number Not Found.'){
                        component.set("v.showData", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            message: respo,
                            type: respo,
                        });
                        toastEvent.fire();
                    }else if(respo === 'Internal Server Error.'){
                        component.set("v.showData", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: respo,
                            type: 'error',
                        });
                        toastEvent.fire();
                    }else{
                        component.set("v.showData", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            message: respo,
                            type: 'error',
                        });
                        toastEvent.fire();
                    }
                }
                component.set("v.spinner", false);
            });
            $A.enqueueAction(action);
        }
    }
})