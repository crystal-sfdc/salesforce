({
    getPORelatedInfo : function(component,event){
        //$A.get("e.force:closeQuickAction").fire()
        var action = component.get("c.getPORelatedInformation");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var shipToAddList = [];
                var customerList = [];
                var divList = [];
                var reasonList = [];
                
                var poDetailWrp = response.getReturnValue();
                
                if(poDetailWrp.isCommunity === true){
                    for ( var key in response.getReturnValue().customerDetailMap ) {
                        if (poDetailWrp.customerName === null || poDetailWrp.customerName === '' || poDetailWrp.customerName === undefined) {
                            poDetailWrp.customerName = response.getReturnValue().customerDetailMap[key];
                        }
                        customerList.push({value:response.getReturnValue().customerDetailMap[key], key:key});
                    }
                    console.log('customerList::::'+customerList);
                    component.set("v.customerDetailList",customerList);
                    
                    for ( var key in response.getReturnValue().shipToAddressMap ) {
                        if (poDetailWrp.shipToAddress === null || poDetailWrp.shipToAddress === '' || poDetailWrp.shipToAddress === undefined) {
                            poDetailWrp.shipToAddress = response.getReturnValue().shipToAddressMap[key];
                        }
                        shipToAddList.push({value:response.getReturnValue().shipToAddressMap[key], key:key});
                    }
                    console.log('shipToAddList::::'+shipToAddList);
                    component.set("v.shipToAddressList",shipToAddList);
                    
                    for ( var key in response.getReturnValue().divisionMap ) {
                        if (poDetailWrp.division === null || poDetailWrp.division === '' || poDetailWrp.division === undefined) {
                            poDetailWrp.division = response.getReturnValue().divisionMap[key];
                        }
                        divList.push({value:response.getReturnValue().divisionMap[key], key:key});
                    }
                    component.set("v.divisionList",divList);
                     for ( var key in response.getReturnValue().reasonMap ) {
                        if (poDetailWrp.reason === null || poDetailWrp.reason === '' || poDetailWrp.reason === undefined) {
                            poDetailWrp.reason = response.getReturnValue().reasonMap[key];
                        }
                        reasonList.push({value:response.getReturnValue().divisionMap[key], key:key});
                    }
                    component.set("v.reasonList",reasonList);
                    
                    component.set("v.poDetail",poDetailWrp);
                    
                    
                }else if(poDetailWrp.isCommunity === false){
                    component.set("v.isOrg", true);
                    component.set("v.isModalOpen", true);
                    component.set("v.isCommunity", false);
                    for ( var key in response.getReturnValue().customerDetailMap ) {
                        if (poDetailWrp.customerName === null || poDetailWrp.customerName === '' || poDetailWrp.customerName === undefined) {
                            poDetailWrp.customerName = response.getReturnValue().customerDetailMap[key];
                        }
                        customerList.push({value:response.getReturnValue().customerDetailMap[key], key:key});
                    }
                    component.set("v.customerDetailList",customerList);
                    component.set("v.poDetail",poDetailWrp);
                }
                component.set("v.spinner", false);
                 var a = component.get('c.fetchPoRelatedInformation');
        		$A.enqueueAction(a);
                
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPoRelatedInformation : function(component,event){
        component.set("v.spinner", true);
        var poDetailInfo = component.get("v.poDetail");
        
       // if(poDetailInfo.customerName != 'None'){
            var action = component.get("c.fetchPoRelatedInfo");
            /*action.setParams({ 
                accId : poDetailInfo.customerName
            });*/
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var shipToAddList = [];
                    var customerList = [];
                    var divList = [];
                    var reasonList = [];
                    
                    var poDetailWrp = response.getReturnValue();             
                    
                    for ( var key in response.getReturnValue().shipToAddressMap ) {
                        if (poDetailWrp.shipToAddress === null || poDetailWrp.shipToAddress === '' || poDetailWrp.shipToAddress === undefined) {
                            poDetailWrp.shipToAddress = response.getReturnValue().shipToAddressMap[key];
                        }
                        shipToAddList.push({value:response.getReturnValue().shipToAddressMap[key], key:key});
                    }
                    component.set("v.shipToAddressList",shipToAddList);
                    
                    for ( var key in response.getReturnValue().divisionMap ) {
                        if (poDetailWrp.division === null || poDetailWrp.division === '' || poDetailWrp.division === undefined) {
                            poDetailWrp.division = response.getReturnValue().divisionMap[key];
                        }
                        divList.push({value:response.getReturnValue().divisionMap[key], key:key});
                    }
                    component.set("v.divisionList",divList);
                    
                     for ( var key in response.getReturnValue().reasonMap ) {
                         if (poDetailWrp.reason === null || poDetailWrp.reason === '' || poDetailWrp.reason === undefined) {
                            poDetailWrp.reason = response.getReturnValue().reasonMap[key];
                        }
                        reasonList.push({value:response.getReturnValue().reasonMap[key], key:key});
                    }
                    component.set("v.reasonList",reasonList);
                    
                    component.set("v.poDetail",poDetailWrp);
                    window.setTimeout(
                        $A.getCallback( function() {
                            component.find("CustName").set("v.value", poDetailWrp.customerName);
                        },300000));
                    
                }
                component.set("v.spinner", false);
            });
            $A.enqueueAction(action);
       /* }else{
            poDetailInfo.billToCode = '';
            poDetailInfo.poExpiryDate = '';
            poDetailInfo.supplierName = '';
            poDetailInfo.supplierCode = '';
            poDetailInfo.shipToAddress = null;
            poDetailInfo.division = null;
            poDetailInfo.status = 'Draft';
            component.set("v.poDetail",poDetailInfo);
            component.set("v.shipToAddressList",null);
            component.set("v.divisionList",null);
        }*/
    },
    
    
    closeQuickActionHelper : function(component,event) {
        $A.get("e.force:closeQuickAction").fire()
    },
    
    closeModelOrgHelper : function(component,event) {
        component.set("v.isModalOpen", false);
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "Order"
        });
        homeEvt.fire();},
    saveRecordHelper : function(component,event){
       /* var selectValid = true;
        var allValid = component.find('NewPo').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        
        component.find('selectNewPo').forEach(element => {
            if(element.get("v.value") === null || element.get("v.value") === '' || element.get("v.value") === undefined){
            selectValid = false;
        }
                                              
                                              });*/
        
       // if (allValid && selectValid) {
        component.set('v.poDetail.OrderId',component.get("v.recordId"));    
        var poDetailInfo = component.get("v.poDetail");
        console.log('JSON::::'+JSON.stringify(poDetailInfo ));
            var action = component.get("c.createNewPoRecord");
            
            action.setParams({ 
                poDetailString : JSON.stringify(poDetailInfo )
            });
            
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('state::'+state);
                console.log('response::'+JSON.stringify(response));
                if (state === "SUCCESS") {
                    var respo = response.getReturnValue();
                    if(respo.split("#")[0] != 'error'){
                        $A.get("e.force:closeQuickAction").fire();
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": respo.split("#")[1],
                            "slideDevName": "detail"
                        });
                        navEvt.fire();
                    }
                }else{
                    var errors = response.getError();
                    console.debug('errors::'+errors);
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error",
                                "message": "Error message: " + errors[0].message,
                                "type": "error"
                            });
                            toastEvent.fire();
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
         $A.enqueueAction(action);
       // }
    }
})