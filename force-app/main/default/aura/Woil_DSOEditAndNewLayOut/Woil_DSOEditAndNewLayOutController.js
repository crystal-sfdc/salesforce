({
    doInit : function(component, event, helper) {
        var rec = component.get("v.recordId");
        console.log(rec);
        var action = component.get('c.DsoDeactivate'); 
        action.setParams({
            "recordId1" : rec
        });
        action.setCallback(this, function(a){
console.log(a.getReturnValue());

            var state = a.getState(); 
            if(state == 'SUCCESS') {   
                console.log(a.getReturnValue());
                if(a.getReturnValue() == 'true'){
                    component.set("v.DisplayMsg"," DSO Deactivated successfully");
                }
                else{
                    component.set("v.DisplayMsg","Already Deactivate");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    CancleClick : function (component, event, helper) {
        $A.get("e.force:closeQuickAction").fire(); 
        window.location.reload() ;
        
    }
})