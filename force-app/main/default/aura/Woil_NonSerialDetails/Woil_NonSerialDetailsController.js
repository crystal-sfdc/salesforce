({
	 fetchItems : function(component, event, helper) {
		$A.util.addClass(component.find("addQuantity"), "slds-hide");
        
          var action = component.get("c.getReasonFieldValue");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push({key: key, value: result[key]});
                }
                component.set("v.reasons", fieldMap);
            }
        });
        $A.enqueueAction(action);
        component.set("v.spinner", false);
        
        component.set('v.mycolumns', [
            {label: 'Sales Invoice', fieldName: 'InvoiceNo', type: 'text'},
            {label: 'Product Code', fieldName: 'skuCode', type: 'text'},
            {label: 'Product Description', fieldName: 'productDesc', type: 'text'},
            {label: 'Quantity', fieldName: 'Quantity',  type: 'decimal', editable: "true", hideDefaultActions: true}
        ]);
        var action = component.get("c.getNonSerializedItems");
        action.setParams({"invoice_no":component.get("v.recordId"),
                          "str_prod":null
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            var rows=null;
            rows=response.getReturnValue();
            var selectedValues=[];
            component.set('v.selectedValues',[]); 
            component.set("v.invList", []);
            console.log('rows::::'+rows);
            if (state === "SUCCESS") {
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];                  
                    selectedValues.push(row.skuCode);
                }
                // setting formatted data to the datatable
                // component.set("v.data", rows);
                console.log('selectedValues::::'+JSON.stringify(selectedValues));
                component.set('v.selectedValues',selectedValues);
                component.set("v.invList", rows);
                component.set("v.filteredList", rows);
                 
             
         
                if(rows.length===0 || rows===null ){
                    component.set("v.Noresults",true);
                   
                }else{
                    component.set("v.Noresults",false);
                }
                var SKUList=[];
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if(SKUList!=null)
                        var index=SKUList.indexOf(row.skuCode);                   
                    if(index===-1){
                        SKUList.push(row.skuCode);
                    }
                }                                         
                component.set("v.SKUList", SKUList);                                
            }
        });
        $A.enqueueAction(action);
         
    },
        SearchInvItems : function(component, event, helper) {
            console.log('inside SearchInvItems::::'); 
        
        component.set("v.invList",component.get("v.filteredList"));
        var allRecords = component.get('v.invList');
        for(let j=0; j < allRecords.length; j++){
            console.log('inside lklklkll::::');            
        }
        
        var srnolist =[];
        var tempArray = [];
        var SKUCode="";
       
            SKUCode= component.get("v.SKU")  ;
        
        console.log('::allRecords::'+allRecords+'::SKUCode::'+SKUCode);
        for(let i=0; i < allRecords.length; i++){
           
            if((SKUCode!="" && allRecords[i].skuCode===SKUCode ) ){
                tempArray.push(allRecords[i]);
            }else if((SKUCode==='---Select---') ){	
                tempArray.push(allRecords[i]);
            }
        }
        
        console.log('tempArray::::'+tempArray+'::FILTER LIST::'+component.get("v.filteredList"));
        var selectedValues=[];
        component.set('v.selectedValues',[]); 
        console.log('SKUCode::::'+SKUCode);
        if(SKUCode==='---Select---'){
            console.log('inside SKUCode::::');
            //var invLst=component.get("v.filteredList");          
            for(let j=0; j < tempArray.length; j++){
                selectedValues.push(tempArray[j].skuCode) ;
                
            }
            
            component.set('v.selectedValues',selectedValues);   
            console.log('tempArray11::::'+selectedValues);
        }
        
        else{
            console.log('inside tempArray::::'+tempArray);
            //component.set('v.invList',tempArray);
            
            for(let j=0; j < tempArray.length; j++){
                console.log('inside tempsafadArray::::');
                selectedValues.push(tempArray[j].skuCode);                
            }          
           
            component.set('v.selectedValues',selectedValues);
        }
        if(selectedValues.length===0 || selectedValues===null ){
                    component.set("v.Noresults",true);
                }else{
                    component.set("v.Noresults",false);
                }
        
    },
      changeSelect: function (cmp, event, helper) {
        //Press button to change the selected option
        console.log('selected::'+cmp.find("select").get("v.value")+':::'+cmp.get("v.SKU"));
        cmp.find("srno").set("v.value","");
    },
     handleSelect: function(component, event, helper) 
    {
        var selectedRows = event.getParam('selectedRows');
        console.log('selectedRows:::'+JSON.stringify(selectedRows));
        var selectedinv=  component.get("v.invSelected");
        component.set("v.RowsSelected",selectedRows);
        var filteredList=component.get("v.filteredList");
        var invListAll=component.get("v.invSelected");
        var invList=component.get("v.invList");
        //component.set("selectAll",false);
        component.find("selectAll").set("v.value", false); 
        for(let row=0;row<=invList.length;row++)
        {
            for(let Srow=0;Srow<=selectedRows.length;Srow++){
                const index = invList.indexOf(selectedRows[Srow]);
                const index1 = filteredList.indexOf(selectedRows[Srow]);
                if (index > -1) {
                    //invList.splice(index, 1);
                    invListAll.push(selectedRows[Srow]);
                }  
                if(index1 >-1){
                    filteredList.splice(index1,1);
                }
            }           
        }
        //var RowsSelected=component.get("v.RowsSelected");
        //RowsSelected.push(selectedRows);
        
        
        //component.set("v.invList",invList);
        component.set("v.filteredList",filteredList);
        component.set("v.invSelected",invListAll);
        
        
    },
     addLineItem:function(component, event, helper) {
        /* console.log("RowsSelected::"+JSON.stringify(component.get("v.RowsSelected")));
        component.set("v.AcceptList",component.get("v.RowsSelected"));
        console.log("accpeted LI::"+JSON.stringify(component.get("v.AcceptList")));
        var totalItems=component.get("v.invList");
        var totalItems1=totalItems;
        var AcceptItems=component.get("v.AcceptList");
        for(let row=0;row<=totalItems.length;row++)
        {
            var index=AcceptItems.indexOf(totalItems[row]);
            if (index > -1) {
                totalItems1.splice(row,1);
            }
        }*/
        /* var invSelected=component.get("v.invSelected");
        component.set("v.AcceptList",invSelected);
        component.set("v.invSelected",null);       
        console.log("accpeted LI11::"+JSON.stringify(component.get("v.AcceptList")));*/
        console.log('jcvdbvjj::'+event.getSource().getLocalId());         
        var getAllId = component.find("boxPack"); 
        var totalItems=component.get("v.invList");
        var acceptLst=[];
        var rejectLst=[];
        console.log('jcvdbvjjcvda::'+getAllId+'::totalItems::'+totalItems);   
        for (var k = 0; k < totalItems.length; k++) {
            console.log('11122::'+getAllId.length);
            if(getAllId.length!=undefined){
                for (var i = 0; i < getAllId.length; i++) {
                    console.log('11133::');
                    if (getAllId[i].get("v.value") === true) {
                        var selectedId =getAllId[i].get("v.text");
                        console.log('selectedId::'+selectedId+'::tot id::'+totalItems[k].skuCode);
                        if(selectedId===totalItems[k].skuCode){
                            if(event.getSource().getLocalId()==='addLineItem')
                                acceptLst.push(totalItems[k]);
                            else
                                rejectLst.push(totalItems[k]);
                        }
                    }               
                }
            }else{
                if(event.getSource().getLocalId()==='addLineItem' && getAllId.get("v.value")===true)
                    acceptLst.push(totalItems[k]);
                else if(event.getSource().getLocalId()==='reject' && getAllId.get("v.value")===true)
                    rejectLst.push(totalItems[k]);
            }
        }
        console.log('acceptLst::::'+acceptLst);
        if(acceptLst.length!=0 || rejectLst.length!=0){
            for(var i=0;i<acceptLst.length;i++){
                acceptLst[i].Id=i;
            }
           component.set("v.SelectedList",acceptLst);
            
            $A.util.removeClass(component.find("addQuantity"), "slds-hide"); 
            $A.util.addClass(component.find("ProductListing"), "slds-hide");
            
            
        }else{
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Info',
                message: 'Please Select Spare Parts to take action',
                duration:' 5000',
                key: 'info_alt',
                type: 'info',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
        
    },
    
    accept : function(component, event, helper) { 
        var updatedRecords = component.find("saveQuantity").get("v.draftValues");
        var recordInfo= component.get("v.SelectedList");
        var recordInfo= component.get("v.SelectedList");        
        var isValid=true,isValid1=true;
        var SelectedList1=component.get("v.SelectedList1");
        for (var i=0; i < updatedRecords.length; i++) {
            var str= updatedRecords[i].Id;   
            
            var rowIndex=str.substring(4);            
            if(recordInfo[rowIndex].Quantity>=updatedRecords[i].Quantity  && updatedRecords[i].Quantity!=0){
                recordInfo[rowIndex].Quantity=updatedRecords[i].Quantity;
                isValid=true; 
            }else{                
                isValid=false;     
            }           
        }
        console.log('isValid:::'+isValid+'::updatedRecords::'+JSON.stringify(updatedRecords)+'::SelectedList1::'+JSON.stringify(SelectedList1));
        for(var j=0; j < updatedRecords.length; j++){
            for(var k=0;k<SelectedList1.length; k++){
                var str= updatedRecords[j].Id;               
                var rowIndex=str.substring(4); 
                if(rowIndex==SelectedList1[k].Id)  {
                    console.log('SelectedList1[k].Quantity:::'+SelectedList1[k].Quantity+'::updatedRecords[j].Quantity::'+updatedRecords[rowIndex].Quantity);
                    if(SelectedList1[k].Quantity>=updatedRecords[rowIndex].Quantity && updatedRecords[rowIndex].Quantity!=0){
                        SelectedList1[k].Quantity=updatedRecords[rowIndex].Quantity;    
                        //isValid1=true; 
                    }else{
                        //isValid1=false;   
                    }                                               
                }              
            }
        }
        console.log('isValid11:::'+isValid1);
        
        if(isValid===true){
        var recordInfoListString=JSON.stringify(SelectedList1); 
        component.set("v.SelectedProdwrapper" ,recordInfoListString);
        var action = component.get("c.createGRNforNonSerial");
        action.setParams({
            "selectedRows": recordInfoListString
            
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var output= response.getReturnValue();
            if (state === "SUCCESS") {
                console.log('inside success of::::');
                var toastEvent = $A.get("e.force:showToast");
               /*if (output.startsWith("Error")) {
                    toastEvent.setParams({
                        "title": "Error",
                        "message": output,
                        "type": "error"
                    });
                    toastEvent.fire();
                }*/
				var action1 = component.get('c.fetchupdatedProd');     
                $A.enqueueAction(action1);
            }
        });

        $A.enqueueAction(action);
        }else{
              var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Info',
                message: 'Please enter valid quantity!',
                duration:' 5000',
                key: 'info_alt',
                type: 'info',
                mode: 'dismissible'
            });
            toastEvent.fire();
            //alert('Please enter valid quantity!');
        }
    },
    reject : function(component, event, helper) { 
          var updatedRecords = component.find("saveQuantity").get("v.draftValues");
        var recordInfo= component.get("v.SelectedList");
        var recordInfo= component.get("v.SelectedList");        
        var isValid=true;
        var SelectedList1=component.get("v.SelectedList1");
         var reason= component.get("v.reject_reason");
        console.log('reason::::'+reason);
        for (var i=0; i < updatedRecords.length; i++) {
            var str= updatedRecords[i].Id;   
            
            var rowIndex=str.substring(4);            
            if(recordInfo[rowIndex].Quantity>=updatedRecords[i].Quantity && updatedRecords[rowIndex].Quantity!=0){
                recordInfo[rowIndex].Quantity=updatedRecords[i].Quantity;
                isValid=true; 
            }else{                
                isValid=false;     
            }           
        }
        console.log('isValid:::'+isValid);
        
        for(var j=0; j < updatedRecords.length; j++){
            for(var k=0;k<SelectedList1.length; k++){
                if(updatedRecords[j].Id==SelectedList1[k].Id)  {
                    if(SelectedList1[k].Quantity>=updatedRecords[j].Quantity ){
                        SelectedList1[k].Quantity=updatedRecords[j].Quantity;    
                        //isValid=true; 
                    }else{
                        //isValid=false;   
                    }                                               
                }              
            }
        }
        if(isValid===true){
        console.log('reason::::'+reason);
        if(reason!="---Select---"){
        var updatedRecords = component.find("saveQuantity").get("v.draftValues");
        var recordInfo= component.get("v.SelectedList");
        console.log('draft:::'+JSON.stringify(updatedRecords)+'::selectedLSt::'+ JSON.stringify(component.get("v.SelectedList")));
        var recordInfo= component.get("v.SelectedList");
        console.log('draft:::'+JSON.stringify(updatedRecords));
        var SelectedList1=component.get("v.SelectedList1");
        for (var i=0; i < updatedRecords.length; i++) {
            var str= updatedRecords[i].Id;   
            console.log('SKU1111:::'+str);
            var rowIndex=str.substring(4);            
            recordInfo[rowIndex].Quantity=updatedRecords[i].Quantity;
        }
        for(var j=0; j < updatedRecords.length; j++){
            for(var k=0;k<SelectedList1.length; k++){
                var str= updatedRecords[j].Id;   
           		 var rowIndex=str.substring(4);
                if(rowIndex==SelectedList1[k].Id)  {                  
                  SelectedList1[k].Quantity=updatedRecords[j].Quantity;
                  SelectedList1[k].Reject_Reason=reason;
                }              
            }
        }
        var recordInfoListString=JSON.stringify(SelectedList1); 
        console.log('recordInfoListString1111::::'+recordInfoListString);
        component.set("v.SelectedProdwrapper" ,recordInfoListString);
        var action = component.get("c.createSRNforSpareParts");
        action.setParams({
            "str_prod": recordInfoListString
            
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var output= response.getReturnValue();
            if (state === "SUCCESS") {
                console.log('inside success of::::');
                var toastEvent = $A.get("e.force:showToast");
               /*if (output.startsWith("Error")) {
                    toastEvent.setParams({
                        "title": "Error",
                        "message": output,
                        "type": "error"
                    });
                    toastEvent.fire();
                }*/
				var action1 = component.get('c.fetchupdatedProd');     
                $A.enqueueAction(action1);
            }
        });

        $A.enqueueAction(action);
        }else{
             alert("Please Select Reject Reason");
        }
        }else{
              var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Info',
                message: 'Please enter valid quantity!',
                duration:' 5000',
                key: 'info_alt',
                type: 'info',
                mode: 'dismissible'
            });
            toastEvent.fire();
         }
    },
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        
        cmp.set("v.SelectedList1",selectedRows); 
        console.log('selectedRows::::'+JSON.stringify(cmp.get("v.SelectedList1")));
    },
    fetchupdatedProd:function(component, event, helper) { 
        
        console.log('inside fetchupdatedProd::::');
        var action1 = component.get("c.getNonSerializedItems");
        action1.setParams({"invoice_no":component.get("v.recordId"),
                           "str_prod":JSON.stringify(component.get("v.SelectedList"))
                          });
        action1.setCallback(this, function(response){
            var state = response.getState();
            var rows=response.getReturnValue();
            var draft=[];
            console.log('::rowssdd::::'+rows.length);
            if (state === "SUCCESS") {   
                for(var i=0;i<rows.length;i++){
                    rows[i].Id=i;
                }
                console.log('::rowssdd11::::'+JSON.stringify(rows)+'::draft::'+JSON.stringify(component.find("saveQuantity").get("v.draftValues")));
                component.find("saveQuantity").set("v.draftValues",draft);
                console.log('::rowssdd22::::'+rows+'::draft22::'+component.find("saveQuantity").get("v.draftValues"))               
                var selectedLst=component.get("v.SelectedList");
                for(var i=0;i<rows.length;i++){
                    if(rows[i].Quantity!=0){
                        selectedLst[i].Quantity=rows[i].Quantity;   
                    }                   
                    else{
                        selectedLst.splice(i,1); 
                        rows.splice(i,1); 
                    }
                }
                if(rows.length==0){
                    selectedLst=[]; 
                    rows=[]; 
                    let btn_accept = component.find('accept');
                    btn_accept.set('v.disabled',true);
                    let btn_reject = component.find('reject');
                    btn_reject.set('v.disabled',true);
                }
                console.log('::rowssss::::'+rows+'::selectedLst::'+selectedLst) 
                component.set("v.SelectedList",selectedLst);
                
            }
        });
        $A.enqueueAction(action1);
        
    },
    closeModel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire()
    },
    backtoProduct:function(component, event, helper) {
        $A.util.addClass(component.find("addQuantity"), "slds-hide"); 
        $A.util.removeClass(component.find("ProductListing"), "slds-hide");  
        var action1 = component.get('c.fetchItems');     
        $A.enqueueAction(action1);
    },
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    },
     handleSelectAllContact:function(component, event){
        
        var selectedHeaderCheck = component.find("selectAll").get("v.value");
        var getAllId = component.find("boxPack"); 
        console.log('getAllId:::'+getAllId);
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("boxPack").set("v.value", true);    
            }else{
                component.find("boxPack").set("v.value", false);
            }
        }else{
            // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
            // and set the all selected checkbox length in selectedCount attribute.
            // if value is false then make all checkboxes false in else part with play for loop 
            // and select count as 0
            
            var selectedvalues=component.get("v.selectedValues");
           console.log('selectedHeaderCheck:::'+selectedHeaderCheck+'::getAllId::'+getAllId+'::selectedvalues::'+selectedvalues);

            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", true);    
                    }
                    
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    for(var j=0;j<selectedvalues.length;j++){
                        if(getAllId[i].get("v.text")===selectedvalues[j])
                        component.find("boxPack")[i].set("v.value", false);   
                    }
                    
                }
            } 
        }  
    },
})