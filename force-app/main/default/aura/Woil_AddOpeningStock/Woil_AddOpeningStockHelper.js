({
    getIfPosted: function(component,event) {
        $A.util.addClass(component.find("addStockLineItem"), "slds-hide");
        $A.util.addClass(component.find("errorUploadItem"), "slds-hide");
        $A.util.addClass(component.find("postButton"), "slds-hide");
        var action = component.get("c.getOpeningStockIsPosted");
        action.setParams({
            "opnStockId": component.get("v.recordId"),         
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result) {
                    /*	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "You cannot add products to a posted Stock",
                "type": "error"
            });
            toastEvent.fire();*/
                    
                    
                }
                else {
                    component.set("v.allowAdd",true);   
                }
            }
        });
        
        $A.enqueueAction(action); 
    },
    
    
    getDropDownValues : function(component,event) {
        console.log('Inside Init');
        $A.util.addClass(component.find("addStockLineItem"), "slds-hide");
        var action = component.get("c.getDropDownValuesinInit");
        action.setParams({
            "opStockId": component.get("v.recordId"),         
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var reponseList = JSON.parse(response.getReturnValue());
                component.set("v.warehouseData",reponseList.wrapWareHouseList);
                component.set("v.productCode",reponseList.getProducts);
                component.set("v.productDetail",reponseList.getProductsMap);
                
                var isPostedOpStock= reponseList.isPosted;
                
                var getStockLineItems= reponseList.returnLineItems;
                
                var urlString = window.location.href;
                var baseURL = urlString.substring(0, urlString.indexOf("/s"));
                var url = baseURL+'/servlet/servlet.FileDownload?file='+reponseList.documentLink;
                component.set("v.docLink",url );
                
                console.log('Value'+reponseList.isSerialized);
                var checkSerialized= reponseList.isSerialized;
                if(isPostedOpStock == false) {
                    console.log('isPostedOpStock is False');
                if(checkSerialized == true) {
                    component.set("v.isSerializedTemp",checkSerialized);
                    if(getStockLineItems.length>0) {
                        component.set("v.postDisable",false);
                    component.set("v.showSelection",getStockLineItems);
                     component.set('v.columns', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'},
                        ]);
                         }
                    console.log(component.get("v.isSerializedTemp"));
                }
                else {
                         console.log('isPostedOpStock is False in Else');
                    component.set("v.isSerializedTemp",'false');
                         if(getStockLineItems.length>0) {
                         component.set("v.postDisable",false);
                         component.set("v.showSelection",getStockLineItems);
                         component.set('v.columns', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Quantity', fieldName: 'Quantity', type: 'Decimal'},
                        ]);      
                         }
                }
            }
                else {
                    console.log('isPostedOpStock is True');
                     if(checkSerialized == true) {
                   component.set("v.allowAdd",false);
                    if(getStockLineItems.length>0) {
                    component.set("v.showListTable",getStockLineItems);
                     component.set('v.columns2', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'},
                        ]);
                         }
                    console.log(component.get("v.isSerializedTemp"));
                }
                else {
                         component.set("v.allowAdd",false);
                         if(getStockLineItems.length>0) {
                         console.log('getStockLineItems',getStockLineItems);
                         component.set("v.showListTable",getStockLineItems);
                         component.set('v.columns2', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Quantity', fieldName: 'Quantity', type: 'Decimal'},
                        ]);      
                         }
                     }
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    createLineItems: function(component,event) {
        console.log('Inside Helper of Add');
        console.log('Inside Helper of Add123');
       
        var isSerialized= component.get("v.isSerializedTemp");
        console.log('isSerialized'+isSerialized);
        var getList= component.get("v.showSelection");
        console.log('getList'+getList);
        
      
        
        
        if(isSerialized == true) {
        var wareHouse= component.find("wareHouseIdSerial");
        console.log('after wareHouse1');
        console.log(wareHouse);
        var wareHouseValue= wareHouse.get("v.value");
        console.log('wareHouseValue'+wareHouseValue)
        var productCode= component.find("productCodeIdSerial");
        var productCodeValue= productCode.get("v.value");
        console.log('productCodeValue'+productCodeValue);
            var serialNumber= component.find("serialNoId");
            var serialNumberValue= serialNumber.get("v.value");
            
            component.set("v.serNoVal","");
            console.log('Just Before NUll Check');  
            if(serialNumberValue.length>16) {
                var toastEvent = $A.get("e.force:showToast");
                   	toastEvent.setParams({
                        "message": "You can only enter upto 16 characters in Serial Number",
                        "type": "error"
                    });
                    toastEvent.fire();   
            }
          
      else  if(!serialNumberValue || serialNumberValue === undefined || serialNumberValue === "" || wareHouseValue === ""||wareHouseValue === undefined ||productCodeValue === undefined || productCodeValue === "" ) {
         console.log('Inside NUll Check');
               var toastEvent = $A.get("e.force:showToast");
                   	toastEvent.setParams({
                        "message": "Please enter all the required values",
                        "type": "error"
                    });
                    toastEvent.fire();   
            }
            
            else { 
            var action = component.get("c.saveOpeningStockLineItem");
            action.setParams({
                "opStockId": component.get("v.recordId"),
                "wareHouseCd": wareHouseValue,
                "productCd": productCodeValue,
                "serialNo": serialNumberValue,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    if(result.length>0) {
                        console.log('RecordStatus'+result[0].recordStatus);
                        if(result[0].checkSerialNo == true) {
                               var toastEvent = $A.get("e.force:showToast");
                   		 toastEvent.setParams({
                        "message": "Serial Number can only accept Alphanumeric values and can only accept 16 characters",
                        "type": "error"
                    });
                    toastEvent.fire(); 
                        }
                        else {
                        var showStatus= result[0].recordStatus;
                        if(showStatus == false) {
                           var toastEvent = $A.get("e.force:showToast");
                   		 toastEvent.setParams({
                        "message": "Opening Stock Line Item with the same Serial Number already exists.",
                        "type": "error"
                    });
                    toastEvent.fire(); 
                        }
                        else {
                        component.set("v.showSelection",result);
                        component.set("v.showListTable", result);
                        component.set("v.postDisable",false);
                        $A.util.removeClass(component.find("addStockLineItem"), "slds-hide");
                        component.set('v.columns', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'},
                        ]);
                            }
                            }
                            }
                            }
                            }); 
                            }
                            }
                            else {
                             var wareHouse= component.find("wareHouseIdNonSerial");
        console.log('after wareHouse1');
        console.log(wareHouse);
        var wareHouseValue= wareHouse.get("v.value");
        console.log('wareHouseValue'+wareHouseValue)
        var productCode= component.find("productCodeIdNonSerial");
        var productCodeValue= productCode.get("v.value");
        console.log('productCodeValue'+productCodeValue);
                            console.log('Inside Else');
                            var quantityId= component.find("quantityId");
                            var quanitityVal= quantityId.get("v.value");
                            component.set("v.quaVal","");
                            console.log(quanitityVal);
                        // if(serialNumberValue === '' || wareHouseValue === ''|| quanitityVal === '' ) {
                            if($A.util.isEmpty(wareHouseValue) || $A.util.isEmpty(productCodeValue) || $A.util.isEmpty(quanitityVal)){
                            console.log("hello");
                            console.log($A.util.isEmpty(wareHouseValue));
							console.log($A.util.isEmpty(productCodeValue));
                            console.log($A.util.isEmpty(quanitityVal));

                            var toastEvent = $A.get("e.force:showToast");
                   					toastEvent.setParams({
                       					 "message": "Please enter all the required values",
                       					 "type": "error"
                    				});
                    toastEvent.fire();   
            }
                            else {        
                            
                            var action = component.get("c.saveOpeningStockLineItem");
                            action.setParams({
                            "opStockId": component.get("v.recordId"),
                            "wareHouseCd": wareHouseValue,
                            "productCd": productCodeValue,
                            "quantityValue": quanitityVal,
                            "serialNo": ""         
                            });
                            action.setCallback(this, function(response) {
                            var state = response.getState();
                            if (state === "SUCCESS") {
                            var result = response.getReturnValue();
                            var showStatus= result[0].recordStatus;
                        if(showStatus == false) {
                           var toastEvent = $A.get("e.force:showToast");
                   		 toastEvent.setParams({
                        "message": "Opening Stock Line Item with the same Product already exists.",
                        "type": "error"
                    });
                    toastEvent.fire(); 
                        }
                            if(result.length>0) {
                            component.set("v.postDisable",false);    
                            component.set("v.showSelection",result);
                            component.set("v.showListTable", result);
                            $A.util.removeClass(component.find("addStockLineItem"), "slds-hide");
                            component.set('v.columns', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Quantity', fieldName: 'Quantity', type: 'Decimal'},
                        ]);        
                    }
                }
            });
        }
       }
        $A.enqueueAction(action);
    },
    
    //Ibrahim
    CSV2JSON: function (component,csv) {
        var arr = []; 
        arr =  csv.split('\n');
        arr.pop();
        var jsonObj = [];
        var headers = arr[0].split(',');
        for(var i = 1; i < arr.length; i++) {
            var data = arr[i].split(',');
            var obj = {};
            for(var j = 0; j < data.length; j++) {
                obj[headers[j].trim()] = data[j].trim();
            }
            jsonObj.push(obj);
        }
        var json = JSON.stringify(jsonObj);
        console.log(json);
        return json;
    },
    
    CreateOpningStockLineItem : function (component,jsonstr){
        
        
        console.log(jsonstr);
        var recId = component.get("v.recordId");
        console.log(recId);
         var isSerialized= component.get("v.isSerializedTemp");
        
        console.log('isSerialized'+isSerialized);
        
        if(isSerialized == true) {
        var action = component.get("c.insertData");
        action.setParams({
            "strfromlex" : jsonstr,
            "OPenStockId":recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var result = response.getReturnValue();
                if(result.length>0) {
                 
                 var showSuccessList= [];
                 var showErrorList= [];
                    
                    for(var i=0;i<result.length;i++) {
                        console.log('recordStatus'+result[i].recordStatus);
                        if(result[i].recordStatus == true) {
                         component.set("v.postDisable",false);  
                       showSuccessList.push(result[i]);
                     
                        }
                        else if(result[i].recordStatus == false){
                        console.log('Inside False');
                        showErrorList.push(result[i]);
                        console.log('After Push');
                        }  
                        } 
                        console.log('ErrorList',showErrorList);
                        if(showErrorList.length>0) {
                            $A.util.removeClass(component.find("errorUploadItem"), "slds-hide");
                            console.log('Inside Error Length');
                            component.set("v.errorList",showErrorList);
                           component.set("v.showErrorItem",true);  
                          component.set('v.columns1', [
                        {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                        {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                        {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'},
                        {label: 'Error Reason', fieldName: 'errorReason', type: 'text'},
                    ]);
                              
                        }
                            if(showSuccessList.length>0) {
                               $A.util.removeClass(component.find("addStockLineItem"), "slds-hide");
                              component.set("v.showSelection",showSuccessList);
                               component.set("v.showListTable", showSuccessList);
                              component.set("v.showListItem",true);
                   			 component.set('v.columns', [
                                {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                                {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                                {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'},
                   	 ]);
                              
                              }     
                        }
                        }
                        else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                        if (errors[0] && errors[0].message) {
                        
                    }
                } else {
                }
            }
        }); 
        }
        else {
            console.log('Inside Else Non');
             var action = component.get("c.insertData");
        action.setParams({
            "strfromlex" : jsonstr,
            "OPenStockId":recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('Inside After return from');
            if (state === "SUCCESS") {  
                var result = response.getReturnValue();
                //console.log('Inside After return from');
                if(result.length>0) {
                    
                      var showSuccessList= [];
                 	  var showErrorList= [];
                    
                    for(var i=0;i<result.length;i++) {
                        console.log('recordStatus'+result[i].recordStatus);
                        if(result[i].recordStatus == true) {
                        component.set("v.postDisable",false);   
                       showSuccessList.push(result[i]);
                     
                        }
                        else if(result[i].recordStatus == false){
                        console.log('Inside False');
                        showErrorList.push(result[i]);
                        console.log('After Push');
                        }  
                        } 
                        console.log('ErrorList',showErrorList);
                        if(showErrorList.length>0) {
                            $A.util.removeClass(component.find("errorUploadItem"), "slds-hide");
                            console.log('Inside Error Length');
                            component.set("v.errorList",showErrorList);
                           component.set("v.showErrorItem",true);  
                          component.set('v.columns1', [
                        {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                        {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                        {label: 'Quantity', fieldName: 'Quantity', type: 'Decimal'},
                        {label: 'Error Reason', fieldName: 'errorReason', type: 'text'},
                    ]);
                              
                        }
                            if(showSuccessList.length>0) {
                              console.log('Inside Quantity Success');
                               $A.util.removeClass(component.find("addStockLineItem"), "slds-hide");
                              component.set("v.showSelection",showSuccessList);
                              component.set("v.showListTable", showSuccessList);
                              component.set("v.showListItem",true);
                   			 component.set('v.columns', [
                                {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                                {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                                {label: 'Quantity', fieldName: 'Quantity', type: 'Decimal'},
                   	 ]);
                              
                              }     
                        }
                     }
                     else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                        if (errors[0] && errors[0].message) {
                        
                    }
                } else {
                }
            }
        }); 
     
        }
        
        $A.enqueueAction(action);    
        
    },
    
    postLineItemshHelper: function(component,event) { 
     let button = event.getSource();
    button.set('v.disabled',true);
        console.log('Inside Post');   
        var postItem= component.get("v.showSelection");
        console.log('Post Item'+postItem);
        var postItemString= JSON.stringify(postItem);
        
        console.log('After Stringify'); 
        
        var action = component.get("c.postLineItem");
        action.setParams({
            "getLineItemString":postItemString,
            "opnStockId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result== "SUCCESS"){
                    component.set("v.allowAdd",false);
                   /* if(result.length>0) {
                        if(result[0].isSerializedFlag == true) {
                        component.set("v.showListTable",result);
                        component.set('v.columns2', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'},
                        ]);

                        }
                            else {
                            component.set("v.showListTable",result);
                        component.set('v.columns2', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Quantity', fieldName: 'Quantity', type: 'text'},
                        ]);
                            
                            
                            }
                    }*/
                    
                    var checkSerialVal= component.get("v.isSerializedTemp");
                        if(checkSerialVal == true) {
                       // component.set("v.showListTable",result);
                        component.set('v.columns2', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'},
                        ]);
                            
                            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Opening Stock Line Items posted successfully",
                        "type": "success"
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();

                        }
                            else {
                          //  component.set("v.showListTable",result);
                        component.set('v.columns2', [
                            {label: 'WareHouse Location', fieldName: 'Warehouse', type: 'text'},
                            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
                            {label: 'Quantity', fieldName: 'Quantity', type: 'text'},
                        ]);
                  }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Opening Stock Line Items posted successfully",
                        "type": "success"
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    
})