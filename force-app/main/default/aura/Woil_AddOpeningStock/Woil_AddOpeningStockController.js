({
	 doInit : function(component, event, helper) {
        helper.getIfPosted(component,event);
        helper.getDropDownValues(component, event);
     },
    
    saveLineItems : function(component, event, helper) {
        console.log('Inside SaveLineItems');
        helper.createLineItems(component, event);
     },
        //Ibrahim

    CreateRecordCsv : function (component, event, helper) {
          var fileInput = event.getSource().get("v.files");
console.log(fileInput[0]);
     //  var fileInput = component.find("file1").getElement();
     var file = fileInput[0];
     //   var file = fileInput.files[0];
        console.log(file);
     if (file) {
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = function (evt) {
            var csv = evt.target.result;
            var result = helper.CSV2JSON(component,csv);
            helper.CreateOpningStockLineItem(component,result);
            
        }
        reader.onerror = function (evt) {
        }
    }

    },
    
    postLineItems : function(component, event, helper) { 
        helper.postLineItemshHelper(component, event);
    
   },
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        
       cmp.set("v.deleteOpenLineItem", selectedRows);
        // console.log(selectedRows);
    },
    DeleteLineItem : function (cmp, event) {
   // console.log(cmp.get("v.deleteOpenLineItem"));
        var DeletedItemCollection = cmp.get("v.deleteOpenLineItem");
        console.log(DeletedItemCollection.length);
                var LineItem = [];

        if(DeletedItemCollection.length == 0){
                  var toastEvent = $A.get("e.force:showToast");
                   					toastEvent.setParams({
                       					 "message": "Please select atleast one Opening Stock LineItem",
                       					 "type": "error"
                    				});
                    toastEvent.fire();   

        }
        else{
        for(var i=0;i<DeletedItemCollection.length;i++){
            LineItem.push(DeletedItemCollection[i].ItemId);
        }
        console.log(LineItem);
        var action = cmp.get('c.DeleteOpenStokItem'); 
          action.setParams({
            "DeleteLineItem":LineItem,         
        });
      action.setCallback(this, function(a){
            var state = a.getState(); 
            if(state == 'SUCCESS') {
              var toastEvent = $A.get("e.force:showToast");
                   					toastEvent.setParams({
                       					 "message": "selected  Opening Stock LineItem successfully deleted",
                       					 "type": "success"
                    				});
                    toastEvent.fire();        
		window.location.reload();   
            }
        });
        $A.enqueueAction(action);
    }
     }   
        

   
})