({
	doInit : function(component, event, helper) {
		var action = component.get("c.getProducts");
        action.setParams({
            "recordID": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
        	var state = response.getState();
           if(state === 'SUCCESS') {
            	var output = response.getReturnValue();
               console.log(response.getReturnValue());
                component.set("v.prodWrapper",output);
            }else {
                alert('Error in getting data');
            }
            
        });
        $A.enqueueAction(action);
	}
})