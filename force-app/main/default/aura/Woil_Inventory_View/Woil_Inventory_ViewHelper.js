({
    
    getPicklist: function(component, event) {
        console.log('inside Get Product Picklist');
        
        var categoryValue = component.get("v.selectedcategoryValue");
        var divisionValue = component.get("v.selectedDivisionValue");
        var action = component.get("c.getProductList");
        action.setParams({
            divisionStr: divisionValue,
            categoryStr : categoryValue}
                        );
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('test'+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                var productNameIdMap =[];
                for(var key in result){
                    productNameIdMap.push({key: key, value: result[key]});
                }
                
                
                component.set("v.options", productNameIdMap);
            }
        });
        $A.enqueueAction(action);
    },
    
    handleProductChangeHelper: function(component, event){
        var selectedValue = component.find('ProductID').get('v.value');
        console.log('Selected Value'+ selectedValue);
        var action = component.get("c.checkProductisSerialized");
        action.setParams({
            
            productId : selectedValue}
                        );
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('test'+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                if(result == false){
                    component.set("v.isNotSerial", true);    
                }
                else{
                    component.set("v.isNotSerial", false);    
                }
                
            }
        });
        $A.enqueueAction(action);
        
    },
    
    handleSelectHelper: function(component, event){
    	console.log('Inside handle select helper');
        var selectedRows = event.getParam('selectedRows'); 
        console.log('selectedRows--->',selectedRows);
        var setRows = [];
         for ( var i = 0; i < selectedRows.length; i++ ){
            setRows.push(selectedRows[i]);
		}
        component.set("v.selectedRowsVal", setRows);
        console.log('Selected value--->',component.get("v.selectedRowsVal"));
    },
    
    getStock: function(component, event){
        
        
        if(component.get("v.selectedcategoryValue")!= null && component.get("v.selectedcategoryValue")!= undefined && component.get("v.selectedcategoryValue")!= ''){
            var categoryValue = component.get("v.selectedcategoryValue");    
        }
        if(component.get("v.selectedDivisionValue")!= null && component.get("v.selectedDivisionValue")!= undefined && component.get("v.selectedDivisionValue")!= ''){
            var divisionValue = component.get("v.selectedDivisionValue");    
        }
        let selectedValue = component.find('ProductID').get('v.value');
        
        console.log('categoryvalue---->'+categoryValue);
        console.log('divisionValue---->'+divisionValue);
        console.log('selectedValue---->'+selectedValue);
        /*if(selectedValue ==="-1"){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select product",
                "type": "error"
            });
            toastEvent.fire();
        }*/
        
        var action = component.get("c.getSerialDetails");
        action.setParams({ 
            productId : selectedValue,
            categoryStr: categoryValue,
            divisionStr: divisionValue
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('test2'+state);
            if (state === "SUCCESS") {
                
                var result = response.getReturnValue();
                if(result.length > 0){
                    $A.util.removeClass(component.find("showButton"), "slds-hide"); 
                    $A.util.removeClass(component.find("showList"), "slds-hide");
                    console.log('GetStock result--->',result);
                    component.set("v.isTransectionType",false);
                    component.set("v.isSerial",true);
                    console.log('isSerial--->',component.get("v.isSerial"));
                    component.set("v.isSerialType",false);
                    component.set("v.gridData", result);
                    console.log('Griddata--->',component.get("v.gridData"));
                    
                }
                else{
                    $A.util.addClass(component.find("showList"), "slds-hide");
                    $A.util.addClass(component.find("showButton"), "slds-hide"); 
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "No product found",
                        "type": "error"
                    });
                    toastEvent.fire();
                }
                
                
            }
        });
        $A.enqueueAction(action);
        
    },
    
    getSerialOrder: function(component,event){
        
        if(component.get("v.selectedcategoryValue")!= null && component.get("v.selectedcategoryValue")!= undefined && component.get("v.selectedcategoryValue")!= ''){
            var categoryValue = component.get("v.selectedcategoryValue");    
        }
        if(component.get("v.selectedDivisionValue")!= null && component.get("v.selectedDivisionValue")!= undefined && component.get("v.selectedDivisionValue")!= ''){
            var divisionValue = component.get("v.selectedDivisionValue");    
        }
        
        var selectedValue = component.find('ProductID').get('v.value');
        var selectedRowValue = component.get("v.selectedRowsVal");
        console.log('Selected Value'+ selectedValue);
        console.log('divisionValue---->'+divisionValue);
        console.log('categoryvalue---->'+categoryValue);
        console.log('selectedRowValue---->'+selectedRowValue);
        
        //calling apex 
        var action = component.get("c.getSerialOrder");
        action.setParams({ 
            productId : selectedValue,
            categoryStr: categoryValue,
            divisionStr: divisionValue,
            selectedDataRow: selectedRowValue
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('test3'+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result of Serial order',result);
                if(result.length > 0){
                    $A.util.removeClass(component.find("showButton"), "slds-hide"); 
                    $A.util.removeClass(component.find("showSerialList"), "slds-hide");
                    component.set("v.isTransectionType",false);
                    component.set("v.isSerial",false);
                    component.set("v.isSerialType",true);
                    component.set("v.gridSerialData", result);
                }
                else{
                    $A.util.addClass(component.find("showSerialList"), "slds-hide");
                    $A.util.addClass(component.find("showButton"), "slds-hide");
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "No product found",
                        "type": "error"
                    });
                    toastEvent.fire();
                }
                
            }
        });
        $A.enqueueAction(action);
        
    },
    
    getTransactionDetails: function(component,event){
        if(component.get("v.selectedcategoryValue")!= null && component.get("v.selectedcategoryValue")!= undefined && component.get("v.selectedcategoryValue")!= ''){
            var categoryValue = component.get("v.selectedcategoryValue");    
        }
        if(component.get("v.selectedDivisionValue")!= null && component.get("v.selectedDivisionValue")!= undefined && component.get("v.selectedDivisionValue")!= ''){
            var divisionValue = component.get("v.selectedDivisionValue");    
        }
        let selectedValue = component.find('ProductID').get('v.value');
        console.log('Selected Value'+ selectedValue);
        
         if(selectedValue ===""){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select product",
                "type": "error"
            });
            toastEvent.fire();
        }
        //calling apex 
        var action = component.get("c.getTransactionDetails");
        action.setParams({ 
            productId : selectedValue,
            categoryStr: categoryValue,
            divisionStr: divisionValue
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('test4'+state);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.length > 0){
                    $A.util.removeClass(component.find("showButton"), "slds-hide"); 
                    $A.util.removeClass(component.find("showTransactionList"), "slds-hide");
                    console.log(result);
                    component.set("v.isTransectionType",true);
                    component.set("v.isSerial",false);
                    component.set("v.isSerialType",false);
                    component.set("v.gridColumnsforTransectionData", result);
                }else{
                    $A.util.addClass(component.find("showTransactionList"), "slds-hide");
                    $A.util.addClass(component.find("showButton"), "slds-hide"); 
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "No product found",
                        "type": "error"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
    
    
    createColumnForSerial: function(component, event){
        var columnsForSerial = [
            
            {
                type: 'text',
                fieldName: 'warehouseLocation',
                label: 'Warehouse Location'
                
            },
            {
                type:'text',
                fieldName: 'materialCodeDesc',
                label: 'Material Description'
            },
            {
                type: 'text',
                fieldName: 'distributorCode',
                label: 'Distributor Code'
            },
            {
                type: 'text',
                fieldName: 'serialNumber',
                label: 'Serial Number'
            },
            {
                type: 'text',
                fieldName: 'transitStatus',
                label: 'Transit Status'
            }
            
            
        ];
        component.set("v.gridColumnsforSerialNum" , columnsForSerial);
        
        
    },
    
    createColumnForTransectionType: function(component, event){
        
        var columnsForTransection = [
            
            {
                type: 'Date',
                fieldName: 'transactionDate',
                label: 'Physical Date'
                
            },
            {
                type: 'text',
                fieldName: 'transactionType',
                label: 'Reference'
            },
            {
                type:'text',
                fieldName: 'referenceId',
                label: 'Reference ID'
            },
            {
                type: 'text',
                fieldName: 'receipt',
                label: 'Receipt'
            },
            {
                type: 'text',
                fieldName: 'issue',
                label: 'Issue'
            },
            {
                type: 'number',
                fieldName: 'quantity',
                label: 'Quantity'
            }
            
            
        ];
        component.set("v.gridColumnsTransection" , columnsForTransection);
    },
    
    createColumn: function(component, event){
        var columns = [
            
            {
                type: 'text',
                fieldName: 'warehouseLocation',
                label: 'Warehouse Location',
                initialWidth: 300
            },
            {
                type: 'text',
                fieldName: 'materialCodeDesc',
                label: 'Material Description'
            },
            {
                type: 'number',
                fieldName: 'onHandStock',
                label: 'On-Hand Stock'
                
            },
            {
                type: 'number',
                fieldName:'onOrder',
                label: 'On Order'
            },
            {
                type: 'number',
                fieldName:'totalAvailable',
                label:'Total Available'
            }
        ];
        component.set("v.gridColumns" , columns);
        
        
    },
    
    next : function(component, event){
        var sObjectList = component.get("v.gridData");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var Paginationlist = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(sObjectList.length > i){
                Paginationlist.push(sObjectList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.PaginationList', Paginationlist);
    }
});