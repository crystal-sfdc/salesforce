({
    init: function(component, event, helper) { 
		$A.util.addClass(component.find("showButton"), "slds-hide");
        helper.createColumn(component, event);
        helper.createColumnForSerial(component, event);
        helper.createColumnForTransectionType(component, event);
    },
    getValueFromLwc : function(component, event, helper) {
        console.log('Value from LWC');
		component.set("v.selectedcategoryValue",event.getParam('value'));
        var categoryValue = component.get("v.selectedcategoryValue");
        console.log('catValue--->'+categoryValue);
        if(categoryValue!= undefined || categoryValue!= ""){
            console.log('inside check of categoryvalue');
            component.set("v.isNotSerial", false);
            helper.getPicklist(component, event);
            
        }
        
	},
    getValueFromDiv: function(component, event, helper) {
        console.log('Value from Div LWC');
        component.set("v.selectedDivisionValue",event.getParam('rectype'));
        var divisionValue = component.get("v.selectedDivisionValue");
        console.log('Divisionvalue LWC--->'+divisionValue);
        component.set("v.selectedValue","");
        component.set("v.isNotSerial", false);
    },
    handleProductChange: function(component, event, helper){
        helper.handleProductChangeHelper(component, event);
    },
    
    getStock: function(component, event, helper){
        
        helper.getStock(component, event);
    },
    handleSelect: function(component, event, helper){
       
        helper.handleSelectHelper(component, event);
    },
    handleDivisionChange: function(component, event, helper){
        helper.getCategoryList(component, event);
    },
    getSerialOrderDetails: function(component, event, helper){
        helper.getSerialOrder(component,event);
    },
    TransactionOrderDetails: function(component, event, helper){
        helper.getTransactionDetails(component,event);
    },
    next: function (component, event, helper) {
    helper.next(component, event);
},
     previous: function (component, event, helper) {
    helper.previous(component, event);
     }
    
});