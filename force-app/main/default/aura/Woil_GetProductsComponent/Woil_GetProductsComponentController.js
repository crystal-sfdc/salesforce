({
    doInit : function(component, event, helper) {
        $A.util.addClass(component.find("addQuantity"), "slds-hide");
        component.set('v.columns', [
            {label: 'Material Code', fieldName: 'prodName', type: 'text'},
            {label: 'Material Description', fieldName: 'prodDescription', type: 'text'},
            {label: 'Dealer Price', fieldName: 'prodDealerPrice', type: 'Decimal'},
            {label: 'Days of Inventory', fieldName: 'prodDaysofInventory', type: 'text'},
            {label: 'Product Category', fieldName: 'prodCategory', type: 'text'}
        ]);
        var action = component.get("c.getProducts");
        action.setParams({
            "orderID": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var output= response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.prodWrapper", output);
                component.set("v.filteredData", output);
                component.set("v.initialList", output);
            }
        });
        $A.enqueueAction(action);
    },
    
    updateSelectedText : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.RowsSelected",selectedRows);
        console.log('selectedRows:::',selectedRows);  
    },
    
    addQuantity : function(component, event, helper) {
        var diffType= 'False';
        var isValid= 'True';
        var selProdList=[];
        selProdList=component.get("v.RowsSelected");
        if(selProdList.length === 0){
            isValid= 'False';
            console.log('is Empty');
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select atleast one product",
                "type": "error"
            });
            toastEvent.fire();
        }
        else if (selProdList.length > 1) {
           for(let i=0; i<selProdList.length-1; i++){
                if((selProdList[i].prodCategory != selProdList[i+1].prodCategory) && (selProdList[i].prodCategory == 'AC' || selProdList[i+1].prodCategory == 'AC') ) {
                    isValid= 'False';
                    diffType= 'True';
                    break;
                }
            }
       }
       if(diffType=='True') {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "You cannot club AC with Non-AC Products. Please select Products of same category.",
                "type": "error"
            });
            toastEvent.fire();
        }
       if(isValid=='True'){
            $A.util.addClass(component.find("showProduct"), "slds-hide");
            $A.util.removeClass(component.find("addQuantity"), "slds-hide");    
            component.set('v.columns2', [
                {label: 'Material Code', fieldName: 'prodName', type: 'text'},
                {label: 'Material Description', fieldName: 'prodDescription', type: 'text'},
                {label: 'Dealer Price', fieldName: 'prodDealerPrice', type: 'Decimal'},
                {label: 'Quantity', fieldName: 'quantity', type: 'decimal', editable: "true", hideDefaultActions: true}
            ]);
        }
      },
    
    searchTable : function(component,event,helper) {
        var allRecords = component.get("v.prodWrapper");
        console.log("Search List",allRecords);
        var searchFilter = event.getSource().get("v.value");
        console.log('searchFilter'+searchFilter);
        
        if(searchFilter==='') {
            component.set("v.prodWrapper", component.get("v.initialList"));
            component.set("v.filteredData", component.get("v.initialList"));  
        }
        else { 
            var action = component.get("c.filteredProd");
            action.setParams({
                "orderID": component.get("v.recordId"),
                "filterValue": searchFilter
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                var output= response.getReturnValue();
                if (state === "SUCCESS") {
                    component.set("v.prodWrapper", output);
                    component.set("v.filteredData", output);
                }
                else if (state === "ERROR") { 
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        } 
    },
    
    backtoProduct : function(component, event, helper) {
        $A.util.removeClass(component.find("showProduct"), "slds-hide");
        $A.util.addClass(component.find("addQuantity"), "slds-hide");
    },
    
    addOrderLineItem : function(component, event, helper) { 
        var updatedRecords = component.find("saveQuantity").get("v.draftValues");
        var recordInfo= component.get("v.RowsSelected");
        
        for (var i=0; i < updatedRecords.length; i++) {
            var str= updatedRecords[i].Id;
            var rowIndex=str.substring(4);
            recordInfo[rowIndex].quantity=updatedRecords[i].quantity;
        }
        var recordInfoListString=JSON.stringify(recordInfo);
        
        var action = component.get("c.addOrderLineItemtoOrder");
        action.setParams({
            "selectedRows": recordInfoListString,
            "ordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var output= response.getReturnValue();
            if (state === "SUCCESS") {
                 var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success",
                "message": "Product selected has been added",
                "type": "success"
            });
            toastEvent.fire();
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            }
            else {
                var errors = response.getError();
                if (errors) {
                var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error",
                "message": "Error in saving the selected Product.Please contact your admin.",
                "type": "error"
            });
            toastEvent.fire();
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})