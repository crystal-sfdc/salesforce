({
    doInit: function(component, event, helper){
        helper.getPOInfo(component,event);
    },

    saveRecord: function(component, event, helper){
        helper.saveRecordHelper(component,event);
    },

    closeModel: function(component, event, helper) {
        helper.closeQuickActionHelper(component,event);
     },
     closeModelOrg: function(component, event, helper) {
        helper.closeModelOrgHelper(component,event);
     },
})