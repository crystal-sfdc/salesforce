({
    getPOInfo : function(component,event) {
        var parentId = component.get('v.recordId');
        var action = component.get("c.getPOInformation");
        action.setParams({
            recordId : parentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var reponseList = JSON.parse(response.getReturnValue());
                if(reponseList.isCommunity == true){
                    if(reponseList.orderList[0].Status === 'Rejected'){
                        component.set("v.disablePoExpiryDate", false);
                    }
                    component.set("v.orderData", reponseList.orderList[0]);
                    component.set("v.isCommunity", true); 
                    component.set("v.isOrg", false);
                    component.set("v.isModalOpen", false);  
                }else{
                    if(reponseList.orderList[0].Status === 'Rejected'){
                        component.set("v.disablePoExpiryDate", false);
                    }
                    component.set("v.orderData", reponseList.orderList[0]);
                    component.set("v.isCommunity", false); 
                    component.set("v.isOrg", true);
                    component.set("v.isModalOpen", true);
                }
            }
            component.set("v.spinner", false);
            
        });
        $A.enqueueAction(action); 
    },

    saveRecordHelper : function(component,event){
        component.set("v.spinner", true);
        var poRecordData = component.get("v.orderData");
        var action = component.get("c.updateOrderRecord");
        action.setParams({ 
            orderRecord : JSON.stringify(poRecordData)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respo = response.getReturnValue();
                console.log('respo---');
                console.log(respo);
                if(respo.split("#")[0] === 'Success'){
                    $A.get("e.force:closeQuickAction").fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                    "recordId": respo.split("#")[1],
                    "slideDevName": "detail"
                    });
                    navEvt.fire();
                }else if(respo.split("#")[0] === 'Failed'){
                    console.log(respo.split("#")[1]);
                    component.set("v.isCommunity", false);
                    component.set("v.isOrg", false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "message": "Some Error occured. Please Contact to your System Admin."
                    });
                    toastEvent.fire();
                    component.set("v.isModalOpen", false);
                    var homeEvt = $A.get("e.force:navigateToObjectHome");
                    homeEvt.setParams({
                        "scope": "Order"
                    });
                    homeEvt.fire();
                    
                }
                
            }else if(state === "ERROR"){
                var errors = response.getError();
             
                if (errors) {
                    if (errors[0] && errors[0].pageErrors[0] && errors[0].pageErrors[0].message) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "error",
                            "message": errors[0].pageErrors[0].message
                        });
                        toastEvent.fire();
                    }
                } else {
                    console.log("Unknown error");
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "error",
                            "message": "Some Error occured. Please Contact to your System Admin."
                        });
                    toastEvent.fire();
                }
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);             
    },

    closeQuickActionHelper : function(component,event) {
        $A.get("e.force:closeQuickAction").fire()
    },

    closeModelOrgHelper : function(component,event) {
        component.set("v.isModalOpen", false);
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "Order"
        });
     homeEvt.fire();}
})