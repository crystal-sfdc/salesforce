({
    doInit: function(component, event, helper){
        helper.getProduct(component,event);
    },
    getAlternateProductsList : function(component, event, helper) {
        helper.getAlternateProductsListHelper(component,event);
    },
    handleSelectAllProduct : function(component, event, helper) {
        helper.handleSelectAllProductHelper(component,event);
    },
    handleSelectedProduct : function(component, event, helper){
        helper.handleSelectedProductHelper(component,event);
    },

    addQuantity : function(component, event, helper){
        helper.addQuantityHelper(component,event);
    },

    backtoProduct : function(component, event, helper){
        helper.backtoProductHelper(component,event)
    },

    searchProduct : function(component, event, helper){
        helper.searchProductHelper(component,event)
    },

    addOrderLineItem : function(component, event, helper){
        helper.addOrderLineItemHelper(component,event)
    },
    closeQuickAction : function(component, event, helper) {
		helper.closeQuickActionHelper(component,event);
	},
    handleSelectAllLFLProduct : function(component, event, helper) {
		helper.handleSelectAllLFLProductHelper(component,event);
	},
    handleSelectedLFLProduct : function(component, event, helper) {
        console.log('Inside LFL Controller');
    	helper.handleSelectedLFLProductHelper(component,event);
    }
})