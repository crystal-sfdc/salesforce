({
    getPORelatedInfo : function(component,event){
        
        const monthNames = ["January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December"
                           ];
        var today = new Date();
        today.setDate(today.getDate() + 1);
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(monthNames[today.getMonth()]);
        var yyyy = today.getFullYear();
        today = dd + '-' + mm + '-' + yyyy;
        component.set("v.todayDate",today);

        var action = component.get("c.getPORelatedInformation");
        action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {

            var shipToAddList = [];
            var customerList = [];
            var divList = [];

            var poDetailWrp = response.getReturnValue();

            if(poDetailWrp.isCommunity === true){
                for ( var key in response.getReturnValue().customerDetailMap ) {
                    if (poDetailWrp.customerName === null || poDetailWrp.customerName === '' || poDetailWrp.customerName === undefined) {
                        poDetailWrp.customerName = response.getReturnValue().customerDetailMap[key];
                    }
                    customerList.push({value:response.getReturnValue().customerDetailMap[key], key:key});
                }
                component.set("v.customerDetailList",customerList);
    
                for ( var key in response.getReturnValue().shipToAddressMap ) {
                    if (poDetailWrp.shipToAddress === null || poDetailWrp.shipToAddress === '' || poDetailWrp.shipToAddress === undefined) {
                        poDetailWrp.shipToAddress = response.getReturnValue().shipToAddressMap[key];
                    }
                    shipToAddList.push({value:response.getReturnValue().shipToAddressMap[key], key:key});
                }
                component.set("v.shipToAddressList",shipToAddList);
    
                for ( var key in response.getReturnValue().divisionMap ) {
                    if (poDetailWrp.division === null || poDetailWrp.division === '' || poDetailWrp.division === undefined) {
                        poDetailWrp.division = response.getReturnValue().divisionMap[key];
                    }
                    divList.push({value:response.getReturnValue().divisionMap[key], key:key});
                }
                component.set("v.divisionList",divList);
    
                component.set("v.poDetail",poDetailWrp);
                
                
            }else if(poDetailWrp.isCommunity === false){
                component.set("v.isOrg", true);
                component.set("v.isModalOpen", true);
                component.set("v.isCommunity", false);
                for ( var key in response.getReturnValue().customerDetailMap ) {
                    if (poDetailWrp.customerName === null || poDetailWrp.customerName === '' || poDetailWrp.customerName === undefined) {
                        poDetailWrp.customerName = response.getReturnValue().customerDetailMap[key];
                    }
                    customerList.push({value:response.getReturnValue().customerDetailMap[key], key:key});
                }
                component.set("v.customerDetailList",customerList);
                component.set("v.poDetail",poDetailWrp);
            }
            component.set("v.spinner", false);

             
        }
        });
        $A.enqueueAction(action);
    },

    saveRecordHelper : function(component,event){

        var selectValid = true;
        var allValid = component.find('NewPo').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        component.find('selectNewPo').forEach(element => {
            if(element.get("v.value") === null || element.get("v.value") === '' || element.get("v.value") === undefined){
                selectValid = false;
            }
            
        });

        if (allValid && selectValid) {
            var poDetailInfo = component.get("v.poDetail");
        
            var action = component.get("c.createNewPoRecord");

            action.setParams({ 
                poDetailString : JSON.stringify(poDetailInfo )
            });
            

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respo = response.getReturnValue();
                    if(respo.split("#")[0] != 'error'){
                        $A.get("e.force:closeQuickAction").fire();
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                        "recordId": respo.split("#")[1],
                        "slideDevName": "detail"
                        });
                        navEvt.fire();
                        //window.reload();
                    }else{
                        console.log(respo.split("#")[1]);
                        component.set("v.isCommunity", false);
                        component.set("v.isOrg", false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "error",
                            "message": "Some Error occured. Please Contact to your System Admin."
                        });
                        toastEvent.fire();
                        component.set("v.isModalOpen", false);
                        var homeEvt = $A.get("e.force:navigateToObjectHome");
                        homeEvt.setParams({
                            "scope": "Order"
                        });
                        homeEvt.fire();
                    }
                    
                }
                
            });
            $A.enqueueAction(action);
        } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "message": "Please fill all required fields."
                });
                toastEvent.fire();
            
        }

                       
    },

    fetchPoRelatedInformation : function(component,event){
        component.set("v.spinner", true);
        var poDetailInfo = component.get("v.poDetail");

        if(poDetailInfo.customerName != 'None'){
            var action = component.get("c.fetchPoRelatedInfo");
            action.setParams({ 
                accId : poDetailInfo.customerName
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var shipToAddList = [];
                    var customerList = [];
                    var divList = [];

                    var poDetailWrp = response.getReturnValue();             
                    
                    for ( var key in response.getReturnValue().shipToAddressMap ) {
                        if (poDetailWrp.shipToAddress === null || poDetailWrp.shipToAddress === '' || poDetailWrp.shipToAddress === undefined) {
                            poDetailWrp.shipToAddress = response.getReturnValue().shipToAddressMap[key];
                        }
                        shipToAddList.push({value:response.getReturnValue().shipToAddressMap[key], key:key});
                    }
                    component.set("v.shipToAddressList",shipToAddList);
        
                    for ( var key in response.getReturnValue().divisionMap ) {
                        if (poDetailWrp.division === null || poDetailWrp.division === '' || poDetailWrp.division === undefined) {
                            poDetailWrp.division = response.getReturnValue().divisionMap[key];
                        }
                        divList.push({value:response.getReturnValue().divisionMap[key], key:key});
                    }
                    component.set("v.divisionList",divList);
        
                    component.set("v.poDetail",poDetailWrp);
                    console.log(poDetailWrp);
                    window.setTimeout(
                        $A.getCallback( function() {
                            component.find("CustName").set("v.value", poDetailWrp.customerName);
                        },300000));
                    
                }
                component.set("v.spinner", false);
            });
            $A.enqueueAction(action);
        }else{
            poDetailInfo.billToCode = '';
            poDetailInfo.poExpiryDate = '';
            poDetailInfo.supplierName = '';
            poDetailInfo.supplierCode = '';
            poDetailInfo.shipToAddress = null;
            poDetailInfo.division = null;
            poDetailInfo.status = 'Draft';
            component.set("v.poDetail",poDetailInfo);
            component.set("v.shipToAddressList",null);
            component.set("v.divisionList",null);
        }
    },

    
    closeQuickActionHelper : function(component,event) {
        $A.get("e.force:closeQuickAction").fire()
    },

    closeModelOrgHelper : function(component,event) {
           component.set("v.isModalOpen", false);
           var homeEvt = $A.get("e.force:navigateToObjectHome");
           homeEvt.setParams({
               "scope": "Order"
           });
        homeEvt.fire();}
})