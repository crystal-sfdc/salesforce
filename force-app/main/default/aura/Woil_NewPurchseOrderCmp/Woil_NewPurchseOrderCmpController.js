({
    doInit: function(component, event, helper){
        helper.getPORelatedInfo(component,event);
    },

    saveRecord: function(component, event, helper){
        helper.saveRecordHelper(component,event);
    },

    closeModel: function(component, event, helper) {
        helper.closeQuickActionHelper(component,event);
     },

     closeModelOrg: function(component, event, helper) {
        helper.closeModelOrgHelper(component,event);
     },

     fetchDetails : function(component, event, helper){
        helper.fetchPoRelatedInformation(component,event)
     },

     saveRecordOrg : function(component, event, helper){
        helper.saveRecordHelper(component,event);
     }
})