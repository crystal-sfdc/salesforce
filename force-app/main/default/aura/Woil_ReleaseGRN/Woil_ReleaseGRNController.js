({
	oninit : function(component, event, helper) {
		  
        helper.ReleaseGRN(component, event, helper);
      
	},
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner1", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner1", false);
    },
      handleConfirmDialog : function(component, event, helper) {
      
        var msg ='Are you sure you want to release this serial Numbers?';
        if (!confirm(msg)) {
            console.log('No');
            return false;
        } else {
            console.log('Yes');
             var a = component.get('c.oninit');
                $A.enqueueAction(a);
            //Write your confirmed logic
        }
    }
})