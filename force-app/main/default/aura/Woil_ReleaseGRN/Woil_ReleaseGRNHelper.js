({
    ReleaseGRN : function(component, event, helper) {
      
        var action = component.get("c.ReleaseGrn");
        action.setParams({"serial_no":component.get("v.recordId")
                         });
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if (state === "SUCCESS") {
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success',
                    message: 'Serial Number Released',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
            }else{
               var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message:'GRN is posted for Serial No!',
                    duration:' 2000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
              $A.get("e.force:closeQuickAction").fire();  
            }
        });
        $A.enqueueAction(action);
    }
})