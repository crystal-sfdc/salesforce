({
	fetchProdList : function(component, event, helper) {
        var action = component.get("c.fetchProductRecords");
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if(state === 'SUCCESS') {
            	var productList = response.getReturnValue();
                component.set("v.productList",productList);
            }else {
                alert('Error in getting data');
            }
        });
        $A.enqueueAction(action);
    }
})