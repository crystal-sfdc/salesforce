({
    getSubdealerInformation : function(component,event) {
        var action = component.get("c.getSubDealerInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var customerList = [];
                var shipToAddList = [];
                var divList = [];
                
                var sodetailwrap = response.getReturnValue();
                
                for ( var key in response.getReturnValue().buyerSeller ) {
                    if (sodetailwrap.customerName === null || sodetailwrap.customerName === '' || sodetailwrap.customerName === undefined) {
                        sodetailwrap.customerName = response.getReturnValue().buyerSeller[key];
                    }
                    customerList.push({value:response.getReturnValue().buyerSeller[key], key:key});
                }
                component.set("v.customerDetailList",customerList);
                
                for ( var key in response.getReturnValue().shipToAddressMap ) {
                    if (sodetailwrap.shipToAddress === null || sodetailwrap.shipToAddress === '' || sodetailwrap.shipToAddress === undefined) {
                        sodetailwrap.shipToAddress = response.getReturnValue().shipToAddressMap[key];
                    }
                    shipToAddList.push({value:response.getReturnValue().shipToAddressMap[key], key:key});
                }
                
                for ( var key in response.getReturnValue().divisionMap ) {
                    if (sodetailwrap.division === null || sodetailwrap.division === '' || sodetailwrap.division === undefined) {
                        sodetailwrap.division = response.getReturnValue().divisionMap[key];
                    }
                    divList.push({value:response.getReturnValue().divisionMap[key], key:key});
                }
                component.set("v.divisionList",divList);
                
                component.set("v.shipToAddressList",shipToAddList);
                
                component.set("v.soDetail",sodetailwrap);
                component.set("v.spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchSoRelatedInformation : function(component,event){
        
        var outList = [];
        
        var soDetailInfo = component.get("v.soDetail");
        var custId = soDetailInfo.customerName;
        if(custId === 'None'){
            soDetailInfo.outletStr = '';
            soDetailInfo.buyerCode = '';
            soDetailInfo.address = '';
            component.set("v.soDetail",soDetailInfo);
            component.set("v.outletList",null);
            
        }else{
            var buyersellMap = soDetailInfo.buyerSellerCode;
            soDetailInfo.buyerCode = buyersellMap[custId];
            
            for ( var key in soDetailInfo.outletMap[custId] ) {
                if (soDetailInfo.outletStr === null || soDetailInfo.outletStr === '' || soDetailInfo.outletStr === undefined) {
                    soDetailInfo.outletStr = soDetailInfo.outletMap[custId][key];
                    soDetailInfo.address = soDetailInfo.outletAddress[soDetailInfo.outletMap[custId][key]];
                }
                outList.push({value:soDetailInfo.outletMap[custId][key], key:key});
                
            }
            component.set("v.outletList",outList);
            
            component.set("v.soDetail",soDetailInfo);
        }
    },
    
    fetchOutletAddress : function(component,event){
        var soDetailInfo = component.get("v.soDetail");
        var selectedOutlet = soDetailInfo.outletStr;
        var addresses = soDetailInfo.outletAddress;
        soDetailInfo.address = addresses[selectedOutlet];
        component.set("v.soDetail",soDetailInfo);
    },
    
    closeQuickActionHelper : function(component,event) {
        $A.get("e.force:closeQuickAction").fire()
    },
    saveRecordHelper : function(component,event){
        component.set("v.spinner", true);
        var selectValid = true;
        
        component.find('selectNewPo').forEach(element => {
            console.log(element.get("v.value"));
            if(element.get("v.value") === null || element.get("v.value") === '' || element.get("v.value") === undefined){
            selectValid = false;
        }
            
        });
            
            if (selectValid) {
            var soDetailInfo = component.get("v.soDetail");
            
            var action = component.get("c.createNewSoRecord");
            
            action.setParams({ 
            sODetailString : JSON.stringify(soDetailInfo )
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respo = response.getReturnValue();
                
                console.log("return value" +respo);
                
                if (respo == 'BuyerError1234'){
                    console.log("return value2" + response.getReturnValue());
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "message": "Approval request is pending for selected deliervy location. Please connect your Area Sales Manager"
                    });
                    toastEvent.fire();
                    component.set("v.spinner", false);
                     $A.get("e.force:closeQuickAction").fire();
                }
                else if(respo.split("#")[0] != 'error'){
                    console.log("if" );
                    
                    $A.get("e.force:closeQuickAction").fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": respo.split("#")[1],
                        "slideDevName": "detail"
                    });
                    component.set("v.spinner", false);
                    navEvt.fire();
                    //window.reload();
                }
                
                
                    else{
                        console.log("else" );
                        console.log(respo.split("#")[1]);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "error",
                            "message": "Some Error occured. Please Contact to your System Admin."
                        });
                        component.set("v.spinner", false);
                        toastEvent.fire();
                        var homeEvt = $A.get("e.force:navigateToObjectHome");
                        homeEvt.setParams({
                            "scope": "Woil_Sales_Order__c"
                        });
                        homeEvt.fire();
                    }
                
            }
            
        });
        $A.enqueueAction(action);
    } else {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
    "type": "error",
    "message": "Please fill all required fields."
});
toastEvent.fire();
component.set("v.spinner", false);

}


}
})