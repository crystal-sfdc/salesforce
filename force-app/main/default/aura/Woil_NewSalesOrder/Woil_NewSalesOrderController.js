({
    doInit : function(component, event, helper) {
        helper.getSubdealerInformation(component,event);
    },
    closeModel: function(component, event, helper) {
        helper.closeQuickActionHelper(component,event);
     },
     fetchDetails : function(component, event, helper){
        helper.fetchSoRelatedInformation(component,event)
     },
     fetchaddress : function(component, event, helper){
        helper.fetchOutletAddress(component,event)
     },
     saveRecord: function(component, event, helper){
        helper.saveRecordHelper(component,event);
    }
})