({
	doInit : function(component, event, helper) {
        helper.getCategoryPicklist(component, event);
        helper.getLink(component, event);
        $A.util.addClass(component.find("showList"), "slds-hide");
        $A.util.addClass(component.find("showError"), "slds-hide");
		
	},
    
    handleClick : function(component, event, helper){
        component.set('v.columns', [
                {label: 'Material Code', fieldName: 'materialCode', type: 'text'},
                {label: 'Material Description', fieldName: 'materialDesc', type: 'text'},
                {label: 'Serial Number', fieldName: 'serialNumb', type: 'text'},
                {label: 'SellOut', fieldName: 'sellOut', type: 'boolean'},
                {label: 'Customer Name', fieldName: 'CustomerName', type: 'text'},
				{label: 'Customer Address', fieldName: 'CustomerAddress', type: 'text'},
                {label: 'Phone', fieldName: 'Phone', type: 'text'},
				{label: 'SellOut Date', fieldName: 'sellDate', type: 'text'}
            ]);
        helper.getDataList(component, event);
    },
    
    handleSave : function(component, event, helper){
    	helper.sendData(component, event);
        let button = event.getSource();
    	button.set('v.disabled',true);
       
    },
    
    handleFilesChange: function (component, event, helper) {
        // This will contain the List of File uploaded data and status
        var uploadFile = event.getSource().get("v.files");
        var self = this;
        var file = uploadFile[0]; // getting the first file, loop for multiple files
        var reader = new FileReader();
        reader.onload =  $A.getCallback(function() {
            var dataURL = reader.result;
           var base64 = 'base64,';
            var dataStart = dataURL.indexOf(base64) + base64.length;
            
            
            dataURL= dataURL.substring(dataStart);            
            component.set('v.columns', [
                {label: 'Material Code', fieldName: 'materialCode', type: 'text'},
                {label: 'Material Description', fieldName: 'materialDesc', type: 'text'},
                {label: 'Serial Number', fieldName: 'serialNumb', type: 'text'},
                {label: 'SellOut', fieldName: 'sellOut', type: 'boolean'},
                  {label: 'Customer Name', fieldName: 'CustomerName', type: 'text'},
				{label: 'Customer Address', fieldName: 'CustomerAddress', type: 'text'},
                {label: 'Phone', fieldName: 'Phone', type: 'text'},
                {label: 'SellOut Date', fieldName: 'sellDate', type: 'text'}
            ]);
            helper.upload(component, file, dataURL)
        });
        reader.readAsDataURL(file);
    }
    
    
})