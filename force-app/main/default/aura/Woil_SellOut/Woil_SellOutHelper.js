({
    getCategoryPicklist: function(component, event) {
        var action = component.get("c.fetchMaterialCodeValues");
        console.log('actionCallBack');
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('inside if');
                var result = response.getReturnValue();
                console.log('response',result);
                var categoryMap = [];
                for(var i=0;i<result.length;i++){
                    categoryMap.push({key: result[i].custFldlabel, value: result[i].custFldvalue});
                }
                component.set("v.fieldMap", categoryMap);
            }
        });
        $A.enqueueAction(action);
    },
    
    
    getDataList: function(component, event) {
        var findMatCode = component.find("filterBox");
        var matCode = findMatCode.get("v.value");
        var inputCheck = false;
        var findSerialNumber = component.find("inpBox");
        var serialNumber = findSerialNumber.get("v.value");
        
        
        //new Change-------------------------------------
        var Customer = component.find("inpBoxName");
        var CustomerName = Customer.get("v.value");
        
                var CustomerAdd = component.find("inpBoxAdd");
        var CustomerAddress = CustomerAdd.get("v.value");
        
                var Phone = component.find("inpBoxPhone");
        var CustomerPhone = Phone.get("v.value");
        //new Change-------------------------------------        
        var dataWrapList= component.get("v.selectedRows");
        
        if(matCode === '' || serialNumber ===''){
            if(dataWrapList.length ==0){
                $A.util.addClass(component.find("showList"), "slds-hide");
            }
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please select the input fields",
                "type": "error"
            });
            toastEvent.fire();
            return;
        }
        
        for(var i=0;i<dataWrapList.length;i++){
            if(dataWrapList[i].materialCode == matCode && dataWrapList[i].serialNumb == serialNumber){
                inputCheck = true;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Sell Out of the Serial Number is already captured",
                    "type": "error"
                });
                toastEvent.fire();
                
                return;
            } 
        }
        
        console.log('matCode'+matCode);
        if(!inputCheck){
            var action = component.get("c.createDataTable");
            action.setParams({
                "materialCode": matCode,
                "serialNumb": serialNumber,
                "CustName":CustomerName,
                "Address":CustomerAddress,
                "Phone":CustomerPhone
            });
            console.log('actionCallBack');
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('inside if data');
                    var result = response.getReturnValue();
                    console.log('response',result);
                    if(result.length>0){
                        
                        
                        if(result[0].errorString != undefined){
                            if(dataWrapList.length ==0){
                                $A.util.addClass(component.find("showList"), "slds-hide");
                            }    
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "message": result[0].errorString,
                                "type": "error"
                            });
                            toastEvent.fire();  
                        }else{
                            
                            if(result.length>0){
                                console.log('dataWrapList1'+dataWrapList);
                                $A.util.removeClass(component.find("showList"), "slds-hide");
                                for(var i=0;i<result.length;i++){
                                    dataWrapList.push(result[i]);
                                    console.log('dataWrapList',dataWrapList);
                                }
                                
                                component.set("v.selectedRows", dataWrapList);
                                console.log('selectedRows', component.get("v.selectedRows"));
                            }
                            else{
                                if(dataWrapList.length ==0){
                                    $A.util.addClass(component.find("showList"), "slds-hide");
                                }
                                console.log('inside datawrapLIst',dataWrapList);
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error!",
                                    "message": "Serial number is not available in the inventory",
                                    "type": "error"
                                });
                                toastEvent.fire();    
                            }
                        }
                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please select correct Serial Number",
                            "type": "error"
                        });
                        toastEvent.fire();  
                    }
                    
                }
            });
            $A.enqueueAction(action);
            
        }
    },
    
    sendData: function(component, event){
        var dataList = component.get("v.selectedRows");
        var recordInfoListString=JSON.stringify(dataList);
        
        var action = component.get("c.addSellOutDetail");
        action.setParams({
            "selectedRows": recordInfoListString
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            console.log('state '+state);
            if (state === "SUCCESS") {
                
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "message":"SellOut of the Serial Number is captured" ,
                    "type": "success"
                });
                toastEvent.fire();
                //$A.get("e.force:refreshView").fire();
                window.location.reload();
            }
        });
        $A.enqueueAction(action);
    },
    
    
    upload: function(component, file, base64Data) {
        console.log('helper',file);
        console.log('data info ',base64Data);
        var checkDataListValue = component.get("v.selectedRows");
        //component.set("v.selectedRows", []);
        var successList = component.get("v.selectedRows");
        
        var errorList= component.get("v.errorData");
        var action = component.get('c.uploadFle');
        action.setParams({File : base64Data, FileName:file.name, FileSize: file.size, FileType:file.type });
        var self = this;
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('inside if');
                var result = response.getReturnValue();
                console.log('result',result);                
                if(result.length>0){
                    for(var i=0;i<result.length;i++){
                        var inputCheck = false;
                        var inputCheckError = false;
                        console.log('inside for loop');
                        if(result[i].sellOut == true){
                            console.log('inside if of upload ',result[i]);
                            
                            console.log('successList length '+successList.length);
                            if(successList.length>0){
                                console.log('successList ',successList);
                                for(var j = 0; j<successList.length;j++){
                                    console.log('successList[j].materialCode'+successList[j].materialCode);
                                    console.log('result[i].materialCode'+result[i].materialCode);
                                    console.log('successList[j].serialNumb'+successList[j].serialNumb);
                                    console.log('result[i].serialNumb'+result[i].serialNumb);
                                    if(successList[j].materialCode != result[i].materialCode && successList[j].serialNumb != result[i].serialNumb && !inputCheck ){
                                        console.log('inside if push',result[i]);
                                        // successList.push(result[i]);
                                        inputCheck = false;
                                        
                                    }
                                    else if(successList[j].materialCode == result[i].materialCode && successList[j].serialNumb == result[i].serialNumb){
                                        inputCheck = true;
                                    }
                                    
                                }
                                if(!inputCheck){
                                    successList.push(result[i]); 
                                }
                            }
                            else{
                                console.log('result[i].materialCode inside else'+result[i].materialCode);
                                console.log('result[i].serialNumb inside else '+result[i].serialNumb);
                                successList.push(result[i]);
                            }
                            //successList.push(result[i]);
                            console.log('successList',successList);
                        }
                        else{
                            console.log('Inside else');
                            if(errorList.length>0){
                                for(var j = 0; j<errorList.length;j++){
                                    
                                    if(errorList[j].materialCode != result[i].materialCode && errorList[j].serialNumb != result[i].serialNumb && !inputCheckError ){
                                        console.log('inside if push',result[i]);
                                        // successList.push(result[i]);
                                        inputCheckError = false;
                                        
                                    }
                                    else if(errorList[j].materialCode == result[i].materialCode && errorList[j].serialNumb == result[i].serialNumb){
                                        inputCheckError = true;
                                    }
                                    
                                }
                                if(!inputCheckError){
                                    errorList.push(result[i]); 
                                }
                            }else{
                                errorList.push(result[i]); 
                            }
                            
                        }
                    }
                }
                console.log('errorList ',errorList);
                console.log('successList ',successList);
                if(successList.length>0){
                    $A.util.removeClass(component.find("showList"), "slds-hide");
                    component.set("v.selectedRows", successList);                    
                }
                if(errorList.length>0){
                    $A.util.removeClass(component.find("showError"), "slds-hide");
                    component.set("v.errorData", errorList);                    
                }
                
                
            }
        });
        $A.enqueueAction(action);
    },
    
    getLink: function(component, event){
        
        var action = component.get("c.getTemplate");
        action.setParams({
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('response',result);
                
                var urlString = window.location.href;
                var baseURL = urlString.substring(0, urlString.indexOf("/s"));
                var url = baseURL+'/servlet/servlet.FileDownload?file='+result;
                component.set("v.docId", url);
                console.log("docId "+ component.get("v.docId"));
              
            }
            
            
        });
        $A.enqueueAction(action);
        
    }
})