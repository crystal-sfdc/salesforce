({
    getDivisionValues : function(component,event) {
        console.log('Inside getDivisionValues');
        $A.util.addClass(component.find("showFGSerialized"), "slds-hide");
        $A.util.addClass(component.find("showSPSerialized"), "slds-hide");
        var action = component.get("c.getDivision");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.divisionList",result);
            }
        });
        
        $A.enqueueAction(action); 
    },
    
    closeModelOrgHelper : function(component,event) {
        //component.set("v.isModalOpen", false);
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "Woil_Opening_Stock__c"
        });
        homeEvt.fire();
    },
    
    saveRecordOrgHelper : function(component,event) {
        var stockDateId= component.find("stockDate");
        var stockDateVal= stockDateId.get("v.value");
        console.log('stockDateVal'+stockDateVal);
        var descId= component.find("newOpStockDesc");
        var desc= descId.get("v.value");
        var divId= component.find("selectDivision");
        var div= divId.get("v.value");
        var isSerId;
        var isSer;
        if(div === 'Finished Good') { 
            isSerId= component.find("serialFGValue");
            isSer= isSerId.get("v.checked"); 
        }
        else if (div === 'Spare Part') {
            isSerId= component.find("serialSPValue");
            isSer= isSerId.get("v.checked"); 
        }
        
        if(desc === '' || div === '' || stockDateVal === '' || stockDateVal == null) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                        "type": "error",
                        "message": "Please fill all the mandatory fields"
                    });
                    toastEvent.fire();
            return;
        }
        
       var action = component.get("c.saveNewRecord");
        action.setParams({
            "descValue": desc,
            "divValue": div,
            "isSeriValue": isSer,
            "stockDate": stockDateVal
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respo = response.getReturnValue();
                if(respo.split("#")[0] != 'error'){
                    $A.get("e.force:closeQuickAction").fire();
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": respo.split("#")[1],
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }else {
                    console.log(respo.split("#")[1]);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "message": "Some Error occured. Please Contact to your System Admin."
                    });
                    toastEvent.fire();
                    var homeEvt = $A.get("e.force:navigateToObjectHome");
                    homeEvt.setParams({
                        "scope": "Woil_Opening_Stock__c"
                    });
                    homeEvt.fire();
                }
            }
        });
        
        $A.enqueueAction(action); 
        
    },
})