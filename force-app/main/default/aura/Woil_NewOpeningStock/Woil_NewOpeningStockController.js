({
    doInit : function(component, event, helper) {
        helper.getDivisionValues(component,event);
    },
    
    closeModelOrg: function(component, event, helper) {
        helper.closeModelOrgHelper(component,event);
    },
    
    saveRecordOrg: function(component, event, helper) {
        helper.saveRecordOrgHelper(component,event);   
    },
    
    getIsSerialized: function(component, event, helper) {
        var divValue= event.getSource().get('v.value');
        if(divValue === 'Finished Good') {
            console.log('Inside FG');
            component.set("v.checkSerialized", false);
            component.set("v.isFG", true);
            component.set("v.isSP", false);
        }
        else if(divValue === 'Spare Part') {
            console.log('Inside SP');
            component.set("v.isFG", false);
            component.set("v.isSP", true);
        }
    },
    
})