({
    getOutletRec: function (component, event, helper) {
        //*************CHECK RECORD IS NOT LOCKED BY APPROVAL PROCESS*****************
        var action = component.get("c.checkObjectLock");
        action.setParams({ recordId : component.get("v.recordId") });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                if(response.getReturnValue()){
                    $A.get("e.force:closeQuickAction").fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Update not allowed!",
                        "message": "Record is in approval process stage",
                        "type" : "warning"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
        
        //*************FETCH RECORD FOR UPDATE*****************
        var action = component.get("c.getOutletRec");
        action.setParams({ recordId : component.get("v.recordId") });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                component.set('v.outletObj', response.getReturnValue());
            }else{
                $A.get("e.force:closeQuickAction").fire();
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message)
                        console.log("Error message: " + errors[0].message);
                   
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    //*************CLOSE_WINDOW****************
    getHandleCancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    //*************SUBMIT****************
    getHandleUpdate: function(component, event, helper) {
        var outletObj = component.get("v.outletObj");
        console.log(outletObj);
        var validation = false;
        
        var toastEvent = $A.get("e.force:showToast");
        var validation = false;
        //Form validation
        
        var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if (allValid == false) {
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please fill all mandatory fields.",
                "type" : "warning"
            });
            toastEvent.fire();
        } 
        else {
            
            if(!(component.get("v.outletObj").Woil_Mobile_Number__c.match(/^\d{10}$/))){
                toastEvent.setParams({
                    "title": "Not Valid!",
                    "message": "Enter only 10 digit mobile number",
                    "type" : "warning"
                });
                toastEvent.fire();
                validation = false;
            }
            else{
                var action = component.get("c.updateOutletRec");
                action.setParams({ 
                    "outletObj": outletObj
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    
                    if (state === "SUCCESS") {
                        var homeEvt = $A.get("e.force:navigateToObjectHome");
                        homeEvt.setParams({
                            "scope": "Outlet_Master__c"
                        });
                        homeEvt.fire();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "The record updated successfully.",
                            "type" : "success"
                        });
                        toastEvent.fire();
                    }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        var homeEvt = $A.get("e.force:navigateToObjectHome");
                        homeEvt.setParams({
                            "scope": "Outlet_Master__c"
                        });
                        homeEvt.fire();
                        
                        toastEvent.setParams({
                            "title": "Failed!",
                            "message": "The record not Updated successfully.",
                            "type" : "warning"
                        });
                        toastEvent.fire();
                    }
                });
                $A.enqueueAction(action);
            }
        }
    }
})