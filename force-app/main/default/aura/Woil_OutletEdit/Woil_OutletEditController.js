({
    doInit: function(component, event, helper){
        helper.getOutletRec(component,event);
    },
    handleCancel: function(component, event, helper){
        helper.getHandleCancel(component,event);
    },
    handleUpdate: function(component, event, helper){
        helper.getHandleUpdate(component,event);
    }
})