({
	doInit : function(component, event, helper) {
    console.log("Next Page list"+JSON.stringify(component.get("v.prodList")));
		  component.set('v.columns', [
            {label: 'Material Code', fieldName: 'prodName', type: 'text'},
                {label: 'Material Description', fieldName: 'prodDescription', type: 'text'},
              {label: 'Dealer Price', fieldName: 'prodDealerPrice', type: 'text', editable: "true", hideDefaultActions: true},
            {label: 'Quantity', fieldName: 'quantity', type: 'text', editable: "true", hideDefaultActions: true}
          ]);
              //component.set("v.prodList",JSON.stringify(component.get("v.prodList")));
              console.log("Print"+component.get("v.prodList"));
     },
             
	backtoProduct : function(component, event, helper) {
		var returnUrl= component.get("v.returntoProductUrl");
         var urlEvent = $A.get("e.force:navigateToURL");
  		 urlEvent.setParams({
    			'url': returnUrl
  				});
  			urlEvent.fire();
	},
    
    closeModel: function(component, event, helper) {
         var returntoOrderUrl= component.get("v.returntoOrderUrl");
         var urlEvent = $A.get("e.force:navigateToURL");
  		 urlEvent.setParams({
    			'url': returntoOrderUrl
  				});
  			urlEvent.fire();
    },
    
   addQuantity: function(component, event, helper) {
     var updatedRecords = component.find("productTable").get("v.draftValues" );
       console.log('Draft Values',updatedRecords);
}
    
})