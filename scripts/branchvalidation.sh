#!/usr/bin/env bash

echo 'Checking PR Dest Branch'

if [ "${1}" = "Develop" ] && [ "${1}" != "master|Release" ]
then 
    echo 'those are the extra checks on Develop'
else
    echo 'target branch branch is not Develop branch'
    exit 1
fi